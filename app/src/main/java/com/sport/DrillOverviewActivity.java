package com.sport;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.sport.database.SQLiteDB;
import com.sport.lists.Drill;
import com.sport.lists.DrillOverviewAdapter;
import com.sport.lists.Group;
import com.sport.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class DrillOverviewActivity extends BasicMain {

    private View includedLayout;
    private ListView lv;
    private com.github.clans.fab.FloatingActionButton fab1, fab2;
    private List<Group> groupArray;
    private List<Drill> drillArray;
    private String[] groupesName;
    private boolean[] itemsSelect;
    private boolean[] itemsSelectArchive;
    private boolean init = false;
    private AlertDialog dialog;
    private DrillOverviewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drill_overview);
        includedLayout = findViewById(R.id.id_drill_list);
        lv = (ListView) includedLayout.findViewById(R.id.list);
        changeToolbarTitle(getResources().getString(R.string.overwiev_drill_title));

        getGroupes();
        getData();

        // new drill
        fab1 = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DrillOverviewNewDrillActivity.class);
                intent.putExtra("DRILL_ID", -1);
                startActivity(intent);
            }
        });
        // filter
        fab2 = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDialog();
            }
        });
    }

    private void getGroupes(){
        // get all groups from DB
        groupArray = new ArrayList<Group>();
        SQLiteDB db = new SQLiteDB(this);
        Cursor itemFromDB = db.getAllGroupes(true);
        List<Group> groupsDBArray = new ArrayList<Group>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("gid"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer cnt = itemFromDB.getInt(itemFromDB.getColumnIndex("cnt"));
            String description = itemFromDB.getString(itemFromDB.getColumnIndex("description"));
            Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
            Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
            groupArray.add(new Group(id, name, description, icon, color, cnt));
        }
        itemFromDB.close();
        db.close();
        groupSelectInit();
    }

    private void getData(){
        drillArray = new ArrayList<Drill>();
        SQLiteDB db = new SQLiteDB(this);
        Cursor itemFromDB = db.getSavedDrillsByGroupId(getSelectedGroupIDs());
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id_drill"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("drill_name"));
            Integer groupeId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_group"));
            Boolean timeWait = itemFromDB.getInt(itemFromDB.getColumnIndex("time_wait")) == 1;
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("time"));
            Boolean alternative = itemFromDB.getInt(itemFromDB.getColumnIndex("alternative")) == 1;
            Integer varPosition = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_position"));
            Boolean sndWait = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_wait")) == 1;
            Integer snd = itemFromDB.getInt(itemFromDB.getColumnIndex("snd"));
            Boolean inflection = itemFromDB.getInt(itemFromDB.getColumnIndex("inflection")) == 1;
            Boolean shuffle = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle")) == 1;
            Boolean shuffleModify = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle_modify")) == 1;
            String groupeName = "";
            int color = 2;
            for(int i = 0; i < groupArray.size(); i++){
                if(groupArray.get(i).getgId() == groupeId) {
                    groupeName = groupArray.get(i).getName();
                    color = groupArray.get(i).getColor();
                    break;
                }
            }
            Drill d = new Drill(id, name, groupeId, groupeName, alternative, varPosition, timeWait, time, sndWait, snd, inflection, shuffle, shuffleModify);
            d.setColor(color);
            drillArray.add(d);
        }
        itemFromDB.close();
        db.close();
        TextView emptyList = (TextView) findViewById(R.id.empty_list);
        if (drillArray.size() > 0) {
            adapter = new DrillOverviewAdapter(this, R.layout.list_item_drill_overview, drillArray);
            lv.setAdapter(adapter);
            ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);
        } else {
            lv.setVisibility(View.GONE);
            emptyList.setVisibility(View.VISIBLE);
        }
    }

    private ArrayList<Integer> getSelectedGroupIDs(){
        ArrayList<Integer> result = new ArrayList<>();
        for(int i = 0; i < groupArray.size(); i++){
            if(itemsSelect[i]){
                result.add(groupArray.get(i).getgId());
            }
        }
        return result;
    }

    private void groupSelectInit(){
        groupesName = new String[groupArray.size()];
        if(!init)
            itemsSelect = new boolean[groupArray.size()];
        for(int i = 0; i < groupArray.size(); i++){
            groupesName[i] = groupArray.get(i).getName();
            if(!init)
                itemsSelect[i] = true;
        }
        init = true;
    }

    private void filterDialog(){
        itemsSelectArchive = new boolean[itemsSelect.length];
        for(int i = 0; i < itemsSelect.length; i++){
            itemsSelectArchive[i] = itemsSelect[i];
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Filtrování podle skupin");
        builder.setMultiChoiceItems(groupesName, itemsSelect, new DialogInterface.OnMultiChoiceClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int position, boolean isChecked){
                itemsSelect[position] = isChecked;
            }
        });
        builder.setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                getData();
                adapter.notifyDataSetChanged();
            }
        }).setNegativeButton(this.getResources().getString(R.string.back_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                itemsSelect = itemsSelectArchive;
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }
}
