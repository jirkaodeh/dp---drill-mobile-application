package com.sport;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sport.fragments.main.MainDrillsFragment;
import com.sport.fragments.main.MainGroupFragment;
import com.sport.lists.TabViewAdapter;
import com.sport.utils.Color;

public class GroupDrillSetsActivity extends BasicMain {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabViewAdapter adapter;
    private int GROUP_ID, GROUP_COLOR;
    private String GROUP_NAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_drill_sets);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        final Intent inIntent = getIntent();
        Bundle bundle = inIntent.getExtras();
        if(bundle != null) {
            GROUP_ID = (int) bundle.get("GROUP_ID");
            GROUP_COLOR = (int) bundle.get("GROUP_COLOR");
            GROUP_NAME = (String) bundle.get("GROUP_NAME");
            super.changeToolbarTitle("Skupina: "+GROUP_NAME);

            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(GROUP_COLOR))));
        }
    }

    // Setting viewPager and setting fragment for each bookmark
    private void setupViewPager(ViewPager viewPager) {
        adapter = new TabViewAdapter(getSupportFragmentManager());
        adapter.addFrag(new MainDrillsFragment(), "");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount() - 1);
    }

    public int getGroupId(){
        return GROUP_ID;
    }
}
