package com.sport;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.sport.R;
import com.sport.fragments.main.MainDrillsFragment;
import com.sport.fragments.main.MainGroupFragment;
import com.sport.lists.TabViewAdapter;
import com.sport.utils.Permission;

public class MainActivity extends BasicMain {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);


        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/
        Permission.checkAndRequestPermissions(this);
    }

    // Setting viewPager and setting fragment for each bookmark
    private void setupViewPager(ViewPager viewPager) {
        adapter = new TabViewAdapter(getSupportFragmentManager());
        adapter.addFrag(new MainDrillsFragment(), "Cvičební sady");
        adapter.addFrag(new MainGroupFragment(), "Skupiny");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount() - 1);
    }

    public int getGroupId(){
        return -1;
    }


}
