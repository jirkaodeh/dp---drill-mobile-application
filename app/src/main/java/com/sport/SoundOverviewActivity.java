package com.sport;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.sport.database.InflectionDB;
import com.sport.database.PreferenceStore;
import com.sport.database.SQLiteDB;
import com.sport.lists.Feature;
import com.sport.lists.Inflection;
import com.sport.lists.InflectionAdapter;
import com.sport.lists.Snd;
import com.sport.lists.SndEditAdapter;
import com.sport.lists.VawRecordAdapter;
import com.sport.sound.FeatureFile;
import com.sport.sound.StoreFile;
import com.sport.sound.Train;
import com.sport.utils.HelpDialogs;
import com.sport.utils.ListUtils;
import com.sport.utils.Permission;

import java.util.ArrayList;

public class SoundOverviewActivity extends BasicMain {

    private View includedLayout;
    private ListView lv;
    private TextView emptyList;
    private FloatingActionButton fab;
    private SndEditAdapter adapter;
    private ArrayList<Snd> sndArr;
    private android.support.v7.app.AlertDialog trainingDialog;
    private TrainFileTask trainTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_overview);
        super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.snd_settings_title));

        Permission.permisionForReadExternalStorage(this);
        Permission.permisionForReadExternalStorage(this);
        Permission.verifyStoragePermissions(this);

        includedLayout = findViewById(R.id.id_snd_list);
        lv = (ListView) includedLayout.findViewById(R.id.list);
        emptyList = (TextView) includedLayout.findViewById(R.id.empty_list);

        StoreFile sf = new StoreFile(this);
        sf.getFilesName(this);

        final SoundOverviewActivity act = this;
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewClassDialog(act, false, null, act);
            }
        });
    }

    public void updateList(){
        if (sndArr.size() > 0) {
            adapter = new SndEditAdapter(this, R.layout.list_item_edit_snd, sndArr, this);
            lv.setAdapter(adapter);
            ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);
        }
        else{
            emptyList.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }
    }

    public static void createNewClassDialog(final Context ctx, final boolean edit, final Snd snd, final Activity activity) {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(ctx);
        final LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);

        final View viewlayout = inflater.inflate(R.layout.dialog_create_sound_class,
                (ViewGroup) activity.findViewById(R.id.layout_dialog));
        final EditText sndClassName = viewlayout.findViewById(R.id.snd_class_name);
        final TextView warningTv = viewlayout.findViewById(R.id.warning);

        if(edit && snd != null){
            TextView title = (TextView)viewlayout.findViewById(R.id.title);
            title.setText(activity.getResources().getString(R.string.snd_edit_class_title));
            sndClassName.setText(snd.getName());
        }

        popDialog.setView(viewlayout);

        popDialog.setPositiveButton((edit ? activity.getResources().getString(R.string.change_name) : activity.getResources().getString(R.string.create_group)), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        popDialog.setNegativeButton(activity.getResources().getString(R.string.decline), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = popDialog.create();
        // listener for close dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ctx.getResources().getColor(R.color.colorAccent));
            }
        });
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sndClassName.getText().toString().isEmpty()){
                    warningTv.setVisibility(View.VISIBLE);
                }
                else {
                    warningTv.setVisibility(View.GONE);
                    SQLiteDB db = new SQLiteDB(activity);
                    if(edit && snd != null){
                        db.setSndName(snd.getId(), sndClassName.getText().toString());
                        ((SoundCreateEditClassActivity)activity).updateTitle();
                    }else {
                        // duplicate feature file
                        FeatureFile.copyDefailtFeatures(ctx);
                        // create new class
                        long newSndClassId = db.insertSnd(sndClassName.getText().toString(), "", -1, 0, 1);
                        // start class setting activity
                        Intent intent = new Intent(ctx, SoundCreateEditClassActivity.class);
                        intent.putExtra("SOUND_ID", (int) newSndClassId);
                        activity.startActivity(intent);
                    }
                    db.close();
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        sndArr = Snd.getAllSndData(this, false);
        updateList();
    }

    @Override
    public void onBackPressed() {
        if(FeatureFile.isModified) {
            Log.i("SVM train", "need train!");
            trainingDialog = HelpDialogs.loadingDialog(this, this, "Probíhá učení zvuků", "Následující akce trvá přibližně " + (PreferenceStore.getAppLoadingTime(this)+5) + "s");
            trainTask = new TrainFileTask();
            startTraining();
            //while (!trainTask.isCancelled()) { }
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    public void startTraining(){
        mStatusChecker.run();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                trainTask.execute();
            } finally {}
        }
    };

    public class TrainFileTask extends AsyncTask<Void, Void, Boolean> {


        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Train.modelTrained = false;
                Train.doTrainNoAsynchron(getApplicationContext());
            }catch (Exception e){
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean result) {
            if(trainingDialog != null)
                trainingDialog.dismiss();
            FeatureFile.isModified = false;
            finish();
        }
    }
}
