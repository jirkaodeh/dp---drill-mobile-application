package com.sport.importExport;

import android.content.Context;
import android.database.Cursor;

import com.sport.database.InflectionDB;
import com.sport.database.SQLiteDB;
import com.sport.lists.DrillInSet;
import com.sport.lists.DrillSet;

import java.util.ArrayList;
/**
 * Created by Jirka on 16.3.2019.
 * Class for export drill set
 */
public final class Export {

    /**
     * Public function for export drill set
     * @param ctx - aplication context
     * @param setId - drill set id
     * @return exported string
     */
    public static String exportDrillSet(Context ctx, int setId){
        DrillSet drillSet = getDrillSetData(ctx, setId);
        ArrayList<DrillInSet> drillArray = getDrillInSetData(ctx, setId);
        if(drillSet == null){
            return null;
        }
        String setExport = convertDrillSetToExport(drillSet);
        String drillsExport = convertDrillInSetToExport(ctx, drillArray);
        return setExport+drillsExport;
    }

    /**
     * Get all drills in drill set
     * @param ctx - aplication context
     * @param setId - drill set id
     * @return - list of drills
     */
    private static ArrayList<DrillInSet> getDrillInSetData(Context ctx, int setId){
        ArrayList<DrillInSet> drillArray = new ArrayList<DrillInSet>();
        SQLiteDB db = new SQLiteDB(ctx);
        Cursor itemFromDB = db.getAllDrillsFromDrillSet(setId);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id_drill_in_set"));
            Integer drillId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_id"));
            Integer position = itemFromDB.getInt(itemFromDB.getColumnIndex("position"));
            Boolean isPause = itemFromDB.getInt(itemFromDB.getColumnIndex("is_pause")) == 1;
            Integer pauseTime = itemFromDB.getInt(itemFromDB.getColumnIndex("pause_time"));
            Integer variantXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_x_times"));
            String editName = itemFromDB.getString(itemFromDB.getColumnIndex("edit_name"));
            String drillName = itemFromDB.getString(itemFromDB.getColumnIndex("drill_name"));
            Boolean prev = itemFromDB.getInt(itemFromDB.getColumnIndex("joint_to_prev_drill")) == 1;
            Boolean timeWait = itemFromDB.getInt(itemFromDB.getColumnIndex("time_wait")) == 1;
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("time"));
            Boolean alternative = itemFromDB.getInt(itemFromDB.getColumnIndex("alternative")) == 1;
            Integer varPosition = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_position"));
            Boolean sndWait = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_wait")) == 1;
            Integer snd = itemFromDB.getInt(itemFromDB.getColumnIndex("snd"));
            Boolean inflection = itemFromDB.getInt(itemFromDB.getColumnIndex("inflection")) == 1;
            Integer sndDetectXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_detect_x_times"));
            Boolean shuffle = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle")) == 1;
            Boolean shuffleModify = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle_modify")) == 1;
            drillArray.add(new DrillInSet(id, drillId, position, isPause, pauseTime, variantXTimes, alternative, drillName, editName,
                    prev, timeWait, time, varPosition, sndWait, snd, sndDetectXTimes, inflection, shuffle, shuffleModify));
        }
        itemFromDB.close();
        db.close();
        return drillArray;
    }

    /**
     * Get data about drill set
     * @param ctx - aplication context
     * @param setId - drill set id
     * @return
     */
    private static DrillSet getDrillSetData(Context ctx, int setId){
        SQLiteDB db = new SQLiteDB(ctx);
        Cursor itemFromDB = db.getDrillSetById(setId);
        DrillSet set = null;
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer dsId = itemFromDB.getInt(itemFromDB.getColumnIndex("dsid"));
            Integer repeat = itemFromDB.getInt(itemFromDB.getColumnIndex("repeat"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("set_name"));
            Integer group = itemFromDB.getInt(itemFromDB.getColumnIndex("set_group"));
            set = new DrillSet(dsId, name, group, -1, repeat);
        }
        itemFromDB.close();
        db.close();
        return set;
    }

    /**
     * Get inflection words for drill
     * @param ctx - aplication context
     * @param variantXtimes - variant number from drill alternative
     * @param word - inflection word
     * @return list of inflection words
     */
    private static ArrayList<String> getInflectionRecord(Context ctx, int variantXtimes, String word){
        ArrayList<String> results = new ArrayList<>();
        String name1 = null;
        String name24 = null;
        String name5 = null;
        if(variantXtimes <= 1)
            name1 = word;
        else if(variantXtimes < 5 && variantXtimes > 1)
            name24 = word;
        else if(variantXtimes > 4)
            name5 = word;
        InflectionDB db = new InflectionDB(ctx);
        Cursor itemFromDB = db.getInflection(name1, name24, name5);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            results.add(itemFromDB.getString(itemFromDB.getColumnIndex("name1")));
            results.add(itemFromDB.getString(itemFromDB.getColumnIndex("name24")));
            results.add(itemFromDB.getString(itemFromDB.getColumnIndex("name5")));
        }
        db.close();
        return results;
    }

    /**
     * Convert drillSet to format for export
     * @param set - drillSet to convert
     * @return final string with set data for export
     */
    private static String convertDrillSetToExport(DrillSet set){
        StringBuilder builder = new StringBuilder("DS::");
        builder.append(set.getDsId());
        builder.append(";;");
        builder.append(set.getName());
        builder.append(";;");
        builder.append(set.getRepeatCnt());
        return builder.toString();
    }

    /**
     * Convert all drills to format for export
     * @param drills - list of drills
     * @return final string with drills data for export
     */
    private static String convertDrillInSetToExport(Context ctx, ArrayList<DrillInSet> drills){
        StringBuilder builder = new StringBuilder();
        ArrayList<String> inflection = new ArrayList<>();
        for (DrillInSet drill : drills){
            inflection.clear();
            if(drill.isInflection() && drill.isAlternative()){
                String[] parts = drill.getName().split(" ");
                if(parts.length > (drill.getVariantPosition()-1)) {
                    inflection = getInflectionRecord(ctx, drill.getVariantXTimes(), parts[drill.getVariantPosition()]);
                }
            }
            builder.append("\n");
            builder.append("D::");
            builder.append(drill.getName());
            builder.append(";;");
            builder.append(drill.getPosition());
            builder.append(";;");
            builder.append((drill.isPause() ? 1 : 0));
            builder.append(";;");
            builder.append(drill.getPauseTime());
            builder.append(";;");
            builder.append(drill.getVariantXTimes());
            builder.append(";;");
            builder.append((drill.isJointToPrevDrill() ? 1 : 0));
            builder.append(";;");
            builder.append((drill.isTimeWait() ? 1 : 0));
            builder.append(";;");
            builder.append(drill.getTime());
            builder.append(";;");
            builder.append((drill.isAlternative() ? 1 : 0));
            builder.append(";;");
            builder.append((drill.getVariantPosition()));
            builder.append(";;");
            builder.append((drill.isSndWait() ? 1 : 0));
            builder.append(";;");
            builder.append(drill.getSnd());
            builder.append(";;");
            builder.append((drill.isInflection() ? 1 : 0));
            builder.append(";;");
            builder.append(drill.getSndDetectXTtimes());
            builder.append(";;");
            builder.append((drill.isShuffle() ? 1 : 0));
            builder.append(";;");
            builder.append((drill.isShuffleModify() ? 1 : 0));
            if(inflection.size() == 3){
                for(String i : inflection){
                    builder.append(";;");
                    builder.append(i);
                }
            }
        }
        return builder.toString();
    }
}
