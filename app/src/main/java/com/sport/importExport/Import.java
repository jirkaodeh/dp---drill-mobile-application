package com.sport.importExport;

import android.content.Context;
import android.database.Cursor;

import com.sport.database.InflectionDB;
import com.sport.database.SQLiteDB;
import com.sport.lists.Drill;
import com.sport.lists.DrillInSet;

import java.util.ArrayList;

/**
 * Created by Jirka on 16.3.2019.
 * Class for import drill set
 */
public final class Import {

    /**
     *  Public function for import drill set
     * @param ctx - aplication context
     * @param importedStr - string with imported drill set in export string format
     * @param idGroup - group id
     * @return
     */
    public static boolean importDrillSet(Context ctx, String importedStr, int idGroup){
        String[] lines = importedStr.split("\n");
        if(lines.length < 1){
            return false;
        }
        int drillSetId = parseImportDrillSet(ctx, lines[0], idGroup);
        if(drillSetId == -1){
            return false;
        }
        boolean parseDrillsResult = parseImportDrills(ctx, drillSetId, idGroup, 1, lines);
        if(!parseDrillsResult){
            // undo remove inserted drillSet in DB
            SQLiteDB db = new SQLiteDB(ctx);
            db.deleteDrillSet(drillSetId);
            db.close();
            return false;
        }
        return true;
    }

    /**
     * Parse drill set values and save to DB
     * @param ctx - aplication context
     * @param drillSetData - string data about drill set
     * @param idGroup - group id
     * @return return id of added drill set
     */
    private static int parseImportDrillSet(Context ctx, String drillSetData, int idGroup){
        String[] prefixParts = drillSetData.split("::");
        if(prefixParts.length == 2 && prefixParts[0].compareTo("DS") == 0) {
            String[] drillSetParts = prefixParts[1].split(";;");
            if (drillSetParts.length != 3) {
                return -1;
            }
            try {
                String drillSetName = drillSetParts[1];
                int repeat = Integer.valueOf(drillSetParts[2]);
                SQLiteDB db = new SQLiteDB(ctx);
                long drillSetId = db.insertDrillSet(drillSetName, idGroup, repeat);
                db.close();
                return (int)drillSetId;
            }
            catch (Exception e){
                return -1;
            }
        }
        return -1;
    }

    /**
     * Parse all drills in imported drill set
     * @param context - aplication context
     * @param drillSetId - added drill set id
     * @param idGroup - group id
     * @param startIndex - start index in parse array
     * @param drillsArr - parse array with info about drills
     * @return true - all ok | false - error during parsing
     */
    private static boolean parseImportDrills(Context context, int drillSetId, int idGroup, int startIndex, String[] drillsArr){
        // check and parse imported drills
        ArrayList<DrillInSet> preparedDrillInSetArr = new ArrayList<>();
        ArrayList<Drill> preparedDrillArr = new ArrayList<>();
        String drillName, word1, word24, word5;
        int position, pauseTime, variantXTimes, time, variantPosition, snd, sndDetectXTtimes;
        boolean isPause, jointToPrevDrill, timeWait, alternative, sndWait, inflection;
        boolean shuffle, shuffleModify;
        for(int i = startIndex; i < drillsArr.length; i++){
            String[] prefixParts = drillsArr[i].split("::");
            if(prefixParts.length == 2 && prefixParts[0].compareTo("D") == 0) {
                String[] drillParts = prefixParts[1].split(";;");
                if (drillParts.length != 16 && drillParts.length != 19) {
                    return false;
                }
                try {
                    drillName = drillParts[0];
                    position = Integer.valueOf(drillParts[1]);
                    isPause = chceckIntForBool(Integer.valueOf(drillParts[2]));
                    pauseTime = Integer.valueOf(drillParts[3]);
                    variantXTimes = Integer.valueOf(drillParts[4]);
                    jointToPrevDrill = chceckIntForBool(Integer.valueOf(drillParts[5]));
                    timeWait = chceckIntForBool(Integer.valueOf(drillParts[6]));
                    time = Integer.valueOf(drillParts[7]);
                    alternative = chceckIntForBool(Integer.valueOf(drillParts[8]));
                    variantPosition = Integer.valueOf(drillParts[9]);
                    sndWait = chceckIntForBool(Integer.valueOf(drillParts[10]));
                    snd = Integer.valueOf(drillParts[11]);
                    inflection = chceckIntForBool(Integer.valueOf(drillParts[12]));
                    sndDetectXTtimes = Integer.valueOf(drillParts[13]);
                    shuffle = chceckIntForBool(Integer.valueOf(drillParts[14]));
                    shuffleModify = chceckIntForBool(Integer.valueOf(drillParts[15]));
                    if(drillParts.length == 19){
                        addInflectionToDB(context, drillParts[16], drillParts[17], drillParts[18]);
                    }
                    preparedDrillArr.add(new Drill(-1, drillName, -1, "", alternative,
                            position, timeWait, time, sndWait, snd, inflection, shuffle, shuffleModify));
                    preparedDrillInSetArr.add(new DrillInSet(-1, -1, position, isPause, pauseTime, variantXTimes, alternative,
                     drillName, drillName, jointToPrevDrill, timeWait, time,
                    variantPosition, sndWait, snd, sndDetectXTtimes, inflection, shuffle, shuffleModify));
                }
                catch (Exception e){
                    return false;
                }
            }
            else{
                return false;
            }
        }
        SQLiteDB db = new SQLiteDB(context);
        for(int i = 0; i < preparedDrillInSetArr.size(); i++){
            long drillId = db.insertDrill(preparedDrillInSetArr.get(i).getName(), idGroup, 0, boolToInt(preparedDrillInSetArr.get(i).isSndWait()),
                    preparedDrillInSetArr.get(i).getSnd(), preparedDrillInSetArr.get(i).getVariantXTimes(), boolToInt(preparedDrillInSetArr.get(i).isTimeWait()),
                    preparedDrillInSetArr.get(i).getTime(), boolToInt(preparedDrillInSetArr.get(i).isAlternative()), preparedDrillInSetArr.get(i).getVariantPosition(),
                    boolToInt(preparedDrillInSetArr.get(i).isInflection()), boolToInt(preparedDrillInSetArr.get(i).isShuffle()),
                    boolToInt(preparedDrillInSetArr.get(i).isShuffleModify()), 0);
            db.insertDrillIntoSet(drillSetId, (int)drillId, preparedDrillInSetArr.get(i).getPosition(), boolToInt(preparedDrillInSetArr.get(i).isPause()),
                    preparedDrillInSetArr.get(i).getPauseTime(), preparedDrillInSetArr.get(i).getVariantXTimes(), preparedDrillInSetArr.get(i).getName(),
                    boolToInt(preparedDrillInSetArr.get(i).isJointToPrevDrill()));
        }
        db.close();
        return true;
    }

    /**
     * Check if parset boolean value is 0 or 1
     * @param value - value for check
     * @return 1 - true | 0 - false
     * @throws Exception - if value is otherwise
     */
    private static boolean chceckIntForBool(int value) throws Exception{
        if(value == 1 || value == 0)
            return value == 1;
        else
            throw new Exception("Error boolean format in parse line!");
    }

    /**
     * Convert bool value to int
     * @param value - bool value
     * @return value in int
     */
    private static int boolToInt(boolean value){
        return value ? 1 : 0;
    }

    /**
     * Add inflection words to local DB
     * @param context - aplication context
     * @param word1 - word type 1
     * @param word24 - word type 2
     * @param word5 - word type 3
     */
    private static void addInflectionToDB(Context context, String word1, String word24, String word5){
        InflectionDB db = new InflectionDB(context);
        Cursor itemFromDB = db.getInflection(word1, word24, word5);
        boolean iter = false;
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            iter = true;
        }
        if(!iter) {
            itemFromDB.close();
            db.close();
            if (!iter) {
                db.insertInflection(word1, word24, word5);
            }
        }
        itemFromDB.close();
        db.close();
    }
}
