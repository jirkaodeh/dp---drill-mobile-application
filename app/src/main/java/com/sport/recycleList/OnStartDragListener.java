package com.sport.recycleList;

import android.support.v7.widget.RecyclerView;

/**
 * Code taken from https://medium.com/@ipaulpro/drag-and-swipe-with-recyclerview-6a6f0c422efd
 * licensed under Apache 2.0
 *
 * Listener for manual initiation of a drag.
 */
public interface OnStartDragListener {

    /**
     * Called when a view is requesting a start of a drag.
     * @param viewHolder The holder of the view to drag.
     */
    void onStartDrag(RecyclerView.ViewHolder viewHolder);

}
