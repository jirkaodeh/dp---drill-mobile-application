package com.sport.fragments.main;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sport.CreateNewDrillActivity;
import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.lists.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jirka on 08.11.2018.
 *
 */
public class CreateNewPauseFragment extends Fragment {
    private View myFragmentView;
    private Button createBtn;
    private EditText pauseTime;
    private TextView warningTV;
    private CreateNewDrillActivity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // show fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (CreateNewDrillActivity) getActivity();
        myFragmentView = inflater.inflate(R.layout.fragment_new_pause, container, false);

        createBtn = (Button) myFragmentView.findViewById(R.id.create_pause_button);
        pauseTime = (EditText) myFragmentView.findViewById(R.id.input_pause_time);
        warningTV = (TextView) myFragmentView.findViewById(R.id.warning);

        if(activity.DRILL_ID != -1){
            editPause();
        }

        // create new pause
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pauseTimeStr = pauseTime.getText().toString();
                Log.i("pauseTimeStr", ""+pauseTimeStr);
                if(pauseTimeStr.equals("")){
                    warningTV.setText(activity.getResources().getString(R.string.warning_create_pause_time));
                    warningTV.setVisibility(View.VISIBLE);
                }
                else if(Integer.parseInt(pauseTimeStr) == 0){
                    warningTV.setText(activity.getResources().getString(R.string.warning_create_pause_time_zero));
                    warningTV.setVisibility(View.VISIBLE);
                }
                else{
                    warningTV.setVisibility(View.GONE);
                    SQLiteDB db = new SQLiteDB(activity);
                    // edit pause
                    if(activity.DRILL_ID_IN_SET != -1){
                        db.editPause(activity.DRILL_ID_IN_SET, Integer.parseInt(pauseTimeStr));
                    }
                    // add pause
                    else {
                        // get position in drill set
                        Cursor itemFromDB = db.getLastDrillInSetPosition(activity.SET_ID);
                        Integer max = 0;
                        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
                            max = itemFromDB.getInt(itemFromDB.getColumnIndex("max"));
                        }
                        itemFromDB.close();
                        Long l = db.insertDrillIntoSet(activity.SET_ID, 0, max + 1, 1, Integer.parseInt(pauseTimeStr),
                                0, "", 0);
                    }
                    db.close();
                    activity.finish();
                }
            }
        });
        return myFragmentView;
    }

    private void editPause(){
        int pauseTimeInt = 0;
        SQLiteDB db = new SQLiteDB(activity);
        Cursor itemFromDB = db.getDrillById(activity.DRILL_ID_IN_SET);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            pauseTimeInt = itemFromDB.getInt(itemFromDB.getColumnIndex("pause_time"));
        }
        itemFromDB.close();
        db.close();
        String time = ""+pauseTimeInt;
        pauseTime.setText(time);
        pauseTime.setSelection(time.length());
        createBtn.setText("Editovat pauzu");
    }
}

