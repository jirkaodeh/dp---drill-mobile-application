package com.sport.fragments.main;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.sport.FinishActivity;
import com.sport.GlobalStatisticsActivity;
import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.lists.CompleteSet;
import com.sport.lists.Group;
import com.sport.lists.StatisticsHistoryCompleteSetAdapter;
import com.sport.utils.ListUtils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
/**
 * Created by Jirka on 12.12.2018.
 */
public class GlobalStatisticsGraphFragment  extends Fragment {

    private View myFragmentView;
    private BarChart daysChart;
    private HorizontalBarChart horizontalChart;
    private ArrayList<BarEntry> entries;
    private List<Group> groupArray;
    private ArrayList<CompleteSet> completeSetArray;
    private ArrayList<Popularity> setPopularityArray;
    private Date newestDate;
    private ArrayList<Date> days;
    private ArrayList<Integer> months;
    private ArrayList<Integer> daysValue;
    private TextView horizontalLabel;
    private int allTime;
    private static int GRAPH_ANIMATION = 1500;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        GlobalStatisticsActivity activity = (GlobalStatisticsActivity) getActivity();
        myFragmentView = inflater.inflate(R.layout.fragment_statistics_graph, container, false);
        this.entries = new ArrayList<>();
        horizontalLabel = (TextView) myFragmentView.findViewById(R.id.graph_horizontal_label);
        getData();
        getDataSetPopularity();
        init();
        return myFragmentView;
    }

    private void init(){

        final TextView daysTv = myFragmentView.findViewById(R.id.days);
        final TextView monthsTv = myFragmentView.findViewById(R.id.months);
        TextView timeTv = myFragmentView.findViewById(R.id.time);
        TextView setsTv = myFragmentView.findViewById(R.id.sets);

        timeTv.setText(FinishActivity.getTime(allTime));
        setsTv.setText(completeSetArray.size()+"");

        daysTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daysTv.setTextColor(getResources().getColor(R.color.colorAccent));
                monthsTv.setTextColor(getResources().getColor(R.color.drillText));
                showDayGraph();
            }
        });
        monthsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daysTv.setTextColor(getResources().getColor(R.color.drillText));
                monthsTv.setTextColor(getResources().getColor(R.color.colorAccent));
                showMonthGraph();
            }
        });
        initChart();
    }

    private void initChart(){
        // BarChart daysChart
        daysChart = (BarChart) myFragmentView.findViewById(R.id.days_chart);

        daysChart.setDrawBarShadow(false);
        daysChart.setDrawBorders(false);
        daysChart.setTouchEnabled(false);

        daysChart.getLegend().setEnabled(false);

        daysChart.getAxisLeft().setEnabled(true);
        YAxis leftAxis = daysChart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setAxisMinimum(0);
        daysChart.getAxisRight().setEnabled(false);

        daysChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        daysChart.getXAxis().setDrawLabels(true);
        daysChart.getXAxis().setDrawGridLines(false);
        daysChart.getXAxis().setGranularityEnabled(true);
        daysChart.getXAxis().setGranularity(1.0f);
        daysChart.getAxisLeft().setGranularityEnabled(true);


        XAxis bottomAxis = daysChart.getXAxis();
        bottomAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        bottomAxis.setLabelCount(12);

        daysChart.setDrawGridBackground(false);

        daysChart.getAxisLeft().setTextColor(getResources().getColor(R.color.drillText)); // left y-axis
        daysChart.getXAxis().setTextColor(getResources().getColor(R.color.drillText));
        daysChart.getLegend().setTextColor(getResources().getColor(R.color.drillText));
        daysChart.setDescription(null);

        daysChart.animateY(GRAPH_ANIMATION);
        showDayGraph();

        // HorizontalBarChart horizontalChart
        horizontalChart = (HorizontalBarChart) myFragmentView.findViewById(R.id.set_popularity);

        horizontalChart.setDrawBarShadow(false);
        horizontalChart.setDrawBorders(false);
        horizontalChart.setTouchEnabled(false);
        horizontalChart.setDrawValueAboveBar(true);

        horizontalChart.setDrawGridBackground(false);
        horizontalChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        horizontalChart.getXAxis().setDrawLabels(true);
        horizontalChart.getXAxis().setDrawGridLines(false);

        horizontalChart.getAxisLeft().setEnabled(true);
        YAxis leftAxis2 = daysChart.getAxisLeft();
        leftAxis2.setAxisMinimum(0);
        horizontalChart.getAxisRight().setEnabled(false);
        horizontalChart.getLegend().setEnabled(false);

        horizontalChart.getXAxis().setGranularityEnabled(true);
        horizontalChart.getXAxis().setGranularity(1.0f);
        horizontalChart.getAxisLeft().setGranularityEnabled(true);
        horizontalChart.getAxisLeft().setGranularity(1.0f);

        horizontalChart.getAxisLeft().setTextColor(getResources().getColor(R.color.drillText));
        horizontalChart.getXAxis().setTextColor(getResources().getColor(R.color.drillText));
        horizontalChart.getLegend().setTextColor(getResources().getColor(R.color.drillText));
        horizontalChart.setDescription(null);

        horizontalChart.animateY(GRAPH_ANIMATION);
        showPopularityGraph();
    }

    private void getData(){
        // get all groups from DB
        completeSetArray = new ArrayList<CompleteSet>();
        SQLiteDB db = new SQLiteDB(this.getActivity());
        Cursor itemFromDB = db.getAllSetComplete();
        precomputeDates();
        allTime = 0;
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer setCompleteId = itemFromDB.getInt(itemFromDB.getColumnIndex("complete_id"));
            String dateTime = itemFromDB.getString(itemFromDB.getColumnIndex("date_time"));
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("set_time"));
            String note = itemFromDB.getString(itemFromDB.getColumnIndex("note"));
            Integer percentage = itemFromDB.getInt(itemFromDB.getColumnIndex("percentage"));
            String setName = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer set_group = itemFromDB.getInt(itemFromDB.getColumnIndex("set_group"));
            Integer mode = itemFromDB.getInt(itemFromDB.getColumnIndex("mode"));
            CompleteSet cs = new CompleteSet(setCompleteId, setName, dateTime, percentage, (long)time, note,
                    set_group, mode, this.getActivity());
            completeSetArray.add(cs);
            allTime += time;

            DateFormat formatDay = new SimpleDateFormat("dd");
            DateFormat formatYear = new SimpleDateFormat("yyyy");
            DateFormat formatMonths = new SimpleDateFormat("MM");
            int val;
            for(int i = 0; i < days.size(); i++){
                if(formatDay.format(completeSetArray.get(completeSetArray.size()-1).getDateTime()).compareTo(formatDay.format(days.get(i))) == 0 && formatMonths.format(completeSetArray.get(completeSetArray.size()-1).getDateTime()).compareTo(formatMonths.format(days.get(i))) == 0) {
                    val = daysValue.get(i) + 1;//time
                    daysValue.set(i, val);
                }
            }
            for(int i = 0; i < months.size(); i++){
                String month = ((i+1) < 10 ? "0"+(i+1) : ""+(i+1));
                if(formatMonths.format(completeSetArray.get(completeSetArray.size()-1).getDateTime()).compareTo(month) == 0){
                    val = months.get(i) + 1;//time
                    months.set(i, val);
                }
            }
        }
        itemFromDB.close();
        db.close();
        eliminateIrelevantGroupes();
    }

    private void getDataSetPopularity(){
        setPopularityArray = new ArrayList<Popularity>();
        SQLiteDB db = new SQLiteDB(this.getActivity());
        Cursor itemFromDB = db.getDrillSetPopularity();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer popularity = itemFromDB.getInt(itemFromDB.getColumnIndex("popularity"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("set_name"));
            setPopularityArray.add(new Popularity(name, popularity));
        }
        itemFromDB.close();
        db.close();
    }

    private void precomputeDates(){
        days = new ArrayList<Date>();
        months = new ArrayList<Integer>();
        daysValue = new ArrayList<Integer>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        DateFormat format = new SimpleDateFormat("dd.MM");

        // precompute days back
        for (int i = 0; i < 12; i++) {
            if(i != 0)
                cal.add(Calendar.DATE, -1);
            Date nowMinus = cal.getTime();
            //Log.i("Date", format.format(nowMinus));
            days.add(nowMinus);
            daysValue.add(0);
        }
        // precompute months
        for (int i = 1; i < 13; i++){
            months.add(0);
        }

    }

    private void showDayGraph(){
        this.entries = new ArrayList<>();
        //float[] rankArr = new float[days.size()];
        for(int i = 0; i < days.size(); i++){
            entries.add(new BarEntry(i, daysValue.get(i)));
            //rankArr[i] = daysValue.get(i)/1000/60;
            //rankArr[i] = daysValue.get(i);
        }
        BarDataSet dataset = new BarDataSet(entries, "");
        dataset.setColor(getResources().getColor(R.color.colorPrimary));
        dataset.setValueTextColor(getResources().getColor(R.color.drillText));
        dataset.setValueFormatter(new MyValueFormatter());
        XAxis xAxis = daysChart.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                DateFormat format = new SimpleDateFormat("dd.MM");
                return format.format(days.get(Math.round(value)));
            }
        });
        xAxis.setLabelRotationAngle(-45);
        horizontalLabel.setText(myFragmentView.getResources().getString(R.string.statistics_graph_horizontal_days));

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataset);

        BarData barData = new BarData(dataSets);
        daysChart.setData(barData);
        daysChart.invalidate();
    }

    private void showMonthGraph(){
        this.entries = new ArrayList<>();
        //float[] rankArr = new float[months.size()];
        for(int i = 0; i < months.size(); i++){
            entries.add(new BarEntry(i+1, months.get(i)));
            //rankArr[i] = months.get(i)/1000/60;
            //rankArr[i] = months.get(i);
        }
        BarDataSet dataset = new BarDataSet(entries, "");
        dataset.setColor(getResources().getColor(R.color.colorPrimary));
        dataset.setValueTextColor(getResources().getColor(R.color.drillText));
        dataset.setValueFormatter(new MyValueFormatter());
        XAxis xAxis = daysChart.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                    return Math.round(value)+"";
            }
        });
        xAxis.setLabelRotationAngle(0);
        horizontalLabel.setText(myFragmentView.getResources().getString(R.string.statistics_graph_horizontal_months));

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataset);

        BarData barData = new BarData(dataSets);
        daysChart.setData(barData);
        daysChart.invalidate();
    }

    private void showPopularityGraph(){
        this.entries = new ArrayList<>();
        Log.i("Val", setPopularityArray.size()+"");
        for(int i = 0; i < setPopularityArray.size(); i++){
            entries.add(new BarEntry(i, setPopularityArray.get(i).getPopularity()));
        }
        BarDataSet dataset = new BarDataSet(entries, "");
        dataset.setColor(getResources().getColor(R.color.colorPrimary));
        dataset.setValueTextColor(getResources().getColor(R.color.drillText));
        dataset.setValueFormatter(new MyValueFormatter());
        XAxis xAxis = horizontalChart.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if(Math.round(value) < setPopularityArray.size())
                    return setPopularityArray.get(Math.round(value)).getSetName();
                return "";
            }
        });
        xAxis.setLabelRotationAngle(0);

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataset);

        BarData barData = new BarData(dataSets);
        horizontalChart.setData(barData);
        horizontalChart.invalidate();
    }

    private void eliminateIrelevantGroupes(){
        groupArray = new ArrayList<>();
        boolean added;

        for(int i = 0; i < completeSetArray.size(); i++){
            getNewestDate(completeSetArray.get(i).getDateTime());
            added = false;
            for(int j = 0; j < groupArray.size(); j++){
                if(completeSetArray.get(i).getGroup().getgId() == groupArray.get(j).getgId()){
                    added = true;
                    break;
                }
            }
            if(!added){
                groupArray.add(completeSetArray.get(i).getGroup());
            }
        }
    }

    private void getNewestDate(Date d){
        if(newestDate == null){
            newestDate = d;
        }
        else if(d.compareTo(newestDate) > 0){
            newestDate = d;
        }
    }

    // Trida specifikujici format popisu hodnot pro grafy
    public class MyValueFormatter implements IValueFormatter {

        private DecimalFormat mFormat;

        public MyValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0");
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

            return mFormat.format(value);
        }
    }

    protected class Popularity{
        private String setName;
        private int popularity;
        private int MAX_SIZE = 18;
        public Popularity(String setName, int popularity){
            this.setName = setName;
            this.popularity = popularity;
        }

        public String getSetName() {
            if(setName.length() > MAX_SIZE)
                return setName.substring(0, MAX_SIZE)+"...";
            else
                return setName;
        }

        public int getPopularity() {
            return popularity;
        }
    }
}
