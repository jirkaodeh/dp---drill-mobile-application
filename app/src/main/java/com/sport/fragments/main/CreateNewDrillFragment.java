package com.sport.fragments.main;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.sport.CreateNewDrillActivity;
import com.sport.MainActivity;
import com.sport.R;
import com.sport.database.InflectionDB;
import com.sport.database.SQLiteDB;
import com.sport.lists.DrillInSet;
import com.sport.lists.Group;
import com.sport.lists.Snd;
import com.sport.lists.SndAdapter;
import com.sport.utils.CreateDrillUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jirka on 08.11.2018.
 *
 */
public class CreateNewDrillFragment extends Fragment {
    private View myFragmentView;
    private Button createBtn;
    private Switch joinSwitch, waitSwitch, variableSwitch, sndXTimesSwitch, saveDrillSwitch, inflectionSwitch, shuffleSwitch;
    private Switch shuffleValueChangeSwitch;
    private EditText drillNameText, waitTimeText;
    private LinearLayout waitTimeBox, waitBoxTime, waitBoxSnd, variableBox, shuffleBox;
    private ImageButton positionInfoButton, changesndButton;
    private RadioButton radioTime, radioSnd;
    private CreateNewDrillActivity activity;
    private Spinner groupSpinner, positionSpinner;
    private TextView warningTV, selectedSndTv;
    private List<Integer> intAlternativePositionList;
    List<Group> groupesList;
    private int selectedGroup = 1;
    private int selectedPosition;
    private List<String> groupesStringList = new ArrayList<String>();
    private boolean join = false;
    private boolean wait = false;
    private boolean sndSelect = false;
    private int sndCls = -1;
    private boolean variable = false;
    private boolean save = false;
    private boolean inflection = false;
    private boolean shuffle, shuffleModifyValue;
    private AlertDialog dialog;
    private DrillInSet editDrill = null;
    private Group editDrillGroup = null;
    private boolean inflectionDialogDecline = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // show fragment
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (CreateNewDrillActivity) getActivity();
        myFragmentView = inflater.inflate(R.layout.fragment_new_drill, container, false);

        createBtn = (Button) myFragmentView.findViewById(R.id.create_drill_button);
        joinSwitch = (Switch) myFragmentView.findViewById(R.id.switch_join);
        waitSwitch = (Switch) myFragmentView.findViewById(R.id.switch_wait);
        shuffleSwitch = (Switch) myFragmentView.findViewById(R.id.switch_shuffle);
        shuffleValueChangeSwitch = (Switch) myFragmentView.findViewById(R.id.switch_shuffle_drill_value_change);
        variableSwitch = (Switch) myFragmentView.findViewById(R.id.switch_variable);
        inflectionSwitch = (Switch)myFragmentView.findViewById(R.id.switch_position_inflect);
        //sndXTimesSwitch = (Switch) myFragmentView.findViewById(R.id.switch_snd_x_times);
        saveDrillSwitch = (Switch) myFragmentView.findViewById(R.id.switch_save);
        drillNameText = (EditText) myFragmentView.findViewById(R.id.input_drill_name);
        waitTimeBox = (LinearLayout) myFragmentView.findViewById(R.id.input_wait_time_box);
        waitBoxSnd = (LinearLayout) myFragmentView.findViewById(R.id.input_wait_time_box_snd);
        waitBoxTime = (LinearLayout) myFragmentView.findViewById(R.id.input_wait_time_box_time);
        variableBox = (LinearLayout) myFragmentView.findViewById(R.id.switch_variable_box);
        shuffleBox = (LinearLayout) myFragmentView.findViewById(R.id.input_shuffle_box);
        waitTimeText = (EditText) myFragmentView.findViewById(R.id.input_wait_time);
        positionInfoButton = (ImageButton) myFragmentView.findViewById((R.id.position_info_button));
        radioSnd = (RadioButton) myFragmentView.findViewById(R.id.radio_snd);
        radioTime = (RadioButton) myFragmentView.findViewById(R.id.radio_time);
        groupSpinner = (Spinner) myFragmentView.findViewById(R.id.select_group);
        positionSpinner = (Spinner) myFragmentView.findViewById(R.id.select_editable_position);
        warningTV = (TextView) myFragmentView.findViewById(R.id.warning);
        selectedSndTv = (TextView) myFragmentView.findViewById(R.id.select_snd);
        changesndButton = (ImageButton) myFragmentView.findViewById(R.id.snd_change);

        // edit - set stored info
        if(activity.DRILL_ID_IN_SET != -1){
            editDrill();
        }

        // create new drill
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean warning = false;
                // no write pause time
                String drillNameTextStr = drillNameText.getText().toString();
                if(drillNameTextStr.equals("")){
                    warningTV.setText(activity.getResources().getString(R.string.warning_create_drill_drill_name));
                    warningTV.setVisibility(View.VISIBLE);
                    warning = true;
                }
                else{
                    warningTV.setVisibility(View.GONE);
                    // get position in drill set
                    SQLiteDB db = new SQLiteDB(activity);
                    Cursor itemFromDB = db.getLastDrillInSetPosition(activity.SET_ID);
                    Integer max = 0;
                    for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
                        max = itemFromDB.getInt(itemFromDB.getColumnIndex("max"));
                    }
                    db.close();
                    // get group id
                    int cnt = 1, groupId = 1;
                    for (Group g : groupesList) {
                        if (selectedGroup == cnt){
                            groupId = g.getgId();
                            break;
                        }
                        cnt++;
                    }
                    // selected position
                    int positionInsert = 0;
                    int variableInsert = 0;
                    int variableValue = 0;
                    if(variable && intAlternativePositionList.size() > 0){
                        variableInsert = 1;
                        positionInsert = selectedPosition;
                        if(variableInsert == 1 && selectedPosition <= 0){
                            positionInsert = Integer.valueOf(positionSpinner.getSelectedItem().toString());
                        }
                        String[] parts = drillNameTextStr.split(" ");
                        if(parts.length > positionInsert)
                            variableValue = Integer.valueOf(parts[positionInsert-1]);
                    }
                    // wait time
                    int waitInsert = 0;
                    int waitTimeInsert = 0;
                    int sndInsert = 0;
                    int sndSoundInsert = 0;
                    String waitTimeStr = waitTimeText.getText().toString();
                    if(wait && radioTime.isChecked()){
                        waitInsert = 1;
                        if(waitTimeStr.equals("")){
                            warningTV.setText(activity.getResources().getString(R.string.warning_create_drill_wait_time));
                            warningTV.setVisibility(View.VISIBLE);
                            warning = true;
                        }
                        else{
                            waitTimeInsert = Integer.parseInt(waitTimeText.getText().toString());
                            warningTV.setVisibility(View.GONE);
                        }
                    }
                    // wait snd
                    else if(waitSwitch.isChecked() && radioSnd.isChecked()){
                        if(sndCls == -1){
                            warningTV.setText(activity.getResources().getString(R.string.warning_create_drill_wait_time));
                            warningTV.setVisibility(View.VISIBLE);
                            warning = true;
                        }
                        else{
                            sndSoundInsert = sndCls;
                        }
                        sndInsert = 1;
                    }
                    //inflection
                    int inflectionInt = (inflection ? 1 : 0);
                    int shuffleInt = (shuffle ? 1 : 0);
                    int shuffleModifyInt = 0;
                    if(shuffle && variableInsert == 1){
                        shuffleModifyInt = (shuffleModifyValue ? 1 : 0);
                    }
                    Long l = Long.valueOf(-1);
                    if(!warning) {
                        // edit
                        if(activity.DRILL_ID_IN_SET != -1) {
                            db = new SQLiteDB(activity);
                            if (editDrill != null){
                                db.editDrill(editDrill.getIdDrill(), editDrill.getIdDrill_drill_table(), drillNameTextStr, sndInsert, sndSoundInsert,
                                        waitInsert, waitTimeInsert, variableInsert, inflectionInt, shuffleInt, shuffleModifyInt, positionInsert,
                                        variableValue, (join ? 1 : 0), groupId);
                            }
                            db.close();
                            itemFromDB.close();
                            activity.finish();
                        }
                        // create
                        else {
                            db = new SQLiteDB(activity);
                            if (save) {
                                db.insertDrill(drillNameTextStr, groupId, 1, sndInsert, sndSoundInsert, 0,
                                        waitInsert, waitTimeInsert, variableInsert, positionInsert, inflectionInt, shuffleInt, shuffleModifyInt, 1);
                            }
                            Long drilId = db.insertDrill(drillNameTextStr, groupId, 1, sndInsert, sndSoundInsert, 0,
                                    waitInsert, waitTimeInsert, variableInsert, positionInsert, inflectionInt, shuffleInt, shuffleModifyInt, 0);
                            l = db.insertDrillIntoSet(activity.SET_ID, drilId.intValue(), max + 1, 0, 0,
                                    variableValue, "", (join ? 1 : 0));
                            db.close();
                            itemFromDB.close();
                            activity.finish();
                        }
                    }
                    /*if(!warning && l != -1){
                        getActivity().finish();
                    }*/
                }

            }
        });

        // set wait - show wait edit text
        waitSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wait = isChecked;
                if(isChecked){
                    waitTimeBox.setVisibility(View.VISIBLE);
                }
                else{
                    waitTimeBox.setVisibility(View.GONE);
                }
            }
        });

        // set shuffle
        shuffleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                shuffle = isChecked;
                if(isChecked){
                    shuffleBox.setVisibility(View.VISIBLE);
                }
                else{
                    shuffleBox.setVisibility(View.GONE);
                }
            }
        });

        // modify
        shuffleValueChangeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                shuffleModifyValue = isChecked; // true - modify | false - not modify
            }
        });

        // set join to previous drill
        joinSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                join = isChecked; // true - join | false - not join
            }
        });


        inflectionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                inflection = isChecked;
                callInflectionCheck();
            }
        });

        // drill is fast editable
        variableSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setVariable(isChecked);
                if(isChecked){
                    shuffleValueChangeSwitch.setEnabled(true);
                    variableBox.setVisibility(View.VISIBLE);
                    callInflectionCheck();
                }
                else{
                    shuffleValueChangeSwitch.setEnabled(false);
                    variableBox.setVisibility(View.GONE);
                }
            }
        });

        // save drill for next use
        saveDrillSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                save = isChecked;
            }
        });

        // click on info button in position spinner
        positionInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateDrillUtils.positionInfoDialog(activity);
            }
        });

        // checked wait for time
        radioTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((RadioButton) v).isChecked();
                if(checked){
                    waitBoxTime.setVisibility(View.VISIBLE);
                    waitBoxSnd.setVisibility(View.GONE);
                    sndSelect = false;
                }
                else{
                    waitBoxTime.setVisibility(View.GONE);
                    waitBoxSnd.setVisibility(View.GONE);
                }
            }
        });

        // checked wait for snd
        radioSnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((RadioButton) v).isChecked();
                if(checked){
                    waitBoxTime.setVisibility(View.GONE);
                    waitBoxSnd.setVisibility(View.VISIBLE);
                    dialogSelectSound();
                    sndSelect = true;
                }
                else{
                    waitBoxTime.setVisibility(View.GONE);
                    waitBoxSnd.setVisibility(View.GONE);
                }
            }
        });

        changesndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectSound();
                sndSelect = true;
            }
        });

        // wait for sound x times
        /*sndXTimesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                snd_x_times = isChecked;
            }
        });*/

        // select group
        groupesList = CreateDrillUtils.getListOfGroupesObject(activity);
        int counter = 0;
        for (Group g : groupesList) {
            groupesStringList.add(g.getName());
            if(g.getName().compareTo(activity.GROUP) == 0){
                selectedGroup = counter+1;
            }
            counter++;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, groupesStringList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(adapter);
        groupSpinner.setSelection(selectedGroup-1);
        // edit group
        if(editDrillGroup != null){
            for (int i = 0; i < groupesStringList.size(); i++) {
                if(groupesStringList.get(i).compareTo(editDrillGroup.getName()) == 0){
                    groupSpinner.setSelection(i);
                    selectedGroup = i+1;
                }
            }
        }
        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedGroup = (int)id+1;
                //Log.v("item "+selectedGroup, (String)parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        positionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedPosition = (int)id;
                callInflectionCheck();
                //Log.v("position "+selectedPosition, (String)parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // prepare position by parse dril name (find num in words)
        drillNameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                intAlternativePositionList = CreateDrillUtils.splitString(s.toString());
                ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(activity, android.R.layout.simple_spinner_dropdown_item, intAlternativePositionList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                positionSpinner.setAdapter(adapter);
                inflectionDialogDecline = false;
            }
        });
        drillNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                    CreateDrillUtils.hideKeyboard(v, activity);
                    callInflectionCheck();
                }
            }
        });
        waitTimeText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                    CreateDrillUtils.hideKeyboard(v, activity);
            }
        });

        return myFragmentView;
    }

    private void setVariable(boolean var){
        this.variable = var;
    }



    private void dialogSelectSound(){
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);

        ArrayList<Snd> sndArr = new ArrayList<>();
        SQLiteDB db = new SQLiteDB(activity);
        Cursor itemFromDB = db.getAllSnd();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            String example = itemFromDB.getString(itemFromDB.getColumnIndex("example"));
            Integer cls = itemFromDB.getInt(itemFromDB.getColumnIndex("class"));
            Integer active = itemFromDB.getInt(itemFromDB.getColumnIndex("active"));
            Integer added = itemFromDB.getInt(itemFromDB.getColumnIndex("added"));
            if(active == 1)
                sndArr.add(new Snd(id, name, example, cls, active, added));
        }
        itemFromDB.close();
        db.close();

        final View Viewlayout = inflater.inflate(R.layout.dialog_select_snd,
                (ViewGroup) myFragmentView.findViewById(R.id.layout_dialog));
        View includedLayout = Viewlayout.findViewById(R.id.id_drill_list);
        ListView lv = (ListView) includedLayout.findViewById(R.id.list);
        lv.setAdapter(new SndAdapter(myFragmentView.getContext(), R.layout.list_item_select_snd, sndArr, this));

        popDialog.setView(Viewlayout);

        popDialog.setPositiveButton(activity.getResources().getString(R.string.back_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        dialog = popDialog.create();
        // listener for close dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(activity.getResources().getColor(R.color.colorAccent));
            }
        });
        dialog.show();
    }

    public void setSnd(int cls, String sndName){
        sndCls = cls;
        selectedSndTv.setText(getResources().getString(R.string.snd_select_text)+" "+sndName);
        if(dialog != null)
            dialog.dismiss();
    }

    private void editDrill(){
        SQLiteDB db = new SQLiteDB(activity);
        Cursor itemFromDB = db.getDrillById(activity.DRILL_ID_IN_SET);
        Integer groupId = 0;
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id_drill_in_set"));
            Integer drillId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_id"));
            Integer position = itemFromDB.getInt(itemFromDB.getColumnIndex("position"));
            Boolean isPause = itemFromDB.getInt(itemFromDB.getColumnIndex("is_pause")) == 1;
            Integer pauseTime = itemFromDB.getInt(itemFromDB.getColumnIndex("pause_time"));
            Integer variantXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_x_times"));
            String editName = itemFromDB.getString(itemFromDB.getColumnIndex("edit_name"));
            String drillName = itemFromDB.getString(itemFromDB.getColumnIndex("drill_name"));
            Boolean prev = itemFromDB.getInt(itemFromDB.getColumnIndex("joint_to_prev_drill")) == 1;
            Boolean timeWait = itemFromDB.getInt(itemFromDB.getColumnIndex("time_wait")) == 1;
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("time"));
            Boolean alternative = itemFromDB.getInt(itemFromDB.getColumnIndex("alternative")) == 1;
            Integer varPosition = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_position"));
            Boolean sndWait = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_wait")) == 1;
            Integer snd = itemFromDB.getInt(itemFromDB.getColumnIndex("snd"));
            Boolean inflection = itemFromDB.getInt(itemFromDB.getColumnIndex("inflection")) == 1;
            Integer sndDetectXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_detect_x_times"));
            Boolean shuffle = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle")) == 1;
            Boolean shuffleModify = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle_modify")) == 1;
            groupId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_group"));
            editDrill = new DrillInSet(id, drillId, position, isPause, pauseTime, variantXTimes, alternative, drillName, editName,
                    prev, timeWait, time, varPosition, sndWait, snd, sndDetectXTimes, inflection, shuffle, shuffleModify);
        }
        itemFromDB = db.getGroupById(groupId,true);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("gid"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer cnt = itemFromDB.getInt(itemFromDB.getColumnIndex("cnt"));
            String description = itemFromDB.getString(itemFromDB.getColumnIndex("description"));
            Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
            Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
            editDrillGroup = new Group(id, name, description, icon, color, cnt);
        }
        db.close();
        // set stored info
        if(editDrill != null){
            drillNameText.setText(editDrill.getName());
            variableSwitch.setChecked(editDrill.isAlternative());
            variableBox.setVisibility((editDrill.isAlternative() ? View.VISIBLE : View.GONE));
            if(editDrill.isAlternative()) {
                inflectionSwitch.setChecked(editDrill.isInflection());
                setVariable(editDrill.isAlternative());
                intAlternativePositionList = CreateDrillUtils.splitString(editDrill.getName().toString());
                ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(activity, android.R.layout.simple_spinner_dropdown_item, intAlternativePositionList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                positionSpinner.setAdapter(adapter);
                for(int i = 0; i < intAlternativePositionList.size(); i++){
                    if(editDrill.getPosition() == intAlternativePositionList.get(i)){
                        positionSpinner.setSelection(i);
                        selectedPosition = i;
                    }
                }

                inflection = editDrill.isInflection();
            }
            else{
                intAlternativePositionList = CreateDrillUtils.splitString(editDrill.getName().toString());
                ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(activity, android.R.layout.simple_spinner_dropdown_item, intAlternativePositionList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                positionSpinner.setAdapter(adapter);
            }
            shuffleSwitch.setChecked(editDrill.isShuffle());
            shuffleBox.setVisibility((editDrill.isShuffle() ? View.VISIBLE : View.GONE));
            if(editDrill.isShuffle() && editDrill.isAlternative()) {
                shuffleValueChangeSwitch.setChecked(editDrill.isShuffleModify());
                shuffleModifyValue = editDrill.isShuffleModify();
                shuffle = true;
            }
            else if(!editDrill.isAlternative()){
                shuffleValueChangeSwitch.setEnabled(false);
            }
            joinSwitch.setChecked(editDrill.isJointToPrevDrill());
            join = editDrill.isJointToPrevDrill();
            if(editDrill.isTimeWait() || editDrill.isSndWait()){
                wait = true;
                waitSwitch.setChecked(true);
                waitTimeBox.setVisibility(View.VISIBLE);
                if(editDrill.isTimeWait()){
                    waitBoxTime.setVisibility(View.VISIBLE);
                    radioTime.setChecked(true);
                    waitTimeText.setText(editDrill.getTime()+"");
                }
                else if(editDrill.isSndWait()){
                    radioSnd.setChecked(true);
                    waitBoxSnd.setVisibility(View.VISIBLE);
                    sndSelect = true;
                    Snd snd = editDrill.getSndInfo(activity);
                    if(snd != null) {
                        sndCls = snd.getCls();
                        selectedSndTv.setText(getResources().getString(R.string.snd_select_text) + " " + snd.getName());
                    }
                    else {
                        selectedSndTv.setTextColor(getResources().getColor(R.color.warning));
                        selectedSndTv.setText(getResources().getString(R.string.warning_no_snd));
                    }
                }
            }

            saveDrillSwitch.setVisibility(View.GONE);
            createBtn.setText("Editovat cvik");
        }
    }

    private void callInflectionCheck(){
        if(!inflectionDialogDecline && variableSwitch.isChecked() && inflectionSwitch.isChecked()) {
            // TODO request to DB
            String drillName = drillNameText.getText().toString();
            String[] parts = drillName.split(" ");
            // check variant
            if(parts.length > (selectedPosition-1)) {
                // check word after variant
                if(variableSwitch.isChecked() && selectedPosition <= 0){
                    try {
                        selectedPosition = Integer.valueOf(positionSpinner.getSelectedItem().toString());
                    } catch (Exception e){}
                }
                if(parts.length > selectedPosition && parts.length != 0 && selectedPosition != 0) {
                    int xTimes = Integer.valueOf(parts[selectedPosition - 1]);
                    String name1 = null;
                    String name24 = null;
                    String name5 = null;
                    if(xTimes <= 1)
                        name1 = parts[selectedPosition];
                    else if(xTimes < 5 && xTimes > 1)
                        name24 = parts[selectedPosition];
                    else if(xTimes > 4)
                        name5 = parts[selectedPosition];
                    InflectionDB db = new InflectionDB(getContext());
                    Cursor itemFromDB = db.getInflection(name1, name24, name5);
                    boolean iter = false;
                    for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
                        iter = true;
                    }
                    itemFromDB.close();
                    db.close();
                    if(!iter) {
                        newInflectionnDialog(getContext(), parts[selectedPosition], xTimes);
                    }
                }
            }
        }
    }

    public void newInflectionnDialog(final Context ctx, String word, int variantX) {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(ctx);
        final LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);

        final View viewlayout = inflater.inflate(R.layout.dialog_create_inflection,
                (ViewGroup) activity.findViewById(R.id.layout_dialog));
        ((TextView)viewlayout.findViewById(R.id.title)).setText(String.format(getResources().getString(R.string.inflection_dialog_title), word));
        final EditText word1Edit = viewlayout.findViewById(R.id.word1);
        final EditText word24Edit = viewlayout.findViewById(R.id.word24);
        final EditText word5Edit = viewlayout.findViewById(R.id.word5);
        final TextView warningTv = viewlayout.findViewById(R.id.warning);
        if(variantX <= 1) {
            word1Edit.setText(word);
        }
        else if(variantX < 5 && variantX > 1) {
            word24Edit.setText(word);
        }
        else if(variantX > 4) {
            word5Edit.setText(word);
        }

        popDialog.setView(viewlayout);

        popDialog.setPositiveButton(activity.getResources().getString(R.string.save), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        popDialog.setNegativeButton(activity.getResources().getString(R.string.decline), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = popDialog.create();
        // listener for close dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ctx.getResources().getColor(R.color.colorAccent));
            }
        });
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(word1Edit.getText().toString().isEmpty() || word24Edit.getText().toString().isEmpty() ||
                        word5Edit.getText().toString().isEmpty()){
                    warningTv.setVisibility(View.VISIBLE);
                }
                else {
                    warningTv.setVisibility(View.GONE);
                    InflectionDB infDB = new InflectionDB(activity);
                    infDB.insertInflection(word1Edit.getText().toString(), word24Edit.getText().toString(), word5Edit.getText().toString());
                    inflectionDialogDecline = true;
                    dialog.dismiss();
                }
            }
        });
    }
}
