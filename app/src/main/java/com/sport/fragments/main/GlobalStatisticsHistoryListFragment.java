package com.sport.fragments.main;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.sport.CreateNewDrillActivity;
import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.lists.CompleteSet;
import com.sport.lists.Drill;
import com.sport.lists.Group;
import com.sport.lists.GroupAdapter;
import com.sport.lists.StatisticsHistoryCompleteSetAdapter;
import com.sport.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by Jirka on 12.12.2018.
 */
public class GlobalStatisticsHistoryListFragment  extends Fragment {

    private View myFragmentView;
    private FloatingActionButton filterFab;
    View includedLayout;
    private StatisticsHistoryCompleteSetAdapter adapter;
    private ListView lv;
    private List<Group> groupArray;
    private String[] groupesName;
    private boolean[] itemsSelect;
    private boolean[] itemsSelectArchive;
    private boolean init = false;
    private AlertDialog dialog;
    private ArrayList<CompleteSet> completeSetArray, actualCompleteSetArray;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_statistics_history_list, container, false);
        includedLayout = myFragmentView.findViewById(R.id.id_drill_list);
        lv = (ListView) includedLayout.findViewById(R.id.list);

        filterFab = myFragmentView.findViewById(R.id.fab);
        filterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog();
            }
        });

        getData();

        return myFragmentView;
    }

    private void getSelectedGroupIDs(){
        actualCompleteSetArray.clear();
        for(int j = 0; j < completeSetArray.size(); j++) {
            for (int i = 0; i < groupArray.size(); i++) {
                if (completeSetArray.get(j).getGroup().getgId() == groupArray.get(i).getgId() && itemsSelect[i]) {
                    actualCompleteSetArray.add(completeSetArray.get(j));
                    break;
                }
            }
        }

    }

    private void groupSelectInit(){
        groupesName = new String[groupArray.size()];
        if(!init)
            itemsSelect = new boolean[groupArray.size()];
        for(int i = 0; i < groupArray.size(); i++){
            groupesName[i] = groupArray.get(i).getName();
            if(!init)
                itemsSelect[i] = true;
        }
        init = true;
    }

    public void removeCompleteSet(int id){
        int cnt = 0;
        while(cnt < completeSetArray.size()){
            if(completeSetArray.get(cnt).getSetCompleteId() == id)
                completeSetArray.remove(cnt);
            cnt++;
        }
    }

    private void getData(){
        // get all groups from DB
        completeSetArray = new ArrayList<CompleteSet>();
        actualCompleteSetArray = new ArrayList<CompleteSet>();
        SQLiteDB db = new SQLiteDB(this.getActivity());
        Cursor itemFromDB = db.getAllSetComplete();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer setCompleteId = itemFromDB.getInt(itemFromDB.getColumnIndex("complete_id"));
            String dateTime = itemFromDB.getString(itemFromDB.getColumnIndex("date_time"));
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("set_time"));
            String note = itemFromDB.getString(itemFromDB.getColumnIndex("note"));
            Integer percentage = itemFromDB.getInt(itemFromDB.getColumnIndex("percentage"));
            String setName = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer set_group = itemFromDB.getInt(itemFromDB.getColumnIndex("set_group"));
            Integer mode = itemFromDB.getInt(itemFromDB.getColumnIndex("mode"));
            CompleteSet cs = new CompleteSet(setCompleteId, setName, dateTime, percentage, (long)time, note,
                    set_group, mode, this.getActivity());
            completeSetArray.add(cs);
            actualCompleteSetArray.add(cs);
        }
        itemFromDB.close();
        db.close();
        TextView emptyList = (TextView) myFragmentView.findViewById(R.id.empty_list);
        if (completeSetArray.size() > 0) {
            adapter = new StatisticsHistoryCompleteSetAdapter(this.getActivity(), R.layout.list_item_history, actualCompleteSetArray, this);
            lv.setAdapter(adapter);
            ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);
        } else {
            lv.setVisibility(View.GONE);
            emptyList.setVisibility(View.VISIBLE);
        }
        eliminateIrelevantGroupes();
        groupSelectInit();
    }

    private void eliminateIrelevantGroupes(){
        groupArray = new ArrayList<>();
        boolean added;
        for(int i = 0; i < completeSetArray.size(); i++){
            added = false;
            for(int j = 0; j < groupArray.size(); j++){
                if(completeSetArray.get(i).getGroup().getgId() == groupArray.get(j).getgId()){
                    added = true;
                    break;
                }
            }
            if(!added){
                groupArray.add(completeSetArray.get(i).getGroup());
            }
        }
    }

    private void filterDialog(){
        itemsSelectArchive = new boolean[itemsSelect.length];
        for(int i = 0; i < itemsSelect.length; i++){
            itemsSelectArchive[i] = itemsSelect[i];
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Filtrování podle skupin");
        builder.setMultiChoiceItems(groupesName, itemsSelect, new DialogInterface.OnMultiChoiceClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int position, boolean isChecked){
                itemsSelect[position] = isChecked;
            }
        });
        builder.setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                getSelectedGroupIDs();
                adapter.notifyDataSetChanged();
            }
        }).setNegativeButton(this.getResources().getString(R.string.back_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                itemsSelect = itemsSelectArchive;
            }
        });
        dialog = builder.create();
        dialog.show();
    }
}
