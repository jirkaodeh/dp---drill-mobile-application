package com.sport.fragments.main;

import com.sport.CreateNewGroupActivity;
import com.sport.utils.ListUtils;
import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.lists.Group;
import com.sport.lists.GroupAdapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jirka on 30.10.2016.
 */
public class MainGroupFragment  extends Fragment {
    private View myFragmentView;
    private ListView lv;
    View includedLayout;
    FloatingActionButton fab;
    List<Group> groupsArray;
    private boolean isFragmentVisible = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // show fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_main_group, container, false);
        includedLayout = myFragmentView.findViewById(R.id.id_group_list);
        lv = (ListView) includedLayout.findViewById(R.id.list);
        lv.setVisibility(View.GONE);

        getData();

        fab = (FloatingActionButton) myFragmentView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = getContext();
                Intent intent = new Intent(context, CreateNewGroupActivity.class);
                context.startActivity(intent);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });

        return myFragmentView;
    }

    private void getData(){

        // get all groups from DB
        groupsArray = new ArrayList<Group>();
        SQLiteDB db = new SQLiteDB(this.getActivity());
        Cursor itemFromDB = db.getAllGroupes(true);
        List<Group> groupsDBArray = new ArrayList<Group>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("gid"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer cnt = itemFromDB.getInt(itemFromDB.getColumnIndex("cnt"));
            String description = itemFromDB.getString(itemFromDB.getColumnIndex("description"));
            Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
            Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
            groupsDBArray.add(new Group(id, name, description, icon, color, cnt));
        }
        itemFromDB.close();
        db.close();
        TextView emptyList = (TextView) myFragmentView.findViewById(R.id.empty_list);
        if (groupsDBArray.size() > 0) {
            lv.setAdapter(new GroupAdapter(this.getActivity(), R.layout.list_item_group, groupsDBArray));
            ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);
        } else {
            lv.setVisibility(View.GONE);
            emptyList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }
}

