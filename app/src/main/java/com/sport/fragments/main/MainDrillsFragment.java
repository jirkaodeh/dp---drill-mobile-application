package com.sport.fragments.main;

import com.sport.CreateNewDrillSetActivity;
import com.sport.GroupDrillSetsActivity;
import com.sport.ImportDrillSetActivity;
import com.sport.utils.ListUtils;
import com.sport.MainActivity;
import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.lists.DrillSet;
import com.sport.lists.DrillSetAdapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jirka on 30.10.2018.
 *
 */
public class MainDrillsFragment  extends Fragment {
    private View myFragmentView;
    private ListView lv;
    View includedLayout;
    private com.github.clans.fab.FloatingActionButton fab1, fab2;
    List<DrillSet> drillSetArray;
    private boolean isFragmentVisible = false;
    private int GROUP_ID;
    private boolean fragmentVisible = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // show fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*MainActivity activity = (MainActivity) getActivity();
        GroupDrillSetsActivity activity2 = (GroupDrillSetsActivity) getActivity();*/
        myFragmentView = inflater.inflate(R.layout.fragment_main_drills, container, false);
        includedLayout = myFragmentView.findViewById(R.id.id_drill_list);
        lv = (ListView) includedLayout.findViewById(R.id.list);
        lv.setVisibility(View.GONE);

        try {
            MainActivity activity = (MainActivity) getActivity();
            GROUP_ID = activity.getGroupId();
        } catch (Exception e){
            GroupDrillSetsActivity activity = (GroupDrillSetsActivity) getActivity();
            GROUP_ID = activity.getGroupId();
        }


        getData();
        // handler for import drill set button
        fab1 = (com.github.clans.fab.FloatingActionButton) myFragmentView.findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = getContext();
                Intent intent = new Intent(context, ImportDrillSetActivity.class);
                context.startActivity(intent);
            }
        });

        // handler for add drill set button
        fab2 = (com.github.clans.fab.FloatingActionButton) myFragmentView.findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = getContext();
                Intent intent = new Intent(context, CreateNewDrillSetActivity.class);
                context.startActivity(intent);
            }
        });
        fragmentVisible = true;

        return myFragmentView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && fragmentVisible){
            getData();
        }

    }

    private void getData(){
        // get all drill sets from DB
        drillSetArray = new ArrayList<DrillSet>();
        SQLiteDB db = new SQLiteDB(this.getActivity());
        Cursor itemFromDB;
        if(GROUP_ID == -1) {
            itemFromDB = db.getAllDrillSets();
        }
        else {
            itemFromDB = db.getDrillSetsByGroupId(GROUP_ID);
        }
        List<DrillSet> drillSetsDBArray = new ArrayList<DrillSet>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("dsid"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("set_name"));
            Integer group = itemFromDB.getInt(itemFromDB.getColumnIndex("set_group"));
            Integer repeat = itemFromDB.getInt(itemFromDB.getColumnIndex("repeat"));
            String groupName = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
            Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
            drillSetsDBArray.add(new DrillSet(id, name, group, repeat, groupName, icon, color));
        }
        itemFromDB.close();
        db.close();
        TextView emptyList = (TextView) myFragmentView.findViewById(R.id.empty_list);
        if (drillSetsDBArray.size() > 0) {
            lv.setAdapter(new DrillSetAdapter(this.getActivity(), R.layout.list_item_drill_set, drillSetsDBArray, getActivity()));
            ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);
        } else {
            lv.setVisibility(View.GONE);
            emptyList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }
}
