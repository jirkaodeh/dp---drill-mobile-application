package com.sport.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Locale;

/**
 * Created by Jirka on 13.2.2019.
 * Class represents inflection word database and operation over it
 */
public class InflectionDB {
    private static final String DATABASE_NAME = "voicesports-inflection";
    private static final int DATABASE_VERSION = 1;

    private static final String TB_NAME_INFLECTION = "tb_inflection_word";
    private static final String INFLECTION_COLUMN_ID = "id";
    private static final String INFLECTION_COLUMN_NAME1 = "name1";
    private static final String INFLECTION_COLUMN_NAME24 = "name24";
    private static final String INFLECTION_COLUMN_NAME5 = "name5";
    private static final String INFLECTION_COLUMN_ADD = "is_add";
    public static final String[] inclectionColumns = { INFLECTION_COLUMN_ID, INFLECTION_COLUMN_NAME1, INFLECTION_COLUMN_NAME24, INFLECTION_COLUMN_NAME5, INFLECTION_COLUMN_ADD};

    private SQLiteOpenHelper openHelper;

    static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TB_NAME_INFLECTION + " ("
                    + INFLECTION_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + INFLECTION_COLUMN_NAME1 + " TEXT NOT NULL,"
                    + INFLECTION_COLUMN_NAME24 + " TEXT NOT NULL,"
                    + INFLECTION_COLUMN_NAME5 + " TEXT NOT NULL,"
                    + INFLECTION_COLUMN_ADD + " INTEGER NOT NULL"
                    + ");");
            initInsertInflection(db, "dřep", "dřepy", "dřepů");
            initInsertInflection(db, "klik", "kliky", "kliků");
            initInsertInflection(db, "krok", "kroky", "kroků");
            initInsertInflection(db, "přešlap", "přešlapy", "přešlapů");
            initInsertInflection(db, "skok", "skoky", "skoků");
            initInsertInflection(db, "výstřel", "výstřely", "výstřelů");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            /*db.execSQL("DROP TABLE IF EXISTS "+TB_NAME_INFLECTION);
            onCreate(db);*/
        }

        /**
         * Insert new set of inflection word during initialization
         * @param db - database handler
         * @param name1 - word type 1
         * @param name24 - word type 2
         * @param name5 - word type 3
         */
        private void initInsertInflection(SQLiteDatabase db, String name1, String name24, String name5) {
            ContentValues values = new ContentValues();
            values.put(INFLECTION_COLUMN_NAME1, name1);
            values.put(INFLECTION_COLUMN_NAME24, name24);
            values.put(INFLECTION_COLUMN_NAME5, name5);
            values.put(INFLECTION_COLUMN_ADD, 0);
            db.insert(TB_NAME_INFLECTION, null, values);

        }
    }

    public InflectionDB(Context ctx) {
        openHelper = new InflectionDB.DatabaseHelper(ctx);
    }

    /**
     * Insert new set of inflection word
     * @param name1 - word type 1
     * @param name24 - word type 2
     * @param name5 - word type 3
     */
    public long insertInflection(String name1, String name24, String name5) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(INFLECTION_COLUMN_NAME1, name1);
        values.put(INFLECTION_COLUMN_NAME24, name24);
        values.put(INFLECTION_COLUMN_NAME5, name5);
        values.put(INFLECTION_COLUMN_ADD, 1);

        long columnId = db.insert(TB_NAME_INFLECTION, null, values);
        db.close();
        return columnId;
    }

    /**
     * Get word inflections by one of inflection type
     * @param name1 - word type 1
     * @param name24 - word type 2
     * @param name5 - word type 3
     * @return cursor with result
     */
    public Cursor getInflection(String name1, String name24, String name5) {
        String where = "WHERE ";
        boolean cnt = false;
        if(name1 != null && !name1.isEmpty()) {
            where += INFLECTION_COLUMN_NAME1+" = '"+name1.toLowerCase()+"' ";
            cnt = true;
        }
        if(name24 != null && !name24.isEmpty()){
            where += (cnt ? " AND " : "")+INFLECTION_COLUMN_NAME24+" = '"+name24.toLowerCase()+"' ";
            cnt = true;
        }
        if(name5 != null && !name5.isEmpty()){
            where += (cnt ? " AND " : "")+INFLECTION_COLUMN_NAME5+" = '"+name5.toLowerCase()+"' ";
        }
        if(where.equals("WHERE "))
            return null;
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT "+INFLECTION_COLUMN_ID+", "+INFLECTION_COLUMN_NAME1+", "+INFLECTION_COLUMN_NAME24+", "+INFLECTION_COLUMN_NAME5+" FROM "+
                TB_NAME_INFLECTION + " "+where;
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Get all inflection word (added by user)
     * @param onlyUserAdded - true - get only words added by user | false - get all words
     * @return cursor with result
     */
    public Cursor getAllInflection(boolean onlyUserAdded) {
        String where = "";
        if(onlyUserAdded){
            where += " WHERE "+INFLECTION_COLUMN_ADD+" = 1 ";
        }
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT * FROM "+
                TB_NAME_INFLECTION + " "+where+" ORDER BY "+INFLECTION_COLUMN_NAME1;
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Update inflection words
     * @param inflectionId - inflection identifier
     * @param name1 - name1
     * @param name24 - name24
     * @param name5 - name5
     */
    public void updateInflection(int inflectionId, String name1, String name24, String name5){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(INFLECTION_COLUMN_NAME1, name1);
        cv.put(INFLECTION_COLUMN_NAME24, name24);
        cv.put(INFLECTION_COLUMN_NAME5, name5);
        db.update(TB_NAME_INFLECTION, cv, "id = "+inflectionId, null);
        db.close();
    }

    /**
     * close database handeler
     */
    public void close() {
        openHelper.close();
    }
}
