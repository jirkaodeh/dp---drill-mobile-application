package com.sport.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;

import static java.sql.Types.NULL;

/**
 * Created by Jirka on 1.11.2018.
 * Class represents database and operation over it
 */
public class SQLiteDB {
    private static final String DATABASE_NAME = "voicesports";
    private static final int DATABASE_VERSION = 1;
    // table overview
    private static final String TB_NAME_GROUP = "tb_group";
    private static final String TB_NAME_DRILLS_SET = "tb_drills_set";
    private static final String TB_NAME_DRILL = "tb_drill";
    private static final String TB_NAME_DRILL_IN_SET = "tb_drill_in_set";
    private static final String TB_NAME_SET_COMPLETE = "tb_set_complete";
    private static final String TB_NAME_DRILL_INTERMEDIATE_TIME = "drill_intermediate_time";
    private static final String TB_NAME_SND = "tb_snd";

    // table group
    // Special value "_id", for easy use SimpleCursorAdapteru
    private static final String GROUP_COLUMN_ID = "id";
    private static final String GROUP_COLUMN_NAME = "name";
    private static final String GROUP_DESCRIPTION = "description";
    private static final String GROUP_COLUMN_ICON = "icon";
    private static final String GROUP_COLUMN_COLOR = "color";
    private static final String GROUP_COLUMN_ACTIVE = "active"; // 0/1
    private static final String[] groupColumns = { GROUP_COLUMN_ID, GROUP_COLUMN_NAME, GROUP_DESCRIPTION, GROUP_COLUMN_ICON, GROUP_COLUMN_COLOR};

    // table drills set
    private static final String DRILLS_SET_COLUMN_ID = "id";
    private static final String DRILLS_SET_COLUMN_SET_NAME = "set_name";
    private static final String DRILLS_SET_COLUMN_GROUP = "set_group";
    private static final String DRILLS_SET_COLUMN_REPEAT = "repeat";
    private static final String DRILLS_SET_COLUMN_ACTIVE = "active"; // 0/1
    private static final String[] drillsSetColumns = { DRILLS_SET_COLUMN_ID, DRILLS_SET_COLUMN_SET_NAME, DRILLS_SET_COLUMN_GROUP, DRILLS_SET_COLUMN_REPEAT};

    // table drill
    private static final String DRILL_COLUMN_ID = "id";
    private static final String DRILL_COLUMN_DRILL_NAME = "drill_name";
    private static final String DRILL_COLUMN_DRILL_GROUP = "drill_group";
    private static final String DRILL_COLUMN_CNT = "cnt"; // counter
    private static final String DRILL_COLUMN_SOUND_WAIT = "snd_wait"; // 0/1
    private static final String DRILL_COLUMN_SOUND = "snd"; // counter
    private static final String DRILL_COLUMN_SOUND_DETECT_X_TIMES = "snd_detect_x_times"; // x times counter
    private static final String DRILL_COLUMN_TIME_WAIT = "time_wait"; // 0/1
    private static final String DRILL_COLUMN_TIME = "time";
    private static final String DRILL_COLUMN_ALTERNATIVE = "alternative";
    private static final String DRILL_COLUMN_VARIANT_POSITION = "variant_position";
    private static final String DRILL_COLUMN_INFLECTION = "inflection";
    private static final String DRILL_COLUMN_SHUFFLE = "shuffle";
    private static final String DRILL_COLUMN_SHUFFLE_MODIFY = "shuffle_modify";
    private static final String DRILL_COLUMN_SAVE = "save"; // 0/1
    private static final String DRILL_COLUMN_ACTIVE = "active"; // 0/1
    private static final String[] drillColumns = { DRILL_COLUMN_ID, DRILL_COLUMN_DRILL_NAME, DRILL_COLUMN_DRILL_GROUP, DRILL_COLUMN_CNT, DRILL_COLUMN_SOUND_WAIT, DRILL_COLUMN_SOUND, DRILL_COLUMN_SOUND_DETECT_X_TIMES, DRILL_COLUMN_TIME_WAIT, DRILL_COLUMN_TIME, DRILL_COLUMN_ALTERNATIVE, DRILL_COLUMN_VARIANT_POSITION, DRILL_COLUMN_SAVE};

    // table drill_in_set
    private static final String DRILL_IN_SET_COLUMN_ID = "id";
    private static final String DRILL_IN_SET_COLUMN_DRILL_SET_ID = "id_set";
    private static final String DRILL_IN_SET_COLUMN_DRILL_ID = "id_drill";
    private static final String DRILL_IN_SET_COLUMN_POSITION_IN_SET = "position";
    private static final String DRILL_IN_SET_COLUMN_IS_PAUSE = "is_pause"; // 0/1
    private static final String DRILL_IN_SET_COLUMN_PAUSE_TIME = "pause_time";
    private static final String DRILL_IN_SET_COLUMN_VARIANT_NUM = "variant_x_times"; // x times counter
    private static final String DRILL_IN_SET_COLUMN_EDIT_NAME = "edit_name";
    private static final String DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN = "joint_to_prev_drill";
    private static final String[] drillInSetColumns = { DRILL_IN_SET_COLUMN_ID, DRILL_IN_SET_COLUMN_DRILL_SET_ID, DRILL_IN_SET_COLUMN_DRILL_ID, DRILL_IN_SET_COLUMN_POSITION_IN_SET, DRILL_IN_SET_COLUMN_IS_PAUSE, DRILL_IN_SET_COLUMN_PAUSE_TIME, DRILL_IN_SET_COLUMN_VARIANT_NUM, DRILL_IN_SET_COLUMN_EDIT_NAME, DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN};

    // table set_complete
    private static final String SET_COMPLETE_COLUMN_ID = "id";
    private static final String SET_COMPLETE_COLUMN_ID_DRILL_SET = "id_set";
    private static final String SET_COMPLETE_COLUMN_FINAL_SET_TIME = "set_time";
    private static final String SET_COMPLETE_COLUMN_DATE_TIME = "date_time";
    private static final String SET_COMPLETE_COLUMN_PERCENTAGE = "percentage";
    private static final String SET_COMPLETE_COLUMN_END = "set_end";
    private static final String SET_COMPLETE_COLUMN_NOTE = "note";
    private static final String SET_COMPLETE_COLUMN_MODE = "mode";
    private static final String[] setCompleteColumns = {SET_COMPLETE_COLUMN_ID, SET_COMPLETE_COLUMN_ID_DRILL_SET, SET_COMPLETE_COLUMN_FINAL_SET_TIME, SET_COMPLETE_COLUMN_DATE_TIME, SET_COMPLETE_COLUMN_PERCENTAGE, SET_COMPLETE_COLUMN_NOTE};

    // table drill_intermediate_time
    private static final String DRILL_INTERMEDIATE_COLUMN_ID = "id";
    private static final String DRILL_INTERMEDIATE_COLUMN_ID_SET_COMPLETE = "id_set_complete";
    private static final String DRILL_INTERMEDIATE_COLUMN_ID_DRILL = "id_drill";
    private static final String DRILL_INTERMEDIATE_COLUMN_TIME = "time";
    private static final String DRILL_INTERMEDIATE_COLUMN_DETECTED_CNT = "detected_cnt";
    private static final String DRILL_INTERMEDIATE_COLUMN_POSITION = "position";
    private static final String DRILL_INTERMEDIATE_COLUMN_ITERATION = "iteration";
    private static final String DRILL_INTERMEDIATE_COLUMN_SKIPPED = "skipped";
    private static final String DRILL_INTERMEDIATE_COLUMN_NOTE = "note";
    private static final String[] drillIntermediateColumns = {DRILL_INTERMEDIATE_COLUMN_ID, DRILL_INTERMEDIATE_COLUMN_ID_SET_COMPLETE, DRILL_INTERMEDIATE_COLUMN_ID_DRILL, DRILL_INTERMEDIATE_COLUMN_TIME, DRILL_INTERMEDIATE_COLUMN_DETECTED_CNT, DRILL_INTERMEDIATE_COLUMN_POSITION, DRILL_INTERMEDIATE_COLUMN_ITERATION, DRILL_INTERMEDIATE_COLUMN_NOTE};

    // table with sound example
    private static final String SND_COLUMN_ID = "id";
    private static final String SND_COLUMN_CLASS = "class";
    private static final String SND_COLUMN_NAME = "name";
    private static final String SND_COLUMN_EXAMPLE = "example";
    private static final String SND_COLUMN_ACTIVE = "active";
    private static final String SND_COLUMN_ADDED = "added";
    public static final int SND_FIRST_ADD_ID = 20;

    private SQLiteOpenHelper openHelper;

    static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // Create all tables in database
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TB_NAME_GROUP + " ("
                    + GROUP_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + GROUP_COLUMN_NAME + " TEXT NOT NULL,"
                    + GROUP_DESCRIPTION + " TEXT,"
                    + GROUP_COLUMN_ICON + " INTEGER NOT NULL,"
                    + GROUP_COLUMN_COLOR + " INTEGER NOT NULL,"
                    + GROUP_COLUMN_ACTIVE + " INTEGER NOT NULL"
                    + ");");
            db.execSQL("CREATE TABLE " + TB_NAME_DRILLS_SET + " ("
                    + DRILLS_SET_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + DRILLS_SET_COLUMN_SET_NAME + " TEXT,"
                    + DRILLS_SET_COLUMN_GROUP + " INTEGER NOT NULL,"
                    + DRILLS_SET_COLUMN_REPEAT + " INTEGER,"
                    + DRILLS_SET_COLUMN_ACTIVE + " INTEGER NOT NULL"
                    + ");");
            db.execSQL("CREATE TABLE " + TB_NAME_DRILL + " ("
                    + DRILL_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + DRILL_COLUMN_DRILL_NAME + " TEXT NOT NULL,"
                    + DRILL_COLUMN_DRILL_GROUP + " INTEGER,"
                    + DRILL_COLUMN_CNT + " INTEGER NOT NULL,"
                    + DRILL_COLUMN_SOUND_WAIT + " INTEGER NOT NULL,"
                    + DRILL_COLUMN_SOUND + " INTEGER,"
                    + DRILL_COLUMN_SOUND_DETECT_X_TIMES + " INTEGER,"
                    + DRILL_COLUMN_TIME_WAIT + " INTEGER NOT NULL,"
                    + DRILL_COLUMN_TIME + " INTEGER,"
                    + DRILL_COLUMN_ALTERNATIVE + " INTEGER,"
                    + DRILL_COLUMN_VARIANT_POSITION + " INTEGER,"
                    + DRILL_COLUMN_INFLECTION + " INTEGER,"
                    + DRILL_COLUMN_SHUFFLE + " INTEGER NOT NULL,"
                    + DRILL_COLUMN_SHUFFLE_MODIFY + " INTEGER NOT NULL,"
                    + DRILL_COLUMN_SAVE + " INTEGER NOT NULL,"
                    + DRILL_COLUMN_ACTIVE + " INTEGER NOT NULL"
                    + ");");
            db.execSQL("CREATE TABLE " + TB_NAME_DRILL_IN_SET + " ("
                    + DRILL_IN_SET_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + DRILL_IN_SET_COLUMN_DRILL_SET_ID + " INTEGER,"
                    + DRILL_IN_SET_COLUMN_DRILL_ID + " INTEGER,"
                    + DRILL_IN_SET_COLUMN_POSITION_IN_SET + " INTEGER NOT NULL,"
                    + DRILL_IN_SET_COLUMN_IS_PAUSE + " INTEGER NOT NULL,"
                    + DRILL_IN_SET_COLUMN_PAUSE_TIME + " INTEGER,"
                    + DRILL_IN_SET_COLUMN_VARIANT_NUM + " INTEGER,"
                    + DRILL_IN_SET_COLUMN_EDIT_NAME + " TEXT,"
                    + DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN + " INTEGER"
                    + ");");
            db.execSQL("CREATE TABLE " + TB_NAME_SET_COMPLETE + " ("
                    + SET_COMPLETE_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + SET_COMPLETE_COLUMN_ID_DRILL_SET + " INTEGER NOT NULL,"
                    + SET_COMPLETE_COLUMN_FINAL_SET_TIME + " INTEGER NOT NULL,"
                    + SET_COMPLETE_COLUMN_DATE_TIME + " TEXT,"
                    + SET_COMPLETE_COLUMN_PERCENTAGE + " INTEGER NOT NULL,"
                    + SET_COMPLETE_COLUMN_END + " INTEGER NOT NULL,"
                    + SET_COMPLETE_COLUMN_MODE + " INTEGER NOT NULL,"
                    + SET_COMPLETE_COLUMN_NOTE + " TEXT"
                    + ");");
            db.execSQL("CREATE TABLE " + TB_NAME_DRILL_INTERMEDIATE_TIME + " ("
                    + DRILL_INTERMEDIATE_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + DRILL_INTERMEDIATE_COLUMN_ID_SET_COMPLETE + " INTEGER NOT NULL,"
                    + DRILL_INTERMEDIATE_COLUMN_ID_DRILL + " INTEGER NOT NULL,"
                    + DRILL_INTERMEDIATE_COLUMN_TIME + " INTEGER NOT NULL,"
                    + DRILL_INTERMEDIATE_COLUMN_DETECTED_CNT + " INTEGER,"
                    + DRILL_INTERMEDIATE_COLUMN_POSITION + " INTEGER,"
                    + DRILL_INTERMEDIATE_COLUMN_ITERATION + " INTEGER,"
                    + DRILL_INTERMEDIATE_COLUMN_SKIPPED + " INTEGER NOT NULL,"
                    + DRILL_INTERMEDIATE_COLUMN_NOTE + " TEXT"
                    + ");");
            db.execSQL("CREATE TABLE " + TB_NAME_SND + " ("
                    + SND_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + SND_COLUMN_NAME + " TEXT NOT NULL,"
                    + SND_COLUMN_EXAMPLE + " TEXT NOT NULL,"
                    + SND_COLUMN_CLASS + " INTEGER NOT NULL,"
                    + SND_COLUMN_ACTIVE + " INTEGER NOT NULL,"
                    + SND_COLUMN_ADDED + " INTEGER NOT NULL"
                    + ");");


            /***** insert default groups****/
            db.execSQL("INSERT INTO " + TB_NAME_GROUP + "(" +
                    GROUP_COLUMN_ID + ", " +
                    GROUP_COLUMN_NAME + ", " +
                    GROUP_DESCRIPTION + ", " +
                    GROUP_COLUMN_ICON  + ", " +
                    GROUP_COLUMN_COLOR  + ", " +
                    GROUP_COLUMN_ACTIVE  + " )" +
                    " VALUES (1, 'Demo cvičení', 'Demonstrační cvičební skupina', 1, 2, 1);");
            /** drill set **/
            db.execSQL("INSERT INTO " + TB_NAME_DRILLS_SET + "(" +
                    DRILLS_SET_COLUMN_ID + ", " +
                    DRILLS_SET_COLUMN_SET_NAME + ", " +
                    DRILLS_SET_COLUMN_GROUP + ", " +
                    DRILLS_SET_COLUMN_REPEAT + ", " +
                    DRILLS_SET_COLUMN_ACTIVE  + " )" +
                    " VALUES (1, 'Demo cvičení', 1, 1, 1);");
            /** default drill and drill in set **/
            db.execSQL("INSERT INTO " + TB_NAME_DRILL + "(" +
                    DRILL_COLUMN_ID + ", " +
                    DRILL_COLUMN_DRILL_NAME + ", " +
                    DRILL_COLUMN_DRILL_GROUP + ", " +
                    DRILL_COLUMN_CNT + ", " +
                    DRILL_COLUMN_SOUND_WAIT + ", " +
                    DRILL_COLUMN_SOUND + ", " +
                    DRILL_COLUMN_SOUND_DETECT_X_TIMES + ", " +
                    DRILL_COLUMN_TIME_WAIT + ", " +
                    DRILL_COLUMN_TIME + ", " +
                    DRILL_COLUMN_ALTERNATIVE + ", " +
                    DRILL_COLUMN_VARIANT_POSITION + ", " +
                    DRILL_COLUMN_INFLECTION + ", " +
                    DRILL_COLUMN_SHUFFLE + ", " +
                    DRILL_COLUMN_SHUFFLE_MODIFY + ", " +
                    DRILL_COLUMN_ACTIVE + ", " +
                    DRILL_COLUMN_SAVE  + " )" +
                    " VALUES (1, '2 kroky vpřed', 1,1,0,NULL,0,1,2,1,1,1,1,1,1,1);");
            db.execSQL("INSERT INTO " + TB_NAME_DRILL_IN_SET + "(" +
                    DRILL_IN_SET_COLUMN_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_SET_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_ID + ", " +
                    DRILL_IN_SET_COLUMN_POSITION_IN_SET + ", " +
                    DRILL_IN_SET_COLUMN_IS_PAUSE + ", " +
                    DRILL_IN_SET_COLUMN_PAUSE_TIME + ", " +
                    DRILL_IN_SET_COLUMN_VARIANT_NUM + ", " +
                    DRILL_IN_SET_COLUMN_EDIT_NAME + ", " +
                    DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN +" )" +
                    " VALUES (1, 1, 1, 1, 0, 0, 2, NULL, 0);");
            db.execSQL("INSERT INTO " + TB_NAME_DRILL + "(" +
                    DRILL_COLUMN_ID + ", " +
                    DRILL_COLUMN_DRILL_NAME + ", " +
                    DRILL_COLUMN_DRILL_GROUP + ", " +
                    DRILL_COLUMN_CNT + ", " +
                    DRILL_COLUMN_SOUND_WAIT + ", " +
                    DRILL_COLUMN_SOUND + ", " +
                    DRILL_COLUMN_SOUND_DETECT_X_TIMES + ", " +
                    DRILL_COLUMN_TIME_WAIT + ", " +
                    DRILL_COLUMN_TIME + ", " +
                    DRILL_COLUMN_ALTERNATIVE + ", " +
                    DRILL_COLUMN_VARIANT_POSITION + ", " +
                    DRILL_COLUMN_INFLECTION + ", " +
                    DRILL_COLUMN_SHUFFLE + ", " +
                    DRILL_COLUMN_SHUFFLE_MODIFY + ", " +
                    DRILL_COLUMN_ACTIVE + ", " +
                    DRILL_COLUMN_SAVE  + " )" +
                    " VALUES (2, '3 úkroky vpravo', 1,1,0,NULL,0,1,6,1,1,1,1,1,1,1);");
            db.execSQL("INSERT INTO " + TB_NAME_DRILL_IN_SET + "(" +
                    DRILL_IN_SET_COLUMN_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_SET_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_ID + ", " +
                    DRILL_IN_SET_COLUMN_POSITION_IN_SET + ", " +
                    DRILL_IN_SET_COLUMN_IS_PAUSE + ", " +
                    DRILL_IN_SET_COLUMN_PAUSE_TIME + ", " +
                    DRILL_IN_SET_COLUMN_VARIANT_NUM + ", " +
                    DRILL_IN_SET_COLUMN_EDIT_NAME + ", " +
                    DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN +" )" +
                    " VALUES (2, 1, 2, 2, 0, 0, 3, NULL, 0);");
            db.execSQL("INSERT INTO " + TB_NAME_DRILL + "(" +
                    DRILL_COLUMN_ID + ", " +
                    DRILL_COLUMN_DRILL_NAME + ", " +
                    DRILL_COLUMN_DRILL_GROUP + ", " +
                    DRILL_COLUMN_CNT + ", " +
                    DRILL_COLUMN_SOUND_WAIT + ", " +
                    DRILL_COLUMN_SOUND + ", " +
                    DRILL_COLUMN_SOUND_DETECT_X_TIMES + ", " +
                    DRILL_COLUMN_TIME_WAIT + ", " +
                    DRILL_COLUMN_TIME + ", " +
                    DRILL_COLUMN_ALTERNATIVE + ", " +
                    DRILL_COLUMN_VARIANT_POSITION + ", " +
                    DRILL_COLUMN_INFLECTION + ", " +
                    DRILL_COLUMN_SHUFFLE + ", " +
                    DRILL_COLUMN_SHUFFLE_MODIFY + ", " +
                    DRILL_COLUMN_ACTIVE + ", " +
                    DRILL_COLUMN_SAVE  + " )" +
                    " VALUES (3, '3 krát tleskni', 1,1,1,1,3,0,0,1,1,0,0,0,1,0);");
            db.execSQL("INSERT INTO " + TB_NAME_DRILL_IN_SET + "(" +
                    DRILL_IN_SET_COLUMN_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_SET_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_ID + ", " +
                    DRILL_IN_SET_COLUMN_POSITION_IN_SET + ", " +
                    DRILL_IN_SET_COLUMN_IS_PAUSE + ", " +
                    DRILL_IN_SET_COLUMN_PAUSE_TIME + ", " +
                    DRILL_IN_SET_COLUMN_VARIANT_NUM + ", " +
                    DRILL_IN_SET_COLUMN_EDIT_NAME + ", " +
                    DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN +" )" +
                    " VALUES (3, 1, 3, 3, 0, 0, 3, NULL, 0);");
            db.execSQL("INSERT INTO " + TB_NAME_DRILL + "(" +
                    DRILL_COLUMN_ID + ", " +
                    DRILL_COLUMN_DRILL_NAME + ", " +
                    DRILL_COLUMN_DRILL_GROUP + ", " +
                    DRILL_COLUMN_CNT + ", " +
                    DRILL_COLUMN_SOUND_WAIT + ", " +
                    DRILL_COLUMN_SOUND + ", " +
                    DRILL_COLUMN_SOUND_DETECT_X_TIMES + ", " +
                    DRILL_COLUMN_TIME_WAIT + ", " +
                    DRILL_COLUMN_TIME + ", " +
                    DRILL_COLUMN_ALTERNATIVE + ", " +
                    DRILL_COLUMN_VARIANT_POSITION + ", " +
                    DRILL_COLUMN_INFLECTION + ", " +
                    DRILL_COLUMN_SHUFFLE + ", " +
                    DRILL_COLUMN_SHUFFLE_MODIFY + ", " +
                    DRILL_COLUMN_ACTIVE + ", " +
                    DRILL_COLUMN_SAVE  + " )" +
                    " VALUES (4, 'vlevo v bok', 1,1,0,NULL,0,0,0,0,0,0,0,0,1,0);");
            db.execSQL("INSERT INTO " + TB_NAME_DRILL_IN_SET + "(" +
                    DRILL_IN_SET_COLUMN_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_SET_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_ID + ", " +
                    DRILL_IN_SET_COLUMN_POSITION_IN_SET + ", " +
                    DRILL_IN_SET_COLUMN_IS_PAUSE + ", " +
                    DRILL_IN_SET_COLUMN_PAUSE_TIME + ", " +
                    DRILL_IN_SET_COLUMN_VARIANT_NUM + ", " +
                    DRILL_IN_SET_COLUMN_EDIT_NAME + ", " +
                    DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN +" )" +
                    " VALUES (4, 1, 4, 4, 0, 0, 0, NULL, 0);");
            db.execSQL("INSERT INTO " + TB_NAME_DRILL + "(" +
                    DRILL_COLUMN_ID + ", " +
                    DRILL_COLUMN_DRILL_NAME + ", " +
                    DRILL_COLUMN_DRILL_GROUP + ", " +
                    DRILL_COLUMN_CNT + ", " +
                    DRILL_COLUMN_SOUND_WAIT + ", " +
                    DRILL_COLUMN_SOUND + ", " +
                    DRILL_COLUMN_SOUND_DETECT_X_TIMES + ", " +
                    DRILL_COLUMN_TIME_WAIT + ", " +
                    DRILL_COLUMN_TIME + ", " +
                    DRILL_COLUMN_ALTERNATIVE + ", " +
                    DRILL_COLUMN_VARIANT_POSITION + ", " +
                    DRILL_COLUMN_INFLECTION + ", " +
                    DRILL_COLUMN_SHUFFLE + ", " +
                    DRILL_COLUMN_SHUFFLE_MODIFY + ", " +
                    DRILL_COLUMN_ACTIVE + ", " +
                    DRILL_COLUMN_SAVE  + " )" +
                    " VALUES (5, '3 úkroky vpravo', 1,1,0,NULL,0,0,0,0,0,0,1,0,1,0);");
            db.execSQL("INSERT INTO " + TB_NAME_DRILL_IN_SET + "(" +
                    DRILL_IN_SET_COLUMN_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_SET_ID + ", " +
                    DRILL_IN_SET_COLUMN_DRILL_ID + ", " +
                    DRILL_IN_SET_COLUMN_POSITION_IN_SET + ", " +
                    DRILL_IN_SET_COLUMN_IS_PAUSE + ", " +
                    DRILL_IN_SET_COLUMN_PAUSE_TIME + ", " +
                    DRILL_IN_SET_COLUMN_VARIANT_NUM + ", " +
                    DRILL_IN_SET_COLUMN_EDIT_NAME + ", " +
                    DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN +" )" +
                    " VALUES (5, 1, 5, 5, 0, 0, 0, NULL, 0);");

            /** default sound class **/
            db.execSQL("INSERT INTO " + TB_NAME_SND + "(" +
                    SND_COLUMN_ID + ", " +
                    SND_COLUMN_CLASS + ", " +
                    SND_COLUMN_NAME + ", " +
                    SND_COLUMN_ACTIVE + ", " +
                    SND_COLUMN_ADDED + ", " +
                    SND_COLUMN_EXAMPLE  +" )" +
                    " VALUES (1, 1, 'tlesknutí', 1, 0, 'clap_example');");
            db.execSQL("INSERT INTO " + TB_NAME_SND + "(" +
                    SND_COLUMN_ID + ", " +
                    SND_COLUMN_CLASS + ", " +
                    SND_COLUMN_NAME + ", " +
                    SND_COLUMN_ACTIVE + ", " +
                    SND_COLUMN_ADDED + ", " +
                    SND_COLUMN_EXAMPLE  +" )" +
                    " VALUES (2, 2, 'hvízdnutí', 1, 0, 'whistle_example');");
            db.execSQL("INSERT INTO " + TB_NAME_SND + "(" +
                    SND_COLUMN_ID + ", " +
                    SND_COLUMN_CLASS + ", " +
                    SND_COLUMN_NAME + ", " +
                    SND_COLUMN_ACTIVE + ", " +
                    SND_COLUMN_ADDED + ", " +
                    SND_COLUMN_EXAMPLE  +" )" +
                    " VALUES (3, 3, 'odraz balónu', 1, 0, 'ball_example');");
            db.execSQL("INSERT INTO " + TB_NAME_SND + "(" +
                    SND_COLUMN_ID + ", " +
                    SND_COLUMN_CLASS + ", " +
                    SND_COLUMN_NAME + ", " +
                    SND_COLUMN_ACTIVE + ", " +
                    SND_COLUMN_ADDED + ", " +
                    SND_COLUMN_EXAMPLE  +" )" +
                    " VALUES (5, 5, 'výstřel ze sportovní zbraně (brokovnice)', 1, 0, 'gun_example');");
        }

        /** Upgrade DB if the number of old database is less than current DATABASE_VERSION
         * @param db - database
         * @param oldVersion - number of old database version
         * @param newVersion - number of new database version
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            /*db.execSQL("DROP TABLE IF EXISTS "+TB_NAME_GROUP);
            db.execSQL("DROP TABLE IF EXISTS "+TB_NAME_DRILLS_SET);
            db.execSQL("DROP TABLE IF EXISTS "+TB_NAME_DRILL);
            db.execSQL("DROP TABLE IF EXISTS "+TB_NAME_DRILL_IN_SET);
            db.execSQL("DROP TABLE IF EXISTS "+TB_NAME_SET_COMPLETE);
            db.execSQL("DROP TABLE IF EXISTS "+TB_NAME_DRILL_INTERMEDIATE_TIME);*/
            //db.execSQL("DROP TABLE IF EXISTS "+TB_NAME_SND);
            //onCreate(db);
            /*db.execSQL("CREATE TABLE " + TB_NAME_SND + " ("
                    + SND_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + SND_COLUMN_NAME + " TEXT NOT NULL,"
                    + SND_COLUMN_EXAMPLE + " TEXT NOT NULL,"
                    + SND_COLUMN_CLASS + " INTEGER NOT NULL,"
                    + SND_COLUMN_ACTIVE + " INTEGER NOT NULL,"
                    + SND_COLUMN_ADDED + " INTEGER NOT NULL"
                    + ");");
            db.execSQL("INSERT INTO " + TB_NAME_SND + "(" +
                    SND_COLUMN_ID + ", " +
                    SND_COLUMN_CLASS + ", " +
                    SND_COLUMN_NAME + ", " +
                    SND_COLUMN_ACTIVE + ", " +
                    SND_COLUMN_ADDED + ", " +
                    SND_COLUMN_EXAMPLE  +" )" +
                    " VALUES (1, 1, 'tlesknutí', 1, 0, 'clap_example');");
            db.execSQL("INSERT INTO " + TB_NAME_SND + "(" +
                    SND_COLUMN_ID + ", " +
                    SND_COLUMN_CLASS + ", " +
                    SND_COLUMN_NAME + ", " +
                    SND_COLUMN_ACTIVE + ", " +
                    SND_COLUMN_ADDED + ", " +
                    SND_COLUMN_EXAMPLE  +" )" +
                    " VALUES (2, 2, 'hvízdnutí', 1, 0, 'whistle_example');");
            db.execSQL("INSERT INTO " + TB_NAME_SND + "(" +
                    SND_COLUMN_ID + ", " +
                    SND_COLUMN_CLASS + ", " +
                    SND_COLUMN_NAME + ", " +
                    SND_COLUMN_ACTIVE + ", " +
                    SND_COLUMN_ADDED + ", " +
                    SND_COLUMN_EXAMPLE  +" )" +
                    " VALUES (3, 3, 'odraz balónu', 1, 0, 'ball_example');");
            db.execSQL("INSERT INTO " + TB_NAME_SND + "(" +
                    SND_COLUMN_ID + ", " +
                    SND_COLUMN_CLASS + ", " +
                    SND_COLUMN_NAME + ", " +
                    SND_COLUMN_ACTIVE + ", " +
                    SND_COLUMN_ADDED + ", " +
                    SND_COLUMN_EXAMPLE  +" )" +
                    " VALUES (5, 5, 'výstřel ze sportovní zbraně (brokovnice)', 1, 0, 'gun_example');");*/
        }

    }

    public SQLiteDB(Context ctx) {
        openHelper = new DatabaseHelper(ctx);
    }


    /**********************************************************************
     *  Group
     **********************************************************************/


    /**
     * Get all groupes
     * @param active - true - only active groupes | false - all groupes
     * @return Cursor with groupes
     */
    public Cursor getAllGroupes(boolean active) {
        String act = (active ? TB_NAME_GROUP+".active = 1 " : "");
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT "+TB_NAME_GROUP+".id AS gid, "+GROUP_COLUMN_NAME+", "+GROUP_DESCRIPTION+", "+GROUP_COLUMN_ICON+", "+GROUP_COLUMN_COLOR+", COUNT("+ TB_NAME_DRILLS_SET+".id) AS cnt FROM "+
                TB_NAME_GROUP + " LEFT JOIN " +
                TB_NAME_DRILLS_SET + " ON "+ TB_NAME_GROUP + ".id=" + TB_NAME_DRILLS_SET + "."+ DRILLS_SET_COLUMN_GROUP +
                " WHERE "+act+
                " GROUP BY " +TB_NAME_GROUP + ".id";
        return db.rawQuery(rawQuery, null);
    }
    /**
     *Insert new group to DB
     * @param name - name of group
     * @param descriptor - description of group
     * @param icon - selected icon
     * @param color - selected color
     */
    public long insertGroup(String name, String descriptor, Integer icon, Integer color) {
        SQLiteDatabase db = openHelper.getWritableDatabase();
        Integer active = 1;

        ContentValues values = new ContentValues();
        values.put(GROUP_COLUMN_NAME, name);
        values.put(GROUP_DESCRIPTION, descriptor);
        values.put(GROUP_COLUMN_ICON, icon);
        values.put(GROUP_COLUMN_COLOR, color);
        values.put(GROUP_COLUMN_ACTIVE, active);

        long columnId = db.insert(TB_NAME_GROUP, null, values);
        db.close();
        return columnId;
    }

    // get group by id

    /**
     * Get group informations by identifier
     * @param id - group identifier
     * @param active - group is active
     * @return information about one group
     */
    public Cursor getGroupById(int id, boolean active) {
        String act = (active ? TB_NAME_GROUP+".active = 1 AND " : "");
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT "+TB_NAME_GROUP+".id AS gid, "+GROUP_COLUMN_NAME+", "+GROUP_DESCRIPTION+", "+GROUP_COLUMN_ICON+", "+GROUP_COLUMN_COLOR+", COUNT("+ TB_NAME_DRILLS_SET+".id) AS cnt FROM "+
                TB_NAME_GROUP + " LEFT JOIN " +
                TB_NAME_DRILLS_SET + " ON " + TB_NAME_GROUP + ".id=" + TB_NAME_DRILLS_SET + "."+ DRILLS_SET_COLUMN_GROUP +
                " WHERE "+act+TB_NAME_GROUP + ".id = "+id;
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Get active drill sets in group
     * @param id - group id
     * @return number of active drill sets
     */
    public int getActiveSetsInGroup(int id){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT COUNT(id) AS cnt FROM "+TB_NAME_DRILLS_SET+" WHERE "+DRILLS_SET_COLUMN_GROUP+" = "+id+
                " AND "+DRILLS_SET_COLUMN_ACTIVE+"=1";
        Cursor cursor = db.rawQuery(rawQuery, null);
        Integer cnt = 0;
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            cnt = cursor.getInt(cursor.getColumnIndex("cnt"));
        }
        cursor.close();
        db.close();
        return cnt;
    }

    /**
     * Update group
     * @param id - group id
     * @param name - group name
     * @param descriptor - group description
     * @param icon - group icon
     * @param color - group color
     */
    public void updateGroup(int id, String name, String descriptor, Integer icon, Integer color){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(GROUP_COLUMN_NAME, name);
        cv.put(GROUP_DESCRIPTION, descriptor);
        cv.put(GROUP_COLUMN_ICON, icon);
        cv.put(GROUP_COLUMN_COLOR, color);
        db.update(TB_NAME_GROUP, cv, "id = "+id, null);
        db.close();
    }

    /**
     * Delete group by group id
     * @param groupID - group id
     */
    public void deleteGroup(int groupID){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(GROUP_COLUMN_ACTIVE, 0);
        db.update(TB_NAME_GROUP, cv, "id = "+groupID, null);
        db.close();
    }

    /**
     * Delete group with all drills in group
     * @param groupID - group id that be deleted
     */
    public void deleteGroupWithDrills(int groupID){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        // delete drill sets
        Cursor itemFromDB = getDrillSetsByGroupId(groupID);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer dsid = itemFromDB.getInt(itemFromDB.getColumnIndex("dsid"));
            Cursor itemFromDB2 = getAllDrillsFromDrillSet(dsid); //position
            ArrayList<Integer> drillsIdArray = new ArrayList<Integer>();
            ArrayList<Integer> drillInSetIdArray = new ArrayList<Integer>();
            for (itemFromDB2.moveToFirst(); !itemFromDB2.isAfterLast(); itemFromDB2.moveToNext()) {
                drillInSetIdArray.add(itemFromDB2.getInt(itemFromDB2.getColumnIndex("id_drill_in_set")));
                drillsIdArray.add(itemFromDB2.getInt(itemFromDB2.getColumnIndex("drill_id")));
            }
            for(int i = 0; i < drillsIdArray.size(); i++){
                deleteDrill(drillInSetIdArray.get(i), drillsIdArray.get(i));
            }
            deleteDrillSet(dsid);
        }
        // delete group
        deleteGroup(groupID);
        // delete drills
        ContentValues cv = new ContentValues();
        db = openHelper.getWritableDatabase();
        cv.put(DRILL_COLUMN_ACTIVE, 0);
        db.update(TB_NAME_DRILL, cv, "drill_group = "+groupID, null);
        db.close();
    }


    /**********************************************************************
     *  Drill set
     **********************************************************************/

    /**
     * Get all drill sets
     * @return list (cursor) of all drill sets
     */
    public Cursor getAllDrillSets() {
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT "+TB_NAME_DRILLS_SET+".id AS dsid, set_name, set_group, repeat, name, icon, color FROM "+
                TB_NAME_DRILLS_SET + ", " +
                TB_NAME_GROUP + " WHERE " +
                "set_group = "+TB_NAME_GROUP+".id AND "+TB_NAME_DRILLS_SET+".active = 1 AND "+TB_NAME_GROUP+".active = 1";
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Get all drill sets by group id
     * @param gid - group id
     * @return list (cursor) of drill sets in group identified by gid
     */
    public Cursor getDrillSetsByGroupId(int gid) {
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT " + TB_NAME_DRILLS_SET + ".id AS dsid, set_name, set_group, repeat, name, icon, color FROM " +
                TB_NAME_DRILLS_SET + ", " +
                TB_NAME_GROUP + " WHERE " +
                "set_group = " + TB_NAME_GROUP + ".id AND " + TB_NAME_DRILLS_SET + ".active = 1 AND " + TB_NAME_GROUP + ".active = 1" +
                " AND set_group = " + gid;
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Insert new drill set
     * @param name - drill set name
     * @param group - drill set group
     * @param repeat - number of repeat drill set
     * @return
     */
    public long insertDrillSet(String name, Integer group, Integer repeat) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DRILLS_SET_COLUMN_SET_NAME, name);
        values.put(DRILLS_SET_COLUMN_GROUP, group);
        values.put(DRILLS_SET_COLUMN_REPEAT, repeat);
        values.put(DRILLS_SET_COLUMN_ACTIVE, 1);

        long columnId = db.insert(TB_NAME_DRILLS_SET, null, values);
        db.close();
        return columnId;
    }

    /**
     * Get drill set by id
     * @param id - drill set id
     * @return informations about drill set
     */
    public Cursor getDrillSetById(int id) {
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT "+TB_NAME_DRILLS_SET+".id AS dsid, set_name, set_group, repeat, name, icon, color FROM "+
                TB_NAME_DRILLS_SET + ", " +
                TB_NAME_GROUP + " WHERE " +
                "set_group = "+TB_NAME_GROUP+".id " +
                "AND dsid = "+id;
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Update repeat of all set
     * @param id id of drill set
     * @param repeat number of repeating
     */
    public void updateDrillSetRepeat(int id, int repeat){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("repeat", repeat);
        db.update(TB_NAME_DRILLS_SET, cv, "id="+id, null);
        db.close();
        /*String rawQuery = "UPDATE "+TB_NAME_DRILLS_SET+" SET repeat = "+repeat+" WHERE id = "+id;
        db.execSQL(rawQuery);*/
    }

    /**
     * Update drill set
     * @param id - id drill set
     * @param name - new drill set name
     * @param group - change drill set group
     */
    public void updateDrillSet(int id, String name, Integer group){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DRILLS_SET_COLUMN_SET_NAME, name);
        cv.put(DRILLS_SET_COLUMN_GROUP, group);
        db.update(TB_NAME_DRILLS_SET, cv, "id = "+id, null);
        db.close();
    }

    /**
     * Delete drill set
     * @param idDrillSet - id of drill set
     */
    public void deleteDrillSet(int idDrillSet){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DRILLS_SET_COLUMN_ACTIVE, 0);
        db.update(TB_NAME_DRILLS_SET, cv, "id = "+idDrillSet, null);
        db.close();
    }

    /**********************************************************************
     *  Drill
     **********************************************************************/

    /**
     * Insert new drill
     * @param name - original name of drill
     * @param group - id od group
     * @param cnt - drill usage counter
     * @param sndWait - wait to sound (true - wait to sound detection, false - not wait to detection)
     * @param snd - sound class
     * @param xTimes - sound detected x times
     * @param timeWait - wait before go to next drill
     * @param time - wait time before go to next drill in seconds
     * @param alt - is drill varint
     * @param altPosition - variant positon in drill name
     * @param inflection - is drill inflection
     * @param shuffle - is allow drill shuffle
     * @param shuffleModify - is allow drill shuffle modify
     * @param save - is drill save as template
     * @return - new drill id
     */
    public long insertDrill(String name, Integer group, Integer cnt, Integer sndWait, Integer snd, Integer xTimes,
                            Integer timeWait, Integer time, Integer alt, Integer altPosition, Integer inflection, Integer shuffle,
                            Integer shuffleModify, Integer save) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DRILL_COLUMN_DRILL_NAME, name);
        values.put(DRILL_COLUMN_DRILL_GROUP, group);
        values.put(DRILL_COLUMN_CNT, cnt);
        values.put(DRILL_COLUMN_SOUND_WAIT, sndWait);
        values.put(DRILL_COLUMN_SOUND, snd);
        values.put(DRILL_COLUMN_SOUND_DETECT_X_TIMES, xTimes);
        values.put(DRILL_COLUMN_TIME_WAIT, timeWait);
        values.put(DRILL_COLUMN_TIME, time);
        values.put(DRILL_COLUMN_ALTERNATIVE, alt);
        values.put(DRILL_COLUMN_VARIANT_POSITION, altPosition);
        values.put(DRILL_COLUMN_INFLECTION, inflection);
        values.put(DRILL_COLUMN_SAVE, save);
        values.put(DRILL_COLUMN_SHUFFLE, shuffle);
        values.put(DRILL_COLUMN_SHUFFLE_MODIFY, shuffleModify);
        values.put(DRILL_COLUMN_ACTIVE, 1);

        long columnId = db.insert(TB_NAME_DRILL, null, values);
        db.close();
        return columnId;
    }

    /**
     * Get all drill sets by group ids
     * @param groupIDs - list of selected group id
     * @return list (cursor) of drill set
     */
    public Cursor getSavedDrillsByGroupId(ArrayList<Integer> groupIDs){
        String queryIDs, queryIDsEnd;
        if(groupIDs.size() > 1) {
            queryIDs = "AND (";
            queryIDsEnd = ")";
        }
        else {
            queryIDs = "AND ";
            queryIDsEnd = "";
        }
        for(int i = 0; i < groupIDs.size(); i++){
            if(i == 0)
                queryIDs += " drill_group="+groupIDs.get(i)+" ";
            else
                queryIDs += " OR drill_group="+groupIDs.get(i)+" ";
        }
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT "+TB_NAME_DRILL+".id AS id_drill, drill_name, drill_group, alternative, variant_position,"+
                "time_wait, time, inflection, snd, snd_detect_x_times, snd_wait, shuffle, shuffle_modify"+
                " FROM "+TB_NAME_DRILL+
                " WHERE save = 1 "+queryIDs+queryIDsEnd+
                " AND "+TB_NAME_DRILL+".active = 1 ";
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Get only drill info by drill id
     * @param idDrill - drill id
     * @return informations about drill
     */
    public Cursor getOnlyDrillById(int idDrill){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT "+TB_NAME_DRILL+".id as drill_id, drill_name, time_wait, time, alternative, shuffle, shuffle_modify, drill_group, " +
                " variant_position, snd_wait, snd, snd_detect_x_times, inflection  FROM " +
                TB_NAME_DRILL + " WHERE "+TB_NAME_DRILL+".id = "+idDrill+
                " AND ("+TB_NAME_DRILL+".active = 1 )";
        return db.rawQuery(rawQuery, null);
    }


    /**********************************************************************
     *  Drills in set
     **********************************************************************/
    /**
     * get information about drills in set
     * @param id - drill set identificator
     * @return cursor with info about information about drill set identified by id
     */
    public Cursor getAllDrillsFromDrillSet(int id) {
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT " + TB_NAME_DRILL_IN_SET + ".id AS id_drill_in_set, "+TB_NAME_DRILL+".id as drill_id, position, is_pause, pause_time, variant_x_times," +
                " edit_name, drill_name, joint_to_prev_drill, time_wait, time, alternative, shuffle, shuffle_modify, "+TB_NAME_DRILL+".variant_position as variant_position, snd_wait, snd, snd_detect_x_times, inflection   FROM " +
                TB_NAME_DRILL_IN_SET + " LEFT JOIN " +
                TB_NAME_DRILL + " ON " + TB_NAME_DRILL_IN_SET + "." + DRILL_IN_SET_COLUMN_DRILL_ID + " = " + TB_NAME_DRILL + "." + DRILL_COLUMN_ID +
                " WHERE " + DRILL_IN_SET_COLUMN_DRILL_SET_ID + " = "+id+
                " AND ("+TB_NAME_DRILL+".active = 1 OR is_pause = 1 ) "+
                " ORDER BY " + DRILL_IN_SET_COLUMN_POSITION_IN_SET;
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Get last position of drill in set
     * @param id - drill set id
     * @return position for new drill in set
     */
    public Cursor getLastDrillInSetPosition(int id){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT MAX(position) AS max FROM " + TB_NAME_DRILL_IN_SET
                + " WHERE " + DRILL_IN_SET_COLUMN_DRILL_SET_ID + " = "+id;
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Insert exist drill into drill set
     * @param idDrillSet - drill set id
     * @param idDrill - drill id
     * @param positionInSet - drill position in drill set
     * @param isPause - is drill pause
     * @param pauseTime - pause time
     * @param varNum - variant value
     * @param editName - edited drill name
     * @param join - is drill join to prev drill
     * @return - id drill in set
     */
    public long insertDrillIntoSet(Integer idDrillSet, Integer idDrill, Integer positionInSet, Integer isPause, Integer pauseTime,
                                   Integer varNum, String editName, Integer join) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DRILL_IN_SET_COLUMN_DRILL_SET_ID, idDrillSet);
        values.put(DRILL_IN_SET_COLUMN_DRILL_ID, idDrill);
        values.put(DRILL_IN_SET_COLUMN_POSITION_IN_SET, positionInSet);
        values.put(DRILL_IN_SET_COLUMN_IS_PAUSE, isPause);
        values.put(DRILL_IN_SET_COLUMN_PAUSE_TIME, pauseTime);
        values.put(DRILL_IN_SET_COLUMN_VARIANT_NUM, varNum);
        values.put(DRILL_IN_SET_COLUMN_EDIT_NAME, editName);
        values.put(DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN, join);

        long columnId = db.insert(TB_NAME_DRILL_IN_SET, null, values);
        db.close();
        return columnId;
    }

    /**
     * Update drill and drill set name and variant value
     * @param idDrillInSet - id drill in set
     * @param idDrill - drill id
     * @param num - new variant value
     * @param drillName - new drill name
     */
    public void updateDrillInSetVariantNumAndName(int idDrillInSet, int idDrill, int num, String drillName){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv1 = new ContentValues();
        ContentValues cv2 = new ContentValues();
        cv1.put(DRILL_IN_SET_COLUMN_VARIANT_NUM, num);
        cv2.put(DRILL_COLUMN_DRILL_NAME, drillName);
        if(idDrillInSet != -1)
            db.update(TB_NAME_DRILL_IN_SET, cv1, "id = "+idDrillInSet, null);
        db.update(TB_NAME_DRILL, cv2, "id = "+idDrill, null);
        db.close();
    }

    /**
     * Update drill wait time
     * @param idDrill - drill id
     * @param waitTime - new wait time
     */
    public void updateDrillInSetWaitTime(int idDrill, int waitTime){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DRILL_COLUMN_TIME, waitTime);
        db.update(TB_NAME_DRILL, cv, "id = "+idDrill, null);
        db.close();
    }

    /**
     * delete drill in drill set
     * @param idDrillInSet - drill in set id
     * @param idDrill - drill id
     */
    public void deleteDrill(int idDrillInSet, int idDrill){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        db.delete(TB_NAME_DRILL_IN_SET, "id = "+idDrillInSet, null);
        ContentValues cv = new ContentValues();
        cv.put(DRILL_COLUMN_ACTIVE, 0);
        db.update(TB_NAME_DRILL, cv, "id = "+idDrill, null);
        //db.delete(TB_NAME_DRILL, "id = "+idDrill, null);
        db.close();
    }

    /**
     * delete drill in set and drill
     * @param idDrill drill id
     */
    public void deleteDrill(int idDrill){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DRILL_COLUMN_ACTIVE, 0);
        db.update(TB_NAME_DRILL, cv, "id = "+idDrill, null);
        db.close();
    }

    /**
     * Get drill by drill in set id
     * @param idDrillInSet - drill in set id
     * @return
     */
    public Cursor getDrillById(int idDrillInSet){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT " + TB_NAME_DRILL_IN_SET + ".id AS id_drill_in_set, "+TB_NAME_DRILL+".id as drill_id, position, is_pause, pause_time, variant_x_times," +
                " edit_name, drill_name, joint_to_prev_drill, time_wait, time, alternative, shuffle, shuffle_modify, drill_group, "+TB_NAME_DRILL+".variant_position as variant_position, snd_wait, snd, snd_detect_x_times, inflection   FROM " +
                TB_NAME_DRILL_IN_SET + " LEFT JOIN " +
                TB_NAME_DRILL + " ON " + TB_NAME_DRILL_IN_SET + "." + DRILL_IN_SET_COLUMN_DRILL_ID + " = " + TB_NAME_DRILL + "." + DRILL_COLUMN_ID +
                " WHERE "+TB_NAME_DRILL_IN_SET+".id = "+idDrillInSet+
                " AND ("+TB_NAME_DRILL+".active = 1 OR is_pause = 1 )"+
                " ORDER BY " + DRILL_IN_SET_COLUMN_POSITION_IN_SET;
        return db.rawQuery(rawQuery, null);
    }


    /**
     * Store new pause time
     * @param idDrillInSet - drill in set id
     * @param pauseTime - new pause time
     */
    public void editPause(int idDrillInSet, Integer pauseTime){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DRILL_IN_SET_COLUMN_PAUSE_TIME, pauseTime);
        db.update(TB_NAME_DRILL_IN_SET, cv, "id = "+idDrillInSet, null);
        db.close();
    }

    /**
     * Edit drill position in drill set
     * @param idDrillInSet - drill in set id
     * @param position - new drill position
     */
    public void editPosition(int idDrillInSet, int position){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DRILL_IN_SET_COLUMN_POSITION_IN_SET, position);
        db.update(TB_NAME_DRILL_IN_SET, cv, "id = "+idDrillInSet, null);
        db.close();
    }

    /**
     * Update drill in set
     * @param idDrillInSet - id drill in set
     * @param idDrill - drill id
     * @param name - original name of drill
     * @param group - id of group
     * @param sndWait - wait to sound (true - wait to sound detection, false - not wait to detection)
     * @param sndCls - sound class
     * @param timeWait - wait before go to next drill
     * @param time - wait time before go to next drill in seconds
     * @param alternative - is drill varint
     * @param variantPosition - variant positon in drill name
     * @param inflection - is drill inflection
     * @param shuffle - is allow drill shuffle
     * @param shuffleModify - is allow drill shuffle modify
     * @param join - join dril to prev drill
     */
    public void editDrill(int idDrillInSet, int idDrill, String name, Integer sndWait, Integer sndCls, Integer timeWait, Integer time,
                          Integer alternative, Integer inflection, Integer shuffle, Integer shuffleModify, Integer variantPosition,
                          Integer variantXTimes, Integer join, Integer group){

        editDrill(idDrill, name, sndWait, sndCls, timeWait, time, alternative, inflection, shuffle, shuffleModify, variantPosition, group);

        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv2 = new ContentValues();
        cv2.put(DRILL_IN_SET_COLUMN_VARIANT_NUM, variantXTimes);
        cv2.put(DRILL_IN_SET_COLUMN_PREV_DRILL_JOIN, join);
        db.update(TB_NAME_DRILL_IN_SET, cv2, "id = "+idDrillInSet, null);

        db.close();
    }

    /**
     * Update drill
     * @param idDrill - drill id
     * @param name - original name of drill* @param group - id of group
     * @param sndWait - wait to sound (true - wait to sound detection, false - not wait to detection)
     * @param sndCls - sound class
     * @param timeWait - wait before go to next drill
     * @param time - wait time before go to next drill in seconds
     * @param alternative - is drill varint
     * @param variantPosition - variant positon in drill name
     * @param inflection - is drill inflection
     * @param shuffle - is allow drill shuffle
     * @param shuffleModify - is allow drill shuffle modify
     * @param group - id of group
     */
    public void editDrill(int idDrill, String name, Integer sndWait, Integer sndCls, Integer timeWait, Integer time,
                          Integer alternative, Integer inflection, Integer shuffle, Integer shuffleModify, Integer variantPosition,
                          Integer group){
        SQLiteDatabase db = openHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DRILL_COLUMN_DRILL_NAME, name);
        cv.put(DRILL_COLUMN_SOUND_WAIT, sndWait);
        cv.put(DRILL_COLUMN_SOUND, sndCls);
        cv.put(DRILL_COLUMN_TIME_WAIT, timeWait);
        cv.put(DRILL_COLUMN_TIME, time);
        cv.put(DRILL_COLUMN_ALTERNATIVE, alternative);
        cv.put(DRILL_COLUMN_VARIANT_POSITION, variantPosition);
        cv.put(DRILL_COLUMN_INFLECTION, inflection);
        cv.put(DRILL_COLUMN_SHUFFLE, shuffle);
        cv.put(DRILL_COLUMN_SHUFFLE_MODIFY, shuffleModify);
        cv.put(DRILL_COLUMN_DRILL_GROUP, group);
        db.update(TB_NAME_DRILL, cv, "id = "+idDrill, null);

        db.close();
    }


    /**********************************************************************
     *  Set complete table
     **********************************************************************/

    /**
     * Create record about complete drill set
     * @param idSet - drill set id
     * @param finalSetTime - final time of drill set
     * @param dateTime - date and time of drill set execute
     * @param percentage - percentage of drill set complete
     * @param note - note added to complete set
     * @param mode - mode of execute drill set
     * @param end - flag - indicate that drill set complete
     * @return
     */
    public long insertSetComplete(Integer idSet, Integer finalSetTime, String dateTime, Integer percentage, String note, Integer mode,
            Integer end) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SET_COMPLETE_COLUMN_ID_DRILL_SET, idSet);
        values.put(SET_COMPLETE_COLUMN_FINAL_SET_TIME, finalSetTime);
        values.put(SET_COMPLETE_COLUMN_DATE_TIME, dateTime);
        values.put(SET_COMPLETE_COLUMN_PERCENTAGE, percentage);
        values.put(SET_COMPLETE_COLUMN_NOTE, note);
        values.put(SET_COMPLETE_COLUMN_MODE, mode);
        values.put(SET_COMPLETE_COLUMN_END, end);

        long columnId = db.insert(TB_NAME_SET_COMPLETE, null, values);
        db.close();
        return columnId;
    }

    /**
     * Get all complete drill set
     * @return list (cursor) of complete drill set
     */
    public Cursor getAllSetComplete() {
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT " + TB_NAME_SET_COMPLETE + ".id AS complete_id, set_time, date_time, percentage, mode, note," +
                TB_NAME_DRILLS_SET+".set_name as name, set_group FROM " +
                TB_NAME_SET_COMPLETE + ", " + TB_NAME_DRILLS_SET +
                " WHERE " + TB_NAME_SET_COMPLETE + "." + SET_COMPLETE_COLUMN_ID_DRILL_SET + " = " + TB_NAME_DRILLS_SET + "." + DRILLS_SET_COLUMN_ID +
                " AND "+TB_NAME_SET_COMPLETE+".set_end = 1 "+
                " ORDER BY " + TB_NAME_SET_COMPLETE+"."+SET_COMPLETE_COLUMN_ID+" DESC";
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Delete all report abotut complete set and intermediate info
     * @param idSetComplete - id of set complete
     */
    public void deleteSetComplete(int idSetComplete){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        db.delete(TB_NAME_SET_COMPLETE, "id = "+idSetComplete, null);
        db.delete(TB_NAME_DRILL_INTERMEDIATE_TIME, "id_set_complete = "+idSetComplete, null);
        db.close();
    }

    /**
     * Remove unfinished drill set
     */
    public void deleteUnfinishedSetComplete(){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        db.delete(TB_NAME_SET_COMPLETE, "set_end = 0", null);
        db.close();
    }

    /**
     * Set execute drill set as complete
     * @param idSetComplete - complete drill set
     * @param percentage - percentage of drill set complete
     * @param note - note added to complete drill set
     * @param time - final time of drill set
     */
    public void updateSetComplete(int idSetComplete, int percentage, String note, int time){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SET_COMPLETE_COLUMN_FINAL_SET_TIME, time);
        cv.put(SET_COMPLETE_COLUMN_PERCENTAGE, percentage);
        cv.put(SET_COMPLETE_COLUMN_END, 1);
        cv.put(SET_COMPLETE_COLUMN_NOTE, note);
        db.update(TB_NAME_SET_COMPLETE, cv, "id = "+idSetComplete, null);
        db.close();
    }


    /**********************************************************************
     *  Drill intermediate time
     **********************************************************************/
    /**
     * Get all intermediate time in complete drill set
     * @param id - complete drill set id
     * @return all intermediate time in complete drill set
     */
    public Cursor getAllIntermediateTimeById(int id){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT " + TB_NAME_DRILL_INTERMEDIATE_TIME + ".id AS intermediate_id, iteration, position, skipped, note," +
                TB_NAME_DRILL_INTERMEDIATE_TIME+".time as intermediate_time, "+
                TB_NAME_DRILL+".id AS drill_id, drill_name  FROM " +
                TB_NAME_DRILL_INTERMEDIATE_TIME + ", " + TB_NAME_DRILL +
                " WHERE " + TB_NAME_DRILL_INTERMEDIATE_TIME + ".id_drill = "+TB_NAME_DRILL + ".id " +
                " AND "+DRILL_INTERMEDIATE_COLUMN_ID_SET_COMPLETE + " = " + id +
                " ORDER BY " + TB_NAME_DRILL_INTERMEDIATE_TIME+".time ASC";

        return db.rawQuery(rawQuery, null);
    }


    /**
     * Insert new intermediate time to complete drill set
     * @param idSetComplete - complete drill set id
     * @param idDrill - drill id
     * @param time - intermediate time
     * @param detectedCnt - detected counter
     * @param position - drill position in drill set
     * @param iteration - number of iteration
     * @param skipped - indicate that drill is skipped
     * @param note - note to drill
     * @return id of intermediate dril in complete drill set
     */
    public long insertIntermediateTime(Integer idSetComplete, Integer idDrill, Integer time, Integer detectedCnt,
            Integer position, Integer iteration, Integer skipped, String note) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DRILL_INTERMEDIATE_COLUMN_ID_SET_COMPLETE, idSetComplete);
        values.put(DRILL_INTERMEDIATE_COLUMN_ID_DRILL, idDrill);
        values.put(DRILL_INTERMEDIATE_COLUMN_TIME, time);
        values.put(DRILL_INTERMEDIATE_COLUMN_DETECTED_CNT, detectedCnt);
        values.put(DRILL_INTERMEDIATE_COLUMN_POSITION, position);
        values.put(DRILL_INTERMEDIATE_COLUMN_ITERATION, iteration);
        values.put(DRILL_INTERMEDIATE_COLUMN_SKIPPED, skipped);
        values.put(DRILL_INTERMEDIATE_COLUMN_NOTE, note);

        long columnId = db.insert(TB_NAME_DRILL_INTERMEDIATE_TIME, null, values);
        db.close();
        return columnId;
    }

    /**
     * Get number how many times it was the drill set executed
     * @return cursor with records
     */
    public Cursor getDrillSetPopularity() {
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT " + TB_NAME_DRILLS_SET + ".id AS id_drill_set, set_name, COUNT(*) AS popularity" +
                " FROM " + TB_NAME_DRILLS_SET + " LEFT JOIN " +
                TB_NAME_SET_COMPLETE + " ON " + TB_NAME_DRILLS_SET + ".id = " + TB_NAME_SET_COMPLETE + "." + SET_COMPLETE_COLUMN_ID_DRILL_SET +
                " WHERE "+SET_COMPLETE_COLUMN_END+" = 1 "+
                " GROUP BY " + SET_COMPLETE_COLUMN_ID_DRILL_SET +
                " ORDER BY popularity ASC";
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Return all records about execude drill set
     * @param drillSetId - id of drill set
     * @return Cursor of all execuded drill set identified by drillSetId
     */
    public Cursor getBestTimeOfDrillSet(int drillSetId){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT id, " +SET_COMPLETE_COLUMN_FINAL_SET_TIME+
                " FROM " + TB_NAME_SET_COMPLETE + " WHERE "+SET_COMPLETE_COLUMN_END+" = 1 "+
                " AND "+SET_COMPLETE_COLUMN_ID_DRILL_SET+" = "+drillSetId;
        return db.rawQuery(rawQuery, null);
    }



    /**********************************************************************
     *  Sounds
     **********************************************************************/

    /**
     * Get all sound class
     * @return sound class
     */
    public Cursor getAllSnd(){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT id, class, name, example, active, added "+
                " FROM " + TB_NAME_SND + " ORDER BY name";
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Insert new sound class into db
     * @param name - sound class name
     * @param example - sound example file name
     * @param cls - imber of class
     * @return id of inserted snd
     */
    public long insertSnd(String name, String example, int cls, int active, int added) {
        ContentValues values = new ContentValues();
        int maxId = this.getLastSndId();
        if(maxId < SND_FIRST_ADD_ID) {
            values.put(SND_COLUMN_ID, SND_FIRST_ADD_ID);
            values.put(SND_COLUMN_CLASS, SND_FIRST_ADD_ID);
        }
        else {
            values.put(SND_COLUMN_ID, maxId + 1);
            values.put(SND_COLUMN_CLASS, maxId + 1);
        }
        values.put(SND_COLUMN_NAME, name);
        values.put(SND_COLUMN_EXAMPLE, example);
        values.put(SND_COLUMN_ACTIVE, active);
        values.put(SND_COLUMN_ADDED, added);

        SQLiteDatabase db = openHelper.getWritableDatabase();
        long columnId = db.insert(TB_NAME_SND, null, values);
        db.close();
        return columnId;
    }

    /**
     * Set snd as active/deactive
     * @param sndId - snd id
     * @param active - 1 - active | 0 - deactive
     */
    public void setSndActive(int sndId, int active){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SND_COLUMN_ACTIVE, active);
        db.update(TB_NAME_SND, cv, "id = "+sndId, null);
        db.close();
    }

    /**
     * Set new sound class name
     * @param sndId - snd id
     * @param name - new name of sound class
     */
    public void setSndName(int sndId, String name){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SND_COLUMN_NAME, name);
        db.update(TB_NAME_SND, cv, "id = "+sndId, null);
        db.close();
    }

    /**
     * Set new sound class name
     * @param sndId - snd id
     * @param example - new name of sound class
     */
    public void setSndExample(int sndId, String example){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SND_COLUMN_EXAMPLE, example);
        db.update(TB_NAME_SND, cv, "id = "+sndId, null);
        db.close();
    }

    /**
     * Get sound by class
     * @param cls - sound class id
     * @return sound
     */
    public Cursor getSndById(int cls){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT id, class, name, example, active, added "+
                " FROM " + TB_NAME_SND + " WHERE class = "+cls;
        return db.rawQuery(rawQuery, null);
    }

    /**
     * Get last max id in sound table
     * @return max id
     */
    public int getLastSndId(){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT MAX(id) AS max FROM " + TB_NAME_SND;
        Cursor itemFromDB = db.rawQuery(rawQuery, null);
        Integer max = 0;
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            max = itemFromDB.getInt(itemFromDB.getColumnIndex("max"));
        }
        itemFromDB.close();
        db.close();
        return max;
    }

    /**
     * Delete sound class
     * @param id - id of delteting class
     */
    public void deleteSndById(int id){
        SQLiteDatabase db = openHelper.getWritableDatabase();
        db.delete(TB_NAME_SND, "id = "+id, null);
        db.close();
    }

    public HashSet<Integer> getActiveSndClass(){
        SQLiteDatabase db = openHelper.getReadableDatabase();
        String rawQuery = "SELECT class FROM " + TB_NAME_SND + " WHERE active = 1";
        Cursor itemFromDB = db.rawQuery(rawQuery, null);
        HashSet<Integer> clsSet = new HashSet<>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            clsSet.add(itemFromDB.getInt(itemFromDB.getColumnIndex("class")));
        }
        itemFromDB.close();
        db.close();
        return clsSet;
    }

    // close database handeler
    public void close() {
        openHelper.close();
    }
}
