package com.sport.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Jirka on 20.3.2019.
 * Class represents operation with shared preferences
 */
public final class PreferenceStore {
    private static int prefVoiceSpeed, prefVoicePitch, prefDispersionValue, prefAppLoadingTime;
    private static boolean prefSound, prefImmediatelyCounting;

    /**
     * Update class atributes from shared preferences
     * @param ctx - aplication context
     */
    private static void updatePreference(Context ctx){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        prefVoiceSpeed = sharedPrefs.getInt("prefVoiceSpeed", 100);
        prefVoicePitch = sharedPrefs.getInt("prefVoicePitch", 100);
        prefAppLoadingTime = sharedPrefs.getInt("prefAppLoadingTime", 10);
        prefImmediatelyCounting = sharedPrefs.getBoolean("prefImmediatelyCounting", true);
        prefSound = sharedPrefs.getBoolean("prefSound", true);
        prefDispersionValue = Integer.valueOf(sharedPrefs.getString("prefDispersionValue", "5"));
    }

    /**
     * Get shared preference about TTS voice speed
     * @param ctx - aplication context
     * @return - speed of TTS voice
     */
    public static int getTTSSpeed(Context ctx){
        updatePreference(ctx);
        return prefVoiceSpeed;
    }

    /**
     * Get shared preference about TTS voice pitch
     * @param ctx - aplication context
     * @return - pitch of TTS voice
     */
    public static int getTTSPitch(Context ctx){
        updatePreference(ctx);
        return prefVoicePitch;
    }

    /**
     * Get shared preference about immediate counting after rum drill start
     * @param ctx - aplication context
     * @return - true - start counting | false - don't start counting
     */
    public static boolean getImmediatelyCountingStatus(Context ctx){
        updatePreference(ctx);
        return  prefImmediatelyCounting;
    }

    /**
     * Get shared preference about status sound
     * @param ctx - aplication context
     * @return - true - sound on | false - sound off
     */
    public static boolean getSoundStatus(Context ctx){
        updatePreference(ctx);
        return  prefSound;
    }

    /**
     * Get shared preference about dispersion value
     * @param ctx - aplication context
     * @return - dispersion value
     */
    public static int getDispersion(Context ctx){
        updatePreference(ctx);
        return prefDispersionValue;
    }

    /**
     * Get time for loading aplication
     * @param ctx - aplication context
     * @return time in seconds
     */
    public static int getAppLoadingTime(Context ctx){
        updatePreference(ctx);
        return prefAppLoadingTime;
    }

    /**
     * Set shared preference about status sound
     * @param ctx - aplication context
     * @param soundStatus - sound status
     */
    public static void setSoundStatus(Context ctx, boolean soundStatus){
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
        edit.putBoolean("prefSound", soundStatus);
        edit.apply();
    }

    /**
     * Set time for loading aplication
     * @param ctx - aplication context
     * @param time - loading bar time
     */
    public static void setAppLoadingTime(Context ctx, int time){
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
        edit.putInt("prefAppLoadingTime", time);
        edit.apply();
    }
}
