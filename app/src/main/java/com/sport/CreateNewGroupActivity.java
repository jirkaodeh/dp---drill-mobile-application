package com.sport;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.sport.database.SQLiteDB;
import com.sport.lists.Group;
import com.sport.lists.IconSelectorAdapter;
import com.sport.utils.Color;
import com.sport.utils.SportIcon;

import java.util.ArrayList;
import java.util.List;

import petrov.kristiyan.colorpicker.ColorPicker;

/**
 * Created by Jirka on 04.11.2018.
 */
public class CreateNewGroupActivity extends BasicMain {

    private int GROUP_ID = 0;
    private int GROUP_COLOR = 0;
    Button button;
    RelativeLayout colorPalette;
    ImageView iconPallete;
    int selectColor, selectIcon;
    int tmpSelectIcon = -1;
    View prevSelectIcon;
    Group group;
    EditText groupTitle, groupDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_group);
        button =  findViewById(R.id.create_group_button);
        colorPalette = findViewById(R.id.color_palette);
        iconPallete = findViewById(R.id.icon_palette);
        groupTitle = findViewById(R.id.input_group_title);
        groupDescription = findViewById(R.id.input_group_anotation);

        Intent inIntent = getIntent();
        Bundle bundle = inIntent.getExtras();
        // get data from intent
        if(bundle != null) {
            GROUP_ID = (int) bundle.get("GROUP_ID");
            GROUP_COLOR = (int) bundle.get("GROUP_COLOR");
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.edit_group_title));
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(GROUP_COLOR))));
            button.setText(getApplicationContext().getResources().getString(R.string.edit_group));

            SQLiteDB db = new SQLiteDB(getApplicationContext());
            Cursor itemFromDB = db.getGroupById(GROUP_ID, true);
            List<Group> groupsDBArray = new ArrayList<Group>();
            for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
                Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("gid"));
                String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
                Integer cnt = itemFromDB.getInt(itemFromDB.getColumnIndex("cnt"));
                String description = itemFromDB.getString(itemFromDB.getColumnIndex("description"));
                Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
                Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
                group = new Group(id, name, description, icon, color, cnt);
            }
            itemFromDB.close();
            db.close();

            groupTitle.setText(group.getName());
            groupDescription.setText(group.getDescription());
            iconPallete.setImageDrawable(null);
            iconPallete.setBackgroundResource(SportIcon.getGroupIconBlack(group.getIcon()));
            colorPalette.setBackgroundColor(getApplicationContext().getResources().getColor(Color.getColorById(group.getColor())));
            selectColor = group.getColor();
            selectIcon = group.getIcon();
        }
        else{
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(2))));
            selectColor = 2;
            selectIcon = 1;
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.create_group_title));
            button.setText(getApplicationContext().getResources().getString(R.string.create_group));
        }

        colorPalette.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPicker colorPicker = new ColorPicker(CreateNewGroupActivity.this);
                colorPicker.setColors(Color.getColorArray(getApplicationContext()));
                colorPicker.setTitle(getResources().getString(R.string.select_color));
                colorPicker.setDefaultColorButton(1);
                colorPicker.show();
                colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
                    @Override
                    public void onChooseColor(int position, int color) {
                        selectColor = position+1;
                        colorPalette.setBackgroundColor(color);
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(selectColor))));
                        /*Snackbar.make(fullView, position+" "+color, Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();*/
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
        });

        iconPallete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showIconSelectorDialog();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDB db = new SQLiteDB(getApplicationContext());
                // edit
                if(GROUP_ID != 0) {
                    String name = groupTitle.getText().toString();
                    String descriptor = groupDescription.getText().toString();
                    db.updateGroup(GROUP_ID, name, descriptor, selectIcon, selectColor);
                    //Long l = db.insertGroup(null, name, descriptor, 1, selectColor);
                    /*Snackbar.make(fullView, "Edit", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();*/
                }
                // insert
                else{
                    String name = groupTitle.getText().toString();
                    String descriptor = groupDescription.getText().toString();
                    Long l = db.insertGroup(name, descriptor, selectIcon, selectColor);
                    /*Snackbar.make(fullView, "Insert "+l, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();*/

                }
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_back_white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    /**
     * Show Icon selector dialog
     */
    private void showIconSelectorDialog(){
        final GridView gridView = new GridView(this);

        gridView.setAdapter(new IconSelectorAdapter(getApplicationContext(), SportIcon.iconsBlack));
        gridView.setNumColumns(3);
        gridView.setGravity(Gravity.CENTER);
        gridView.setHorizontalSpacing(20);
        gridView.setVerticalSpacing(20);
        gridView.setStretchMode(GridView.STRETCH_SPACING_UNIFORM);
        gridView.setScrollContainer(true);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(tmpSelectIcon != -1)
                    prevSelectIcon.setBackgroundColor(getResources().getColor(R.color.white));
                prevSelectIcon = view;
                tmpSelectIcon = position + 1;
                view.setBackgroundColor(getResources().getColor(R.color.iconSelector));
            }
        });

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(gridView);
        builder.setTitle(getResources().getString(R.string.select_icon));
        builder.setNegativeButton(getResources().getText(R.string.back_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getResources().getText(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(tmpSelectIcon != -1){
                    selectIcon = tmpSelectIcon;
                    tmpSelectIcon = -1;
                    iconPallete.setImageDrawable(null);
                    iconPallete.setBackgroundResource(SportIcon.getGroupIconBlack(selectIcon));
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


}
