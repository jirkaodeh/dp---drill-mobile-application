package com.sport;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.sport.database.SQLiteDB;
import com.sport.lists.Drill;
import com.sport.lists.DrillInSet;
import com.sport.lists.Group;
import com.sport.lists.Snd;
import com.sport.lists.SndAdapter;
import com.sport.utils.Color;
import com.sport.utils.CreateDrillUtils;

import java.util.ArrayList;
import java.util.List;

public class DrillOverviewNewDrillActivity extends BasicMain {

    private Button createBtn;
    private Switch joinSwitch, waitSwitch, variableSwitch, sndXTimesSwitch, saveDrillSwitch, inflectionSwitch, shuffleSwitch;
    private Switch shuffleValueChangeSwitch;
    private EditText drillNameText, waitTimeText;
    private LinearLayout waitTimeBox, waitBoxTime, waitBoxSnd, variableBox, shuffleBox;
    private ImageButton positionInfoButton, changesndButton;
    private RadioButton radioTime, radioSnd;
    private Spinner groupSpinner, positionSpinner;
    private TextView warningTV, selectedSndTv;
    private int DRILL_ID, COLOR;
    private List<Integer> intAlternativePositionList;
    private DrillOverviewNewDrillActivity act;
    List<Group> groupesList;
    private int selectedGroup = 1;
    private int selectedPosition;
    private List<String> groupesStringList = new ArrayList<String>();
    private boolean wait = false;
    private int sndCls = -1;
    private boolean sndSelect = false;
    private boolean variable = false;
    private boolean inflection = false;
    private AlertDialog dialog;
    private Drill editDrill = null;
    private Group editDrillGroup = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drill_overview_new_drill);
        act = this;

        Intent inIntent = getIntent();
        Bundle bundle = inIntent.getExtras();
        if(bundle != null) {
            try{
                DRILL_ID = (int) bundle.get("DRILL_ID");
                COLOR = (int) bundle.get("COLOR");
            }catch (Exception e){
                COLOR = -1;
                DRILL_ID = -1;
            }
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.edit_drill_set_title));
            if(COLOR != -1) {
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(COLOR))));
            }
        }

        createBtn = (Button) findViewById(R.id.create_drill_button);
        waitSwitch = (Switch) findViewById(R.id.switch_wait);
        variableSwitch = (Switch) findViewById(R.id.switch_variable);
        inflectionSwitch = (Switch)findViewById(R.id.switch_position_inflect);
        drillNameText = (EditText) findViewById(R.id.input_drill_name);
        waitTimeBox = (LinearLayout) findViewById(R.id.input_wait_time_box);
        waitBoxSnd = (LinearLayout) findViewById(R.id.input_wait_time_box_snd);
        waitBoxTime = (LinearLayout) findViewById(R.id.input_wait_time_box_time);
        variableBox = (LinearLayout) findViewById(R.id.switch_variable_box);
        waitTimeText = (EditText) findViewById(R.id.input_wait_time);
        positionInfoButton = (ImageButton) findViewById((R.id.position_info_button));
        radioSnd = (RadioButton) findViewById(R.id.radio_snd);
        radioTime = (RadioButton) findViewById(R.id.radio_time);
        groupSpinner = (Spinner) findViewById(R.id.select_group);
        positionSpinner = (Spinner) findViewById(R.id.select_editable_position);
        warningTV = (TextView) findViewById(R.id.warning);
        selectedSndTv = (TextView) findViewById(R.id.select_snd);
        changesndButton = (ImageButton) findViewById(R.id.snd_change);

        // edit - set stored info
        if(DRILL_ID != -1){
            editDrill();
        }

        // create new drill
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean warning = false;
                // no write pause time
                String drillNameTextStr = drillNameText.getText().toString();
                if(drillNameTextStr.equals("")){
                    warningTV.setText(getResources().getString(R.string.warning_create_drill_drill_name));
                    warningTV.setVisibility(View.VISIBLE);
                    warning = true;
                }
                else{
                    // get group id
                    int cnt = 1, groupId = 1;
                    for (Group g : groupesList) {
                        if (selectedGroup == cnt){
                            groupId = g.getgId();
                            break;
                        }
                        cnt++;
                    }
                    // selected position
                    int positionInsert = 0;
                    int variableInsert = 0;
                    int variableValue = 0;
                    if(variable && intAlternativePositionList.size() > 0){
                        variableInsert = 1;
                        positionInsert = selectedPosition;
                        if(variableInsert == 1 && selectedPosition <= 0){
                            positionInsert = Integer.valueOf(positionSpinner.getSelectedItem().toString());
                        }
                        String[] parts = drillNameTextStr.split(" ");
                        if(parts.length > positionInsert)
                            variableValue = Integer.valueOf(parts[positionInsert-1]);
                    }
                    // wait time
                    int waitInsert = 0;
                    int waitTimeInsert = 0;
                    int sndInsert = 0;
                    int sndSoundInsert = 0;
                    String waitTimeStr = waitTimeText.getText().toString();
                    if(wait && radioTime.isChecked()){
                        waitInsert = 1;
                        if(waitTimeStr.equals("")){
                            warningTV.setText(getResources().getString(R.string.warning_create_drill_wait_time));
                            warningTV.setVisibility(View.VISIBLE);
                            warning = true;
                        }
                        else{
                            waitTimeInsert = Integer.parseInt(waitTimeText.getText().toString());
                            warningTV.setVisibility(View.GONE);
                        }
                    }
                    // wait snd
                    else if(waitSwitch.isChecked() && radioSnd.isChecked()){
                        if(sndCls == -1){
                            warningTV.setText(getResources().getString(R.string.warning_create_drill_wait_time));
                            warningTV.setVisibility(View.VISIBLE);
                            warning = true;
                        }
                        else{
                            sndSoundInsert = sndCls;
                        }
                        sndInsert = 1;
                    }
                    //inflection
                    int inflectionInt = (inflection ? 1 : 0);
                    if(!warning) {
                        // edit
                        if(DRILL_ID != -1){
                            SQLiteDB db = new SQLiteDB(getApplicationContext());
                            if(editDrill != null) {
                                db.editDrill(editDrill.getIdDrill(), drillNameTextStr, sndInsert, sndSoundInsert,
                                        waitInsert, waitTimeInsert, variableInsert, inflectionInt, 0, 0, positionInsert,
                                        groupId);
                            }
                            db.close();
                            finish();
                        }
                        // create
                        else {
                            SQLiteDB db = new SQLiteDB(getApplicationContext());
                            db.insertDrill(drillNameTextStr, groupId, 1, sndInsert, sndSoundInsert, 0,
                                        waitInsert, waitTimeInsert, variableInsert, positionInsert, inflectionInt, 0, 0, 1);
                            db.close();
                            finish();
                        }
                    }
                }
            }
        });

        // set wait - show wait edit text
        waitSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wait = isChecked;
                if(isChecked){
                    waitTimeBox.setVisibility(View.VISIBLE);
                }
                else{
                    waitTimeBox.setVisibility(View.GONE);
                }
            }
        });

        inflectionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //TODO check word after variant number
                inflection = isChecked;
            }
        });

        // drill is fast editable
        variableSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setVariable(isChecked);
                if(isChecked){
                    variableBox.setVisibility(View.VISIBLE);
                }
                else{
                    variableBox.setVisibility(View.GONE);
                }
            }
        });

        // click on info button in position spinner
        positionInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateDrillUtils.positionInfoDialog(act);
            }
        });

        // checked wait for time
        radioTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((RadioButton) v).isChecked();
                if(checked){
                    waitBoxTime.setVisibility(View.VISIBLE);
                    waitBoxSnd.setVisibility(View.GONE);
                    sndSelect = false;
                }
                else{
                    waitBoxTime.setVisibility(View.GONE);
                    waitBoxSnd.setVisibility(View.GONE);
                }
            }
        });

        // checked wait for snd
        radioSnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((RadioButton) v).isChecked();
                if(checked){
                    waitBoxTime.setVisibility(View.GONE);
                    waitBoxSnd.setVisibility(View.VISIBLE);
                    dialogSelectSound();
                    sndSelect = true;
                }
                else{
                    waitBoxTime.setVisibility(View.GONE);
                    waitBoxSnd.setVisibility(View.GONE);
                }
            }
        });

        changesndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectSound();
                sndSelect = true;
            }
        });

        // select group
        groupesList = CreateDrillUtils.getListOfGroupesObject(this);
        for (Group g : groupesList) {
            groupesStringList.add(g.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(act, android.R.layout.simple_spinner_dropdown_item, groupesStringList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(adapter);
        // edit group
        if(editDrillGroup != null && DRILL_ID != -1){
            Log.i("test", editDrillGroup.getName()+" "+DRILL_ID);
            for (int i = 0; i < groupesStringList.size(); i++) {
                if(groupesStringList.get(i).compareTo(editDrillGroup.getName()) == 0){
                    groupSpinner.setSelection(i);
                    selectedGroup = i+1;
                }
            }
        }
        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedGroup = (int)id+1;
                //Log.v("item "+selectedGroup, (String)parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        positionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedPosition = (int)id;
                //Log.v("position "+selectedPosition, (String)parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // prepare position by parse dril name (find num in words)
        drillNameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                intAlternativePositionList = CreateDrillUtils.splitString(s.toString());
                ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(act, android.R.layout.simple_spinner_dropdown_item, intAlternativePositionList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                positionSpinner.setAdapter(adapter);
            }
        });
        drillNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                    CreateDrillUtils.hideKeyboard(v, act);
            }
        });
        waitTimeText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                    CreateDrillUtils.hideKeyboard(v, act);
            }
        });
    }

    private void setVariable(boolean var){
        this.variable = var;
    }

    private void dialogSelectSound(){
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
        final LayoutInflater inflater = (LayoutInflater) this.getSystemService(this.LAYOUT_INFLATER_SERVICE);

        ArrayList<Snd> sndArr = Snd.getAllSndData(this, false);

        final View Viewlayout = inflater.inflate(R.layout.dialog_select_snd,
                (ViewGroup) findViewById(R.id.layout_dialog));
        View includedLayout = Viewlayout.findViewById(R.id.id_drill_list);
        ListView lv = (ListView) includedLayout.findViewById(R.id.list);
        lv.setAdapter(new SndAdapter(this, R.layout.list_item_select_snd, sndArr, this));

        popDialog.setView(Viewlayout);

        popDialog.setPositiveButton(getResources().getString(R.string.back_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        dialog = popDialog.create();
        // listener for close dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(act.getResources().getColor(R.color.colorAccent));
            }
        });
        dialog.show();
    }

    public void setSnd(int cls, String sndName){
        sndCls = cls;
        selectedSndTv.setText(getResources().getString(R.string.snd_select_text)+" "+sndName);
        if(dialog != null)
            dialog.dismiss();
    }



    private void editDrill(){
        SQLiteDB db = new SQLiteDB(this);
        Cursor itemFromDB = db.getOnlyDrillById(DRILL_ID);
        Integer groupId = 0;
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer drillId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_id"));
            String drillName = itemFromDB.getString(itemFromDB.getColumnIndex("drill_name"));

            Boolean timeWait = itemFromDB.getInt(itemFromDB.getColumnIndex("time_wait")) == 1;
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("time"));
            Boolean alternative = itemFromDB.getInt(itemFromDB.getColumnIndex("alternative")) == 1;
            Integer varPosition = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_position"));
            Boolean sndWait = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_wait")) == 1;
            Integer snd = itemFromDB.getInt(itemFromDB.getColumnIndex("snd"));
            Boolean inflection = itemFromDB.getInt(itemFromDB.getColumnIndex("inflection")) == 1;
            Boolean shuffle = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle")) == 1;
            Boolean shuffleModify = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle_modify")) == 1;
            groupId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_group"));

            editDrill = new Drill(drillId, drillName, groupId, "", alternative, varPosition,
                    timeWait, time, sndWait, snd, inflection, shuffle, shuffleModify);
        }
        itemFromDB = db.getGroupById(groupId,true);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("gid"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer cnt = itemFromDB.getInt(itemFromDB.getColumnIndex("cnt"));
            String description = itemFromDB.getString(itemFromDB.getColumnIndex("description"));
            Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
            Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
            editDrillGroup = new Group(id, name, description, icon, color, cnt);
        }
        db.close();
        // set stored info
        if(editDrill != null){
            drillNameText.setText(editDrill.getDrillName());
            variableSwitch.setChecked(editDrill.isAlternative());
            variableBox.setVisibility((editDrill.isAlternative() ? View.VISIBLE : View.GONE));
            if(editDrill.isAlternative()) {
                inflectionSwitch.setChecked(editDrill.isInflection());
                setVariable(editDrill.isAlternative());
                intAlternativePositionList = CreateDrillUtils.splitString(editDrill.getDrillName().toString());
                ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_dropdown_item, intAlternativePositionList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                positionSpinner.setAdapter(adapter);
                for(int i = 0; i < intAlternativePositionList.size(); i++){
                    if(editDrill.getVariantPosition() == intAlternativePositionList.get(i)){
                        positionSpinner.setSelection(i);
                        selectedPosition = i;
                    }
                }

                inflection = editDrill.isInflection();
            }
            /*shuffleSwitch.setChecked(editDrill.isShuffle());
            shuffleBox.setVisibility((editDrill.isShuffle() ? View.VISIBLE : View.GONE));
            if(editDrill.isShuffle() && editDrill.isAlternative()) {
                shuffleValueChangeSwitch.setChecked(editDrill.isShuffleModify());
                shuffleModifyValue = editDrill.isShuffleModify();
                shuffle = true;
            }
            else
            if(!editDrill.isAlternative()){
                shuffleValueChangeSwitch.setEnabled(false);
            }
            joinSwitch.setChecked(editDrill.isJointToPrevDrill());
            join = editDrill.isJointToPrevDrill();*/
            if(editDrill.isTimeWait() || editDrill.isSndWait()){
                wait = true;
                waitSwitch.setChecked(true);
                waitTimeBox.setVisibility(View.VISIBLE);
                if(editDrill.isTimeWait()){
                    waitBoxTime.setVisibility(View.VISIBLE);
                    radioTime.setChecked(true);
                    waitTimeText.setText(editDrill.getTime()+"");
                }
                else if(editDrill.isSndWait()){
                    radioSnd.setChecked(true);
                    waitBoxSnd.setVisibility(View.VISIBLE);
                    sndSelect = true;
                    Snd snd = editDrill.getSndInfo(this);
                    sndCls = snd.getCls();
                    selectedSndTv.setText(getResources().getString(R.string.snd_select_text)+" "+snd.getName());
                }
            }

            createBtn.setText("Editovat cvik");
        }
    }
}
