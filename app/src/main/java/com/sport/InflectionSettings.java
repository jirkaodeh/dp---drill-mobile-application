package com.sport;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.sport.database.InflectionDB;
import com.sport.database.SQLiteDB;
import com.sport.lists.Group;
import com.sport.lists.Inflection;
import com.sport.lists.InflectionAdapter;
import com.sport.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class InflectionSettings extends BasicMain {

    private View includedLayout;
    private ListView lv;
    private TextView emptyList;
    private FloatingActionButton fab;
    private InflectionAdapter adapter;
    private List<Inflection> inflectionData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inflection_settings);
        super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.inflection_settings_title));

        includedLayout = findViewById(R.id.id_inflection_list);
        lv = (ListView) includedLayout.findViewById(R.id.list);
        emptyList = (TextView) includedLayout.findViewById(R.id.empty_list);

        getData();
        updateList();
        final InflectionSettings act = this;
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InflectionAdapter.editInflectionnDialog(act, null, true, act);
            }
        });

        // edit toolbaru
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    /**
     * Get inflection data
     */
    public void getData(){
        InflectionDB db = new InflectionDB(this);
        Cursor itemFromDB = db.getAllInflection(true);
        inflectionData = new ArrayList<Inflection>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            String[] item = new String[4];
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id"));
            String name1 = itemFromDB.getString(itemFromDB.getColumnIndex("name1"));
            String name24 = itemFromDB.getString(itemFromDB.getColumnIndex("name24"));
            String name5 = itemFromDB.getString(itemFromDB.getColumnIndex("name5"));
            Inflection i = new Inflection(id, name1, name24, name5, true);
            inflectionData.add(i);
        }
        itemFromDB.close();
        db.close();
    }

    public void updateList(){
        if (inflectionData.size() > 0) {
            adapter = new InflectionAdapter(this, R.layout.list_item_inflection_edit, inflectionData, this);
            lv.setAdapter(adapter);
            ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);
        }
        else{
            emptyList.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
