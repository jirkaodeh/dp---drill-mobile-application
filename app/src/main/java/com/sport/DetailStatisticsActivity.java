package com.sport;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sport.database.SQLiteDB;
import com.sport.lists.Drill;
import com.sport.lists.DrillOverviewAdapter;
import com.sport.lists.IntermediateDrill;
import com.sport.lists.IntermediateDrillAdapter;
import com.sport.utils.Color;
import com.sport.utils.ListUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DetailStatisticsActivity extends BasicMain {

    private int DRILL_SET_COMPLETE_ID, DRILL_SET_COMPLETE_COLOR, DRILL_SET_COMPLETE_GROUP_ID;
    private int DRILL_SET_COMPLETE_PERCENTAGE;
    private long DRILL_SET_COMPLETE_ELAPSED_TIME;
    private Date DRILL_SET_COMPLETE_DATE;
    private String DRILL_SET_COMPLETE_NAME, DRILL_SET_COMPLETE_GROUP_NAME, DRILL_SET_COMPLETE_GROUP;
    private String DRILL_SET_COMPLETE_NOTE, DRILL_SET_COMPLETE_MODE;
    private View includedLayout;
    private ListView lv;
    private LinearLayout detailBox;
    private ImageButton moreLessBtn;
    private ArrayList<IntermediateDrill> drillArray;
    private IntermediateDrillAdapter adapter;
    private int iterationMax = 0;
    private int show = 1;
    private int height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_statistics);

        LinearLayout titleBar = (LinearLayout)findViewById(R.id.title_bar);
        includedLayout = findViewById(R.id.id_drill_list);
        lv = (ListView) includedLayout.findViewById(R.id.list);

        TextView titleTv = (TextView)findViewById(R.id.title_text);
        TextView timeTv = (TextView) findViewById(R.id.time);
        TextView percentageTv = (TextView) findViewById(R.id.percentage);
        TextView drillsTv = (TextView) findViewById(R.id.drills);
        TextView repeatTv = (TextView) findViewById(R.id.repeat);
        TextView dateTv = (TextView) findViewById(R.id.date);
        TextView modeTv = (TextView) findViewById(R.id.finish_mode);
        TextView intermediateText = (TextView) findViewById(R.id.intermediateText);
        LinearLayout noteBox = (LinearLayout) findViewById(R.id.note_box);
        TextView noteTV = (TextView) findViewById(R.id.note);

        detailBox = (LinearLayout) findViewById(R.id.detail_box);
        moreLessBtn = (ImageButton) findViewById(R.id.button_less_more);

        final Intent inIntent = getIntent();
        Bundle bundle = inIntent.getExtras();
        if(bundle != null) {
            DRILL_SET_COMPLETE_ID = (int) bundle.get("DRILL_SET_COMPLETE_ID");
            DRILL_SET_COMPLETE_NAME = (String) bundle.get("DRILL_SET_COMPLETE_NAME");
            DRILL_SET_COMPLETE_GROUP_NAME = (String) bundle.get("DRILL_SET_COMPLETE_GROUP_NAME");
            DRILL_SET_COMPLETE_GROUP_ID = (int) bundle.get("DRILL_SET_COMPLETE_GROUP_ID");
            DRILL_SET_COMPLETE_COLOR = (int) bundle.get("DRILL_SET_COMPLETE_COLOR");
            DRILL_SET_COMPLETE_PERCENTAGE = (int) bundle.get("DRILL_SET_COMPLETE_PERCENTAGE");
            DRILL_SET_COMPLETE_ELAPSED_TIME = (long) bundle.get("DRILL_SET_COMPLETE_ELAPSED_TIME");
            DRILL_SET_COMPLETE_DATE = (Date) bundle.get("DRILL_SET_COMPLETE_DATE");
            DRILL_SET_COMPLETE_NOTE = (String) bundle.get("DRILL_SET_COMPLETE_NOTE");
            DRILL_SET_COMPLETE_MODE = (String) bundle.get("DRILL_SET_COMPLETE_MODE");
            super.changeToolbarTitle("");

            getData();
            titleTv.setText(""+DRILL_SET_COMPLETE_NAME);
            titleBar.setBackgroundColor(getResources().getColor(Color.getColorById(DRILL_SET_COMPLETE_COLOR)));
            intermediateText.setBackgroundColor(getResources().getColor(Color.getColorById(DRILL_SET_COMPLETE_COLOR)));
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(DRILL_SET_COMPLETE_COLOR))));
            percentageTv.setText(DRILL_SET_COMPLETE_PERCENTAGE+"%");
            timeTv.setText(FinishActivity.getTime(DRILL_SET_COMPLETE_ELAPSED_TIME));
            repeatTv.setText(""+iterationMax);
            SimpleDateFormat format = new SimpleDateFormat("dd.MM (HH:mm)");
            dateTv.setText(format.format(DRILL_SET_COMPLETE_DATE));
            drillsTv.setText(""+drillArray.size());
            modeTv.setText(DRILL_SET_COMPLETE_MODE);
            if(DRILL_SET_COMPLETE_NOTE.isEmpty()){
                noteBox.setVisibility(View.GONE);
            }
            else {
                noteBox.setVisibility(View.VISIBLE);
                noteTV.setText(DRILL_SET_COMPLETE_NOTE);
            }
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        moreLessBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHideDetails();
                if(show == 0)
                    moreLessBtn.setImageResource(R.drawable.ic_more_white);
                else
                    moreLessBtn.setImageResource(R.drawable.ic_less_white);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void getData(){
        drillArray = new ArrayList<IntermediateDrill>();
        SQLiteDB db = new SQLiteDB(this);
        Cursor itemFromDB = db.getAllIntermediateTimeById(DRILL_SET_COMPLETE_ID);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("intermediate_id"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("drill_name"));
            Integer idDrill = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_id"));
            Boolean skipped = itemFromDB.getInt(itemFromDB.getColumnIndex("skipped")) == 1;
            Integer iteration = itemFromDB.getInt(itemFromDB.getColumnIndex("iteration"));
            String note = itemFromDB.getString(itemFromDB.getColumnIndex("note"));
            Integer position = itemFromDB.getInt(itemFromDB.getColumnIndex("position"));
            Integer intermediateTime = itemFromDB.getInt(itemFromDB.getColumnIndex("intermediate_time"));
            drillArray.add(new IntermediateDrill(id, idDrill, DRILL_SET_COMPLETE_ID, name, iteration, position, skipped, intermediateTime,
                    note));
            if(iteration > iterationMax)
                iterationMax = iteration;
        }
        itemFromDB.close();
        db.close();
        TextView emptyList = (TextView) findViewById(R.id.empty_list);
        if (drillArray.size() > 0) {
            adapter = new IntermediateDrillAdapter(this, R.layout.list_item_drill_overview, drillArray);
            lv.setAdapter(adapter);
            ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);
        } else {
            lv.setVisibility(View.GONE);
            emptyList.setVisibility(View.VISIBLE);
        }
    }

    private void showHideDetails(){
        if(show == 1){
            show = 0;
            if(android.os.Build.VERSION.SDK_INT > 11) {
                height = detailBox.getHeight();
                ValueAnimator slideAnimator = ValueAnimator.ofInt(height, 0).setDuration(300);
                slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        Integer value = null;
                        if(android.os.Build.VERSION.SDK_INT > 11)
                            value = (Integer) animation.getAnimatedValue();
                        detailBox.getLayoutParams().height = value.intValue();
                        detailBox.requestLayout();
                    }
                });
                AnimatorSet set = new AnimatorSet();
                set.play(slideAnimator);
                set.setInterpolator(new AccelerateDecelerateInterpolator());
                set.start();
            }
            else {
                detailBox.setVisibility(View.GONE);
            }
        }
        else{
            if(android.os.Build.VERSION.SDK_INT > 11) {
                ValueAnimator slideAnimator = ValueAnimator.ofInt(0, height).setDuration(300);
                slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        Integer value = null;
                        if(android.os.Build.VERSION.SDK_INT > 11)
                            value = (Integer) animation.getAnimatedValue();
                        detailBox.getLayoutParams().height = value.intValue();
                        detailBox.requestLayout();
                    }
                });
                AnimatorSet set = new AnimatorSet();
                set.play(slideAnimator);
                set.setInterpolator(new AccelerateDecelerateInterpolator());
                set.start();
            }
            else {
                detailBox.setVisibility(View.VISIBLE);
            }
            show = 1;
        }
    }
}
