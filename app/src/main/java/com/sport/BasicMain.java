package com.sport;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.sport.R;
import com.sport.utils.HelpDialogs;

public class BasicMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout fullView;
    Toolbar toolbar;

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }*/

    @Override
    public void setContentView(int layoutResID) {
        fullView = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_main, null);
        FrameLayout activityContainer = (FrameLayout) fullView.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullView);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    // zavreni menu tlacitkem zpet
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(getApplicationContext(), SettingActivity.class);
            startActivityForResult(i, 1);
        }
        else if (id == R.id.action_info){
            if(this.getClass().getSimpleName().equals("DrillSetListActivity")){
                HelpDialogs.drillListHelpDialog(this, this);
            }
            else if(this.getClass().getSimpleName().equals("MainActivity") || this.getClass().getSimpleName().equals("GroupDrillSetsActivity") ||
                    this.getClass().getSimpleName().equals("CreateNewGroupActivity") || this.getClass().getSimpleName().equals("CreateNewDrillSetActivity")){
                HelpDialogs.drillSetAndGroupHelpDialog(this, this);
            }
            else if(this.getClass().getSimpleName().equals("RunDrillActivity") || this.getClass().getSimpleName().equals("FinishActivity")){
                HelpDialogs.runDrillSetHelpDialog(this, this);
            }
            else if(this.getClass().getSimpleName().equals("CreateNewDrillActivity") || this.getClass().getSimpleName().equals("DrillOverviewNewDrillActivity")){
                HelpDialogs.createNewDrillHelpDialog(this, this);
            }
            else if(this.getClass().getSimpleName().equals("CreateNewDrillSetActivity") || this.getClass().getSimpleName().equals("CreateNewGroupActivity")){
                HelpDialogs.drillSetAndGroupHelpDialog(this, this);
            }
            else if(this.getClass().getSimpleName().equals("DrillOverviewActivity")){
                HelpDialogs.drillOverviewHelpDialog(this, this);
            }
            else if(this.getClass().getSimpleName().equals("GlobalStatisticsActivity") || this.getClass().getSimpleName().equals("DetailStatisticsActivity")){
                HelpDialogs.statisticsHelpDialog(this, this);
            }
            else if(this.getClass().getSimpleName().equals("SoundCreateEditClassActivity")){
                HelpDialogs.soundClassRecordHelpDialog(this, this);
            }
            else if(this.getClass().getSimpleName().equals("SoundOverviewActivity")){
                HelpDialogs.soundClassOverviewHelpDialog(this, this);
            }
            else {

            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Akce proveene pri stisknuti prislusne polozky v menu
     * @param item polozka menu
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.nav_group && !this.getClass().getSimpleName().equals("MainActivity")) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_drills_overview && !this.getClass().getSimpleName().equals("DrillOverviewActivity")) {
            Intent intent = new Intent(getApplicationContext(), DrillOverviewActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_statistics && !this.getClass().getSimpleName().equals("GlobalStatisticsActivity")) {
            Intent intent = new Intent(getApplicationContext(), GlobalStatisticsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent i = new Intent(getApplicationContext(), SettingActivity.class);
            startActivityForResult(i, 1);
        } else if (id == R.id.nav_sound_settings) {
            Intent intent = new Intent(getApplicationContext(), SoundOverviewActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_about) {
            HelpDialogs.aboutAplicationDialog(this, this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeToolbarTitle(String title){
        this.toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

}
