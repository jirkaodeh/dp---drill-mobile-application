package com.sport.sound;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.sport.database.SQLiteDB;
import com.sport.lists.DrillInSet;
import com.sport.lists.Feature;
import com.sport.utils.Permission;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Class for duplicate feature file for add new fetures
 */
public class FeatureFile {

    public static ArrayList<Feature> featureArrays = new ArrayList<Feature>();
    private static String dirName = ".voicesport";
    private static String fetureFileName = "features.txt";
    public static boolean isModified = false;

    /**
     * Constructor
     * @param ctx - aplication context
     */
    public FeatureFile(Context ctx){
        Permission.permisionForReadExternalStorage(ctx);
        if(featureArrays.size() <= 0)
            featuresInit(ctx);
    }

    /**
     * Load features from internal or external storage
     * @param ctx - aplication context
     */
    public static void featuresInit(Context ctx){
        featureArrays.clear();
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(filepath,dirName);
        File feat = new File(dir.getAbsolutePath(),fetureFileName);
        Object[] o;
        if (feat.exists()) {
            o = loadFromFileExternalStore(ctx);
        }
        else{
            o = Store.loadFromFileInternalStore(ctx);
        }
        ArrayList<double[]> freatures = (ArrayList<double[]>) o[0];
        double[] refCls = (double[]) o[1];
        for(int i = 0; i < freatures.size(); i++){
            featureArrays.add(new Feature(refCls[i], freatures.get(i)));
        }
    }

    /**
     * Save feature vector to file
     * @param v - feature vector to save
     */
    public void addFeatureVectorToFile(Context ctx, String v){
        isModified = true;
        Permission.permisionForWriteExternalStorage(ctx);
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(filepath,dirName);
        File file = new File(dir.getAbsolutePath(),fetureFileName);
        if (file.exists()) {
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file, true);
                OutputStreamWriter outputWriter = new OutputStreamWriter(out);
                outputWriter.append(v+"\n");
                outputWriter.flush();
                out.flush();
                outputWriter.close();
                out.close();
            }
            catch (IOException e){
                Log.d("ERROR:", "Write to file "+fetureFileName);
            }
        }
        else {
            Log.d("ERROR:", "File "+fetureFileName+" doesn't exist!");
        }
        featuresInit(ctx);
    }

    /**
     * Remove feature vector by sound class
     * @param ctx - aplication context
     * @param cls - sound class identifief
     */
    public static void removeFeatureFromFile(Context ctx, int cls){
        isModified = true;
        if(featureArrays.size() <= 0)
            featuresInit(ctx);
        ArrayList<Integer> indexArr = new ArrayList<>();
        for(int i = 0; i < featureArrays.size(); i++){
            if(cls == (int)featureArrays.get(i).getCls()){
                indexArr.add(i);
            }
        }
        while(indexArr.size() != 0){
            featureArrays.remove((int)(indexArr.get((indexArr.size()-1))));
            indexArr.remove(indexArr.size()-1);
        }

        updateFeatureFile(ctx);
    }

    /**
     * Remove feature vector by sound class and position
     * @param ctx - aplication context
     * @param cls - sound class identifief
     * @param position - position in file
     */
    public static void removeFeatureFromFile(Context ctx, int cls, int position){
        int cnt = 0;
        int index = 0;
        isModified = true;
        if(featureArrays.size() <= 0)
            featuresInit(ctx);
        for(int i = 0; i < featureArrays.size(); i++){
            if(cls == (int)featureArrays.get(i).getCls()){
                if(cnt == position){
                    index = i;
                    break;
                }
                cnt++;
            }
        }
        featureArrays.remove(index);

        updateFeatureFile(ctx);
    }

    /**
     * Store all (edited) vectors to file
     * @param ctx - aplication context
     */
    private static void updateFeatureFile(Context ctx){
        Permission.permisionForWriteExternalStorage(ctx);
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(filepath,dirName);
        File file = new File(dir.getAbsolutePath(),fetureFileName);
        if (file.exists()) {
            FileOutputStream out = null;
            try {
                new PrintWriter(dir.getAbsolutePath()+"/"+fetureFileName).close();
                out = new FileOutputStream(file, true);
                OutputStreamWriter outputWriter = new OutputStreamWriter(out);
                for(int i = 0; i < featureArrays.size(); i++) {
                    outputWriter.append(featureVectorToString(i) + "\n");
                    outputWriter.flush();
                }
                out.flush();
                outputWriter.close();
                out.close();
            }
            catch (IOException e){
                Log.d("ERROR:", "Write to file "+fetureFileName);
            }
        }
        else {
            Log.d("ERROR:", "File "+fetureFileName+" doesn't exist!");
        }
    }

    /**
     * Convert feature vector to string
     * @param vectorIndex - index of feature vector in file
     * @return feature vector string
     */
    private static String featureVectorToString(int vectorIndex){
        Log.i("Iter:", vectorIndex+" ");
        if(vectorIndex < featureArrays.size()){
            StringBuilder result = new StringBuilder((int)featureArrays.get(vectorIndex).getCls()+"");
            double[] vector = featureArrays.get(vectorIndex).getFeatures();
            for(int i = 0; i < vector.length; i++){
                result.append(","+vector[i]);
            }
            return result.toString();
        }
        return "";
    }

    /**
     * Load file with features from external storage
     * @param ctx - aplication context
     * @return object with features and classes
     */
    public static Object[] loadFromFileExternalStore(Context ctx){
        ArrayList<double[]> features = new ArrayList<>();
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(filepath,dirName);
        File feat = new File(dir.getAbsolutePath(),fetureFileName);

        try {
            FileInputStream stream = new FileInputStream(feat);
            InputStreamReader inputStream = new InputStreamReader(stream);
            BufferedReader bufferedReader = new BufferedReader(inputStream);
            features = Store.loadFeatures(bufferedReader);
            ArrayList<double[]> res = new ArrayList<>();
            double[] cls = new double[features.size()];
            for(int i = 0; i < features.size(); i++){
                cls[i] = features.get(i)[0];
                res.add(Arrays.copyOfRange(features.get(i), 1, features.get(i).length));
            }
            Log.i("Features:", "external OK");
            return new Object[]{res, cls};
        } catch (Exception e){
            return Store.loadFromFileInternalStore(ctx);
        }
    }

    /**
     * Copy features from internal resources to external resources
     * @param ctx - aplication context
     */
    public static void copyDefailtFeatures(Context ctx){
        InputStream stream = ctx.getResources().openRawResource(ctx.getResources().getIdentifier("features", "raw", ctx.getPackageName()));
        InputStreamReader inputStream = new InputStreamReader(stream);
        BufferedReader bufferedReader = new BufferedReader(inputStream);

        String filepath = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(filepath,dirName);
        File feat = new File(dir.getAbsolutePath(),fetureFileName);
        if (feat.exists()) {
            Log.w("Warning","Feature files exist! ");
            return;
        }
        String line;
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(dir.getAbsolutePath(),"features.txt")));
            while((line = bufferedReader.readLine()) != null){
                bufferedWriter.write(line);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            bufferedWriter.flush();
            bufferedReader.close();
            bufferedWriter.close();
        } catch (Exception e){
            Log.e("Error","Error, can't duplicate features file! "+e);
        }
    }

    /**
     * Convert Feature to sound class and feture vectors array
     * @param ctx - aplication context
     * @param activeClsSet - set of active sound class
     * @return
     */
    public static Object[] getTwoArrayFeatures(Context ctx, HashSet<Integer> activeClsSet){
        //Permission.permisionForReadExternalStorage(ctx);
        if(featureArrays.size() <= 0)
            featuresInit(ctx);
        ArrayList<double[]> res = new ArrayList<>();
        ArrayList<Double> clsArr = new ArrayList<>();
        for(int i = 0; i < featureArrays.size(); i++){
            if(activeClsSet.contains((int)(featureArrays.get(i).getCls())) ||
                    ((int)(featureArrays.get(i).getCls()) < 20 && (int)(featureArrays.get(i).getCls()) > 10)) {
                clsArr.add(featureArrays.get(i).getCls());
                res.add(featureArrays.get(i).getFeatures());
            }
        }
        double[] cls = new double[clsArr.size()];
        for(int i = 0; i < clsArr.size(); i ++){
            cls[i] = clsArr.get(i);
        }
        return new Object[]{res, cls};
    }

    /**
     * Check if is no active snd class in drill list
     * @param ctx - aplication context
     * @param drillList - list of drill in drill set
     * @return - true - in list is no active snd class | false - in list is only acctive snd classes
     */
    public static boolean isNoActiveSndClassInDrillSet(Context ctx, List<DrillInSet> drillList){
        SQLiteDB db = new SQLiteDB(ctx);
        HashSet<Integer> activeClsSet = db.getActiveSndClass();
        for(int i = 0; i < drillList.size(); i++){
            if(drillList.get(i).isSndWait() && !activeClsSet.contains(drillList.get(i).getSnd())){
                return true;
            }
        }
        return false;
    }
}
