package com.sport.sound;

import android.app.Activity;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.sport.SoundCreateEditClassActivity;
import com.sport.utils.Permission;

import java.util.ArrayList;
import java.util.Arrays;

import static android.os.AsyncTask.Status.PENDING;
import static android.os.AsyncTask.Status.RUNNING;
import static com.sport.sound.Record.concatenate;
import static javax.net.ssl.SSLEngineResult.HandshakeStatus.FINISHED;

/**
 * Class for record sound/voice to store in file
 */
public class RecordFile {

    private static final int SAMPLE_RATE = 16000;
    private static final double TRESHOLD = 0.1;     // treshold for silent detection
    private static final int BUFFER_SIZE = 6 * AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

    private Context ctx;
    private Activity act;
    private int clsId;
    private RecordFileTask recordFileTask = null;
    private ArrayList<byte[]> buf;
    private boolean stopWithoutSave;
    private boolean normalStop;

    /**
     * Constructor
     * @param ctx - aplication context
     * @param act - activity
     * @param clsId - identifier of new record class
     */
    public RecordFile(Context ctx, Activity act, int clsId){
        this.ctx = ctx;
        this.act = act;
        recordFileTask = new RecordFileTask();
        this.clsId = clsId;
        this.stopWithoutSave = false;
        this.normalStop = false;
    }


    /**
     * Start asynch task with record
     */
    public void startRecord(){
        Permission.checkPermisionForRecordSound(ctx, act);
        Permission.verifyStoragePermissions(act);
        mStatusChecker.run();
    }

    /**
     * Stop asynch task with record
     */
    public void stopRecord(boolean stopWithoutSave){
        this.stopWithoutSave = stopWithoutSave;
        if (!recordFileTask.isCancelled() && recordFileTask.getStatus() == RUNNING) {
            recordFileTask.cancel(false);
        }
    }

    /**
     * Set flag with stop as normal
     */
    public void stop() {
        normalStop = true;
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                recordFileTask = new RecordFileTask();
                recordFileTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);//execute();
            } finally {

            }
        }
    };

    /**
     * Remove silence region in record file
     * @param data - array with signal in byte
     * @return - array with signal in byte with no silence regions
     */
    public byte[] trimSilence(byte[] data){
        double[] sig = new double[data.length/2+(data.length%2 != 0 ? 1 : 0)];
        int cnt = 0;
        int startIndex = 0;
        int endIndex = data.length;
        double max = 0.0;
        //Log.i("Trim", "1");
        for(int i = 1; i<data.length;i=i+2){
            sig[cnt] = (double)((data[i] << 8) | (data[i-1] & 0xFF) );
            if(sig[cnt] > max)
                max = sig[cnt];
            cnt++;
        }
        //Log.i("Trim", "2");
        for(int i = 0; i< sig.length; i++){
            sig[i] = (Math.abs(sig[i])) / (max);
            if(sig[i] < TRESHOLD){
                startIndex += 2;
            }
            else{
                break;
            }
        }
        //Log.i("Trim", "3");
        for(int j = sig.length-32; j > startIndex; j = j - 1){
            //Log.i("j", ""+endIndex);
            sig[j] = (Math.abs(sig[j])) / (max);
            if(sig[j] < TRESHOLD){
                endIndex -= 2;
            }
            else{
                //Log.i("sig[j]", ""+sig[j]);
                break;
            }
        }
        if((endIndex - startIndex) % 2 == 1)
            endIndex++;
        if(endIndex - startIndex < 800){
            //Log.i("Trim", "4");
            if(endIndex + 400 < data.length)
                endIndex += 400;
            if(startIndex - 400 > 0)
                startIndex -= 400;
            else
                startIndex = 0;
        }
        //Log.i("Trim", startIndex+" "+endIndex);
        //System.out.println("Data: "+Arrays.toString(sig));
        return Arrays.copyOfRange(data, startIndex, endIndex);
    }


    /**
     * Asynch task with record sound
     */
    private class RecordFileTask extends AsyncTask<Void, Void, Boolean> {
        AudioTrack track;
        double[] MFCCresult;
        byte allData[] = new byte[1];

        private RecordFileTask() {
            buf = new ArrayList<byte[]>();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            AudioRecord audioRecord = null;
            try {
                // Open our two resources
                audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                        AudioFormat.ENCODING_PCM_16BIT, BUFFER_SIZE);
                int minSize = AudioTrack.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_CONFIGURATION_MONO
                        , AudioFormat.ENCODING_PCM_16BIT);
                track = new AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE_RATE, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                        AudioFormat.ENCODING_PCM_16BIT, minSize, AudioTrack.MODE_STREAM);


                // Avoiding loop allocations
                //byte[] buffer = new byte[BUFFER_SIZE];
                int read;

                audioRecord.startRecording();

                int i = 0, cnt = 0;
                while (!normalStop && !isCancelled()) {
                    if(normalStop)
                        break;
                    byte[] buffer = new byte[BUFFER_SIZE];
                    read = audioRecord.read(buffer, 0, buffer.length);
                    if(cnt == 0){
                        allData = buffer;
                    }
                    else{
                        allData = concatenate(allData, buffer);
                    }
                    cnt++;
                    buf.add(buffer);
                    i++;
                }
                allData = trimSilence(allData);
                MFCC mfcc = new MFCC(allData, SAMPLE_RATE, 13, 0.025, 0.01, -1);
                ArrayList<double[]> mfccResult = mfcc.mfcc();
                ArrayList<double[]> deltaResult = mfcc.delta(mfccResult, 2);
                MFCCresult = MFCC.concatMFCCAndDelta1D(mfccResult, deltaResult);
            } catch (Exception ex) {
                return false;
            } finally {
                if (audioRecord != null) {
                    try {
                        if (audioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                            audioRecord.stop();
                        }
                    } catch (IllegalStateException ex) {
                        //
                    }
                    if (audioRecord.getState() == AudioRecord.STATE_INITIALIZED) {
                        audioRecord.release();
                    }
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean result) {
            if(result) {
                /*
                int i = 0;
                while (i < buf.size()) {
                    track.write(buf.get(i), 0, buf.get(i).length);
                    i++;
                }*/
                if(!stopWithoutSave){
                    Log.i("Saving", "...");
                    //System.out.println("Data: "+Arrays.toString(allData));

                    StoreFile sf = new StoreFile(ctx);
                    int position = sf.getMaxRecordPositionByClassId(clsId)+1;

                    FeatureFile featFile = new FeatureFile(ctx);
                    featFile.addFeatureVectorToFile(ctx, MFCC.addClassToFeatures(String.valueOf(clsId), MFCC.doubleArrayToString(MFCCresult)));
                    ArrayList<byte[]> b = new ArrayList<>();
                    b.add(allData);
                    sf.writeAudioDataToFile(b, clsId+"-"+position, BUFFER_SIZE, SAMPLE_RATE);
                    sf.getFilesName(ctx);
                    ((SoundCreateEditClassActivity)act).updateList(sf);
                }
            }
        }

    }
}
