package com.sport.sound;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import libsvm.*;

public class SVM {
    private static svm_model model = null;
    private static svm_model oneClassModel = null;

    /**
     * SVM constructor (also make training model)
     * @param xtrain - MFCC coefficients
     * @param ytrain - classes for each MFCC (ytrain.size() == ytrain.length)
     */
    public SVM(ArrayList<double[]> xtrain, double[] ytrain){
        System.out.println("Training:");
        model = svmTrain2(xtrain, ytrain);
        /*System.out.println("Training2:");
        for (int i = 0; i <ytrain.length; i++) {
            ytrain[i] = 1.0;
        }
        oneClassModel = svmOneClassTrain(xtrain, ytrain);*/
    }

    public static void oneSVM(ArrayList<double[]> xtrain, double[] ytrain){
        double[] ytrainNew = new double[ytrain.length];
        System.out.println("Training2:");
        for (int i = 0; i <ytrain.length; i++) {
            ytrainNew[i] = 1.0;
        }
        oneClassModel = svmOneClassTrain(xtrain, ytrainNew);
    }

    /**
     * Call methods for testing/recognition
     * @param testx
     * @param testy
     */
    public static int callSVM(ArrayList<double[]> testx, double[] testy){
        if(model == null || oneClassModel == null) {
            Log.i("Error", "SVM model is not initialized!");
            return -1;
        }
        System.out.println("One class predict:");
        double[] ypredOneClass = svmOneClassPredict(testx, oneClassModel);
        for (int i = 0; i < testx.size(); i++) {
            System.out.println("SVM OC (Actual:" + testy[i] + " Prediction:" + ypredOneClass[i] + ")");
        }
        System.out.println("Predict:");
        double[] ypred = svmPredict2(testx, model);
        for (int i = 0; i < testx.size(); i++) {
            System.out.println("(Actual:" + testy[i] + " Prediction:" + ypred[i] + ")");
        }
        printPredictSVM(testy, ypred);

        if(ypredOneClass[0] == 1){
            return (int)ypred[0];
        }
        return -1;
    }

    static svm_model svmTrain2(ArrayList<double[]> xtrain, double[] ytrain) {
        int featureCount = 0;
        for (int i = 0; i < xtrain.size(); i ++){
            if(xtrain.get(i).length > featureCount)
                featureCount = xtrain.get(i).length;
        }
        svm_problem prob = new svm_problem();
        int recordCount = xtrain.size();
        prob.y = new double[recordCount];
        prob.l = recordCount;
        prob.x = new svm_node[recordCount][featureCount];

        for (int i = 0; i < recordCount; i++) {
            double[] features = xtrain.get(i);
            prob.x[i] = new svm_node[features.length];
            for (int j = 0; j < features.length; j++) {
                svm_node node = new svm_node();
                //node.index = j;
                node.value = features[j];
                prob.x[i][j] = node;
            }
            prob.y[i] = ytrain[i];
        }

        svm_parameter param = new svm_parameter();
        param.probability = 0;
        param.gamma = 0.001; //0.000001 0.000013
        //param.nu = 0.5;
        param.C = 0.89; // 0.97 0.89
        //param.degree = 3;
        param.svm_type = svm_parameter.C_SVC;
        param.kernel_type = svm_parameter.LINEAR;
        //param.shrinking = 0;
        param.cache_size = 40000;
        param.eps = 0.001;
        param.coef0 = 0.0;

        svm_model model = svm.svm_train(prob, param);

        return model;
    }

    static double[] svmPredict2(ArrayList<double[]> xtest, svm_model model) {

        double[] yPred = new double[xtest.size()];

        for (int k = 0; k < xtest.size(); k++) {

            double[] fVector = xtest.get(k);

            svm_node[] nodes = new svm_node[fVector.length];
            for (int i = 0; i < fVector.length; i++) {
                svm_node node = new svm_node();
                //node.index = i;
                node.value = xtest.get(k)[i];
                nodes[i] = node;
            }

            int totalClasses = 11;
            int[] labels = new int[totalClasses];
            svm.svm_get_labels(model, labels);

            double[] prob_estimates = new double[totalClasses];
            yPred[k] = svm.svm_predict_probability(model, nodes, prob_estimates);

            for (int i = 0; i < totalClasses; i++){
                System.out.print("(" + labels[i] + ":" + prob_estimates[i] + ")");
            }
            System.out.println("(Actual:" + Arrays.toString(xtest.get(k)) + " Prediction:" + yPred[k] + ")");

        }

        return yPred;
    }

    /*static svm_model svmTrain(double[][] xtrain, double[][] ytrain) {
        svm_problem prob = new svm_problem();
        int recordCount = xtrain.length;
        int featureCount = xtrain[0].length;
        prob.y = new double[recordCount];
        prob.l = recordCount;
        prob.x = new svm_node[recordCount][featureCount];

        for (int i = 0; i < recordCount; i++) {
            double[] features = xtrain[i];
            prob.x[i] = new svm_node[features.length];
            for (int j = 0; j < features.length; j++) {
                svm_node node = new svm_node();
                node.index = j;
                node.value = features[j];
                System.out.println(ytrain[i][0]+"--"+features[j]);
                prob.x[i][j] = node;
            }
            prob.y[i] = ytrain[i][0];
        }

        svm_parameter param = new svm_parameter();
        param.probability = 0;
        param.gamma = 0.5;
        param.nu = 0.5;
        param.C = 100;
        param.svm_type = svm_parameter.C_SVC;
        param.kernel_type = svm_parameter.LINEAR;
        param.cache_size = 30000;
        param.eps = 0.001;

        svm_model model = svm.svm_train(prob, param);

        return model;
    }

    static double[] svmPredict(double[][] xtest, svm_model model) {

        double[] yPred = new double[xtest.length];

        for (int k = 0; k < xtest.length; k++) {

            double[] fVector = xtest[k];

            svm_node[] nodes = new svm_node[fVector.length];
            for (int i = 0; i < fVector.length; i++) {
                svm_node node = new svm_node();
                node.index = i;
                node.value = xtest[k][i];
                nodes[i] = node;
            }

            int totalClasses = 2;
            int[] labels = new int[totalClasses];
            svm.svm_get_labels(model, labels);

            double[] prob_estimates = new double[totalClasses];
            yPred[k] = svm.svm_predict_probability(model, nodes, prob_estimates);

            for (int i = 0; i < totalClasses; i++){
                System.out.print("(" + labels[i] + ":" + prob_estimates[i] + ")");
            }
            System.out.println("(Actual:" + Arrays.toString(xtest[k]) + " Prediction:" + yPred[k] + ")");

        }

        return yPred;
    }*/

    public static void printPredictSVM(double[] testClass, double[] predicted){
        double[] tmp = new double[testClass.length+predicted.length];
        System.arraycopy(testClass, 0, tmp, 0, testClass.length);
        System.arraycopy(predicted, 0, tmp, testClass.length, predicted.length);

        ArrayList<String> cls = new ArrayList<>();
        boolean result;
        for(double d : tmp){
            String s2 = String.valueOf(d);
            result = false;
            for(String s1 : cls){
                if(s1.compareTo(s2) == 0)
                    result = true;
            }
            if(!result)
                cls.add(s2);
        }
        int[][] matrix = new int[cls.size()][cls.size()];
        for(int i = 0; i < cls.size(); i++){
            for(int j = 0; j < cls.size(); j++) {
                for (int k = 0; k < testClass.length; k++) {
                    if (cls.get(j).compareTo(String.valueOf(predicted[k])) == 0 && cls.get(i).compareTo(String.valueOf(testClass[k])) == 0)
                        matrix[i][j]++;
                }
            }
        }
        System.out.println("************************************************************************");
        System.out.println("*                           Predicted result                           *");
        System.out.println("************************************************************************");
        // header
        System.out.print("\t");
        for (int i = 0; i<cls.size(); i++){
            System.out.print("\t"+cls.get(i)+"\t");
        }
        System.out.println();
        // body
        for (int k = 0; k<cls.size(); k++){
            System.out.print(cls.get(k)+"\t\t");
            //inner
            //for(int i = k; i < cls.size(); i++) {
            for (int j = 0; j < cls.size(); j++) {
                System.out.print("\t"+matrix[k][j]+"\t");
            }
            //}
            System.out.println();
        }
        System.out.println("************************************************************************");
    }


    /**************************************************************
     ******************** One model train ************************
     *************************************************************/
    static svm_model svmOneClassTrain(ArrayList<double[]> xtrain, double[] ytrain) {
        int featureCount = 0;
        for (int i = 0; i < xtrain.size(); i ++){
            if(xtrain.get(i).length > featureCount)
                featureCount = xtrain.get(i).length;
        }
        svm_problem prob = new svm_problem();
        int recordCount = xtrain.size();
        prob.y = new double[recordCount];
        prob.l = recordCount;
        prob.x = new svm_node[recordCount][featureCount];

        for (int i = 0; i < recordCount; i++) {
            double[] features = xtrain.get(i);
            prob.x[i] = new svm_node[features.length];
            for (int j = 0; j < features.length; j++) {
                svm_node node = new svm_node();
                //node.index = j;
                node.value = features[j];
                prob.x[i][j] = node;
            }
            prob.y[i] = ytrain[i];
        }

        svm_parameter param = new svm_parameter();
        //param.probability = 0;
        param.gamma = 0.00001;//0.00001 0.000013
        param.nu = 0.05; //0.045
        //param.C = 0.97; // 0.97
        //param.degree = 3;
        param.svm_type = svm_parameter.ONE_CLASS;
        param.kernel_type = svm_parameter.LINEAR;
        param.shrinking = 0;
        param.cache_size = 40000;
        param.eps = 0.001;
        param.coef0 = 0.0;
        /*double[] d = {1,1,1,1,1,1,1,1,1,1.3,1};
        param.weight = d;*/

        svm_model model = svm.svm_train(prob, param);

        return model;
    }

    static double[] svmOneClassPredict(ArrayList<double[]> xtest, svm_model model) {

        double[] yPred = new double[xtest.size()];

        for (int k = 0; k < xtest.size(); k++) {

            double[] fVector = xtest.get(k);

            svm_node[] nodes = new svm_node[fVector.length];
            for (int i = 0; i < fVector.length; i++) {
                svm_node node = new svm_node();
                //node.index = i;
                node.value = xtest.get(k)[i];
                nodes[i] = node;
            }

            int totalClasses = 1;
            int[] labels = new int[totalClasses];
            svm.svm_get_labels(model, labels);

            double[] prob_estimates = new double[totalClasses];
            yPred[k] = svm.svm_predict_probability(model, nodes, prob_estimates);

            for (int i = 0; i < totalClasses; i++){
                System.out.print("(" + labels[i] + ":" + prob_estimates[i] + ")");
            }
            System.out.println("SVM OC (Actual:" + Arrays.toString(xtest.get(k)) + " Prediction:" + yPred[k] + ")");

        }

        return yPred;
    }
}
