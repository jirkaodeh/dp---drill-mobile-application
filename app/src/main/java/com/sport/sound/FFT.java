package com.sport.sound;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class FFT {
    private double[] sin, cos;
    private int FFTsize, size;
    private double precompute;


    public FFT(int FFTsize){
        this.FFTsize = FFTsize;
        this.precompute = (1.0 / this.FFTsize);
        this.size = (int)(Math.log(FFTsize) / Math.log(2));
        precomputeSinCos();
    }

    private void precomputeSinCos(){
        sin = new double[this.FFTsize/2];
        cos = new double[this.FFTsize/2];
        for(int i=0; i < FFTsize/2; i++){
            sin[i] = Math.sin(-2*Math.PI*i/FFTsize);
            cos[i] = Math.cos(-2*Math.PI*i/FFTsize);
        }
    }

    public ArrayList<double[]> fftCompute(ArrayList<double[]> frames){
        ArrayList<double[]> f = new ArrayList<double[]>();
        double[] im;// = new double[this.FFTsize/*frames.get(i).length*/];
        for(int i=0; i<frames.size(); i++){
            // ~5ms
            im = new double[this.FFTsize/*frames.get(i).length*/];
            f.add(fft(frames.get(i), im));
        }
        return f;
    }


    public double[] fft(double[] re, double[] im){
        int divideFFTsize = this.FFTsize;
        int tmpFFTsize;
        double tmp;
        //cnt == j; tmpFFTsize == n1
        double[] newRe = new double[divideFFTsize];
        if(re.length < divideFFTsize){
            double[] tmpArr = new double[divideFFTsize];
            System.arraycopy(re, 0, tmpArr, 0, re.length);
            re = tmpArr;
            /*double[] tmpArr2 = new double[divideFFTsize];
            System.arraycopy(im, 0, tmpArr2, 0, im.length);
            im = tmpArr2;*/
        }
        int cnt = 0;
        divideFFTsize = divideFFTsize/2;
        // Bit-reverse
        for(int i=1; i<FFTsize-1;i++){
            tmpFFTsize = divideFFTsize;
            while(cnt >= tmpFFTsize){
                cnt = cnt - tmpFFTsize;
                tmpFFTsize = tmpFFTsize/2;
            }
            cnt = cnt + tmpFFTsize;
            if(i < cnt){
                // swap real
                tmp = re[i];
                re[i] = re[cnt];
                re[cnt] = tmp;
                //swap img
                tmp = im[i];
                im[i] = im[cnt];
                im[cnt] = tmp;
            }
        }
        //FFT
        int n1;
        int n2 = 1;
        int shift;
        double s, c, tmp2, absoluteTmp;
        for(int i=0; i<this.size;i++){
            n1 = n2;
            n2 = n2 + n2;
            shift = 0;
            for(int j=0; j<n1; j++){
                s = sin[shift];
                c = cos[shift];
                shift += 1 << (this.size-i-1);
                for(int k=j; k < FFTsize; k+=n2){
                    tmp = c * re[k + n1] - s * im[k + n1];
                    tmp2 = s * re[k + n1] + c * im[k + n1];
                    re[k + n1] = re[k] - tmp;
                    im[k + n1] = im[k] - tmp2;
                    re[k] = re[k] + tmp;
                    im[k] = im[k] + tmp2;
                    absoluteTmp = Math.sqrt(re[k] * re[k] + im[k] * im[k]);
                    newRe[k] =  precompute * absoluteTmp * absoluteTmp;
                }
            }
        }
        return Arrays.copyOfRange(newRe, 0, 257);
    }

    private double absolute(double re, double im){
        return Math.sqrt(re * re + im * im);
    }

}
