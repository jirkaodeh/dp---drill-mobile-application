package com.sport.sound;

import android.content.Context;
import android.os.AsyncTask;

import com.sport.database.PreferenceStore;
import com.sport.database.SQLiteDB;
import com.sport.utils.Permission;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public final class Train {
    public static boolean modelTrained = false;
    private static ExecuteTrain trainTask;
    private static ExecuteTrainOneCls oneTrainTask;
    private static Runnable mStatusChecker, mStatusChecker2;
    public static Context context;

    public static void doTrain(Context ctx){
        context = ctx;
        if(!modelTrained) {
            //Permission.permisionForReadExternalStorage(ctx);
            SQLiteDB db = new SQLiteDB(ctx);
            HashSet<Integer> activeClsSet = db.getActiveSndClass();
            Object[] o = FeatureFile.getTwoArrayFeatures(ctx, activeClsSet);
            final ArrayList<double[]> freatures = (ArrayList<double[]>) o[0];
            final double[] refCls = (double[]) o[1];
            System.out.println("Ref cls: "+Arrays.toString((refCls)));

            mStatusChecker = new Runnable() {
                @Override
                public void run() {
                    try {
                        if(trainTask == null) {
                            trainTask = new ExecuteTrain(freatures, refCls);
                            trainTask.execute((Void) null);
                        }
                    } finally {}
                }
            };
            startRepeatingTask();

            mStatusChecker2 = new Runnable() {
                @Override
                public void run() {
                    try {
                        if(oneTrainTask == null) {
                            oneTrainTask = new ExecuteTrainOneCls(freatures, refCls);
                            oneTrainTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    } finally {}
                }
            };
            startOneClsRepeatingTask();

        }
    }

    public static void doTrainNoAsynchron(Context ctx){
        context = ctx;
        double start, stop;
        if(!modelTrained) {
            start = System.currentTimeMillis();
            Permission.permisionForReadExternalStorage(ctx);
            SQLiteDB db = new SQLiteDB(ctx);
            HashSet<Integer> activeClsSet = db.getActiveSndClass();
            Object[] o = FeatureFile.getTwoArrayFeatures(ctx, activeClsSet);
            final ArrayList<double[]> freatures = (ArrayList<double[]>) o[0];
            final double[] refCls = (double[]) o[1];
            System.out.println("Ref cls: "+Arrays.toString((refCls)));
            new SVM(freatures, refCls);
            modelTrained = true;
            stop = System.currentTimeMillis();
            PreferenceStore.setAppLoadingTime(context, ((int)((stop - start) / 1000.0)) - 10);
            SVM.oneSVM(freatures, refCls);
        }
    }

    public static boolean isModelTrained() {
        return modelTrained;
    }

    private static void startRepeatingTask() {
        mStatusChecker.run();
    }

    private static void stopRepeatingTask() {
        if(trainTask != null && trainTask.getStatus() != AsyncTask.Status.FINISHED){
            trainTask.cancel(true);
            trainTask = null;
        }
    }

    /**
     * Sound class SVM train task
     */
    private static class ExecuteTrain extends AsyncTask<Void, Void, Boolean> {

        private ArrayList<double[]> freatures;
        private double[] refCls;
        private double start, stop;

        public ExecuteTrain(ArrayList<double[]> freatures, double[] refCls) {
            this.freatures = freatures;
            this.refCls = refCls;
        }

        @Override
        protected void onPreExecute(){
            start = System.currentTimeMillis();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            new SVM(freatures, refCls);
            System.out.println("Running");
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            modelTrained = true;
            stop = System.currentTimeMillis();
            PreferenceStore.setAppLoadingTime(context, ((int)((stop - start) / 1000.0)) - 10);
            System.out.println("Model train time: "+((stop - start) / 1000.0)+"s");
            System.out.println("Done");
        }
    }







    private static void startOneClsRepeatingTask() {
        mStatusChecker2.run();
    }

    private static void stopOneClsRepeatingTask() {
        if(oneTrainTask != null && oneTrainTask.getStatus() != AsyncTask.Status.FINISHED){
            oneTrainTask.cancel(true);
            oneTrainTask = null;
        }
    }

    /**
     * One class SVM train task
     */
    private static class ExecuteTrainOneCls extends AsyncTask<Void, Void, Boolean> {

        private ArrayList<double[]> freatures;
        private double[] refCls;
        private double start, stop;

        public ExecuteTrainOneCls(ArrayList<double[]> freatures, double[] refCls) {
            this.freatures = freatures;
            this.refCls = refCls;
        }

        @Override
        protected void onPreExecute(){
            start = System.currentTimeMillis();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            SVM.oneSVM(freatures, refCls);
            System.out.println("Running2");
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            stop = System.currentTimeMillis();
            System.out.println("Model one class train time: "+((stop - start) / 1000.0)+"s");
            System.out.println("One class done");
        }
    }
}
