package com.sport.sound;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.sport.lists.VawRecord;
import com.sport.utils.Permission;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * Class for prepare and save wav file
 */
public class StoreFile {

    private static String dirName = ".voicesport";
    public static ArrayList<VawRecord> records;

    /**
     * Constructor
     * @param ctx - aplication context
     */
    public  StoreFile(Context ctx){
        // check permisions
        Permission.permisionForReadExternalStorage(ctx);
        Permission.permisionForWriteExternalStorage(ctx);
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,dirName);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    /**
     * Found records by class id
     * @param clsId - sound calss id
     * @return - array of records (VawRecord objects)
     */
    public static ArrayList<VawRecord> getRecordsByClassId(int clsId){
        ArrayList<VawRecord> found = new ArrayList<>();
        for (VawRecord vr : records) {
            if(vr.getSndClass() == clsId)
                found.add(vr);
        }
        return found;
    }

    /**
     * Get max index of file in sound class
     * @param clsId - soun calss id
     * @return max file index
     */
    public int getMaxRecordPositionByClassId(int clsId){
        int max = 0;
        for (VawRecord vr : records) {
            if(vr.getPosition() > max)
                max = vr.getPosition();
        }
        return max;
    }

    /**
     * Get all files with user's records
     * @param ctx - aplication context
     * @return
     */
    public void getFilesName(Context ctx){
        Permission.permisionForReadExternalStorage(ctx);
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File directory = new File(filepath, dirName);
        Log.d("Dir", "Dir:"+filepath+" "+dirName);
        records = new ArrayList<VawRecord>();
        ArrayList<String> files = new ArrayList<>();
        File[] files2 = directory.listFiles();
        for(int i = 0; i < files2.length; i++){
            String name = files2[i].getName();
            if(files2[i].getName().endsWith(".wav")) {
                //files.add(name);
                name = name.substring(0, name.length() - 4);
                String[] lines = name.split("-");
                if(lines.length == 2) {
                    try{
                        int cls = Integer.valueOf(lines[0]);
                        int position = Integer.valueOf(lines[1]);
                        VawRecord vr = new VawRecord(files2[i].getName(), files2[i].getAbsolutePath(), files2[i], cls, position);
                        records.add(vr);
                        //Log.d("File", "FileName:" + vr.getName() + " " + vr.getAbsPath());
                    }catch (Exception e){
                        Log.e("Err", ""+e);
                    }
                }
            }
        }
        Collections.sort(records, new PositionComparator());
    }

    /**
     * Get wav file by file name
     * @param ctx - aplication context
     * @param fileName - wav file name
     * @return  File descriptor
     */
    public static File getFile(Context ctx,String fileName){
        Permission.permisionForReadExternalStorage(ctx);
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(filepath, dirName);
        File file = new File(dir.getAbsolutePath(),fileName);
        return file;
    }

    /**
     * Write all data to wav file (head + body)
     * @param buf - file content
     * @param fileName - name of wav file
     * @param bufferSize - size of content buffer
     * @param sampleRate - sample rate of saving data
     */
    public void writeAudioDataToFile(ArrayList<byte[]> buf, String fileName, int bufferSize, int sampleRate) {
        /*String filepath = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(filepath,dirName);
        File f = new File(dir.getAbsolutePath(), fileName+".vaw");*/
        String filename = getTempFilename(fileName);
        FileOutputStream os = null;
        int totalDataLen = 36;
        int channels = 1;
        long byteRate = 16 * sampleRate * channels/8;
        int i = 0;
        while (i < buf.size()) {
            totalDataLen += buf.get(i).length;
            i++;
        }

        try {
            os = new FileOutputStream(filename);
            WriteWaveFileHeader(os, totalDataLen-36,
                    totalDataLen, sampleRate, channels,
                    byteRate);
            i = 0;
            while (i < buf.size()) {
                os.write(buf.get(i));
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create new file in folder
     * @param fileName - file name
     * @return atring absolute path to file
     */
    private String getTempFilename(String fileName) {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,dirName);

        if (!file.exists()) {
            file.mkdirs();
        }

        File tempFile = new File(filepath,fileName+".wav");

        if (tempFile.exists())
            tempFile.delete();

        return (file.getAbsolutePath() + "/" + fileName+".wav");
    }

    /**
     * Create vaw header
     * @param out - output stream
     * @param totalAudioLen - size of all audio data
     * @param totalDataLen - size only of data (without header)
     * @param longSampleRate - sample rate of data
     * @param channels - number of chanels
     * @param byteRate - byte rate
     * @throws IOException
     */
    private void WriteWaveFileHeader(
            FileOutputStream out, int totalAudioLen,
            long totalDataLen, long longSampleRate, int channels,
            long byteRate) throws IOException {
        byte[] header = new byte[44];

        header[0] = 'R';  // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f';  // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1;  // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8);  // block align
        header[33] = 0;
        header[34] = 16;  // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0x00FF);
        header[41] = (byte) ((totalAudioLen >> 8) & 0x000000FF);
        header[42] = (byte) ((totalAudioLen >> 16) & 0x000000FF);
        header[43] = (byte) ((totalAudioLen >> 24) & 0x000000FF);

        out.write(header, 0, 44);
    }

    /**
     * Compare of file positions
     */
    class PositionComparator implements Comparator<VawRecord>{
        @Override
        public int compare(VawRecord o1, VawRecord o2) {
            return o1.getPosition() - o2.getPosition();
        }
    }
}
