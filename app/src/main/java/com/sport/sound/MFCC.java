package com.sport.sound;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;

public class MFCC {
    //private double lowMelFreq = 0;
    //private double highMelFreq;
    private int sampleRate;
    private int numCepCoeff;
    private double windowLen;
    private int fftSize;
    private double stepLen;
    private byte[] signal;
    private ArrayList<Double> powerOfFrames;
    private ArrayList<double[]> features;

    //MFCC(16000, 13, 0.025, 0.01, 512)
    public MFCC(byte signal[], int sampleRate, int numCepCoeff, double windowLen, double stepLen, int fftSize){
        this.sampleRate = sampleRate;
        this.numCepCoeff = numCepCoeff;
        this.windowLen = windowLen;
        this.stepLen = stepLen;
        this.signal = signal;
        if(fftSize == -1)
            this.fftSize = sizeOfFFT();
        else
            this.fftSize = fftSize;
        //System.out.println("this.fftSize"+this.fftSize);
        this.powerOfFrames = new ArrayList<Double>();
        /*byte[] b = {-20, 2, 12, 2, 12, 3, -84, 2, 44, 2, -52, 2};
        short[] p = bytesToShort(b);
        System.out.println(Arrays.toString(p));*/
        //ArrayList<double[]> mfcc = mfcc();
        //delta(mfcc, 2);
    }

    /**
     * Calculates the FFT size as a power of two greater than or equal to
     * the number of samples in a single window length.
     * @return FFT size
     */
    private int sizeOfFFT(){
        int fftSize = 1;
        double windowLengthSamples = this.windowLen * this.sampleRate;
        while (fftSize < windowLengthSamples) {
            fftSize *= 2;
        }
        return fftSize;
    }

    /**
     * Compute energy of whole frame
     * @param list - values of sample energy in frame
     * @return energy of whole frame
     */
    private double sumOfFrameEnergy(double[] list){
        double sum = 0.0;
        for(double d: list){
            sum += d;
        }
        return sum;
    }

    public ArrayList<double[]> mfcc(){
        return filterBank();
    }

    /**
     * Compute Mel-filterbank energy features from an audio signal.
     */
    private ArrayList<double[]> filterBank(){

        //System.out.println((new Date()).getTime());
        double[] sig = bytesToDoubleWithNormalization(Arrays.copyOfRange(this.signal, 44, this.signal.length), 0.97);
        //double[] sig = bytesToDouble(Arrays.copyOfRange(this.signal, 44, this.signal.length));
        //System.out.println("Signal "+sig.length+" "+Arrays.toString(sig));
        ArrayList<double[]> framesDouble = framing(sig, (int)(Math.round(windowLen*sampleRate)), (int)(Math.round(stepLen*sampleRate)));
        //ArrayList<double[]> framesDouble = framesToDouble(frames);
        ArrayList<double[]> powerSpectogram;
        //System.out.println("Pocet framu "+framesDouble.size());
        //System.out.println("frames: "+framesDouble.size()+" "+Arrays.toString(framesDouble.get(0)));
        FFT fft = new FFT(this.fftSize);
        double start = System.currentTimeMillis();
        powerSpectogram = fft.fftCompute(framesDouble);
        double stop = System.currentTimeMillis();
        //System.out.println("Elapset time FFT: "+((stop - start) / 1000.0));
        //System.out.println("FFT: 0 "+powerSpectogram.get(0).length+" "+Arrays.toString((powerSpectogram.get(0))));
        //OK
        //System.out.println((new Date()).getTime());
        for(double[] d: powerSpectogram){
            powerOfFrames.add(sumOfFrameEnergy(d));
        }
        ArrayList<double[]> bank = makeFilterBanks(this.numCepCoeff*2, 0, sampleRate/2);
        this.features = multAndLogEnergyWithFilters(powerSpectogram, bank);
        /*ArrayList<double[]> test = new ArrayList<>();
        test.add(new double[]{5,4,2,3,8});
        test.add(new double[]{5,6,4,2,3});
        test.add(new double[]{1,25,4,4,8});
        ArrayList<double[]> test2 = new ArrayList<>();
        test2.add(new double[]{7,5,5,8,1});
        test2.add(new double[]{3,7,4,2,2});
        test2.add(new double[]{3,2,1,4,8});
        ArrayList<double[]> res = multEnergyWithFilters(test, test2);
        System.out.println("Mult: "+res.size()+" "+res.get(0).length+" "+Arrays.toString((res.get(0))));*/
        //System.out.println("energy: "+powerOfFrames.size()+" "+Arrays.toString(powerOfFrames.toArray()));
        //System.out.println("DCT: 0 "+features.get(0).length+" "+Arrays.toString((features.get(0))));
        this.features = lifter(this.features, 22, features.size());
        System.out.println((new Date()).getTime());

        //System.out.println("Apply: col "+features.get(0).length+" row "+features.size());
        //System.out.println("DCT lifter: 0 "+features.get(0).length+" "+Arrays.toString((features.get(0))));
        //System.out.println("DCT lifter: 1 "+features.get(1).length+" "+Arrays.toString((features.get(1))));
        /*System.out.println("Power: "+powerSpectogram.get(0).length+" "+Arrays.toString((powerSpectogram.get(0))));
        System.out.println("Bank: "+bank.get(0).length+" "+Arrays.toString((bank.get(0))));*/

        /*System.out.println(frames.get(frames.size()-2).length+" "+Arrays.toString((frames.get(frames.size()-2))));
        System.out.println(frames.get(frames.size()-1).length+" "+Arrays.toString((frames.get(frames.size()-1))));*/

        /*for(int i = 0; i < frames.size(); i++){
            System.out.println(frames.get(i).length+" "+Arrays.toString((frames.get(i))));
        }*/
        return features;
    }

    private ArrayList<double[]> multAndLogEnergyWithFilters(ArrayList<double[]> energy, ArrayList<double[]> bank){
        double start = System.currentTimeMillis();
        int bsize = bank.size();
        int esize = energy.size();
        int esize0 = energy.get(0).length;
        double[][] en = new double[esize][];
        double[][] bn = new double[bsize][];
        for(int i = 0; i < energy.size(); i++){
            en[i] = energy.get(i);
        }
        for(int i = 0; i < bank.size(); i++){
            bn[i] = bank.get(i);
        }
        ArrayList<double[]> result = new ArrayList<>();
        double[] enTmp, bnTmp;
        double sum;
        for(int i = 0; i < esize; i++){
            enTmp = en[i];
            //result.add(new double[bank.size()]);
            double[] newArr = new double[bsize];
            for(int j = 0; j < bsize; j++){
                bnTmp = bn[j];
                sum = 0.0;
                for(int k = 0; k < esize0; k++){
                    sum += enTmp[k] * bnTmp[k];
                }
                if(sum == 0)
                    sum = Math.log(0.001);
                else
                    sum = Math.log(sum);
                newArr[j] = sum;
            }
            result.add(dct(newArr, true, i));
            //result.set(result.size()-1,dct(result.get(result.size()-1), true, i));
        }

        /*ArrayList<double[]> result = new ArrayList<>();
        for(int i = 0; i < energy.size(); i++){
            result.add(new double[bank.size()]);
            for(int j = 0; j < bank.size(); j++){
                for(int k = 0; k < energy.get(0).length; k++){
                    result.get(i)[j] += energy.get(i)[k] * bank.get(j)[k];
                }
                if(result.get(i)[j] == 0)
                    result.get(i)[j] = Math.log(0.001);
                else
                    result.get(i)[j] = Math.log(result.get(i)[j]);

            }
            result.set(result.size()-1, dct(result.get(result.size()-1)));
        }*/
        double stop = System.currentTimeMillis();
        //System.out.println("Elapset time matrix: "+((stop - start) / 1000.0));
        return result;
    }

    /**
     * Compute Discrete Cosine Transform
     * @param vector - array of values in one frame
     * @return array of 13 cepstral coeficients + power
     */
    private double[] dct(double[] vector, boolean replaceFirstCoefWithFrameEnergy, int frameIndex){
        /*if(frameIndex == 0)
            System.out.println("IN DCT feat: "+vector.length+" "+Arrays.toString((vector)));*/
        int vecLen = vector.length;
        final double pn = Math.PI / vecLen;
        double[] output = new double[numCepCoeff];
        int index = 0;
        if(replaceFirstCoefWithFrameEnergy && frameIndex < powerOfFrames.size()){
            output[0] = Math.log(powerOfFrames.get(frameIndex));
        }
        else {
            for (int i = 0; i < vecLen; ++i) {
                output[0] += vector[i];
            }
            output[0] *= 1.0 / Math.sqrt(2.0) / vecLen;
        }

        double precompute = (2*vecLen);
        double precomputeSuffix = Math.sqrt(1.0/precompute) * 2;
        for(int i=1; i<numCepCoeff; i++){
            for(int j=0; j<vecLen; j++){
                //output[i] += vector[j] * Math.cos(pn * i * (j + 0.5));
                output[i] += vector[j] * Math.cos(Math.PI * i * (2*j+1)/precompute);
            }
            //output[i] /= vector.length;
            //System.out.println(" "+Math.sqrt(1.0/(2*vector.length))+ " "+(double)(1.0/vector.length*2));
            output[i] = output[i] * precomputeSuffix; //Math.sqrt(1/(2*vector.length));
        }
        /*if(frameIndex == 0)
            System.out.println("IN DCT feat OUT: "+output.length+" "+Arrays.toString((output)));*/
        return output;
    }

    private ArrayList<double[]> lifter(ArrayList<double[]> cepstralCoeffFrame, int liftCoefficient, int framesNum){
        double[] lift = new double[this.numCepCoeff];
        for(int i = 0; i < lift.length; i++){
            lift[i] = 1 + (liftCoefficient/2.0)*Math.sin(Math.PI*i/liftCoefficient);
        }
        //System.out.println("Lifter :"+lift.length+" "+Arrays.toString((lift)));
        double[][] en = new double[cepstralCoeffFrame.size()][];
        for(int i = 0; i < cepstralCoeffFrame.size(); i++){
            en[i] = cepstralCoeffFrame.get(i);
        }
        ArrayList<double[]> result = new ArrayList<>();
        for(int i = 0; i < cepstralCoeffFrame.size(); i++){
            result.add(new double[lift.length]);
            result.get(i)[0] = cepstralCoeffFrame.get(i)[0];
            for(int j = 1; j < lift.length; j++){
                result.get(i)[j] = cepstralCoeffFrame.get(i)[j] * lift[j];
            }
        }
        return result;
    }

    /**
     * Compute a Mel-filterbank
     * @param filterNumber - number of filters in filterbank
     * @param lowFreq - lowest frequency in Hz
     * @param highFreq - highest frequency in Hz
     */
    private ArrayList<double[]> makeFilterBanks(int filterNumber, double lowFreq, double highFreq){
        int realFilterNumber = filterNumber + 2;
        double lowMelFreq = frequencyToMel(lowFreq);
        double highMelFreq = frequencyToMel(highFreq);
        double[] melPoints = new double[realFilterNumber];
        double[] bin = new double[realFilterNumber];
        double pointStep = (highMelFreq - lowMelFreq) /(realFilterNumber - 1);
        ArrayList<double[]> bank = new ArrayList<>();
        for(int i=0; i < realFilterNumber; i++){
            melPoints[i] = lowMelFreq + i * pointStep;
            bin[i] = Math.floor((this.fftSize+1)*melToFrequency(melPoints[i])/this.sampleRate);
        }
        /*System.out.println("Mell min: "+lowMelFreq+" max: "+highMelFreq+" high freq: "+highFreq+" fftsize: "+this.fftSize);
        System.out.println("Mell coeff: "+melPoints.length+" "+Arrays.toString((melPoints)));
        System.out.println("Mell bin: "+bin.length+" "+Arrays.toString((bin)));*/
        for(int j=0; j<filterNumber; j++){
            bank.add(new double[this.fftSize/2+1]);
        }
        for(int j=0; j<filterNumber; j++){
            for(int i=(int)bin[j]; i<(int)bin[j+1]; i++){
                bank.get(j)[i] = (i - bin[j]) / (bin[j+1] - bin[j]);
            }
            for(int i=(int)bin[j+1]; i<(int)bin[j+2]; i++){
                bank.get(j)[i] = (bin[j+2] - i) / (bin[j+2] - bin[j+1]);
            }
        }
        /*System.out.println("Filter 0: "+bank.get(0).length+" "+Arrays.toString((bank.get(0))));
        System.out.println("Filter 1: "+bank.get(1).length+" "+Arrays.toString((bank.get(1))));
        System.out.println("Filter 1: "+bank.get(1).length+" "+Arrays.toString((bank.get(1))));
        System.out.println("Filter "+(bank.size()-1)+": "+bank.get(bank.size()-1).length+" "+Arrays.toString((bank.get(bank.size()-1))));
        */
        return bank;
    }

    /**
     * Convert signal bytes to double value
     * @param signal - signal in bytes
     * @return signal in double value
     */
    public static double[] bytesToDouble(byte signal[]){
        double[] sig = new double[signal.length/2+(signal.length%2 != 0 ? 1 : 0)];
        int cnt = 0;
        for(int i = 1; i<signal.length;i=i+2){
            //System.out.print("["+signal[i]+","+signal[i-1]+"]");
            sig[cnt] = (double)(/*(0b01111111<< 8)&*/(signal[i] << 8) | (signal[i-1] & 0xFF) );
            //sig[cnt] = Math.min(sig[cnt] * 2.0, (double)Short.MAX_VALUE);
            cnt++;
        }
        return sig;
    }

    /**
     * Convert signal bytes to double value and also compute normalization of signal
     * @param signal - signal in bytes
     * @param normalCoeff - normalization filter coefficient
     * @return normalzed signal in double value
     */
    private double[] bytesToDoubleWithNormalization(byte signal[], double normalCoeff){
        double[] sig = new double[signal.length/2+(signal.length%2 != 0 ? 1 : 0)];
        int cnt = 0;
        double prev;
        double act = 0.0;
        for(int i = 1; i<signal.length;i=i+2){
            sig[cnt] = (double)((signal[i] << 8) | (signal[i-1] & 0xFF) );
            prev = act;
            act = sig[cnt];
            if(cnt > 0)
                sig[cnt] = sig[cnt] - normalCoeff * prev;
            cnt++;
        }
        return sig;
    }

    /**
     * Convert one frame to double value
     * @param frames - frame with signal
     * @return signal in one frame in double value
     */
    private ArrayList<double[]> framesToDouble(ArrayList<short[]> frames){
        ArrayList<double[]> arrayOfFrames = new ArrayList<double[]>();
        int ok = 0;
        for(int i = 0; i<frames.size();i++){
            double[] doubleData = new double[frames.get(i).length];
            for (int j = 0; j < frames.get(i).length; j++) {
                doubleData[j] = (double)frames.get(i)[j];// 32768.0;
            }
            arrayOfFrames.add(doubleData);
        }
        return arrayOfFrames;
    }

    /**
     * Frame input signal into frames.
     * @param signal - framing signal
     * @param frameLen - length of each frame
     * @param frameStep - num of samples after the start of previosu frame
     */
    private ArrayList<double[]> framing(double signal[], int frameLen, int frameStep){
        //System.out.println("Frame len: "+frameLen+" frame step: "+frameStep);
        ArrayList<double[]> arrayOfFrames = new ArrayList<double[]>();
        int frames, padLen;
        int samplesCount = signal.length;
        if(samplesCount <= frameLen){
            frames = 1;
        }
        else{
            frames = 1 + (int)(Math.ceil((1.0 * samplesCount - frameLen)/frameStep));
        }
        int i = 0;
        double zerosArr[] = new double[frameLen];
        while((i+frameStep) < samplesCount){
            double[] oneFrame;
            if(i+frameLen < samplesCount) {
                oneFrame = Arrays.copyOfRange(signal, i, i+frameLen);
                arrayOfFrames.add(oneFrame);
            }
            else{
                oneFrame = zerosArr;
                int j = 0;
                while(j+i < samplesCount){
                    oneFrame[j] = signal[j+i];
                    j++;
                }
                arrayOfFrames.add(oneFrame);
            }
            i = i + frameStep;
        }
        /*padLen = (int)((frames - 1) * frameStep + frameLen);
        int zerosArr[] = new int[padLen-samplesCount];
        Arrays.fill(zerosArr, 0);*/
        return arrayOfFrames;
    }

    /**
     * Converting from frequency to Mel scale
     * @param freq - frequency in Hz
     * @return frequency in Mel
     */
    private double frequencyToMel(double freq){
        return 1125.0 * Math.log(1.0 + freq / 700);
    }

    /**
     * Converting from Mel to frequency
     * @param mel - frequency in Mel
     * @return frequency in Hz
     */
    private double melToFrequency(double mel){
        return 700.0 * (Math.pow(10, (mel / 2595.0)) - 1);
    }

    /**
     * Compute delta or delta-delta features from a cepstral coefficients/ delta coeficients.
     * @param mfcc - cepstral coeficients (2D array)  frameNum*cepstralCoeficients
     * @param numOfneighboursFrames - preceding and following N frames
     * @return delta cepstral coeficients
     */
    public ArrayList<double[]> delta(ArrayList<double[]> mfcc, int numOfneighboursFrames){
        //System.out.println("Delta start: "+(new Date()).getTime());
        ArrayList<double[]> mfccPadded = new ArrayList<>();
        ArrayList<double[]> deltaCoeff = new ArrayList<>();
        int denominator = 0;
        if(numOfneighboursFrames < 1){
            Log.e("MFCC delta","Neighbours frames error!");
        }
        int frames = mfcc.size();
        for(int i = 1; i < numOfneighboursFrames+1; i++){
            denominator += Math.pow(i,2);
        }
        denominator = denominator * 2;
        for(int i = 0; i < mfcc.size(); i++){
            deltaCoeff.add(new double[mfcc.get(0).length]);
            if(i == 0 || i == frames-1) {
                for(int j = 0; j < numOfneighboursFrames+1; j++) {
                    mfccPadded.add(mfcc.get(i));
                }
            }
            else{
                mfccPadded.add(mfcc.get(i));
            }
        }
        double sum;
        int mfcc0 = mfcc.get(0).length;
        for(int i = 0; i < frames; i++){
            for(int j=1; j<mfcc0; j++) {
                sum = 0;
                for (int m = i; m < i+(2*numOfneighboursFrames+1); m++) {
                    sum += (-numOfneighboursFrames+(m-i)) * mfccPadded.get(m)[j];
                }
                deltaCoeff.get(i)[j] = sum / denominator;
            }
        }
        //System.out.println("Delta: 0 "+deltaCoeff.get(0).length+" "+Arrays.toString((deltaCoeff.get(0))));
        return deltaCoeff;
    }

    /**
     * Compute Shifted Delta Cepstrum coefficients from delta
     * @param delta - delta coeficients for each frame
     * @param K - number of frames being stacked in SDC
     * @param P - is the amount of frame shift
     * @return SDC for each frame
     */
    public ArrayList<double[]> sdc(ArrayList<double[]> delta, int K, int P){
        ArrayList<double[]> deltaPadded = new ArrayList<>(delta);
        ArrayList<double[]> sdcCoeff = new ArrayList<>();
        int sdcSize = K * (delta.get(0).length - 1);
        for(int i = 0; i < K*P; i++){
            deltaPadded.add(delta.get(delta.size()-1));
        }
        for(int i = 0; i < delta.size(); i++){
            double[] sdcVector = new double[sdcSize];
            for(int j = 0; j < K; j++){
                //Log.i("val:", delta.size()+" "+deltaPadded.size()+" "+(i+j*P)+" "+(j*P)+" "+(deltaPadded.get(i+j*P).length-1));
                System.arraycopy(deltaPadded.get(i+j*P), 1, sdcVector, j*P, deltaPadded.get(i+j*P).length-1);
            }
            sdcCoeff.add(sdcVector);
        }
        return sdcCoeff;
    }

    static double[][] concatMFCCAndDelta2D(ArrayList<double[]> mfcc, ArrayList<double[]> delta){
        int mfccSize = mfcc.get(0).length;
        double[][] result = new double[mfcc.size()][mfccSize+delta.get(0).length];
        for(int i = 0; i < mfcc.size(); i++){
            double[] tmp = mfcc.get(i);
            for(int j = 0; j < tmp.length; j++){
                result[i][j] = tmp[j];
                result[i][j+mfccSize] = tmp[j];
            }
        }
        return result;
    }

    static double[] concatMFCCAndDelta1D(ArrayList<double[]> mfcc, ArrayList<double[]> delta){
        int mfccSize = mfcc.get(0).length;
        int resultSize = mfcc.size()*(mfccSize+delta.get(0).length);
        double[] result = new double[resultSize];;
        double[] deltaTmp;
        double[] mfccTmp;
        int cnt = 0;
        for(int i = 0; i < mfcc.size(); i++){
            for(int j = 0; j < mfcc.get(i).length; j++){
                result[cnt] = mfcc.get(i)[j];
                result[cnt+mfccSize] = delta.get(i)[j];
                cnt++;
            }
            cnt = cnt + mfccSize;
        }
        return result;
    }

    static double[] concatMFCCAndAllDeltas1D(ArrayList<double[]> mfcc, ArrayList<double[]> delta, ArrayList<double[]> deltaDelta){
        int mfccSize = mfcc.get(0).length;
        int deltaSize = delta.get(0).length;
        int resultSize = mfcc.size()*(mfccSize+deltaSize+deltaDelta.get(0).length);
        Log.i("Feature vector size", mfcc.get(0).length+delta.get(0).length+deltaDelta.get(0).length+"");
        double[] result = new double[resultSize];
        double[] deltaTmp;
        double[] mfccTmp;
        int cnt = 0;
        for(int i = 0; i < mfcc.size(); i++){
            for(int j = 0; j < mfcc.get(i).length; j++){
                result[cnt] = mfcc.get(i)[j];
                result[cnt+mfccSize] = delta.get(i)[j];
                result[cnt+mfccSize+deltaSize] = deltaDelta.get(i)[j];
                cnt++;
            }
            cnt = cnt + mfccSize + deltaSize;
        }
        return result;
    }

    public static String doubleArrayToString(double[] coeff){
        StringBuilder result = new StringBuilder("");
        for(int i = 0; i < coeff.length; i++){
            result.append(Double.toString(coeff[i]));
            if(i != coeff.length-1)
                result.append(",");
        }
        return result.toString();
    }

    /**
     * Add class number to features
     * @param cls - class id
     * @param features - feature list
     * @return string in format: classID, features...
     */
    public static String addClassToFeatures(String cls, String features){
        return cls+","+features;
    }
}
