package com.sport.sound;


import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;

/* Singleton */
public final class Store {
    private static Store s = null;
    private String featuresFileName;
    private File folder;
    private File file;

    private Store(){
        featuresFileName = "features.txt";
        File sdcard = Environment.getExternalStorageDirectory();
        // Create folder
        folder = new File(sdcard.getAbsoluteFile(), ".voicesport"); //vytvoreni skryteho adresare pro uzivatele
        if (!folder.mkdirs()){
            Log.d("ERROR:", "Directory not created");
        }

        // Create file
        //createFile();
    }

    /**
     * Create file to save features
     */
    private void createFile(){
        file = new File(folder.getAbsoluteFile(), featuresFileName);
        if (file.exists()) {
            return;
        }
        else{
            try{
                file.createNewFile();
            }
            catch (IOException e){
                Log.d("ERROR:", "Create file "+featuresFileName);
            }
        }
    }

    /**
     * Return instance of Store (singleton)
     * @return instance of Store
     */
    public static Store getStore(){
        if(s != null){
            return s;
        }
        else{
            s = new Store();
            return s;
        }
    }

    /**
     * Save string to file
     * @param s - string to save
     */
    public void saveToFile(String s){
        if (file.exists()) {
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file, true);
                OutputStreamWriter outputWriter = new OutputStreamWriter(out);
                outputWriter.append(s+"\n");
                outputWriter.flush();
                out.flush();
                outputWriter.close();
                out.close();
            }
            catch (IOException e){
                Log.d("ERROR:", "Write to file "+featuresFileName);
            }
        }
        else {
            Log.d("ERROR:", "File "+featuresFileName+" doesn't exist!");
        }
    }

    public static ArrayList<double[]> loadFeatures(BufferedReader br){
        String line;
        ArrayList<double[]> features = new ArrayList<>();
        try {
            while((line = br.readLine()) != null){
                String[] parts = line.split(",");
                double[] tmp = new double[parts.length];
                if(parts.length > 1) {
                    for (int i = 0; i < parts.length; i++) {
                        tmp[i] = Double.valueOf(parts[i]);
                    }
                    features.add(tmp);
                }
            }
        }
        catch (IOException e){
            Log.d("ERROR:", "Error during read!");
        }
        return features;
    }

    /**
     * Load file with features from internal resources
     * @param ctx - context
     * @return object with features and classes
     */
    public static Object[] loadFromFileInternalStore(Context ctx){
        ArrayList<double[]> features = new ArrayList<>();
        InputStream stream = ctx.getResources().openRawResource(ctx.getResources().getIdentifier("features", "raw", ctx.getPackageName()));
        InputStreamReader inputStream = new InputStreamReader(stream);
        BufferedReader bufferedReader = new BufferedReader(inputStream);
        features = loadFeatures(bufferedReader);
        ArrayList<double[]> res = new ArrayList<>();
        double[] cls = new double[features.size()];
        for(int i = 0; i < features.size(); i++){
            cls[i] = features.get(i)[0];
            res.add(Arrays.copyOfRange(features.get(i), 1, features.get(i).length));
        }
        return new Object[]{res, cls};
    }

    /**
     * Load file with features from external storage
     * @return object with features and classes
     */
    public Object[] loadFromFile(){
        ArrayList<double[]> features = new ArrayList<>();
        if (file.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                features = loadFeatures(br);
                /*String line;
                while((line = br.readLine()) != null){
                    String[] parts = line.split(",");
                    double[] tmp = new double[parts.length];
                    if(parts.length > 1) {
                        for (int i = 0; i < parts.length; i++) {
                            tmp[i] = Double.valueOf(parts[i]);
                        }
                        features.add(tmp);
                    }
                }*/
            }
            catch (IOException e){
                Log.d("ERROR:", "Error during read!");
            }
        }
        else {
            Log.d("ERROR:", "File didn't load. File "+featuresFileName+" doesn't exist!");
        }
        ArrayList<double[]> res = new ArrayList<>();
        //double[][] result = new double[features.size()][features.get(0).length];
        double[] cls = new double[features.size()];
        for(int i = 0; i < features.size(); i++){
            cls[i] = features.get(i)[0];
            res.add(Arrays.copyOfRange(features.get(i), 1, features.get(i).length));
        }
        return new Object[]{res, cls};
    }

    /**
     * Delete file content
     */
    public void deleteFileContent(){
        if(file.exists()) {
            file.delete();
            createFile();
        }
    }
}