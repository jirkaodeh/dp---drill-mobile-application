package com.sport.sound;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.sport.FinishActivity;
import com.sport.RunDrillActivity;
import com.sport.utils.Permission;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Record {
    public static int runningTask = 0;
    private Context ctx;
    private RunDrillActivity actR;
    private FinishActivity actF;
    private RecordTask recordTask = null;
    private ArrayList<byte[]> buf;
    private int detectedClass;
    // Record parameters
    private static final int SAMPLE_RATE = 16000;
    private static final int BUFFER_SIZE = 6 * AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
    private Set<Integer> classes;
    private int waiting = 0;
    private boolean hardStart = false;

    private boolean play = true;

    public Record(Context ctx, RunDrillActivity act){
        this.ctx = ctx;
        this.actF = null;
        this.actR = act;
        recordTask = new RecordTask(0);
        classes = new HashSet<Integer>();
    }

    public Record(Context ctx, FinishActivity act){
        this.ctx = ctx;
        this.actF = act;
        this.actR = null;
        recordTask = new RecordTask(0);
        classes = new HashSet<Integer>();
    }

    public void startRecord(){
        if(actR != null) {
            Permission.checkPermisionForRecordSound(ctx, actR);
            Permission.verifyStoragePermissions(actR);
        }
        else if(actF != null){
            Permission.checkPermisionForRecordSound(ctx, actF);
            Permission.verifyStoragePermissions(actF);
        }
        mStatusChecker.run();
    }

    public void startRecord(int wait, boolean hard){
        this.hardStart = hard;
        this.waiting = wait;
        if(actR != null) {
            Permission.checkPermisionForRecordSound(ctx, actR);
            Permission.verifyStoragePermissions(actR);
        }
        else if(actF != null){
            Permission.checkPermisionForRecordSound(ctx, actF);
            Permission.verifyStoragePermissions(actF);
        }
        mStatusChecker.run();
    }

    public void stopRecord(){
        if (!recordTask.isCancelled() && recordTask.getStatus() == AsyncTask.Status.RUNNING) {
            recordTask.cancel(true);
            while(!recordTask.isCancelled()){
                try {
                    Thread.sleep(50);
                } catch (Exception e){}
            }
        } else {
            Log.i("RecordTask", "Task not running.");
            //Toast.makeText(act, "Task not running.", Toast.LENGTH_SHORT).show();
        }
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            if(hardStart){
                recordTask = new RecordTask(waiting);
                waiting = 0;
                recordTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                return;
            }
            hardStart = false;
            try {
                switch (recordTask.getStatus()) {
                    case RUNNING:
                        Log.i("Task", "Task already running...");
                        //recordTask = new RecordTask();
                        return;
                    case FINISHED:
                        recordTask = new RecordTask(waiting);
                        break;
                    case PENDING:
                        if (recordTask.isCancelled()) {
                            recordTask = new RecordTask(waiting);
                        }
                }
                waiting = 0;
                recordTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);//execute();
            } finally {

            }
        }
    };

    /**
     * Concatenate two byte array
     * @param first - first array
     * @param second - second array
     * @return return concatenated array [first second]
     */
    public static byte[] concatenate(byte[] first, byte[] second){
        byte[] o = new byte[first.length + second.length];
        System.arraycopy(first, 0, o, 0, first.length);
        System.arraycopy(second, 0, o, first.length, second.length);
        return o;
    }

    /**
     * Add all controll classes into detect set
     */
    public void initClassForDetection(){
        for(int i = 14; i < 19; i++){
            classes.add(i);
        }
    }

    /**
     * Add array classes id to class set  for detection
     * @param clsIdArr - collection of class id
     */
    public void addClass(Collection<Integer> clsIdArr){
        classes.addAll(clsIdArr);
    }

    /**
     * Add one class id to class set  for detection
     * @param clsId - class id
     */
    public void addClass(int clsId){
        classes.add(clsId);
    }

    /**
     * Remove array classes id from class set  for detection
     * @param clsIdArr - collection of class id
     */
    public void removeClass(Collection<Integer> clsIdArr) {
        classes.removeAll(clsIdArr);
    }

    /**
     * Remove one class id from class set for detection
     * @param clsId - class id
     */
    public void removeClass(int clsId){
        classes.remove(clsId);
    }

    public void removeAllClass(){
        classes.clear();
    }

    public class RecordTask extends AsyncTask<Void, Void, Object[]> {
        AudioTrack track;
        String s;
        int wait = 0;
        boolean newStart = true;

        private RecordTask(int wait) {

            //this.ctx = ctx;
            this.wait = wait;
            buf = new ArrayList<byte[]>();
            s = "";
            runningTask++;
        }

        @Override
        protected Object[] doInBackground(Void... params) {
            AudioRecord audioRecord = null;
            long startTime = 0;
            long endTime = 0;
            int read;
            boolean run;
            Log.i("Record", "Record");
            try {
                // Open our two resources
                audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                        AudioFormat.ENCODING_PCM_16BIT, BUFFER_SIZE);
                int minSize = AudioTrack.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_CONFIGURATION_MONO
                        , AudioFormat.ENCODING_PCM_16BIT);
                track = new AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE_RATE, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                        AudioFormat.ENCODING_PCM_16BIT, minSize, AudioTrack.MODE_STREAM);

                run = true;
                // Let's go
                startTime = SystemClock.elapsedRealtime();
                audioRecord.startRecording();

                int i = 0, cnt = 0;
                double stop, start;
                ArrayList<double[]> d = new ArrayList<>();
                if(wait != 0) {
                    Log.i("Wait", ""+wait);
                    try {
                        Thread.sleep(wait);
                    } catch (Exception e) {}
                }
                while (run && !isCancelled()) {
                    if (cnt == 0) {
                        d = new ArrayList<>();
                    }
                    cnt++;
                    byte[] buffer = new byte[BUFFER_SIZE];
                    read = audioRecord.read(buffer, 0, buffer.length);
                    //Log.i("running", "" + i);
                    //buf.add(buffer);
                    start = System.currentTimeMillis();
                    //System.out.println("Buffer "+buffer.length);
                    MFCC mfcc = new MFCC(buffer, SAMPLE_RATE, 13, 0.025, 0.01, 512);
                    ArrayList<double[]> mfccResult = mfcc.mfcc();
                    ArrayList<double[]> deltaResult = mfcc.delta(mfccResult, 2);
                    double[] result = MFCC.concatMFCCAndDelta1D(mfccResult, deltaResult);
                    d.add(result);
                    if (cnt < 2)
                        continue;
                    else {
                        double[] r = new double[d.get(0).length + d.get(1).length];
                        System.arraycopy(d.get(0), 0, r, 0, d.get(0).length);
                        System.arraycopy(d.get(1), 0, r, d.get(0).length, d.get(1).length);
                        d.clear();
                        d.add(r);
                        cnt = 0;
                    }
                    stop = System.currentTimeMillis();
                    System.out.println("MFCC time: " + ((stop - start) / 1000.0));
                    start = stop;
                    // detection by model
                    detectedClass = SVM.callSVM(d, new double[]{0});
                    if(newStart){
                        newStart = false;
                        continue;
                    }
                    if(classes.contains(detectedClass)){
                        return new Object[]{endTime - startTime};
                    }

                    stop = System.currentTimeMillis();
                    System.out.println("SVM time: " + ((stop - start) / 1000.0) + " real: " + (0.10 + (BUFFER_SIZE*2 / 400 * 0.01) + 0.05));
                    //track.write(buffer, 0, buffer.length);
                    //track.play();
                    Log.i("Running tasks ", ""+runningTask);
                    i++;
                }
            } catch (Exception ex) {
                Log.i("Record", ""+ex);
                return new Object[]{ex};
            } finally {
                if (audioRecord != null) {
                    try {
                        if (audioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                            audioRecord.stop();
                            endTime = SystemClock.elapsedRealtime();
                        }
                    } catch (IllegalStateException ex) {
                        //
                    }
                    if (audioRecord.getState() == AudioRecord.STATE_INITIALIZED) {
                        audioRecord.release();
                    }
                }
            }
            return new Object[]{endTime - startTime};
        }

        @Override
        protected void onCancelled(Object[] results) {
            // Handling cancellations and successful runs in the same way
            //onPostExecute(results);
        }

        @Override
        protected void onPostExecute(final Object[] o) {
            boolean repeat = false;
            if(actR != null){
                repeat = actR.detectedHandler(detectedClass);
            } else if(actF != null){
                repeat = actF.detectedHandler(detectedClass);
            }
            if(repeat){
                recordTask = new RecordTask(0);
                recordTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                this.cancel(true);
            }
            /*if(play) {
                int i = 0;
                while (i < buf.size()) {
                    track.write(buf.get(i), 0, buf.get(i).length);
                    track.play();
                    i++;
                }
                Log.i("Play", "" + buf.size());
            }*/
        }
    }
}
