package com.sport;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.sport.database.PreferenceStore;
import com.sport.sound.Train;
import com.sport.utils.Permission;

public class WelcomeActivity extends AppCompatActivity {
    private int DELAY = 1000;
    private ProgressBar pb;
    private Handler progressBarHandler = new Handler();
    private Activity ctx;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // hide status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_welcome);
        pb = findViewById(R.id.progress);
        if(Train.isModelTrained()) {
            DELAY = 800;
            pb.setVisibility(View.GONE);
        }
        else {
            DELAY = PreferenceStore.getAppLoadingTime(this)*1000;
            pb.setVisibility(View.VISIBLE);
        }
        ctx = this;
        //Permission.permisionForReadExternalStorage(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Train.doTrain(getApplicationContext());
                try {
                    Thread.sleep(800);
                } catch (Exception e){}
                progressBarHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        pb.setProgress(12);
                    }
                });

                while(pb.getProgress() < 100){
                    progressBarHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            pb.setProgress(pb.getProgress() + 1);
                        }
                    });
                    try {
                        Thread.sleep(DELAY/88);
                    } catch (Exception e){}
                }
                Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }).start();
        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Train.doTrain(getApplicationContext());
                Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, DELAY); //Start MainActivity after 5s*/
        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int progress = pb.getProgress();
                while(progress < 100){
                    progress += 2;
                    pb.setProgress(progress);
                    try {
                        Thread.sleep(DELAY/50);
                    } catch (Exception e){}
                }
            }
        });*/
    }

    @Override
    public void onResume(){
        super.onResume();
    }
}
