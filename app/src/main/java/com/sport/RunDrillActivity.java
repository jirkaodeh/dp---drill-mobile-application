package com.sport;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gauravbhola.ripplepulsebackground.RipplePulseLayout;
import com.sport.database.PreferenceStore;
import com.sport.database.SQLiteDB;
import com.sport.lists.DrillInSet;
import com.sport.lists.DrillSet;
import com.sport.lists.RunDrillAdapter;
import com.sport.lists.Snd;
import com.sport.sound.FeatureFile;
import com.sport.sound.MFCC;
import com.sport.sound.Record;
import com.sport.sound.SVM;
import com.sport.utils.Color;
import com.sport.utils.HelpDialogs;
import com.sport.utils.ListUtils;
import com.sport.utils.Permission;
import com.sport.utils.ProgressStatus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class RunDrillActivity  extends BasicMain {

    private static final long COUNTDOWN_START_TIME = 5000; // countdown start time in miliseconds (5 seconds)
    private int DRILL_SET_ID = 0;
    private int DRILL_SET_COLOR = 0;
    private int DRILL_SET_MODE = 0;
    private String DRILL_SET_NAME, DRILL_SET_GROUP;

    private TextView circleDrillNameText;   // drill name in circle bar
    private TextView circleDrillTimeText;   // drill time in circle bar
    private TextView runDrillCounterText;   // show actual execute drill
    private TextView nextDrillNameText;
    private ProgressBar mProgress;      // progress circle bar
    public Chronometer chronometer;    // time chronometer
    private CountDownTimer prepareCountdownTimer;     // countdown timer
    private boolean chronometerRunning; // indicate if chronometer running or not (true - running)
    private boolean prepareCountdownRuning; // indicate if countdown running or not (true - runnig)
    private boolean prepareCountdownEnd;    // indicate if countdown end (true - end)
    private long prepareCountdownTimeLeft = COUNTDOWN_START_TIME;
    private long pauseOffset;   // chronometer offset during pause
    private TextToSpeech tts;   // TTS engine
    private boolean volume;     // is volume allowed
    private boolean listScreenActive; // indicate that screen with drill list is activate
    private String[] countdownTextArr;    // speak countdown
    private int prepareCountdownLastArrIndex = 2;       // count down index to countdownTextArr
    private Drawable drawable;
    private ImageButton volumeBtn, prevBtn, nextBtn, stopBtn;
    public Button playBtn, changeScreenBtn;
    View includedLayout;
    private ListView lv;        // list with drills
    public RunDrillAdapter lvAdapter;  // list adapter
    private MediaPlayer whistleMP;      // whistle sound played during start drill set
    private MediaPlayer beepMP;
    private int drillSetCounter, maxDrillSet, inOneIteration = 0; // indicate in whitch iteration is actual program
    private int actualIteration = 0;
    int actualExecutedDrill;    // number of drill that is actualy executed
    List<DrillInSet> drillsArray;   // list of drill prepare to execute
    List<DrillInSet> drillsArrayFinished;   // list of finished drills
    private boolean pause = false;      // pause flag
    private boolean isSetPause = false; // detection if is set pause during actual drill
    private long setCompleteId;
    private ExecuteDrillTask drillTask = null;      // asynch task for execute drills
    public boolean chronometerEndCount = true;      // flag - detect end of counting of drill
    // sound detect atributes
    public Record rec;
    public boolean sndDetected = false;
    public int needDetect;
    public int actualDetect;
    public Snd actualSndDetect = null;
    private ArrayList<Snd> sndArr;
    // sound detect dialog
    public AlertDialog sndDialog = null;
    private TextView sndWaitTitleTv;
    private Chronometer chron;
    private RipplePulseLayout rpl;
    private Button playBtnDialog;

    public RunDrillActivity act;
    public boolean moveTonextPrewDrillIndicator = false;    // indicate move to other drill
    public boolean moveOnlyToPrev = false;
    private boolean prefImmediatelyCounting;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_drill);

        prefImmediatelyCounting = PreferenceStore.getImmediatelyCountingStatus(this);
                includedLayout = findViewById(R.id.run_middle_list);
        lv = (ListView) includedLayout.findViewById(R.id.list);
        act = this;

        countdownTextArr = getResources().getStringArray(R.array.counting_array);

        //intents
        final Intent inIntent = getIntent();
        Bundle bundle = inIntent.getExtras();
        if(bundle != null) {
            DRILL_SET_ID = (int) bundle.get("DRILL_SET_ID");
            DRILL_SET_NAME = (String) bundle.get("DRILL_SET_NAME");
            DRILL_SET_GROUP = (String) bundle.get("DRILL_SET_GROUP");
            DRILL_SET_COLOR = (int) bundle.get("DRILL_SET_COLOR");
            DRILL_SET_MODE = (int) bundle.get("DRILL_SET_MODE");
            super.changeToolbarTitle(DRILL_SET_NAME);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(DRILL_SET_COLOR))));
        }

        // check if drill set isn't empty
        getDrillData();
        sndArr = Snd.getAllSndData(this, true);
        drillsArrayFinished = new ArrayList<>();
        if(drillsArray.size() == 0){
            HelpDialogs.alertNoDrillDialog(this, this);
        }
        // prepare list view with drills
        lvAdapter = new RunDrillAdapter(this, R.layout.list_item_run_drill, drillsArray, this);
        lv.setAdapter(lvAdapter);
        ListUtils.setDynamicHeight(lv);
        lv.setVisibility(View.VISIBLE);

        //allow volume
        volume = true;

        // start whistle sound
        whistleMP = MediaPlayer.create(this, R.raw.whistle);
        beepMP = MediaPlayer.create(this, R.raw.beep);

        // list Screen not showed
        listScreenActive = false;

        // edit toolbaru
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // circle bar
        final Resources res = getResources();
        drawable = res.getDrawable(R.drawable.circular);
        mProgress = (ProgressBar) findViewById(R.id.run_circular_progressbar);
        resetCircleProgress();

        // record init
        rec = new Record(act.getApplicationContext(), act);
        rec.initClassForDetection();
        rec.removeClass(16); // remove STOP


        /*Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, RESULT_OK);*/
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    Locale loc = new Locale("cs");
                    int result = tts.setLanguage(loc);

                    if(result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("TTS", "Nepodporovany jazyk");
                        Intent installIntent = new Intent();
                        installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                        startActivity(installIntent);
                        result = tts.setLanguage(loc);
                    }
                    else{
                        Log.i("TTS", "start countdown timer");
                        // start countdown timer
                        if(drillsArray.size() != 0) {
                            startPrepareTimer();
                            if(!prefImmediatelyCounting) {
                                playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_play_white,0);
                                prepareCountdownRuning = false;
                                prepareCountdownTimer.cancel();
                            }

                        }
                    }
                }
                else{
                    Log.e("TTS", "Init fail");
                }
            }
        });
        speakerSettings();

      /*  ObjectAnimator animation = ObjectAnimator.ofInt(mProgress, "progress", 0, 100);
        animation.setDuration(50000);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();*/

        // texts
        circleDrillNameText = (TextView) findViewById(R.id.circle_drill_text);
        circleDrillTimeText = (TextView) findViewById(R.id.circle_time_text);
        nextDrillNameText = (TextView) findViewById(R.id.circle_next_drill_text);
        runDrillCounterText = (TextView) findViewById(R.id.run_drill_counter);
        drillCounterUpdate(0);

        // buttons
        playBtn = (Button) findViewById(R.id.run_play);
        changeScreenBtn = (Button) findViewById(R.id.run_change_type_of_screen);
        volumeBtn = (ImageButton) findViewById(R.id.run_control_volume);
        prevBtn = (ImageButton) findViewById(R.id.run_control_prev);
        nextBtn = (ImageButton) findViewById(R.id.run_control_next);
        stopBtn = (ImageButton) findViewById(R.id.run_control_stop);

        actualExecutedDrill = 0;
        if(drillsArray.size() > 0) {
            actualDrillCircleUpdate(actualExecutedDrill, nextDrillNameText, true);
        }

        int drills = ((drillsArray.size() == 0) ? 1 : drillsArray.size());
        final float tick = 100 / drills;
        // set time chronometer
        chronometer = (Chronometer) findViewById(R.id.run_drill_timer);
        chronometer.setFormat("%s");
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                int act = actualExecutedDrill+1;
                if(act == drillsArray.size()){
                    mProgress.setProgress(100);
                    circleDrillTimeText.setText("100%");
                }
                if(act < drillsArray.size()){
                    mProgress.setProgress((int)(tick*act));
                    actualDrillCircleUpdate(actualExecutedDrill, circleDrillNameText, false);
                    circleDrillTimeText.setText((int)(tick*act) + "%");
                }
            }
        });

        // buttons listeners
        // play button
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  control countdown timer
                if(!prepareCountdownEnd){
                    if(!prepareCountdownRuning){
                        startPrepareTimer();
                        prepareCountdownRuning = true;
                        playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_pause_white,0);
                    }
                    else{
                        playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_play_white,0);
                        prepareCountdownRuning = false;
                        prepareCountdownTimer.cancel();
                    }
                }
                // control chronometer
                else{
                    if(!chronometerRunning) {
                        startChronometer();
                        //playBtn.setBackgroundResource(R.drawable.ic_pause_white);
                        playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_pause_white,0);
                    }
                    else{
                        pauseChronometer();
                        //playBtn.setBackgroundResource(R.drawable.ic_play_white);
                        playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_play_white,0);
                    }
                }
            }
        });

        // stop button circle_drill_countdown
        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseChronometer();
                runFinishActivity();
            }
        });

        // preview button
        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevBtnHandler();
            }
        });

        // next button
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextBtnHandler();
            }
        });

        // init volume button state
        volume = PreferenceStore.getSoundStatus(this);
        if(volume){
            volumeBtn.setImageResource(R.drawable.ic_volume_on_black);
        }
        else {
            volumeBtn.setImageResource(R.drawable.ic_volume_off_black);
        }
        // volume on/off button
        volumeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(volume){
                    volume = false;
                    volumeBtn.setImageResource(R.drawable.ic_volume_off_black);
                    PreferenceStore.setSoundStatus(getApplicationContext(), volume);
                }
                else{
                    volume = true;
                    volumeBtn.setImageResource(R.drawable.ic_volume_on_black);
                    PreferenceStore.setSoundStatus(getApplicationContext(), volume);
                }
            }
        });

        // change circle/list screen button
        changeScreenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout list_screen = (LinearLayout)findViewById(R.id.run_middle_list_box);
                RelativeLayout circle_screen = (RelativeLayout)findViewById(R.id.run_middle_circle_box);
                if(listScreenActive){
                    listScreenActive = false;
                    list_screen.setVisibility(View.INVISIBLE);
                    circle_screen.setVisibility(View.VISIBLE);
                    changeScreenBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_list_white,0);
                }
                else{
                    listScreenActive = true;
                    circle_screen.setVisibility(View.GONE);
                    list_screen.setVisibility(View.VISIBLE);
                    changeScreenBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_circle_white,0);
                }
            }
        });

        SQLiteDB db = new SQLiteDB(this);
        String time =new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(Calendar.getInstance().getTime());
        setCompleteId = db.insertSetComplete(DRILL_SET_ID, 0, time, 0, "", DRILL_SET_MODE,0);

    }

    /**
     * Change drill do preview
     */
    public void prevBtnHandler(){
        if(drillTask != null && actualExecutedDrill > 0){
            speakerStop();
            moveOnlyToPrev = true;
            lvAdapter.getItem(actualExecutedDrill).stopAndPrevDrill = true;
            lvAdapter.getItem(actualExecutedDrill-1).progressStaus = ProgressStatus.PREPARE;
            lvAdapter.getItem(actualExecutedDrill-1).chronometer = false;
            lvAdapter.getItem(actualExecutedDrill-1).chronometerPaused = false;
            lvAdapter.getItem(actualExecutedDrill-1).chronometerContinue = false;
            lvAdapter.getItem(actualExecutedDrill-1).stopAndResetDrill = false;
            for(int i = actualExecutedDrill; i < lvAdapter.getCount(); i++){
                lvAdapter.getItem(i).progressStaus = ProgressStatus.PREPARE;
            }
            lvAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Change drill do next
     */
    public void nextBtnHandler(){
        if(drillTask != null && actualExecutedDrill < (drillsArray.size())) {
            speakerStop();
            //stopRepeatingTask();
            moveTonextPrewDrillIndicator = true;
            lvAdapter.getItem(actualExecutedDrill).stopAndResetDrill = true;
            for(int i = actualExecutedDrill+1; i < lvAdapter.getCount(); i++){
                lvAdapter.getItem(i).progressStaus = ProgressStatus.PREPARE;
            }
            lvAdapter.notifyDataSetChanged();
            //startRepeatingTask();
        }
    }


    /**
     * Signalization from chronometer to continue on next drill
     */
    public void next(){
        //lvAdapter.getItem(actualExecutedDrill).chronometer = false;
        //lvAdapter.notifyDataSetChanged();
        chronometerEndCount = true;
        System.out.println("signalized "+chronometerEndCount);
        /*lvAdapter.getItem(0).chronometer = false;
        lvAdapter.getItem(5).chronometer = true;*/
        //lvAdapter.notifyDataSetChanged();
    }

    /**
     * Get drill data set maxDrillSet ( repeat of set) and all drills in set drillsArray (list of drills)
     */
    private void getDrillData(){
        // get all drill sets from DB
        SQLiteDB db = new SQLiteDB(this);
        drillsArray = new ArrayList<>();
        Cursor itemFromDB = db.getDrillSetById(DRILL_SET_ID);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            maxDrillSet = itemFromDB.getInt(itemFromDB.getColumnIndex("repeat"));
        }
        itemFromDB = db.getAllDrillsFromDrillSet(DRILL_SET_ID);
        for(int i = 0; i < maxDrillSet; i++){
            for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
                Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id_drill_in_set"));
                Integer drillId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_id"));
                Integer position = itemFromDB.getInt(itemFromDB.getColumnIndex("position"));
                Boolean isPause = itemFromDB.getInt(itemFromDB.getColumnIndex("is_pause")) == 1;
                Integer pauseTime = itemFromDB.getInt(itemFromDB.getColumnIndex("pause_time"));
                Integer variantXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_x_times"));
                String editName = itemFromDB.getString(itemFromDB.getColumnIndex("edit_name"));
                String drillName = itemFromDB.getString(itemFromDB.getColumnIndex("drill_name"));
                Boolean prev = itemFromDB.getInt(itemFromDB.getColumnIndex("joint_to_prev_drill")) == 1;
                Boolean timeWait = itemFromDB.getInt(itemFromDB.getColumnIndex("time_wait")) == 1;
                Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("time"));
                Boolean alternative = itemFromDB.getInt(itemFromDB.getColumnIndex("alternative")) == 1;
                Integer varPosition = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_position"));
                Boolean sndWait = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_wait")) == 1;
                Integer snd = itemFromDB.getInt(itemFromDB.getColumnIndex("snd"));
                Boolean inflection = itemFromDB.getInt(itemFromDB.getColumnIndex("inflection")) == 1;
                Integer sndDetectXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_detect_x_times"));
                Boolean shuffle = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle")) == 1;
                Boolean shuffleModify = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle_modify")) == 1;
                DrillInSet dis = new DrillInSet(id, drillId, position, isPause, pauseTime, variantXTimes, alternative, drillName, editName,
                        prev, timeWait, time, varPosition, sndWait, snd, sndDetectXTimes, inflection, shuffle, shuffleModify);
                dis.setIteration(i+1);
                drillsArray.add(dis);
                inOneIteration++;
            }
            // random mode active - no repeat drill set
            if(DRILL_SET_MODE != 0)
                break;
        }
        itemFromDB.close();
        db.close();
        if(DRILL_SET_MODE != 0){
            shuffleDrills();
        }
    }

    /**
     * Get sound by sound class identifier
     * @param sndClsId - sound class identifier
     * @return SND object with sound info
     */
    private Snd getSndByClassId(int sndClsId){
        for(Snd s : sndArr){
            if(sndClsId == s.getCls())
                return s;
        }
        return null;
    }

    /**
     * Shuffle dril during random execute drill set
     */
    private void shuffleDrills(){
        ArrayList<ArrayList<DrillInSet>> strustureList = new ArrayList<>();
        for(int i = 0; i < drillsArray.size(); i++){
            if(drillsArray.get(i).isShuffle()) {
                if (i > 0 && drillsArray.get(i).isJointToPrevDrill()) {
                    strustureList.get(strustureList.size() - 1).add(drillsArray.get(i));
                } else {
                    strustureList.add(new ArrayList<DrillInSet>());
                    strustureList.get(strustureList.size() - 1).add(drillsArray.get(i));
                }
            }
        }
        drillsArray.clear();
        ArrayList<Integer> strustureListSize = new ArrayList<>();
        while(strustureListSize.size() != strustureList.size()){
            int rand = (int)(Math.random() * strustureList.size());
            if(isIntInArray(rand, strustureListSize))
                continue;
            strustureListSize.add(rand);
            for (int i = 0; i < strustureList.get(rand).size(); i++){
                drillsArray.add(strustureList.get(rand).get(i));
                if(drillsArray.get(drillsArray.size()-1).isAlternative() && drillsArray.get(drillsArray.size()-1).isShuffleModify()){
                    drillsArray.get(drillsArray.size()-1).makeShuffleDrillModify(this);
                }
            }
        }
    }

    /**
     * Search if value is in array
     * @param value - int value
     * @param arr - array of int
     * @return true - value is in array, false - not
     */
    private boolean isIntInArray(int value, ArrayList<Integer> arr){
        for(int i: arr){
            if(value == i)
                return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.running_drill_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_info:
                if(!prepareCountdownEnd){
                    if(prepareCountdownRuning){
                        playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_play_white,0);
                        prepareCountdownRuning = false;
                        prepareCountdownTimer.cancel();
                    }
                }
                else{
                    pauseChronometer();
                    playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_play_white,0);
                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    // reset state of circle bar
    private void resetCircleProgress(){
        mProgress.setProgress(0);   // Main Progress
        mProgress.setSecondaryProgress(100); // Secondary Progress
        mProgress.setMax(100); // Maximum Progress
        mProgress.setProgressDrawable(drawable);
    }

    // start time chronometer counting
    public void startChronometer(){
        if(!chronometerRunning){
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            chronometerRunning = true;

            if(pause) {
                startRunTask();
            }
            startRecordOperator();
        }
    }

    // pause time chronometer counting
    public void pauseChronometer(){
        if(chronometerRunning){
            isSetPause = true;
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            chronometerRunning = false;

            // stop speaker, asynch task, chronometer in list
            speakerStop();
            stopRunTask();
            if(drillsArray.size() > actualExecutedDrill) {
                lvAdapter.getItem(actualExecutedDrill).chronometer = false;
                lvAdapter.getItem(actualExecutedDrill).chronometerPaused = true;
            }
            lvAdapter.notifyDataSetChanged();
            chronometerEndCount = true;
            pause = true;
            pauseRecordOperator();
        }
    }

    // stop and restart time chronometer counting
    public void resetChronometer(){
        chronometer.setBase(SystemClock.elapsedRealtime());
        pauseOffset = 0;
    }

    /**
     * Start prepare countdown timer
     */
    private void startPrepareTimer(){
        Log.i("startPrepareTimer", "startPrepareTimer");
        prepareCountdownTimer = new CountDownTimer(prepareCountdownTimeLeft, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                prepareCountdownTimeLeft = millisUntilFinished;
                updatePrepareCountdownText();
            }

            @Override
            public void onFinish() {
                prepareCountdownEnd = true;
                prepareCountdownRuning = false;
                startChronometer();
                actualDrillCircleUpdate(actualExecutedDrill+1, nextDrillNameText, true);
                //circleDrillNameText.setText("Timer OK");
                // play whistle sound
                playSound(whistleMP);
                // start drill execute
                rec.addClass(16); // add STOP
                startRunTask();
            }
        }.start();
        prepareCountdownRuning = true;
    }

    /**
     * Update countdown time before dril start
     */
    private void updatePrepareCountdownText(){
        int minutes = (int)(prepareCountdownTimeLeft / 1000) / 60;
        int seconds = (int)(prepareCountdownTimeLeft / 1000) % 60;
        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        circleDrillNameText.setText(timeLeftFormatted);
        if(prepareCountdownLastArrIndex >= 0 && prepareCountdownLastArrIndex < 3 && seconds == prepareCountdownLastArrIndex+1) {
            speak(countdownTextArr[prepareCountdownLastArrIndex]);
            Log.i("SPEAK", " "+prepareCountdownLastArrIndex);
            prepareCountdownLastArrIndex--;
        }
    }

    /**
     * start TTS speak
     * @param speakStr - string to speak
     */
    private void speak(String speakStr){
        if(volume) {
            tts.speak(speakStr, TextToSpeech.QUEUE_ADD, null);
        }
    }

    /**
     * Stop speaking
     */
    private void speakerStop(){
        tts.stop();
    }

    /**
     * Set TTS atributes from shared preferences
     */
    private void speakerSettings(){
        if(tts != null) {
            int speed = PreferenceStore.getTTSSpeed(this);
            int pitch = PreferenceStore.getTTSPitch(this);
            //Log.i("Settings", "TTS"+(float)(pitch/100.0)+" "+(float)(speed/100.0));
            tts.setPitch((float)(pitch/100.0));
            tts.setSpeechRate((float)(speed/100.0));
        }
    }

    /**
     * Play sound in mp
     * @param mp - sound that may be play
     */
    private void playSound(MediaPlayer mp){
        if(volume) {
            mp.start();
        }
    }

    /**
     * Update text in drill counter
     * @param actualDrill - actual executed drill
     */
    private void  drillCounterUpdate(int actualDrill){
        runDrillCounterText.setText(actualDrill+"/"+drillsArray.size());
    }

    private void actualDrillCircleUpdate(int actualDrill, TextView textView, boolean next) {
        DrillInSet drill;
        try {
            drill = drillsArray.get(actualDrill);
        } catch (IndexOutOfBoundsException e){
            textView.setText("");
            return;
        }
        if(drill != null) {
            if (drill.isPause()) {
                textView.setText((next ? "Následuje: " : "") + drill.getPauseTimeInString(true));
            } else {
                textView.setText((next ? "Následuje: " : "") + drill.getName());
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        /*if(whistleMP != null) {
            whistleMP.release();
        }*/
        pauseChronometer();
        if(sndDialog != null){
            sndDialog.dismiss();
        }
        if(rec != null){
            rec.stopRecord();
        }
        /*speakerStop();
        stopRunTask();
        if(prepareCountdownTimer != null)
            prepareCountdownTimer.cancel();*/
    }

    @Override
    public void onResume(){
        speakerSettings();
        if(sndDialog != null){
            sndDialog.show();
        }
        if(rec != null){
            rec.startRecord();
        }
        super.onResume();
    }

    @Override
    public void onStop(){
        /*if(whistleMP != null) {
            whistleMP.release();
        }
        pauseChronometer();
        speakerStop();
        stopRunTask();
        if(prepareCountdownTimer != null)
            prepareCountdownTimer.cancel();
        finish();
        Log.i("RunDrillActivity", "STOP");*/
        if(sndDialog != null){
            sndDialog.dismiss();
        }
        if(rec != null){
            rec.stopRecord();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(whistleMP != null) {
            whistleMP.release();
        }
        pauseChronometer();
        speakerStop();
        stopRunTask();
        if(tts != null){
            tts.shutdown();
            Log.i("TTS", "shutdown");
        }
        if(prepareCountdownTimer != null)
            prepareCountdownTimer.cancel();
        Log.i("RunDrillActivity", "DESTROY");
        this.finish();
    }

    @Override
    public void onBackPressed(){
        pauseChronometer();
        super.onBackPressed();
    }

    private void runFinishActivity(){
        Context ctx = getApplicationContext();
        Intent intent = new Intent(ctx, FinishActivity.class);
        intent.putExtra("DRILL_SET_ID", DRILL_SET_ID);
        intent.putExtra("DRILL_SET_NAME", DRILL_SET_NAME);
        intent.putExtra("DRILL_SET_COLOR", DRILL_SET_COLOR);
        intent.putExtra("DRILL_SET_MODE", DRILL_SET_MODE);
        intent.putExtra("DRILL_SET_GROUP", DRILL_SET_GROUP);
        intent.putExtra("FINISH_PERCENTAGE", finishPercentage());
        if(prepareCountdownLastArrIndex > 0)
            intent.putExtra("ELAPSED_TIME", (long)0);
        else
            intent.putExtra("ELAPSED_TIME", SystemClock.elapsedRealtime() - chronometer.getBase());
        intent.putExtra("ITERATION", actualIteration);
        intent.putExtra("SET_COMPLETE_ID", setCompleteId);
        startActivity(intent);
        rec.stopRecord();
        this.finish();
    }

    private int finishPercentage(){
        ArrayList<DrillInSet> tmp = new ArrayList<>();
        boolean write;
        for(int i = 0; i < drillsArrayFinished.size(); i++){
            write = false;
            for(int j = 0; j < tmp.size(); j++){
                if(drillsArrayFinished.get(i).getIdDrill() == tmp.get(j).getIdDrill() && drillsArrayFinished.get(i).getIteration() == tmp.get(j).getIteration())
                    write = true;
            }
            if(!write){
                tmp.add(drillsArrayFinished.get(i));
            }
        }
        if(actualExecutedDrill < drillsArray.size() && drillsArray.get(actualExecutedDrill).isAlternative() && tmp.size() > 0)
            return (int)(100.0 / drillsArray.size() * (tmp.size()-1));
        return (int)(100.0 / drillsArray.size() * tmp.size());
    }


    private void storeIntermediateInfo(boolean skipped, String note){
        int skip = (skipped ? 1 : 0);
        long time = SystemClock.elapsedRealtime() - chronometer.getBase();
        int actIter = (actualIteration == 0 ? 1 : actualIteration);
        SQLiteDB db = new SQLiteDB(this);
        db.insertIntermediateTime((int)setCompleteId, drillsArray.get(actualExecutedDrill).getIdDrill_drill_table(), (int)time, 0,
                (actualExecutedDrill%inOneIteration)+1, actIter, skip, note);
        //SystemClock.elapsedRealtime() - chronometer.getBase()
    }

    // generator spoustejici asynchronni vlakna s dotazy na server
    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                if(drillTask == null) {
                    if(pause){
                        pause = false;
                    }
                    else {
                        if(moveOnlyToPrev)
                            drillCounterUpdate(actualExecutedDrill - 1);
                        else
                            drillCounterUpdate(actualExecutedDrill + 1);
                    }
                    drillTask = new ExecuteDrillTask(RunDrillActivity.this);
                    drillTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);//execute((Void) null);
                }
            } finally {

            }
        }
    };

    // spusteni generatoru pro asynchronni vlakna
    void startRunTask() {
        stopRunTask();
        moveTonextPrewDrillIndicator = false;
        moveOnlyToPrev = false;
        mStatusChecker.run();
    }

    // zastaveni generatoru pro asynchronni vlakna
    void stopRunTask() {
        if(drillTask != null && drillTask.getStatus() != AsyncTask.Status.FINISHED){
            drillTask.cancel(true);
            drillTask = null;
        }
        //mHandler.removeCallbacks(mStatusChecker);
    }

    private class ExecuteDrillTask extends AsyncTask<Void, Void, Boolean> {

        private Context context;
        private RunDrillActivity act;
        private boolean skipp;

        /**
         * Constructor
         * @param act activitz context
         */
        public ExecuteDrillTask(RunDrillActivity act) {
            this.context = act.getApplicationContext();
            this.act = act;
            this.skipp = false;
            if(rec != null){
                rec.initClassForDetection();
            }
        }

        private String getSpeakText(){
            DrillInSet drill = drillsArray.get(actualExecutedDrill);
            String speak;
            if(drill.isPause()){
                speak = drill.getPauseTimeInString(true);
            }
            else {
                speak = drill.getName();
            }
            return speak;
        }

        @Override
        protected void onPreExecute(){
            if(drillsArray.get(actualExecutedDrill).isTimeWait() && drillsArray.get(actualExecutedDrill).getTime() != 0
                || drillsArray.get(actualExecutedDrill).isPause() && drillsArray.get(actualExecutedDrill).getPauseTime() != 0) {
                lvAdapter.getItem(actualExecutedDrill).chronometer = true;
                lvAdapter.getItem(actualExecutedDrill).chronometerPaused = false;
                lvAdapter.notifyDataSetChanged();
                chronometerEndCount = false;
            }
            drillsArrayFinished.add(drillsArray.get(actualExecutedDrill));
            // pre wait for sound
            if(drillsArray.get(actualExecutedDrill).isSndWait() && drillsArray.get(actualExecutedDrill).getSnd() != 0){
                if(!isSetPause) {
                    actualDetect = 0;
                    if (drillsArray.get(actualExecutedDrill).isAlternative() && drillsArray.get(actualExecutedDrill).getVariantXTimes() > 0) {
                        needDetect = drillsArray.get(actualExecutedDrill).getVariantXTimes();
                    } else {
                        needDetect = 1;
                    }
                    actualSndDetect = getSndByClassId(drillsArray.get(actualExecutedDrill).getSnd());
                    rec.addClass(drillsArray.get(actualExecutedDrill).getSnd());
                    testAplicationDialog(act);
                    updateSndWaitTitleTv();
                    sndDetected = false;
                }
            }
        }

        private void asynchTaskSpeak(){
            if(!isSetPause) {
                chronometerEndCount = false;
                speak(getSpeakText());
                while (tts.isSpeaking()) {
                }
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                }
            }
        }

        private void updateActualDrillIteration(){
            actualIteration = ((actualExecutedDrill+1) / inOneIteration) + 1;
            if(actualIteration > maxDrillSet)
                actualIteration--;
        }

        private void waitCycle(){
            if(rec != null) {
                rec.startRecord();
            }
            if(drillsArray.get(actualExecutedDrill).isTimeWait() && drillsArray.get(actualExecutedDrill).getTime() != 0
                    || drillsArray.get(actualExecutedDrill).isPause() && drillsArray.get(actualExecutedDrill).getPauseTime() != 0) {
                while(!chronometerEndCount){
                    if(moveTonextPrewDrillIndicator || moveOnlyToPrev) {
                        if(moveTonextPrewDrillIndicator && !drillsArrayFinished.isEmpty()) {
                            drillsArrayFinished.remove(drillsArrayFinished.get(drillsArrayFinished.size() - 1));
                            this.skipp = true;
                        }
                        moveTonextPrewDrillIndicator = false;
                        break;
                    }
                    try {
                        Thread.sleep(200);
                    } catch (Exception e){}
                }
            }
            else if(drillsArray.get(actualExecutedDrill).isSndWait() && drillsArray.get(actualExecutedDrill).getSnd() != 0){
                while(!sndDetected) {
                    if(actualDetect >= needDetect){
                        sndDetected = true;
                        if(rec != null) {
                            rec.stopRecord();
                        }
                        if(sndDialog != null) {
                            sndDialog.dismiss();
                            sndDialog = null;
                        }
                    }
                    if (moveTonextPrewDrillIndicator || moveOnlyToPrev) {
                        if (moveTonextPrewDrillIndicator && !drillsArrayFinished.isEmpty()) {
                            drillsArrayFinished.remove(drillsArrayFinished.get(drillsArrayFinished.size() - 1));
                            this.skipp = true;
                        }
                        moveTonextPrewDrillIndicator = false;
                        if(sndDialog != null) {
                            sndDialog.dismiss();
                            sndDialog = null;
                        }
                        break;
                    }
                    try {
                        Thread.sleep(200);
                    } catch (Exception e){}
                }
                if(rec != null)
                    rec.stopRecord();
            }
            else{
                chronometerEndCount = true;
                lvAdapter.getItem(actualExecutedDrill).progressStaus = ProgressStatus.DONE;
                if(moveTonextPrewDrillIndicator && !drillsArrayFinished.isEmpty()) {
                    drillsArrayFinished.remove(drillsArrayFinished.get(drillsArrayFinished.size() - 1));
                    this.skipp = true;
                }
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if(actualExecutedDrill == 0) {
                try {
                    Thread.sleep(800);
                } catch (Exception e){}
                finally {
                    asynchTaskSpeak();
                }
                System.out.println("Drill "+actualExecutedDrill+" chronometerEndCount: "+chronometerEndCount+" "+drillsArray.get(actualExecutedDrill).isTimeWait());
                waitCycle();
            }
            else {
                asynchTaskSpeak();
                waitCycle();
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            isSetPause = false;
            if(rec != null) {
                rec.stopRecord();
            }
            Log.i("Test1", ""+actualExecutedDrill+" "+drillsArray.size());
            if (success) {
                Log.i("Test2", ""+actualExecutedDrill+" "+drillsArray.size());
                if(drillsArray.get(actualExecutedDrill).isTimeWait() && drillsArray.get(actualExecutedDrill).getTime() != 0
                        || drillsArray.get(actualExecutedDrill).isPause() && drillsArray.get(actualExecutedDrill).getPauseTime() != 0) {
                    lvAdapter.getItem(actualExecutedDrill).chronometer = false;
                    lvAdapter.getItem(actualExecutedDrill).chronometerPaused = false;
                    lvAdapter.notifyDataSetChanged();
                }
                else if(drillsArray.get(actualExecutedDrill).isSndWait() && drillsArray.get(actualExecutedDrill).getSnd() != 0){
                    // remove detect class after drill detection
                    if(rec != null) {
                        rec.removeClass(drillsArray.get(actualExecutedDrill).getSnd());
                    }
                    if(moveOnlyToPrev)
                        lvAdapter.getItem(actualExecutedDrill).progressStaus = ProgressStatus.PREPARE;
                    else
                        lvAdapter.getItem(actualExecutedDrill).progressStaus = ProgressStatus.DONE;
                    lvAdapter.notifyDataSetChanged();
                }
                else{
                    if(moveOnlyToPrev)
                        lvAdapter.getItem(actualExecutedDrill).progressStaus = ProgressStatus.PREPARE;
                    else
                        lvAdapter.getItem(actualExecutedDrill).progressStaus = ProgressStatus.DONE;
                    lvAdapter.notifyDataSetChanged();
                }
                storeIntermediateInfo(this.skipp, "");
                updateActualDrillIteration();
                // move next/prev
                if(moveOnlyToPrev) {
                    actualExecutedDrill--;
                }
                else {
                    actualExecutedDrill++;
                }
                moveOnlyToPrev = false;
                if(actualExecutedDrill < drillsArray.size()){
                    actualDrillCircleUpdate(actualExecutedDrill, circleDrillNameText, false);
                    drillCounterUpdate(actualExecutedDrill);
                    if(actualExecutedDrill+1 < drillsArray.size()){
                        actualDrillCircleUpdate(actualExecutedDrill+1, nextDrillNameText, true);
                    }
                    else{
                        nextDrillNameText.setText("");
                    }
                    //drillTask = null;
                    startRunTask();
                }
                else{
                    stopRunDrill();
                }
            }
        }
    }

    /**
     * Stop running drill and start FinishActivity
     */
    public void stopRunDrill(){
        chronometer.stop();
        playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button,0,R.drawable.ic_play_white,0);
        circleDrillNameText.setText("Konec");
        nextDrillNameText.setText("");
        runFinishActivity();
    }

    /**
     * Sound/voice wait dialog
     * @param act - activity context
     */
    public void testAplicationDialog( final Activity act) {
        if(sndDialog == null) {
            final AlertDialog.Builder popDialog = new AlertDialog.Builder(act);
            final LayoutInflater inflater = (LayoutInflater) act.getSystemService(act.LAYOUT_INFLATER_SERVICE);

            final View Viewlayout = inflater.inflate(R.layout.dialog_sound_wait,
                    (ViewGroup) act.findViewById(R.id.layout_dialog));
            sndWaitTitleTv = Viewlayout.findViewById(R.id.snd_wait_text);
            rpl = Viewlayout.findViewById(R.id.ripple_pulse);
            playBtnDialog = (Button) Viewlayout.findViewById(R.id.run_play);
            final ImageButton prevBtn = (ImageButton) Viewlayout.findViewById(R.id.run_control_prev);
            final ImageButton nextBtn = (ImageButton) Viewlayout.findViewById(R.id.run_control_next);
            final ImageButton mic = (ImageButton) Viewlayout.findViewById(R.id.microphone);
            chron = (Chronometer) Viewlayout.findViewById(R.id.snd_timer);
            chron.setFormat("%s");
            chron.setBase(chronometer.getBase());
            chron.start();
            rpl.startRippleAnimation();
            popDialog.setView(Viewlayout);

            mic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(chronometerRunning) {
                        actualDetect++;
                        storeIntermediateInfo(false, actualSndDetect.getName()+" "+actualDetect);
                        updateSndWaitTitleTv();
                    }
                }
            });


            sndDialog = popDialog.create();
            sndDialog.setCancelable(false);
            // listener for close dialog
            sndDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface in) {
                    //dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ctx.getResources().getColor(R.color.colorAccent));
                }
            });
            playBtnDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // control chronometer
                    if (!chronometerRunning) {
                        /*if(rec != null)
                            rec.stopRecord();*/
                        startChronometer();
                        chron.setBase(chronometer.getBase());
                        chron.start();
                        playBtnDialog.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_pause_white, 0);
                        playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_pause_white, 0);
                        rpl.startRippleAnimation();
                        if(rec != null)
                            rec.startRecord(1900, false);
                    } else {
                        /*if(rec != null)
                            rec.stopRecord();*/
                        pauseChronometer();
                        chron.stop();
                        playBtnDialog.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_play_white, 0);
                        playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_play_white, 0);
                        rpl.stopRippleAnimation();
                        if(rec != null)
                            rec.startRecord(1900, false);
                    }
                }
            });
            nextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (chronometerRunning) {
                        nextBtnHandler();
                        if(sndDialog != null) {
                            sndDialog.dismiss();
                            sndDialog = null;
                        }
                        if (actualExecutedDrill >= drillsArray.size() - 1) {
                            stopRunDrill();
                        }
                    }
                }
            });
            prevBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (chronometerRunning) {
                        prevBtnHandler();
                        if(sndDialog != null) {
                            sndDialog.dismiss();
                            sndDialog = null;
                        }
                    }
                }
            });
            sndDialog.show();
        }
    }

    public void updateSndWaitTitleTv(){
        if(sndWaitTitleTv != null){
            sndWaitTitleTv.setText("Čekám na zvuk "+actualSndDetect.getName()+" "+(needDetect-actualDetect)+"x");
        }
    }

    /**
     * Handler get detected sound class and on detected class make action
     * @param detectedCls - identifier of detected class
     * @return - true - repeat start detection | false - not repeat detection
     */
    public boolean detectedHandler(int detectedCls){
        if(tts.isSpeaking())
            return true;
        rec.stopRecord();
        switch (detectedCls){
            case 17: // START
                playSound(beepMP);
                infoSpeak(getResources().getString(R.string.voice_start));
                startChronometer();
                //startRecordOperator();
                playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_pause_white, 0);
                if(sndDialog != null) {
                    chron.setBase(chronometer.getBase());
                    chron.start();
                    playBtnDialog.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_pause_white, 0);
                    rpl.startRippleAnimation();
                }
                break;
            case 18: // PAUSE
                playSound(beepMP);
                infoSpeak(getResources().getString(R.string.voice_pause));
                pauseChronometer();
                playBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_play_white, 0);
                if(sndDialog != null) {
                    chron.stop();
                    playBtnDialog.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_play_white, 0);
                    rpl.stopRippleAnimation();
                }
                break;
            case 16: // STOP
                //playSound(beepMP);
                rec.removeAllClass();    // remove all classes
                pauseChronometer();
                runFinishActivity();
                return false;
            case 14: // BACK
                playSound(beepMP);
                prevBtnHandler();
                if(sndDialog != null) {
                    sndDialog.dismiss();
                    sndDialog = null;
                }
                break;
            case 15: // NEXT
                playSound(beepMP);
                nextBtnHandler();
                if(actualExecutedDrill >= drillsArray.size()-1){
                    stopRunDrill();
                }
                if(sndDialog != null) {
                    sndDialog.dismiss();
                    sndDialog = null;
                }
                break;
        }
        if(actualSndDetect != null && detectedCls == actualSndDetect.getCls()){
            playSound(beepMP);
            actualDetect++;
            // store info about detect sound
            storeIntermediateInfo(false, actualSndDetect.getName()+" "+actualDetect);
            updateSndWaitTitleTv();
        }
        return true;
    }

    /**
     * Set record to detect snd class during run -> pause
     */
    private void pauseRecordOperator(){
        rec.removeAllClass();     // remove all classes
        rec.addClass(17);   // add START
        rec.addClass(16);   // add STOP
    }

    /**
     * Set record to detect snd class during pause -> run
     */
    private void startRecordOperator(){
        rec.initClassForDetection();
        rec.removeClass(13);
        if(actualSndDetect != null) {
            rec.addClass(actualSndDetect.getCls());
        }
    }


    /**
     * Speak info during detected control voice
     * @param speakString - string for speak
     */
    private void infoSpeak(String speakString){
        speak(speakString);
        while (tts.isSpeaking()) {
        }
        try {
            Thread.sleep(200);
        } catch (Exception e){}
    }

}
