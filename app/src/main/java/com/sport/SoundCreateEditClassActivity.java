package com.sport;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.gauravbhola.ripplepulsebackground.RipplePulseLayout;
import com.sport.database.SQLiteDB;
import com.sport.lists.Snd;
import com.sport.lists.SndEditAdapter;
import com.sport.lists.VawRecord;
import com.sport.lists.VawRecordAdapter;
import com.sport.sound.FeatureFile;
import com.sport.sound.RecordFile;
import com.sport.sound.Store;
import com.sport.sound.StoreFile;
import com.sport.utils.HelpDialogs;
import com.sport.utils.ListUtils;

import java.util.ArrayList;

public class SoundCreateEditClassActivity extends BasicMain {

    private int SOUND_ID;
    private View includedLayout;
    private ListView lv;
    private VawRecordAdapter adapter;
    private TextView emptyListTv, sndTitleTv;
    private ImageButton editTitleButton, deleteTitleButton;
    private FloatingActionButton fab;
    private ArrayList<VawRecord> records;
    public AlertDialog sndDialog = null;
    private TextView sndWaitTitleTv;
    private Chronometer chron;
    private RipplePulseLayout rpl;
    private Button playBtnDialog;
    private int chronometerRunning;
    private Snd snd;
    private RecordFile recording;
    private int tick;
    private RemoveFileTask removeFileTask;
    private android.support.v7.app.AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_create_edit_class);
        super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.snd_class_title));

        Intent inIntent = getIntent();
        Bundle bundle = inIntent.getExtras();
        // get data from intent
        if(bundle != null) {
            SOUND_ID = (int) bundle.get("SOUND_ID");
        }

        includedLayout = findViewById(R.id.id_snd_list);
        lv = (ListView) includedLayout.findViewById(R.id.list);
        emptyListTv = (TextView) includedLayout.findViewById(R.id.empty_list);
        sndTitleTv = (TextView) findViewById(R.id.title_text);
        editTitleButton = (ImageButton) findViewById(R.id.button_edit_name);
        deleteTitleButton = (ImageButton) findViewById(R.id.button_delete_name);


        snd = getSndInfo(this, SOUND_ID);

        sndTitleTv.setText(snd.getName());

        final SoundCreateEditClassActivity act = this;
        // edit sound class name
        editTitleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoundOverviewActivity.createNewClassDialog(act, true, snd, act);
            }
        });

        // remove sound class
        deleteTitleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeSndClassDialog(act);
            }
        });



        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recordDialog(act);
            }
        });

        StoreFile st = new StoreFile(getApplicationContext());
        st.getFilesName(getApplicationContext());
        updateList(st);
    }

    public void updateList(StoreFile st){
        if(sndDialog != null){
            sndDialog.dismiss();
            sndDialog = null;
        }
        records = st.getRecordsByClassId(SOUND_ID);
        if (records.size() > 0) {
            SQLiteDB db = new SQLiteDB(this);
            db.setSndExample(SOUND_ID, records.get(0).getName());
            adapter = new VawRecordAdapter(this, R.layout.list_item_vaw_file, records, this);
            lv.setAdapter(adapter);
            ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyListTv.setVisibility(View.GONE);
        }
        else{
            emptyListTv.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }
    }

    public void updateTitle(){
        snd = getSndInfo(this, SOUND_ID);
        sndTitleTv.setText(snd.getName());
    }

    /**
     * Get sound class info
     * @param ctx - aplication context
     * @param sndId - sound identifier
     * @return Snd class with sound info
     */
    public Snd getSndInfo(Context ctx, int sndId){
        Snd sndResult = null;
        SQLiteDB db = new SQLiteDB(ctx);
        Cursor itemFromDB = db.getSndById(sndId);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            String example = itemFromDB.getString(itemFromDB.getColumnIndex("example"));
            Integer cls = itemFromDB.getInt(itemFromDB.getColumnIndex("class"));
            Integer active = itemFromDB.getInt(itemFromDB.getColumnIndex("active"));
            Integer added = itemFromDB.getInt(itemFromDB.getColumnIndex("added"));
            sndResult = new Snd(id, name, example, cls, active, added);
        }
        db.close();
        return sndResult;
    }

    /**
     * Sound/voice wait dialog
     * @param act - activity context
     */
    public void recordDialog( final Activity act) {
        if(sndDialog == null) {
            final AlertDialog.Builder popDialog = new AlertDialog.Builder(act);
            final LayoutInflater inflater = (LayoutInflater) act.getSystemService(act.LAYOUT_INFLATER_SERVICE);

            final View Viewlayout = inflater.inflate(R.layout.dialog_record_sound,
                    (ViewGroup) act.findViewById(R.id.layout_dialog));
            sndWaitTitleTv = Viewlayout.findViewById(R.id.snd_text);
            rpl = Viewlayout.findViewById(R.id.ripple_pulse);
            playBtnDialog = (Button) Viewlayout.findViewById(R.id.run_play);
            final ImageButton mic = (ImageButton) Viewlayout.findViewById(R.id.microphone);
            chron = (Chronometer) Viewlayout.findViewById(R.id.snd_timer);
            chron.setFormat("%s");
            chron.setBase(SystemClock.elapsedRealtime());
            popDialog.setView(Viewlayout);
            chronometerRunning = 0;
            tick = 0;
            chron.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                @Override
                public void onChronometerTick(Chronometer chronometer) {
                    if(tick > 3){
                        stopRecording(false);
                        chron.stop();
                        rpl.stopRippleAnimation();
                        sndWaitTitleTv.setText(getResources().getString(R.string.record_do));
                    }
                    tick++;
                }
            });

            popDialog.setNegativeButton(act.getResources().getString(R.string.decline), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    stopRecording(true);
                    sndDialog.dismiss();
                    sndDialog = null;
                }
            });
            sndDialog = popDialog.create();
            sndDialog.setCancelable(false);
            // listener for close dialog
            /*sndDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface in) {
                    sndDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(act.getResources().getColor(R.color.colorAccent));
                }
            });*/
            playBtnDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (chronometerRunning == 0) {
                        // start chronometer
                        sndWaitTitleTv.setText(getResources().getString(R.string.record_record));
                        chron.setBase(SystemClock.elapsedRealtime());
                        chron.start();
                        rpl.startRippleAnimation();
                        playBtnDialog.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_stop_white, 0);
                        chronometerRunning = 1;
                        recording = new RecordFile(act, act, SOUND_ID);
                        if(recording != null)
                            recording.startRecord();
                    } else if(chronometerRunning == 1) {
                        stopRecording(false);
                        chron.stop();
                        rpl.stopRippleAnimation();
                        sndWaitTitleTv.setText(getResources().getString(R.string.record_do));
                    }
                }
            });
            sndDialog.show();
        }
    }

    private void stopRecording(boolean stopWithoutSave){
        chron.stop();
        //playBtnDialog.setCompoundDrawablesWithIntrinsicBounds(R.drawable.round_button, 0, R.drawable.ic_stop_white, 0);
        rpl.stopRippleAnimation();
        if(recording != null) {
            if(stopWithoutSave)
                recording.stopRecord(stopWithoutSave);
            else
                recording.stop();
        }
    }

    /**
     * Warning dialog about remove sound class
     * @param act - activity context
     */
    private void removeSndClassDialog(final SoundCreateEditClassActivity act) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.warning_remove_snd_class_title));
        builder.setMessage(getResources().getString(R.string.warning_remove_snd_class_text));
        builder
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dia, int id) {
                        dialog = HelpDialogs.loadingDialog(act , act, "Mazání zvukové třídy");
                        startRemove(act);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.drillSubText));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.drillSubText));
            }
        });
        dialog.show();
    }

    public void startRemove(Context ctx){
        removeFileTask = new RemoveFileTask(ctx);
        mStatusChecker2.run();
    }

    Runnable mStatusChecker2 = new Runnable() {
        @Override
        public void run() {
            try {
                removeFileTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } finally {}
        }
    };

    public class RemoveFileTask extends AsyncTask<Void, Void, Boolean> {

        Context ctx;

        public RemoveFileTask(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            for(VawRecord wav : records){
                wav.getFile().delete();
            }
            SQLiteDB db = new SQLiteDB(getApplicationContext());
            db.deleteSndById(SOUND_ID);
            FeatureFile.removeFeatureFromFile(getApplicationContext(), SOUND_ID);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean result) {
            if(dialog != null)
                dialog.dismiss();
            finish();
        }
    }
}
