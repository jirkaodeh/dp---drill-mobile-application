package com.sport.lists;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jirka on 30.10.2018.
 * Class for switch tab with fragments in activity
 */
public class TabViewAdapter extends FragmentPagerAdapter {
    private final List<Fragment> fragmentList = new ArrayList<>();
    private final List<String> fragmentTitleList = new ArrayList<>();

    public TabViewAdapter(FragmentManager manager) {
        super(manager);
    }

    /**
     * Return fragment on position
     * @param position item position
     * @return fragment show on refered position
     */
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    /**
     * Get count of all fragments
     * @return number of fragments
     */
    @Override
    public int getCount() {
        return fragmentList.size();
    }

    /**
     * Add fragment to tab slider
     * @param fragment insert fragment
     * @param title title of bookmark tab
     */
    public void addFrag(Fragment fragment, String title) {
        fragmentList.add(fragment);
        fragmentTitleList.add(title);
    }

    /**
     * Get title from tab bookmark
     * @param position tab position
     * @return tab title
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }
}
