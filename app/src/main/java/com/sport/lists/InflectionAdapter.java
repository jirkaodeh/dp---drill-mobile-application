package com.sport.lists;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sport.InflectionSettings;
import com.sport.R;
import com.sport.database.InflectionDB;
import com.sport.fragments.main.CreateNewDrillFragment;

import java.util.ArrayList;
import java.util.List;

public class InflectionAdapter extends ArrayAdapter<Inflection> {

    private List<Inflection> data;
    private Context context;
    private InflectionSettings activity;

    /**
     * Constructor inflection list adapter
     *
     * @param context          app context
     * @param layoutResourceId reference to layout on list item style
     * @param data             array of Snd objects with informations abbout each sound
     */
    public InflectionAdapter(Context context, int layoutResourceId, List<Inflection> data, InflectionSettings activity) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.activity = activity;
    }

    /**
     * Return count of all snd in set
     *
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     *
     * @param position index to ArrayList to concrete object
     * @return Snd object with data
     */
    @Override
    public Inflection getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        InflectionAdapter.ViewHolder holder;
        final Inflection inflection = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_inflection_edit, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (InflectionAdapter.ViewHolder) convertView.getTag();
        }
        holder.inflectionText.setText(inflection.getName1()+" / "+inflection.getName24()+" / "+inflection.getName5());
        holder.inflectionEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editInflectionnDialog(context, inflection, false, activity);
            }
        });
        return convertView;
    }

    /**
     * Create new holder
     *
     * @param v - view to get holder
     * @return new holder object
     */
    private InflectionAdapter.ViewHolder createViewHolder(View v) {
        InflectionAdapter.ViewHolder holder = new InflectionAdapter.ViewHolder();
        holder.inflectionText = (TextView) v.findViewById(R.id.inflection_name);
        holder.inflectionEditBtn = (ImageButton) v.findViewById(R.id.inflection_edit);
        return holder;
    }

    private static class ViewHolder {
        public TextView inflectionText;
        public ImageButton inflectionEditBtn;
    }

    public static void editInflectionnDialog(final Context ctx, final Inflection inflection, final boolean add, final InflectionSettings activity) {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(ctx);
        final LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);

        final View viewlayout = inflater.inflate(R.layout.dialog_create_inflection,
                (ViewGroup) activity.findViewById(R.id.layout_dialog));
        ((TextView)viewlayout.findViewById(R.id.title)).setText((add ? "Přidat slova pro číselné varianty": "Editovat slova pro číselné varianty"));
        final EditText word1Edit = viewlayout.findViewById(R.id.word1);
        final EditText word24Edit = viewlayout.findViewById(R.id.word24);
        final EditText word5Edit = viewlayout.findViewById(R.id.word5);
        final TextView warningTv = viewlayout.findViewById(R.id.warning);
        if(!add && inflection != null) {
            word1Edit.setText(inflection.getName1());
            word24Edit.setText(inflection.getName24());
            word5Edit.setText(inflection.getName5());
        }


        popDialog.setView(viewlayout);

        popDialog.setPositiveButton(activity.getResources().getString(R.string.save), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        popDialog.setNegativeButton(activity.getResources().getString(R.string.decline), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = popDialog.create();
        // listener for close dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ctx.getResources().getColor(R.color.colorAccent));
            }
        });
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(word1Edit.getText().toString().isEmpty() || word24Edit.getText().toString().isEmpty() ||
                        word5Edit.getText().toString().isEmpty()){
                    warningTv.setVisibility(View.VISIBLE);
                }
                else {
                    warningTv.setVisibility(View.GONE);
                    InflectionDB infDB = new InflectionDB(activity);
                    if(add)
                        infDB.insertInflection(word1Edit.getText().toString(), word24Edit.getText().toString(), word5Edit.getText().toString());
                    else
                        infDB.updateInflection(inflection.getId(), word1Edit.getText().toString(), word24Edit.getText().toString(), word5Edit.getText().toString());
                    activity.getData();
                    activity.updateList();
                    dialog.dismiss();
                }
            }
        });
    }
}
