package com.sport.lists;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.sport.database.SQLiteDB;
import com.sport.importExport.Export;
import com.sport.sound.FeatureFile;
import com.sport.utils.Color;
import com.sport.CreateNewDrillSetActivity;
import com.sport.DrillSetListActivity;
import com.sport.R;
import com.sport.RunDrillActivity;
import com.sport.utils.HelpDialogs;
import com.sport.utils.SportIcon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jirka on 10.10.2018.
 * Adapter for print item listview for drill sets lists
 */
public class DrillSetAdapter extends ArrayAdapter<DrillSet> {
    private final List<DrillSet> drillSets;
    private Context context;
    private Activity activity;
    private int layoutResourceId;

    /** Constructor drill set list adapter
     *
     * @param context app context
     * @param layoutResourceId  reference to layout on list item style
     * @param data array of DrillSet objects with informations abbout each drill set
     */
    public DrillSetAdapter(Context context, int layoutResourceId, List<DrillSet> data, Activity activity) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.drillSets = data;
        this.activity = activity;
    }

    /**
     * Return count of all drill set
     * @return number that represent count of all drill sets in list
     */
    @Override
    public int getCount() {
        if (drillSets != null) {
            return drillSets.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     * @param position index to ArrayList to concrete object
     * @return DrillSet object with data
     */
    @Override
    public DrillSet getItem(int position) {
        if (drillSets != null) {
            return drillSets.get(position);
        } else {
            return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final DrillSet drillSet = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_drill_set, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.drillSetName.setText(drillSet.getName());
        holder.drillSetTextGroup.setText("Skupina: "+drillSet.getGroupName());
        String iter = "Iterace";
        if(drillSet.getRepeatCnt() > 4)
            iter = "Iterací";
        holder.drillSetTextRepeat.setText(iter+": "+String.valueOf(drillSet.getRepeatCnt()));
        holder.drillSetTextBestTime.setText("Nejlepší čas: "+drillSet.getBestTime(context));
        holder.drillSetIcon.setImageDrawable(null);
        holder.drillSetIcon.setBackgroundResource(SportIcon.getGroupIconWhite(drillSet.getIcon()));
        holder.drillSetBox.setBackgroundColor(getContext().getResources().getColor(Color.getColorById(drillSet.getColor())));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DrillSetListActivity.class);
                intent.putExtra("DRILL_SET_ID", drillSets.get(position).getDsId());
                intent.putExtra("DRILL_SET_NAME", drillSets.get(position).getName());
                intent.putExtra("DRILL_SET_COLOR", drillSets.get(position).getColor());
                intent.putExtra("DRILL_SET_GROUP", drillSets.get(position).getGroupName());
                context.startActivity(intent);
            }
        });

        ImageButton settings = (ImageButton) convertView.findViewById(R.id.drill_set_settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(v, position);
            }
        });
        ImageButton play = (ImageButton) convertView.findViewById(R.id.drill_set_play);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(FeatureFile.isNoActiveSndClassInDrillSet(context, getData(drillSet.getDsId()))){
                    HelpDialogs.noActiveSndClassWarningDialog(context, activity);
                }
                else {
                    Intent intent = new Intent(context, RunDrillActivity.class);
                    intent.putExtra("DRILL_SET_ID", drillSets.get(position).getDsId());
                    intent.putExtra("DRILL_SET_NAME", drillSets.get(position).getName());
                    intent.putExtra("DRILL_SET_COLOR", drillSets.get(position).getColor());
                    intent.putExtra("DRILL_SET_GROUP", drillSets.get(position).getGroupName());
                    intent.putExtra("DRILL_SET_MODE", 0); // normal mode
                    context.startActivity(intent);
                }
            }
        });
        return convertView;
    }

    private void showMenu(View v, final int position){
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.item_edit:
                        Intent intent = new Intent(context, CreateNewDrillSetActivity.class);
                        intent.putExtra("DRILL_SET_ID", drillSets.get(position).getDsId());
                        intent.putExtra("DRILL_SET_NAME", drillSets.get(position).getName());
                        intent.putExtra("DRILL_SET_COLOR", drillSets.get(position).getColor());
                        intent.putExtra("DRILL_SET_GROUP", drillSets.get(position).getGroupName());
                        context.startActivity(intent);
                        return true;
                    case R.id.item_remove:
                        removeDrillSetDialog(position);
                        return true;
                    case R.id.item_export:
                        String exportStr = Export.exportDrillSet(context, drillSets.get(position).getDsId());
                        Intent it = new Intent(Intent.ACTION_SEND);
                        it.setType("text/plain");
                        it.putExtra(Intent.EXTRA_TEXT, exportStr);
                        context.startActivity(Intent.createChooser(it, context.getResources().getString(R.string.export_dialog_title)));
                        return true;
                    default:
                        return false;
                }
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.drill_set_list_menu, popup.getMenu());
        popup.show();
    }

    /**
     * Vytvoreni noveho holderu
     * @param v - vychozi view pro ziskani prvku ukladanych do holderu
     * @return novy objekt holderu ViewHolder
     */
    private ViewHolder createViewHolder(View v) {
        ViewHolder holder = new ViewHolder();
        holder.drillSetName = (TextView) v.findViewById(R.id.drill_set_name);
        holder.drillSetTextGroup = (TextView) v.findViewById(R.id.drill_set_text_group);
        holder.drillSetTextRepeat = (TextView) v.findViewById(R.id.drill_set_text_repeat);
        holder.drillSetIcon = (ImageView) v.findViewById(R.id.drill_set_icon);
        holder.drillSetBox = (LinearLayout) v.findViewById(R.id.drill_set_item);
        holder.drillSetTextBestTime = (TextView)v.findViewById(R.id.drill_set_text_best_time);
        return holder;
    }

    private static class ViewHolder {
        public TextView drillSetName;
        public ImageView drillSetIcon;
        public TextView drillSetTextGroup;
        public TextView drillSetTextRepeat;
        public TextView drillSetTextBestTime;
        public LinearLayout drillSetBox;
    }

    /**
     * Warning dialog about remove drill set
     * @param position - id drill set
     */
    private void removeDrillSetDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getResources().getString(R.string.remove_drills_set_title));
        builder.setMessage(getContext().getResources().getString(R.string.remove_drills_set_text));
        builder
                .setCancelable(false)
                .setPositiveButton(getContext().getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        removeDrillSet(position);
                    }
                })
                .setNegativeButton(getContext().getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.drillSubText));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getContext().getResources().getColor(R.color.drillSubText));
            }
        });
        dialog.show();
    }

    /**
     * Remove drill set and all drills in it
     * @param position
     */
    private void removeDrillSet(int position){
        SQLiteDB db = new SQLiteDB(getContext());
        Cursor itemFromDB = db.getAllDrillsFromDrillSet(drillSets.get(position).getDsId()); //position
        ArrayList<Integer> drillsIdArray = new ArrayList<Integer>();
        ArrayList<Integer> drillInSetIdArray = new ArrayList<Integer>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            drillInSetIdArray.add(itemFromDB.getInt(itemFromDB.getColumnIndex("id_drill_in_set")));
            drillsIdArray.add(itemFromDB.getInt(itemFromDB.getColumnIndex("drill_id")));
        }
        for(int i = 0; i < drillsIdArray.size(); i++){
            db.deleteDrill(drillInSetIdArray.get(i), drillsIdArray.get(i));
        }
        db.deleteDrillSet(drillSets.get(position).getDsId());
        db.close();
        drillSets.remove(position);
        notifyDataSetChanged();
    }

    private ArrayList<DrillInSet> getData(int drillSetId){
        // get all drill sets from DB
        SQLiteDB db = new SQLiteDB(context);
        Cursor itemFromDB = db.getAllDrillsFromDrillSet(drillSetId);
        ArrayList<DrillInSet> drillsDBArray = new ArrayList<DrillInSet>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id_drill_in_set"));
            Integer drillId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_id"));
            Integer position = itemFromDB.getInt(itemFromDB.getColumnIndex("position"));
            Boolean isPause = itemFromDB.getInt(itemFromDB.getColumnIndex("is_pause")) == 1;
            Integer pauseTime = itemFromDB.getInt(itemFromDB.getColumnIndex("pause_time"));
            Integer variantXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_x_times"));
            String editName = itemFromDB.getString(itemFromDB.getColumnIndex("edit_name"));
            String drillName = itemFromDB.getString(itemFromDB.getColumnIndex("drill_name"));
            Boolean prev = itemFromDB.getInt(itemFromDB.getColumnIndex("joint_to_prev_drill")) == 1;
            Boolean timeWait = itemFromDB.getInt(itemFromDB.getColumnIndex("time_wait")) == 1;
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("time"));
            Boolean alternative = itemFromDB.getInt(itemFromDB.getColumnIndex("alternative")) == 1;
            Integer varPosition = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_position"));
            Boolean sndWait = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_wait")) == 1;
            Integer snd = itemFromDB.getInt(itemFromDB.getColumnIndex("snd"));
            Boolean inflection = itemFromDB.getInt(itemFromDB.getColumnIndex("inflection")) == 1;
            Integer sndDetectXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_detect_x_times"));
            Boolean shuffle = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle")) == 1;
            Boolean shuffleModify = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle_modify")) == 1;
            drillsDBArray.add(new DrillInSet(id, drillId, position, isPause, pauseTime, variantXTimes, alternative, drillName, editName,
                    prev, timeWait, time, varPosition, sndWait, snd, sndDetectXTimes, inflection, shuffle, shuffleModify));
        }
        itemFromDB.close();
        db.close();
        return drillsDBArray;
    }
}
