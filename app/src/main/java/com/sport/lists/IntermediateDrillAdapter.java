package com.sport.lists;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.sport.FinishActivity;
import com.sport.R;

import java.util.List;

/**
 * Created by Jirka on 7.2.2019.
 * Adapter print intermediate list with execudet drill
 */
public class IntermediateDrillAdapter extends ArrayAdapter<IntermediateDrill> {
    private List<IntermediateDrill> data;
    private Context context;

    /**
     * Constructor drill list adapter
     *
     * @param context          app context
     * @param layoutResourceId reference to layout on list item style
     * @param data             array of DrillSet objects with informations abbout each drill
     */
    public IntermediateDrillAdapter(Context context, int layoutResourceId, List<IntermediateDrill> data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
    }

    /**
     * Return count of all drill in set
     *
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     *
     * @param position index to ArrayList to concrete object
     * @return DrillSet object with data
     */
    @Override
    public IntermediateDrill getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        IntermediateDrillAdapter.ViewHolder holder;
        final IntermediateDrill drill = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_intermediate_drill, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (IntermediateDrillAdapter.ViewHolder) convertView.getTag();
        }

        holder.drillName.setText(drill.getDrillName());
        String drillPositionIterationText = drill.getIteration()+"/"+drill.getPosition();
        if(!drill.getNote().isEmpty()){
            drillPositionIterationText += " - "+drill.getNote();
        }
        holder.drillPositionIteration.setText(drillPositionIterationText);
        holder.drillTime.setText("Čas: "+FinishActivity.getTime(drill.getTime()));
        holder.drillInfo.setVisibility(View.GONE);

        if(drill.isSkipped()){
            holder.drillSkiped.setVisibility(View.VISIBLE);
        }
        else {
            holder.drillSkiped.setVisibility(View.GONE);
        }

        return convertView;
    }

    /**
     * Create new holder
     *
     * @param v - vychozi view pro ziskani prvku ukladanych do holderu
     * @return novy objekt holderu ViewHolder
     */
    private IntermediateDrillAdapter.ViewHolder createViewHolder(View v) {
        IntermediateDrillAdapter.ViewHolder holder = new IntermediateDrillAdapter.ViewHolder();
        holder.drillName = (TextView) v.findViewById(R.id.drill_name);
        holder.drillTime = (TextView) v.findViewById(R.id.drill_time);
        holder.drillPositionIteration = (TextView) v.findViewById(R.id.drill_position_iteration);
        holder.drillInfo = (TextView) v.findViewById(R.id.drill_info);
        holder.drillSkiped = (ImageView)  v.findViewById(R.id.drill_skiped);
        return holder;
    }

    private static class ViewHolder {
        public TextView drillName;
        public TextView drillTime;
        public TextView drillPositionIteration;
        public TextView drillInfo;
        public ImageView drillSkiped;
    }
}
