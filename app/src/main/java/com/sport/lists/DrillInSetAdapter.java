package com.sport.lists;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.sport.CreateNewDrillActivity;
import com.sport.R;
import com.sport.database.SQLiteDB;

import java.util.List;

import static android.view.View.GONE;

/**
 * Created by Jirka on 25.10.2018.
 * Adapter for print item listview for drill lists
 */
public class DrillInSetAdapter extends ArrayAdapter<DrillInSet>  {

    private List<DrillInSet> data;
    private Context context;
    private int layoutResourceId, color, setId;
    private String groupName;
    private int DrillSetId;
    private DrillInSetAdapter adapter;
    private int mHandler;

    /** Constructor drill set list adapter
     *
     * @param context app context
     * @param layoutResourceId  reference to layout on list item style
     * @param data array of DrillSet objects with informations abbout each drill
     */
    public DrillInSetAdapter(Context context, int layoutResourceId, List<DrillInSet> data, int setId, int color, String groupName) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.DrillSetId = DrillSetId;
        this.adapter = this;
        this.color = color;
        this.groupName = groupName;
        this.setId = setId;
        //setup(data.size());
    }

    public void setData(List<DrillInSet> data){
        this.data = data;
    }

    /**
     * Return count of all drill in set
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     * @param position index to ArrayList to concrete object
     * @return DrillSet object with data
     */
    @Override
    public DrillInSet getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @SuppressLint("ClickableViewAccesibility")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        DrillInSetAdapter.ViewHolder holder;
        final DrillInSet drill = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_drill_in_set, parent, false);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (DrillInSetAdapter.ViewHolder) convertView.getTag();
        }

        holder.drillName.setText(drill.getName());
        // item is repeater
        if(drill.isRepeat()){
            holder.drillRepeatIcon.setVisibility(View.VISIBLE);
            holder.drillPrevIcon.setVisibility(GONE);
            holder.drillWait.setVisibility(GONE);
            holder.drillVariantBox.setVisibility(GONE);
            holder.drillName.setText("Sada opakována "+String.valueOf(drill.getRepeat())+"x");
            holder.drillShuffle.setVisibility(GONE);
        }
        //item is pause
        else if(drill.isPause()){
            holder.drillRepeatIcon.setVisibility(View.GONE);
            holder.drillPrevIcon.setVisibility(GONE);
            holder.drillWait.setVisibility(View.VISIBLE);
            holder.drillVariantBox.setVisibility(GONE);
            holder.drillWait.setText(String.valueOf(drill.getPauseTime())+" s");
            holder.drillName.setText(getContext().getResources().getString(R.string.pause));
            holder.drillShuffle.setVisibility(GONE);
        }
        // item is normal drill
        else{
            holder.drillWait.setVisibility(View.VISIBLE);
            holder.drillRepeatIcon.setVisibility(View.GONE);
            if(drill.isJointToPrevDrill()){
                holder.drillPrevIcon.setVisibility(View.VISIBLE);
            }
            else{
                holder.drillPrevIcon.setVisibility(View.GONE);
            }
            if(drill.isShuffle()){
                holder.drillShuffle.setVisibility(View.VISIBLE);
            }
            else {
                holder.drillShuffle.setVisibility(GONE);
            }
            holder.drillName.setText(drill.getName());
            if(drill.isAlternative()){
                holder.drillVariantBox.setVisibility(View.VISIBLE);
                holder.drillVariantNumber.setText(String.valueOf(getItem(position).getVariantXTimes()));
            }
            else{
                holder.drillVariantBox.setVisibility(View.GONE);
            }
            // wait text
            if(drill.getTime() != 0) {
                holder.drillWait.setText(getContext().getResources().getString(R.string.wait) + " " + String.valueOf(drill.getTime()) + " s");
            }
            else if(drill.isSndWait()){
                Snd snd = drill.getSndInfo(context);
                Log.i("Snd", drill.getSnd() + "");
                if (snd != null) {
                    holder.drillWait.setText(getContext().getResources().getString(R.string.wait_snd) + " " + snd.getName());
                }
                else {
                    holder.drillWait.setTextColor(context.getResources().getColor(R.color.warning));
                    holder.drillWait.setText(context.getResources().getString(R.string.warning_no_snd));
                }
            }
            else {
                holder.drillWait.setText(getContext().getResources().getString(R.string.dont_wait));
            }
        }


        // action plus button click
        holder.drillVariantPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.get(position).setVariantXTimesWithDB(getContext(), 1, 0);
                adapter.notifyDataSetChanged();
            }
        });

        // action minus button click
        holder.drillVariantMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.get(position).setVariantXTimesWithDB(getContext(), -1, 0);
                adapter.notifyDataSetChanged();
            }
        });

        // action when item in list is select
        if(drill.isRepeat()) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    numberSelectDialog(getContext(), position);
                }
            });
        }
        else {
            holder.drillSwipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
            holder.drillSwipeLayout.addDrag(SwipeLayout.DragEdge.Left, convertView.findViewById(R.id.bottom_wrapper));
            holder.drillSwipeLayout.setLeftSwipeEnabled(false);
            holder.drillSwipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
                @Override
                public void onStartOpen(SwipeLayout layout) {
                }

                @Override
                public void onOpen(SwipeLayout layout) {

                }

                @Override
                public void onStartClose(SwipeLayout layout) {

                }

                @Override
                public void onClose(SwipeLayout layout) {

                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                }
            });

            // remove drill
            holder.drillRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SQLiteDB db = new SQLiteDB(context);
                    db.deleteDrill(data.get(position).getIdDrill(), data.get(position).getIdDrill_drill_table());
                    data.remove(position);
                    adapter.notifyDataSetChanged();
                }
            });

            // edit drill
            holder.drillEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!data.get(position).isRepeat()) {
                        Intent intent = new Intent(context, CreateNewDrillActivity.class);
                        intent.putExtra("SET_ID", setId);
                        intent.putExtra("GROUP", groupName);
                        intent.putExtra("DRILL_ID", data.get(position).getIdDrill_drill_table());
                        intent.putExtra("DRILL_ID_IN_SET", data.get(position).getIdDrill());
                        intent.putExtra("COLOR", color);
                        if (data.get(position).isPause())
                            intent.putExtra("MODE", 2);
                        else if (!data.get(position).isPause())
                            intent.putExtra("MODE", 1);
                        context.startActivity(intent);
                        adapter.notifyDataSetChanged();
                    }
                }
            });
        }

        return convertView;
    }

    /**
     * Create new holder
     * @param v - vychozi view pro ziskani prvku ukladanych do holderu
     * @return novy objekt holderu ViewHolder
     */
    private DrillInSetAdapter.ViewHolder createViewHolder(View v) {
        DrillInSetAdapter.ViewHolder holder = new DrillInSetAdapter.ViewHolder();
        holder.drillName = (TextView) v.findViewById(R.id.drill_name);
        holder.drillRepeatIcon = (ImageView) v.findViewById(R.id.repeat_icon);
        holder.drillPrevIcon = (ImageView) v.findViewById(R.id.drill_join_prew_icon);
        holder.drillShuffle = (ImageView) v.findViewById(R.id.drill_shuffle_icon);
        holder.drillWait = (TextView) v.findViewById(R.id.drill_wait);
        holder.drillVariantBox = (LinearLayout) v.findViewById(R.id.variant_box);
        holder.drillVariantNumber = (TextView) v.findViewById(R.id.variant_text);
        holder.drillVariantPlus = (Button) v.findViewById(R.id.variant_plus);
        holder.drillVariantMinus = (Button) v.findViewById(R.id.variant_minus);
        holder.drillRemove = (ImageView)  v.findViewById(R.id.remove);
        holder.drillEdit = (ImageView)  v.findViewById(R.id.edit);
        holder.drillSwipeLayout = (SwipeLayout) v.findViewById(R.id.drill_set_item);
        holder.drillGrab = (ImageView) v.findViewById(R.id.handle);
        return holder;
    }

    private static class ViewHolder {
        public TextView drillName;
        public ImageView drillRepeatIcon;
        public ImageView drillPrevIcon;
        public TextView drillWait;
        public LinearLayout drillVariantBox;
        public TextView drillVariantNumber;
        public Button drillVariantPlus;
        public Button drillVariantMinus;
        public ImageView drillRemove;
        public ImageView drillEdit;
        public ImageView drillShuffle;
        public SwipeLayout drillSwipeLayout;
        public ImageView drillGrab;
    }

    /**
     * Dialog for select number ofrepeat drill set
     * @param ctx - aplication context
     * @param position - value of actual repeat drill set
     */
    private void numberSelectDialog(final Context ctx, final int position){
        int value = data.get(position).getRepeat();
        RelativeLayout linearLayout = new RelativeLayout(ctx);
        ContextThemeWrapper cw = new ContextThemeWrapper(ctx, R.style.NumberPickerText);
        final NumberPicker aNumberPicker = new NumberPicker(cw);
        aNumberPicker.setMaxValue(20);
        aNumberPicker.setMinValue(1);
        aNumberPicker.setValue(value);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        linearLayout.addView(aNumberPicker,numPicerParams);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle(ctx.getResources().getString(R.string.alert_dialog_repeat_set_select_title));
        alertDialogBuilder.setView(linearLayout);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(ctx.getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // redraw new value
                                int newRepeatVal = aNumberPicker.getValue();
                                data.get(position).setRepeat(newRepeatVal);
                                //Log.i("","New repeat value : "+ newRepeatVal);
                                adapter.notifyDataSetChanged();
                                //store new value in DB
                                SQLiteDB db = new SQLiteDB(ctx);
                                db.updateDrillSetRepeat(DrillSetId, newRepeatVal);
                                db.close();

                            }
                        })
                .setNegativeButton(ctx.getResources().getString(R.string.back_text),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
