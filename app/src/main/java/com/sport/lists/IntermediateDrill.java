package com.sport.lists;

/**
 * Created by Jirka on 27.1.2019.
 */
public class IntermediateDrill {

    private int id;             // id of intermediate drill
    private int idDrill;        // drill id
    private int idSetComplete;  // complete set id
    private String drillName;   // intermediate drill name
    private int time;           // intermediate time
    private int detected;       // number of detection
    private int iteration;      // number of actual iteration
    private int position;       // position in drill set
    private String note;        // note
    private boolean skipped;    // is drill skipped

    public IntermediateDrill(int id, int idDrill, int idSetComplete, String drillName, int iteration, int position, boolean skipped, int time,
                             String note){
        this.id = id;
        this.idDrill = idDrill;
        this.idSetComplete = idSetComplete;
        this.drillName = drillName;
        this.iteration = iteration;
        this.position = position;
        this.skipped = skipped;
        this.time = time;
        this.note = note;
    }

    public String getDrillName() {
        return drillName;
    }

    public int getIteration() {
        return iteration;
    }

    public int getDetected() {
        return detected;
    }

    public int getPosition() {
        return position;
    }

    public int getTime() {
        return time;
    }

    public String getNote() {
        return note;
    }

    public boolean isSkipped() {
        return skipped;
    }
}
