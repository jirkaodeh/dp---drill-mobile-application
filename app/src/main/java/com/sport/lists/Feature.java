package com.sport.lists;

public class Feature {
    private double cls;
    private double[] features;

    public Feature(double cls, double[] features) {
        this.cls = cls;
        this.features = features;
    }

    public double getCls() {
        return cls;
    }

    public double[] getFeatures() {
        return features;
    }
}
