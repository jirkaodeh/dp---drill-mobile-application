package com.sport.lists;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.sport.DrillOverviewNewDrillActivity;
import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.utils.Color;

import java.util.List;

import static android.view.View.GONE;

/**
 * Created by Jirka on 18.01.2019.
 * Class represents drill overview in drill list
 */
public class DrillOverviewAdapter extends ArrayAdapter<Drill>{

    private List<Drill> data;
    private Context context;
    private DrillOverviewAdapter adapter;

    /** Constructor drill list adapter
     *
     * @param context app context
     * @param layoutResourceId  reference to layout on list item style
     * @param data array of DrillSet objects with informations abbout each drill
     */
    public DrillOverviewAdapter(Context context, int layoutResourceId, List<Drill> data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.adapter = this;
    }

    /**
     * Return count of all drill in set
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     * @param position index to ArrayList to concrete object
     * @return DrillSet object with data
     */
    @Override
    public Drill getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        DrillOverviewAdapter.ViewHolder holder;
        final Drill drill = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_drill_overview, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (DrillOverviewAdapter.ViewHolder) convertView.getTag();
        }

        holder.drillName.setText(drill.getDrillName());
        holder.drillGroupName.setText("Skupina: "+drill.getDrillGroupName());
        holder.drilGroupColor.setBackgroundColor(getContext().getResources().getColor(Color.getColorById(drill.getColor())));
        if(drill.isAlternative()){
            holder.drillVariantBox.setVisibility(View.VISIBLE);
            holder.drillVariantNumber.setText(String.valueOf(getItem(position).getVariantXTimes()));
        }
        else{
            holder.drillVariantBox.setVisibility(View.GONE);
        }
        // wait text
        if(drill.getTime() != 0) {
            holder.drillWait.setText(getContext().getResources().getString(R.string.wait) + " " + String.valueOf(drill.getTime()) + " s");
        }
        else if(drill.isSndWait()){
            Snd snd = drill.getSndInfo(context);
            if(snd != null) {
                holder.drillWait.setText(getContext().getResources().getString(R.string.wait_snd) + " " + snd.getName());
            }
        }
        else {
            holder.drillWait.setText(getContext().getResources().getString(R.string.dont_wait));
        }

        if(drill.isShuffle()){
            holder.drillShuffle.setVisibility(View.VISIBLE);
        }
        else {
            holder.drillShuffle.setVisibility(GONE);
        }


        // action plus button click
        holder.drillVariantPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.get(position).setVariantXTimesWithDB(getContext(), 1, 0);
                adapter.notifyDataSetChanged();
                //Snackbar.make(v, "+"+position, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        // action minus button click
        holder.drillVariantMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.get(position).setVariantXTimesWithDB(getContext(), -1, 0);
                adapter.notifyDataSetChanged();
                //Snackbar.make(v, "-"+position, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        holder.drillSwipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        holder.drillSwipeLayout.addDrag(SwipeLayout.DragEdge.Left, convertView.findViewById(R.id.bottom_wrapper));
        holder.drillSwipeLayout.setLeftSwipeEnabled(false);
        holder.drillSwipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {
            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        // remove drill
        holder.drillRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDB db = new SQLiteDB(context);
                db.deleteDrill(data.get(position).getIdDrill());
                data.remove(position);
                adapter.notifyDataSetChanged();
            }
        });

        // edit drill
        holder.drillEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DrillOverviewNewDrillActivity.class);
                intent.putExtra("DRILL_ID", data.get(position).getIdDrill());
                intent.putExtra("COLOR", data.get(position).getColor());
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    /**
     * Create new holder
     * @param v - vychozi view pro ziskani prvku ukladanych do holderu
     * @return novy objekt holderu ViewHolder
     */
    private DrillOverviewAdapter.ViewHolder createViewHolder(View v) {
        DrillOverviewAdapter.ViewHolder holder = new DrillOverviewAdapter.ViewHolder();
        holder.drillName = (TextView) v.findViewById(R.id.drill_name);
        holder.drillWait = (TextView) v.findViewById(R.id.drill_wait);
        holder.drillVariantBox = (LinearLayout) v.findViewById(R.id.variant_box);
        holder.drillVariantNumber = (TextView) v.findViewById(R.id.variant_text);
        holder.drillVariantPlus = (Button) v.findViewById(R.id.variant_plus);
        holder.drillVariantMinus = (Button) v.findViewById(R.id.variant_minus);
        holder.drillRemove = (ImageView)  v.findViewById(R.id.remove);
        holder.drillEdit = (ImageView)  v.findViewById(R.id.edit);
        holder.drillSwipeLayout = (SwipeLayout) v.findViewById(R.id.drill_set_item);
        holder.drilGroupColor = (View)v.findViewById(R.id.group_color);
        holder.drillGroupName = (TextView) v.findViewById(R.id.drill_group);
        holder.drillShuffle = (ImageView) v.findViewById(R.id.drill_shuffle_icon);
        return holder;
    }

    private static class ViewHolder {
        public TextView drillName;
        public TextView drillWait;
        public LinearLayout drillVariantBox;
        public TextView drillVariantNumber;
        public Button drillVariantPlus;
        public Button drillVariantMinus;
        public ImageView drillRemove;
        public ImageView drillEdit;
        public SwipeLayout drillSwipeLayout;
        public View drilGroupColor;
        public TextView drillGroupName;
        public ImageView drillShuffle;
    }
}
