package com.sport.lists;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sport.DrillOverviewNewDrillActivity;
import com.sport.R;
import com.sport.SoundCreateEditClassActivity;
import com.sport.database.SQLiteDB;
import com.sport.fragments.main.CreateNewDrillFragment;
import com.sport.sound.FeatureFile;
import com.sport.sound.Record;
import com.sport.sound.StoreFile;
import com.sport.utils.HelpDialogs;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import static android.os.AsyncTask.Status.RUNNING;

public class SndEditAdapter extends ArrayAdapter<Snd> {

    private List<Snd> data;
    private Context context;
    private PlayFileTask playFileTask;
    private Activity activity;

    /**
     * Constructor sound list adapter
     *
     * @param context          app context
     * @param layoutResourceId reference to layout on list item style
     * @param data             array of Snd objects with informations abbout each sound
     */
    public SndEditAdapter(Context context, int layoutResourceId, List<Snd> data,  Activity activity) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.activity = activity;
        this.data = data;
    }

    /**
     * Return count of all snd in set
     *
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     *
     * @param position index to ArrayList to concrete object
     * @return Snd object with data
     */
    @Override
    public Snd getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final SndEditAdapter.ViewHolder holder;
        final Snd snd = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_edit_snd, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (SndEditAdapter.ViewHolder) convertView.getTag();
        }

        if(snd.getCls() < SQLiteDB.SND_FIRST_ADD_ID)
            holder.editBtn.setVisibility(View.GONE);

        holder.sndPatterns.setVisibility(View.GONE);

        if(!snd.isAdded()){
            holder.status.setBackgroundColor(getContext().getResources().getColor(R.color.snd_green));
        }
        else{
            int files = StoreFile.getRecordsByClassId(snd.getCls()).size();
            if(files <= 7){
                holder.status.setBackgroundColor(getContext().getResources().getColor(R.color.snd_red));
            }
            else if(files < 12){
                holder.status.setBackgroundColor(getContext().getResources().getColor(R.color.snd_orange));
            }
            else if(files < 22){
                holder.status.setBackgroundColor(getContext().getResources().getColor(R.color.snd_green));
            }
            else {
                holder.status.setBackgroundColor(getContext().getResources().getColor(R.color.snd_orange));
            }
        }

        holder.sndName.setText(snd.getName());
        // play btn
        holder.playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!snd.getExample().isEmpty()) {
                    if (snd.isAdded()) {
                        startPlay(StoreFile.getFile(context, snd.getExample()));
                    } else {
                        MediaPlayer playSnd = MediaPlayer.create(context, snd.getRawExample());
                        playSound(playSnd);
                        if (!playSnd.isPlaying()) {
                            playSnd.release();
                        }
                    }
                }

            }
        });
        // edit snd
        holder.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SoundCreateEditClassActivity.class);
                intent.putExtra("SOUND_ID", snd.getId());
                context.startActivity(intent);
            }
        });
        // set active
        holder.activeSnd.setChecked(snd.isActive());
        holder.activeSnd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked && actualActiveClass() >= 5){
                    HelpDialogs.limitSoundClassWarningDialog(context, activity);
                    holder.activeSnd.setChecked(false);
                }
                else {
                    FeatureFile.isModified = true;
                    int active = (isChecked ? 1 : 0);
                    SQLiteDB db = new SQLiteDB(context);
                    db.setSndActive(snd.getId(), active);
                    snd.setActive(active);
                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }

    /**
     * Get number of actual active sound class
     * @return number of actual active sound class
     */
    private int actualActiveClass(){
        int actualActive = 0;
        for(int i = 0; i < data.size(); i++){
            if(data.get(i).isActive())
                actualActive++;
        }
        return actualActive;
    }

    /**
     * Play sound in mp
     * @param mp - sound that may be play
     */
    private void playSound(MediaPlayer mp){
        mp.start();
    }

    /**
     * Create new holder
     *
     * @param v - view to get holder
     * @return new holder object
     */
    private SndEditAdapter.ViewHolder createViewHolder(View v) {
        SndEditAdapter.ViewHolder holder = new SndEditAdapter.ViewHolder();
        holder.sndName = (TextView) v.findViewById(R.id.snd_name);
        holder.sndPatterns = (TextView)v.findViewById(R.id.snd_patterns);
        holder.playBtn = (ImageButton) v.findViewById(R.id.snd_play);
        holder.editBtn = (ImageButton) v.findViewById(R.id.snd_edit);
        holder.activeSnd = (CheckBox) v.findViewById(R.id.snd_active);
        holder.status = (View) v.findViewById(R.id.status_color);
        return holder;
    }

    private static class ViewHolder {
        public TextView sndName;
        public TextView sndPatterns;
        public ImageButton playBtn;
        public ImageButton editBtn;
        public CheckBox activeSnd;
        public View status;
    }

    /**
     * Play file sound in asnych task
     * @param f - sound file
     */
    public void startPlay(File f){
        stopPlay();
        playFileTask = new PlayFileTask(f);
        mStatusChecker.run();
    }

    /**
     * Stop asynch task with play sound
     */
    public void stopPlay(){
        if (playFileTask != null && !playFileTask.isCancelled() && playFileTask.getStatus() == RUNNING) {
            playFileTask.cancel(true);
        }
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                playFileTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);//execute();
            } finally {

            }
        }
    };

    public class PlayFileTask extends AsyncTask<Void, Void, Boolean> {

        private int sampleRate = 16000;
        private AudioTrack track;
        private File wav;
        int count = 512 * 1024;

        private PlayFileTask(File wav) {
            track = null;
            this.wav = wav;
        }

        @Override
        protected void onPreExecute(){
            int minSize = AudioTrack.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO
                    , AudioFormat.ENCODING_PCM_16BIT);
            track = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, minSize, AudioTrack.MODE_STREAM);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (track==null){
                Log.d("TCAudio", "audio track is not initialised ");
                return false;
            }

            FileInputStream in = null;
            DataInputStream dataIn = null;
            File file = wav;
            try {
                in = new FileInputStream(file);
                dataIn = new DataInputStream(in);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            byte allData[] = new byte[1];
            byte[] byteData = new byte[(file.length() < count ? (int)file.length()+1 : count)];
            int bytesread = 0, ret = 0;
            int size = (int) file.length()-44;
            try {
                track.play();
                while (bytesread < size) {
                    ret = dataIn.read(byteData, 0, (file.length() < count ? (int) file.length() + 1 : count));
                    if (bytesread == 0) {
                        allData = byteData;
                    } else {
                        allData = Record.concatenate(allData, byteData);
                    }
                    if (ret != -1) { // Write the byte array to the track
                        track.write(Arrays.copyOfRange(byteData, 44, ret), 0, ret-44);
                        bytesread += ret;
                    } else
                        break;
                }
                track.stop();
                track.release();
                if(in != null)
                    in.close();
                if(dataIn != null)
                    dataIn.close();

            } catch (Exception e){
                e.printStackTrace();
            }
            track.release();
            return true;
        }
    }
}
