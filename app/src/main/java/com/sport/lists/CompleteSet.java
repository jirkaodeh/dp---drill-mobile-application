package com.sport.lists;

import android.content.Context;
import android.database.Cursor;

import com.sport.database.SQLiteDB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
/**
 * Created by Jirka on 6.2.2019.
 * Class represent complete drill set (after and during drill set execute)
 */
public class CompleteSet {
    private int setCompleteId;      // complete drill set id
    private String drillSetName;    // drill set name
    private Date dateTime;          // date and time when set is completed
    private String note;            // complete set note
    private int percentage;         // percentage of complete drill set
    private long elapsedTime;       // sum time of execude drill set
    private Group group;            // group id
    private int mode;               // mode of execuded drill set
    private Context ctx;            // aplication context

    // constructor
    public CompleteSet(int setCompleteId, String drillSetName, String dateTimeString, int percentage, long elapsedTime, String note,
        int gid, int mode, Context ctx){
        this.setCompleteId = setCompleteId;
        this.drillSetName = drillSetName;
        this.dateTime = convertDateTime(dateTimeString);
        this.percentage = percentage;
        this.elapsedTime = elapsedTime;
        this.note = note;
        this.mode = mode;
        this.ctx = ctx;
        setGroup(gid);
    }

    /**
     * Convert date and time to print format
     * @param dateTime  - date and time
     * @return  date and time in string
     */
    private Date convertDateTime(String dateTime){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        try {
            return format.parse(dateTime);
        } catch (Exception e){
            return null;
        }
    }

    /**
     * Set group to complete drill set
     * @param gid - id of group
     */
    public void setGroup(int gid){
        SQLiteDB db = new SQLiteDB(ctx);
        Cursor itemFromDB = db.getGroupById(gid, false);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("gid"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer cnt = itemFromDB.getInt(itemFromDB.getColumnIndex("cnt"));
            String description = itemFromDB.getString(itemFromDB.getColumnIndex("description"));
            Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
            Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
            group = new Group(id, name, description, icon, color, cnt);
        }
        itemFromDB.close();
        db.close();
    }

    public Group getGroup() {
        return group;
    }

    public int getPercentage() {
        return percentage;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public String getDrillSetName() {
        return drillSetName;
    }

    public Date getDateTime(){
        return dateTime;
    }

    public int getSetCompleteId() {
        return setCompleteId;
    }

    public String getNote() {
        return note;
    }
    public int getMode() {
        return mode;
    }

    public String getDateTimeString(){
        if(dateTime != null) {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy (HH:mm)");
            return format.format(dateTime);
        }
        return "";
    }

    public String getModeString(){
        if(mode == 0)
            return "standardní pořadí";
        else
            return "náhodné pořadí";
    }
}
