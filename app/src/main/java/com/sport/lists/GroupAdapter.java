package com.sport.lists;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.sport.DrillOverviewActivity;
import com.sport.GroupDrillSetsActivity;
import com.sport.database.SQLiteDB;
import com.sport.utils.Color;
import com.sport.CreateNewGroupActivity;
import com.sport.R;
import com.sport.utils.SportIcon;

import java.util.List;

/**
 * Created by Jirka on 21.10.2018.
 * Adapter for print item listview for groups lists
 */
public class GroupAdapter extends ArrayAdapter<Group> {
    private final List<Group> groups;
    private Context context;
    private int layoutResourceId;

    /** Constructor drill set list adapter
     *
     * @param context app context
     * @param layoutResourceId  reference to layout on list item style
     * @param data array of Group objects with informations abbout each group
     */
    public GroupAdapter(Context context, int layoutResourceId, List<Group> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.groups = data;
    }

    /**
     * Return count of all groups
     * @return number that represent count of all groups in list
     */
    @Override
    public int getCount() {
        if (groups != null) {
            return groups.size();
        } else {
            return 0;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Group group = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_group, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GroupDrillSetsActivity.class);
                intent.putExtra("GROUP_ID", groups.get(position).getgId());
                intent.putExtra("GROUP_COLOR", groups.get(position).getColor());
                intent.putExtra("GROUP_NAME", groups.get(position).getName());
                context.startActivity(intent);
            }
        });

        holder.groupChangeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(v, groups.get(position).getgId(), groups.get(position).getColor(), position);

                //removeGroupDialog(groups.get(position).gId);
            }
        });

        holder.groupName.setText(group.getName());
        holder.groupDescription.setText(group.getDescription());
        holder.groupCoundDrillSets.setText(getContext().getResources().getString(R.string.drills_in_group)+" "+String.valueOf(group.getNumberOfSetsInGroup(context)));
        holder.groupIcon.setImageDrawable(null);
        holder.groupIcon.setBackgroundResource(SportIcon.getGroupIconWhite(group.getIcon()));
        holder.groupBox.setBackgroundColor(getContext().getResources().getColor(Color.getColorById(group.getColor())));

        return convertView;
    }

    private void showMenu(View v, final int groupId, final int color, final int position){
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.item_edit:
                        Intent intent = new Intent(context, CreateNewGroupActivity.class);
                        intent.putExtra("GROUP_ID", groupId);
                        intent.putExtra("GROUP_COLOR", color);
                        context.startActivity(intent);
                        return true;
                    case R.id.item_remove:
                        removeGroupDialog(groupId, position);
                        return true;
                    default:
                        return false;
                }
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.list_menu, popup.getMenu());
        popup.show();
    }


    private void removeGroupDialog(final int removeGroupId, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getResources().getString(R.string.remove_group_title));
        builder.setMessage(getContext().getResources().getString(R.string.remove_group_text));
        builder
                .setCancelable(false)
                .setPositiveButton(getContext().getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        SQLiteDB db = new SQLiteDB(getContext());
                        db.deleteGroupWithDrills(removeGroupId);
                        db.close();
                        groups.remove(position);
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton(getContext().getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.drillSubText));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getContext().getResources().getColor(R.color.drillSubText));
            }
        });
        dialog.show();
    }

    /**
     * Vytvoreni noveho holderu
     * @param v - vychozi view pro ziskani prvku ukladanych do holderu
     * @return novy objekt holderu ViewHolder
     */
    private ViewHolder createViewHolder(View v) {
        ViewHolder holder = new ViewHolder();
        holder.groupName = (TextView) v.findViewById(R.id.group_name);
        holder.groupCoundDrillSets = (TextView) v.findViewById(R.id.group_cout_drill_set);
        holder.groupDescription = (TextView) v.findViewById(R.id.group_description);
        holder.groupIcon = (ImageView) v.findViewById(R.id.group_icon);
        holder.groupBox = (LinearLayout) v.findViewById(R.id.group_box_item);
        holder.groupChangeIcon = (ImageButton) v.findViewById(R.id.group_change);
        return holder;
    }

    private static class ViewHolder {
        public TextView groupName;
        public ImageView groupIcon;
        public TextView groupCoundDrillSets;
        public TextView groupDescription;
        public LinearLayout groupBox;
        public ImageButton groupChangeIcon;
    }
}
