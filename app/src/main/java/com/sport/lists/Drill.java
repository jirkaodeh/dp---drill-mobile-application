package com.sport.lists;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.sport.database.InflectionDB;
import com.sport.database.SQLiteDB;

/**
 * Created by Jirka on 18.01.2019.
 * Class represents drill in aplication
 */
public class Drill {
    private int idDrill; // id of drill in drill tb
    private String drillName; // original name of drill
    private int idGroup; // id od group
    private String drillGroup; // name of drill group
    private boolean alternative; // true - drill is variable  ¦ false - drill is not variable
    private int variantXTimes; // number of how many times must be make this drill (only if alternative is true)
    private int variantPosition; // position variant int text (pr.: go 5 steps to left - variantPosition = 2)
    private boolean timeWait; // wait before go to next drill
    private int time; // wait time before go to next drill in seconds
    private boolean sndWait; // wait to sound (true - wait to sound detection, false - not wait to detection)
    private int snd; // sound (is sndWait is set to true)
    private boolean inflection; // is drill alternative inflection
    private boolean shuffle; // is drill allow to shuffle
    private boolean shuffleModify; // is drill allow to modify
    private int color; // drill color

    public Drill(int idDrill, String drillName, int idGroup, String drillGroup, boolean alternative, int variantPosition,
    boolean timeWait, int time, boolean sndWait, int snd, boolean inflection, boolean shuffle, boolean shuffleModify){
        this.idDrill = idDrill;
        this.drillName = drillName;
        this.idGroup = idGroup;
        this.drillGroup = drillGroup;
        this.alternative = alternative;
        this.variantPosition = variantPosition;
        this.timeWait = timeWait;
        this.time = time;
        this.sndWait = sndWait;
        this.snd = snd;
        this.inflection = inflection;
        this.shuffle = shuffle;
        this.shuffleModify = shuffleModify;
        String[] parts = drillName.split(" ");
        if(alternative && variantPosition > 0 && parts.length > (variantPosition-1)){
            this.variantXTimes = Integer.valueOf(parts[variantPosition-1]);
        }
        else {
            this.variantXTimes = 0;
        }
    }

    /**
     * Set new variantXTimes with DB store
     * @param ctx - context
     * @param variant - 1 -> +1 / -1 -> -1 / otherwise -> xTimes
     * @param xTimes - new nalue of variantXTimes
     */
    public void setVariantXTimesWithDB(Context ctx, int variant, int xTimes) {
        int oldVariantXTimes = variantXTimes;
        if (variant == 1){
            variantXTimes++;
        }
        else if(variant == -1){
            if(variantXTimes <= 1)
                return;
            variantXTimes--;
        }
        else{
            variantXTimes = xTimes;
        }
        if(isTimeWait()){
            SQLiteDB db = new SQLiteDB(ctx);
            double newTimeOnePart = time / oldVariantXTimes;
            time = (int)Math.round(newTimeOnePart * variantXTimes);
            db.updateDrillInSetWaitTime(idDrill, time);
            db.close();
        }
        alternativeDrillName(ctx, variantXTimes, oldVariantXTimes);
        SQLiteDB db = new SQLiteDB(ctx);
        db.updateDrillInSetVariantNumAndName(-1, idDrill, variantXTimes, drillName);
        db.close();
    }

    /**
     * Update drill text by variant xTimes
     * @param ctx - context
     * @param xTimes - new value of variantXTimes
     * @param oldXTimes - old value of variantXTimes
     */
    private void alternativeDrillName(Context ctx, int xTimes, int oldXTimes){
        String[] parts = drillName.split(" ");
        if(parts.length > (variantPosition-1)){
            parts[variantPosition-1] = String.valueOf(xTimes);
            if(/*inflection*/ parts.length > variantPosition){
                parts[variantPosition] = inflectionDrillName(ctx, parts[variantPosition], xTimes, oldXTimes);
            }
            drillName = parts[0]+" ";
            for(int i = 1; i < parts.length; i++){
                drillName += parts[i];
                if(parts.length != (parts.length-1)){
                    drillName += " ";
                }
            }
        }
    }

    /**
     * Change inflection word after variant value
     * @param ctx - context
     * @param word - word that cant be change
     * @param xTimes - new value of variantXTimes
     * @param oldXTimes - old value of variantXTimes
     * @return
     */
    private String inflectionDrillName(Context ctx, String word, int xTimes, int oldXTimes){
        String name1 = null;
        String name24 = null;
        String name5 = null;
        if(oldXTimes <= 1)
            name1 = word;
        else if(oldXTimes < 5 && oldXTimes > 1)
            name24 = word;
        else if(oldXTimes > 4)
            name5 = word;
        InflectionDB db = new InflectionDB(ctx);
        Cursor itemFromDB = db.getInflection(name1, name24, name5);
        boolean iter = false;
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            iter = true;
            name1 = itemFromDB.getString(itemFromDB.getColumnIndex("name1"));
            name24 = itemFromDB.getString(itemFromDB.getColumnIndex("name24"));
            name5 = itemFromDB.getString(itemFromDB.getColumnIndex("name5"));
        }
        db.close();
        String result = word;
        if(iter && inflection){
            switch (xTimes){
                case 1: result = name1; break;
                case 2:
                case 3:
                case 4: result = name24; break;
                default: result = name5; break;
            }
        }
        return result;
    }

    /**
     * Get sound class info
     * @param ctx - aplication context
     * @return Snd class with sound info
     */
    public Snd getSndInfo(Context ctx){
        Snd sndResult = null;
        if(sndWait) {
            SQLiteDB db = new SQLiteDB(ctx);
            Cursor itemFromDB = db.getSndById(snd);
            for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
                Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id"));
                String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
                String example = itemFromDB.getString(itemFromDB.getColumnIndex("example"));
                Integer cls = itemFromDB.getInt(itemFromDB.getColumnIndex("class"));
                Integer active = itemFromDB.getInt(itemFromDB.getColumnIndex("active"));
                Integer added = itemFromDB.getInt(itemFromDB.getColumnIndex("added"));
                sndResult = new Snd(id, name, example, cls, active, added);
            }
            db.close();
        }
        return sndResult;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public String getDrillName() {
        return drillName;
    }

    public String getDrillGroupName(){
        return drillGroup;
    }

    public boolean isShuffle(){
        return shuffle;
    }

    public boolean isShuffleModify() {
        return shuffleModify;
    }

    public boolean isAlternative() {
        return alternative;
    }

    public boolean isInflection() {
        return inflection;
    }

    public int getVariantPosition() {
        return variantPosition;
    }

    public boolean isTimeWait() {
        return timeWait;
    }

    public boolean isSndWait() {
        return sndWait;
    }

    public int getTime() {
        return time;
    }

    public int getIdDrill() {
        return idDrill;
    }

    public int getVariantXTimes() {
        return variantXTimes;
    }

    /**
     * Add drill to drill set
     * @param ctx - context
     * @param setId - drill set id
     */
    public void saveSelectedDrillToDB(Context ctx, int setId){
        SQLiteDB db = new SQLiteDB(ctx);
        Cursor itemFromDB = db.getLastDrillInSetPosition(setId);
        Integer max = 0;
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            max = itemFromDB.getInt(itemFromDB.getColumnIndex("max"));
        }

        Integer sndWaitInt = (sndWait ? 1 : 0);
        Integer timeWaitInt = (timeWait ? 1 : 0);
        Integer alternativeInt = (alternative ? 1 : 0);
        Integer inflectionInt = (inflection ? 1 : 0);
        Integer shuffleInt = (shuffle ? 1 : 0);
        Integer shuffleModifyInt = (shuffleModify ? 1 : 0);
        Integer variableValue = 0;
        if(alternative) {
            String[] parts = drillName.split(" ");
            variableValue = Integer.valueOf(parts[variantPosition - 1]);
        }

        Long drilId = db.insertDrill(drillName, idGroup, 1, sndWaitInt, snd, 0, timeWaitInt, time, alternativeInt, variantPosition,
                inflectionInt, shuffleInt, shuffleModifyInt, 0);

        db.insertDrillIntoSet(setId, drilId.intValue(), max + 1, 0, 0,
                variableValue, "", 0);
        db.close();
    }
}
