package com.sport.lists;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.sport.CreateNewDrillActivity;
import com.sport.R;
import com.sport.recycleList.ItemTouchHelperAdapter;
import com.sport.recycleList.ItemTouchHelperViewHolder;
import com.sport.recycleList.OnStartDragListener;
import com.sport.database.SQLiteDB;

import java.util.Collections;
import java.util.List;

import static android.view.View.GONE;

/**
 * Created by Jirka on 8.2.2019.
 * Class represent drill set list
 */
public class DrillInSetRecycleAdapter extends RecyclerView.Adapter<DrillInSetRecycleAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {

    private List<DrillInSet> data;
    private Context context;
    private int color, setId;
    private String groupName;
    private AlertDialog moveJoinItemDialog = null;

    private final OnStartDragListener mDragStartListener;

    // constructor
    public DrillInSetRecycleAdapter(Context context, OnStartDragListener dragStartListener, List<DrillInSet> data, int setId, int color, String groupName) {
        this.context = context;
        mDragStartListener = dragStartListener;
        this.data = data;
        this.color = color;
        this.groupName = groupName;
        this.setId = setId;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_drill_in_set, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {

        // item is repeater
        if(data.get(position).isRepeat()){
            holder.handleView.setVisibility(GONE);
            holder.drillRepeatIcon.setVisibility(View.VISIBLE);
            holder.drillPrevIcon.setVisibility(GONE);
            holder.drillWait.setVisibility(GONE);
            holder.drillVariantBox.setVisibility(GONE);
            holder.drillName.setText("Sada opakována "+String.valueOf(data.get(position).getRepeat())+"x");
            holder.drillShuffle.setVisibility(GONE);
        }
        //item is pause
        else if(data.get(position).isPause()){
            holder.handleView.setVisibility(View.VISIBLE);
            holder.drillRepeatIcon.setVisibility(View.GONE);
            holder.drillPrevIcon.setVisibility(GONE);
            holder.drillWait.setVisibility(View.VISIBLE);
            holder.drillVariantBox.setVisibility(GONE);
            holder.drillWait.setText(String.valueOf(data.get(position).getPauseTime())+" s");
            holder.drillName.setText(context.getResources().getString(R.string.pause));
            holder.drillShuffle.setVisibility(GONE);
        }
        // item is normal drill
        else{
            holder.handleView.setVisibility(View.VISIBLE);
            holder.drillWait.setVisibility(View.VISIBLE);
            holder.drillRepeatIcon.setVisibility(View.GONE);
            if(data.get(position).isJointToPrevDrill()){
                holder.drillPrevIcon.setVisibility(View.VISIBLE);
            }
            else{
                holder.drillPrevIcon.setVisibility(View.GONE);
            }
            if(data.get(position).isShuffle()){
                holder.drillShuffle.setVisibility(View.VISIBLE);
            }
            else {
                holder.drillShuffle.setVisibility(GONE);
            }
            holder.drillName.setText(data.get(position).getName());
            if(data.get(position).isAlternative()){
                holder.drillVariantBox.setVisibility(View.VISIBLE);
                holder.drillVariantNumber.setText(String.valueOf(data.get(position).getVariantXTimes()));
            }
            else{
                holder.drillVariantBox.setVisibility(View.GONE);
            }
            // wait text
            if(data.get(position).getTime() != 0) {
                holder.drillWait.setText(context.getResources().getString(R.string.wait) + " " + String.valueOf(data.get(position).getTime()) + " s");
            }
            else if(data.get(position).isSndWait()){
                Snd snd = data.get(position).getSndInfo(context);
                if(snd != null) {
                    holder.drillWait.setText(context.getResources().getString(R.string.wait_snd) + " " + snd.getName());
                }
                else {
                    holder.drillWait.setTextColor(context.getResources().getColor(R.color.warning));
                    holder.drillWait.setText(context.getResources().getString(R.string.warning_no_snd));
                }
            }
            else {
                holder.drillWait.setText(context.getResources().getString(R.string.dont_wait));
            }
        }


        // action plus button click
        holder.drillVariantPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.get(position).setVariantXTimesWithDB(context, 1, 0);
                notifyDataSetChanged();
                //Snackbar.make(v, "+"+position, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        // action minus button click
        holder.drillVariantMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.get(position).setVariantXTimesWithDB(context, -1, 0);
                notifyDataSetChanged();
                //Snackbar.make(v, "-"+position, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
        // action when item in list is select
        if(data.get(position).isRepeat()) {
            holder.handleView.setVisibility(GONE);
            holder.bottomWrapper.setVisibility(GONE);
            holder.drillSwipeLayout.setLeftSwipeEnabled(false);
            holder.drillSwipeLayout.setRightSwipeEnabled(false);
            holder.drillItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    numberSelectDialog(context, position);
                }
            });
        }
        else {
            holder.drillSwipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
            holder.drillSwipeLayout.addDrag(SwipeLayout.DragEdge.Left, holder.bottomWrapper);
            holder.drillSwipeLayout.setLeftSwipeEnabled(false);
            holder.drillSwipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
                @Override
                public void onStartOpen(SwipeLayout layout) {
                }

                @Override
                public void onOpen(SwipeLayout layout) {

                }

                @Override
                public void onStartClose(SwipeLayout layout) {

                }

                @Override
                public void onClose(SwipeLayout layout) {

                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                }
            });
        }

        // remove drill
        holder.drillRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDB db = new SQLiteDB(context);
                db.deleteDrill(data.get(position).getIdDrill(), data.get(position).getIdDrill_drill_table());
                data.remove(position);
                notifyDataSetChanged();
            }
        });

        // edit drill
        holder.drillEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!data.get(position).isRepeat()) {
                    Intent intent = new Intent(context, CreateNewDrillActivity.class);
                    intent.putExtra("SET_ID", setId);
                    intent.putExtra("GROUP", groupName);
                    intent.putExtra("DRILL_ID", data.get(position).getIdDrill_drill_table());
                    intent.putExtra("DRILL_ID_IN_SET", data.get(position).getIdDrill());
                    intent.putExtra("COLOR", color);
                    if (data.get(position).isPause())
                        intent.putExtra("MODE", 2);
                    else if (!data.get(position).isPause())
                        intent.putExtra("MODE", 1);
                    context.startActivity(intent);
                    notifyDataSetChanged();
                }
            }
        });

        // Start a drag whenever the handle view it touched
        holder.handleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(holder);
                }
                return false;
            }
        });
    }


    @Override
    public void onItemDismiss(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        //Log.i("Move "+fromPosition+" "+toPosition, data.get(fromPosition).drillName+" "+data.get(toPosition).drillName+" "+data.get(toPosition).isRepeat());
        if(data.get(toPosition).isRepeat()){
            // do nothing
            //Log.i("isRepeat", "repeat");
        }
        else {
            if(data.get(fromPosition).isJointToPrevDrill() == true){
                moveJoinItemDialog(context);
            }
            else {
                SQLiteDB db = new SQLiteDB(context);
                db.editPosition(data.get(fromPosition).getIdDrill(), toPosition + 1);
                db.editPosition(data.get(toPosition).getIdDrill(), fromPosition + 1);
                db.close();
                Collections.swap(data, fromPosition, toPosition);
                notifyItemMoved(fromPosition, toPosition);
            }
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {

        public ImageView handleView;
        public TextView drillName;
        public ImageView drillRepeatIcon;
        public ImageView drillPrevIcon;
        public TextView drillWait;
        public LinearLayout drillVariantBox;
        public TextView drillVariantNumber;
        public Button drillVariantPlus;
        public Button drillVariantMinus;
        public ImageView drillRemove;
        public ImageView drillEdit;
        public ImageView drillShuffle;
        public SwipeLayout drillSwipeLayout;
        public LinearLayout bottomWrapper;
        public LinearLayout drillItem;

        public ItemViewHolder(View itemView) {
            super(itemView);
            drillName = (TextView) itemView.findViewById(R.id.drill_name);
            handleView = (ImageView) itemView.findViewById(R.id.handle);
            drillRepeatIcon = (ImageView) itemView.findViewById(R.id.repeat_icon);
            drillPrevIcon = (ImageView) itemView.findViewById(R.id.drill_join_prew_icon);
            drillShuffle = (ImageView) itemView.findViewById(R.id.drill_shuffle_icon);
            drillWait = (TextView) itemView.findViewById(R.id.drill_wait);
            drillVariantBox = (LinearLayout) itemView.findViewById(R.id.variant_box);
            drillVariantNumber = (TextView) itemView.findViewById(R.id.variant_text);
            drillVariantPlus = (Button) itemView.findViewById(R.id.variant_plus);
            drillVariantMinus = (Button) itemView.findViewById(R.id.variant_minus);
            drillRemove = (ImageView)  itemView.findViewById(R.id.remove);
            drillEdit = (ImageView)  itemView.findViewById(R.id.edit);
            drillSwipeLayout = (SwipeLayout) itemView.findViewById(R.id.drill_set_item);
            bottomWrapper = (LinearLayout) itemView.findViewById(R.id.bottom_wrapper);
            drillItem = (LinearLayout) itemView.findViewById(R.id.drill_item);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    /**
     * Dialog for select number ofrepeat drill set
     * @param ctx - aplication context
     * @param position - value of actual repeat drill set
     */
    private void numberSelectDialog(final Context ctx, final int position){
        int value;
        try {
            value = data.get(position).getRepeat();
        } catch (Exception e){
            value = 1;
        }
        RelativeLayout linearLayout = new RelativeLayout(ctx);
        ContextThemeWrapper cw = new ContextThemeWrapper(ctx, R.style.NumberPickerText);
        final NumberPicker aNumberPicker = new NumberPicker(cw);
        aNumberPicker.setMaxValue(20);
        aNumberPicker.setMinValue(1);
        aNumberPicker.setValue(value);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        linearLayout.addView(aNumberPicker,numPicerParams);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle(ctx.getResources().getString(R.string.alert_dialog_repeat_set_select_title));
        alertDialogBuilder.setView(linearLayout);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(ctx.getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // redraw new value
                                int newRepeatVal = aNumberPicker.getValue();
                                data.get(position).setRepeat(newRepeatVal);
                                //Log.i("","New repeat value : "+ newRepeatVal);
                                notifyDataSetChanged();
                                //store new value to DB
                                SQLiteDB db = new SQLiteDB(ctx);
                                db.updateDrillSetRepeat(setId, newRepeatVal);
                                db.close();

                            }
                        })
                .setNegativeButton(ctx.getResources().getString(R.string.back_text),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void moveJoinItemDialog(final Context ctx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(ctx.getResources().getString(R.string.move_join_item_title));
        builder.setMessage(ctx.getResources().getString(R.string.move_join_item_text));
        builder
                .setCancelable(true)
                .setPositiveButton(ctx.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        moveJoinItemDialog = null;
                    }
                });

        if(moveJoinItemDialog == null) {
            moveJoinItemDialog = builder.create();
            moveJoinItemDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface in) {
                    moveJoinItemDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ctx.getResources().getColor(R.color.drillSubText));
                }
            });
            moveJoinItemDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    moveJoinItemDialog = null;
                }
            });
            moveJoinItemDialog.show();
        }
    }
}

