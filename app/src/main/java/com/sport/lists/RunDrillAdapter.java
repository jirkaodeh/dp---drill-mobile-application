package com.sport.lists;


import android.content.Context;
import android.database.DataSetObserver;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sport.R;
import com.sport.RunDrillActivity;
import com.sport.utils.ProgressStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jirka on 28.11.2018.
 * Adapter for print item listview for drill lists
 */
public class RunDrillAdapter extends ArrayAdapter<DrillInSet>  {

    private final List<DrillInSet> data;
    private Context context;
    private int layoutResourceId;
    private long pauseOffset;
    private static boolean chronometerRunning;
    private RunDrillActivity act;
    RunDrillAdapter.ViewHolder holder;
    private ArrayList<RunDrillAdapter.ViewHolder> holderList;
    private double pStatus;
    private ProgressBar progress;
    private int noTick;
    //private  DataSetObserver dataSetObserver = new AdapterDataSetObserver();

    /** Constructor run drill list adapter
     *
     * @param context app context
     * @param layoutResourceId  reference to layout on list item style
     * @param data array of DrillSet objects with informations abbout each drill
     */
    public RunDrillAdapter(Context context, int layoutResourceId, List<DrillInSet> data, RunDrillActivity act) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.act = act;
        holderList = new ArrayList<>();
        chronometerRunning = false;
        noTick = 0;
    }

    /**
     * Method return object by position in list
     * @param position index to ArrayList to concrete object
     * @return DrillSet object with data
     */
    @Override
    public DrillInSet getItem(int position) {
        if (data != null && data.size() > position) {
            return data.get(position);
        } else {
            return null;
        }
    }

    /*public void myNotifyDataSetChange(){
        super.notifyDataSetChanged();
    }*/

    /**
     * Return count of all drill in set
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    public String formatTime(int time){
        int min = time / 60;
        int sec = time - min * 60;
        String minS = (min < 10 ? "0"+Integer.toString(min) : Integer.toString(min));
        String secS = (sec < 10 ? "0"+Integer.toString(sec) : Integer.toString(sec));
        return minS+":"+secS;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final DrillInSet drill = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_run_drill, null);
            holder = createViewHolder(convertView);
            holderList.add(holder);
            convertView.setTag(holder);

        } else {
            holder = (RunDrillAdapter.ViewHolder) convertView.getTag();
        }

        // drill name
        if(drill.isPause()){
            holder.drillName.setText(getContext().getResources().getString(R.string.pause)+" "+String.valueOf(drill.getPauseTime())+" s");
        }
        else {
            holder.drillName.setText(drill.getName());
        }

        // wait time
        if(drill.isTimeWait() || drill.isPause()) {
            if(drill.isPause()){
                holder.chronometer.setFormat("%s/" + formatTime(drill.getPauseTime()));
                holder.drillInfo.setText("00:00/"+drill.timeToStringFormat(drill.getPauseTime()));
            }
            else {
                holder.chronometer.setFormat("%s/" + formatTime(drill.getTime()));
                holder.drillInfo.setText("00:00/"+drill.timeToStringFormat(drill.getTime()));
            }
            if(drill.progressStaus == ProgressStatus.PREPARE) {
                holder.chronometer.setVisibility(View.GONE);
                holder.drillInfo.setVisibility(View.VISIBLE);
            }
            //holder.chronometer.setVisibility(View.VISIBLE);
            //holder.drillInfo.setVisibility(View.GONE);
        }
        else if(data.get(position).isSndWait()){
            Snd snd = data.get(position).getSndInfo(context);
            if(snd != null) {
                holder.drillInfo.setText(context.getResources().getString(R.string.wait_snd) + " " + snd.getName());
            }
            holder.chronometer.setVisibility(View.GONE);
            holder.drillInfo.setVisibility(View.VISIBLE);
        }
        else{
            holder.chronometer.setVisibility(View.GONE);
            holder.drillInfo.setVisibility(View.VISIBLE);
        }

        // stop drill execute and reset flags
        if(drill.stopAndResetDrill){
            drill.progressStaus = ProgressStatus.DONE;
            drill.chronometer = false;
            drill.chronometerPaused = false;
            drill.chronometerContinue = false;
            drill.stopAndResetDrill = false;
            drill.stopAndPrevDrill = false;
            holder.chronometer.stop();
            chronometerRunning = false;
            act.next();
        }
        else if(drill.stopAndPrevDrill){
            drill.progressStaus = ProgressStatus.PREPARE;
            drill.chronometer = false;
            drill.chronometerPaused = false;
            drill.chronometerContinue = false;
            drill.stopAndResetDrill = false;
            drill.stopAndPrevDrill = false;
            holder.chronometer.stop();
            holder.chronometer.setBase(SystemClock.elapsedRealtime());
            chronometerRunning = false;
            act.next();
        }

        if(!chronometerRunning && drill.chronometer && !drill.chronometerPaused && !drill.chronometerContinue) {
            if (drill.chronometer) {
                //holder.chronometer.setBase(SystemClock.elapsedRealtime());
                //System.out.println("ehm tady");
                if(drill.isTimeWait())
                    pStatus = -2 * (100.0 / (double)drill.getTime());
                else if(drill.isPause())
                    pStatus = -2 * (100.0 / (double)drill.getPauseTime());
                drill.progressStaus = ProgressStatus.ACTIIVE;
                pauseOffset = 0;
                startChronometer(holder.chronometer);
            } else {
                pauseChronometer(holder.chronometer);
            }
        }

        if(drill.chronometerPaused && !drill.chronometer){
            pauseChronometer(holder.chronometer);
            drill.chronometerContinue = true;
        }
        else if(!drill.chronometerPaused && drill.chronometer && drill.chronometerContinue){
            noTick = 2;
            startChronometer(holder.chronometer);
            drill.chronometerContinue = false;
        }

        //set prepare progressbar
        if(drill.progressStaus == ProgressStatus.PREPARE){
            holder.drillProgressBar.setProgress(0);
            holder.drillStick.setVisibility(View.GONE);
        }
        //set done progressbar
        else if(drill.progressStaus == ProgressStatus.DONE){
            holder.drillProgressBar.setProgress(100);
            holder.drillStick.setVisibility(View.VISIBLE);
        }
        //set active progressbar
        if(drill.progressStaus == ProgressStatus.ACTIIVE && drill.chronometer) {
            if(drill.isTimeWait() || drill.isPause()) {
                holder.chronometer.setVisibility(View.VISIBLE);
                holder.drillInfo.setVisibility(View.GONE);
            }
            holder.drillStick.setVisibility(View.GONE);
            if(progress != null && progress != holder.drillProgressBar){
                progress.setProgress(100);
            }

            progress = holder.drillProgressBar;
        }

        // chronometer handler
        holder.chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                if(drill.isTimeWait()) {
                    if(noTick > 0){
                        noTick--;
                    }
                    else if(drill.progressStaus == ProgressStatus.ACTIIVE && drill.chronometer) {
                        if (pStatus < 100.0) {
                            pStatus += (100.0 / (double)drill.getTime());
                            System.out.print(pStatus);
                            if(progress != null) {
                                System.out.println("print "+(int) pStatus);
                                progress.setProgress((int) pStatus);
                            }
                        }
                    }
                    if ((SystemClock.elapsedRealtime() - chronometer.getBase()) >= (drill.getTime() * 1000)) {
                        pauseChronometer(chronometer);
                        chronometer.stop();
                        drill.progressStaus = ProgressStatus.DONE;
                        if(progress != null)
                            progress.setProgress(100);
                        act.next();
                    }
                }
                else if(drill.isPause()) {
                    if(noTick > 0){
                        noTick--;
                    }
                    else if(drill.progressStaus == ProgressStatus.ACTIIVE && drill.chronometer) {
                        if (pStatus < 100.0) {
                            pStatus += (100.0 / (double)drill.getPauseTime());
                            System.out.print(pStatus);
                            if(progress != null) {
                                System.out.println("print "+(int) pStatus);
                                progress.setProgress((int) pStatus);
                            }
                        }
                    }
                    if ((SystemClock.elapsedRealtime() - chronometer.getBase()) >= (drill.getPauseTime() * 1000)) {
                        pauseChronometer(chronometer);
                        chronometer.stop();
                        drill.progressStaus = ProgressStatus.DONE;
                        if(progress != null)
                            progress.setProgress(100);
                        act.next();
                    }
                }
            }
        });

        return convertView;
    }




    /**
     * Create new holder
     * @param v - vychozi view pro ziskani prvku ukladanych do holderu
     * @return novy objekt holderu ViewHolder
     */
    private RunDrillAdapter.ViewHolder createViewHolder(View v) {
        RunDrillAdapter.ViewHolder holder = new RunDrillAdapter.ViewHolder();
        holder.drillName = (TextView) v.findViewById(R.id.drill_run_name);
        holder.drillStick = (ImageView) v.findViewById(R.id.run_drill_stick);
        holder.chronometer = (Chronometer) v.findViewById(R.id.drill_run_chronometer);
        holder.drillInfo = (TextView) v.findViewById(R.id.drill_run_info);
        holder.drillProgressBar = (ProgressBar) v.findViewById(R.id.run_circular_list_progressbar);
        return holder;
    }

    private static class ViewHolder {
        public TextView drillName;
        public ImageView drillStick;
        public ProgressBar drillProgressBar;
        public Chronometer chronometer;
        public TextView drillInfo;
    }



    // start time chronometer counting
    public void startChronometer(Chronometer chronometer){
        if(!chronometerRunning){
            System.out.println("tady");
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            chronometerRunning = true;
        }
    }

    // pause time chronometer counting
    public void pauseChronometer(Chronometer chronometer){
        if(chronometerRunning){
            System.out.println("tady2");
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            chronometerRunning = false;
        }
    }

    // stop and restart time chronometer counting
    public void resetChronometer(Chronometer chronometer){
        chronometer.setBase(SystemClock.elapsedRealtime());
        pauseOffset = 0;
    }
}
