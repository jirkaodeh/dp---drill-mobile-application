package com.sport.lists;

import android.content.Context;
import android.database.Cursor;

import com.sport.FinishActivity;
import com.sport.database.SQLiteDB;

/**
 * Created by Jirka on 10.10.2018.
 * Class with drill set info
 */
public class DrillSet {
    private int dsId;       // drill set id
    private String name;    // drill set name
    private int group;      // group id
    private String groupName;   // group name
    private int repeatCnt;  // repeat of drill set
    private int icon;       // set icon
    private int color;      // set color

    // constructor
    public DrillSet(int dsId, String name, int group, int color, int repeatCnt) {
        this.dsId = dsId;
        this.group = group;
        this.name = name;
        this.repeatCnt = repeatCnt;
        this.color = color;
    }

    // constructor
    public DrillSet(int dsId, String name, int group, int repeatCnt, String groupName, int icon, int color) {
        this.dsId = dsId;
        this.group = group;
        this.name = name;
        this.repeatCnt = repeatCnt;
        this.groupName = groupName;
        this.icon = icon;
        this.color = color;
    }

    /**
     * Get best time of drill set
     * @param ctx - context
     * @return - best time in string
     */
    public String getBestTime(Context ctx){
        int bestTime = Integer.MAX_VALUE;
        SQLiteDB db = new SQLiteDB(ctx);
        Cursor itemFromDB = db.getBestTimeOfDrillSet(dsId);
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("set_time"));
            if(time < bestTime){
                bestTime = time;
            }
        }
        itemFromDB.close();
        db.close();
        if(bestTime == Integer.MAX_VALUE){
            return "N/A";
        }
        return FinishActivity.getTime(bestTime);
    }

    public String getName() {
        return name;
    }

    public int getDsId() {
        return dsId;
    }

    public int getGroup() {
        return group;
    }

    public int getColor() {
        return color;
    }

    public int getIcon() {
        return icon;
    }

    public int getRepeatCnt() {
        return repeatCnt;
    }

    public String getGroupName() {
        return groupName;
    }
}
