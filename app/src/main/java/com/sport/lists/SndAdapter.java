package com.sport.lists;

import android.app.Activity;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.sport.DrillOverviewNewDrillActivity;
import com.sport.R;
import com.sport.fragments.main.CreateNewDrillFragment;
import com.sport.fragments.main.GlobalStatisticsHistoryListFragment;
import com.sport.sound.Record;
import com.sport.sound.StoreFile;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import static android.os.AsyncTask.Status.RUNNING;

/**
 * Created by Jirka on 2.2.2019.
 * Adapter to show and select sound
 */
public class SndAdapter extends ArrayAdapter<Snd> {

    private List<Snd> data;
    private Context context;
    private CreateNewDrillFragment fragmentCreateNewDrillFragment;
    private DrillOverviewNewDrillActivity activityDrillOverviewNewDrillActivity;
    private PlayFileTask playFileTask;

    /**
     * Constructor sound list adapter
     *
     * @param context          app context
     * @param layoutResourceId reference to layout on list item style
     * @param data             array of Snd objects with informations abbout each sound
     */
    public SndAdapter(Context context, int layoutResourceId, List<Snd> data, CreateNewDrillFragment fragment) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.fragmentCreateNewDrillFragment = fragment;
    }

    public SndAdapter(Context context, int layoutResourceId, List<Snd> data, DrillOverviewNewDrillActivity activity) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.activityDrillOverviewNewDrillActivity = activity;
    }

    /**
     * Return count of all snd in set
     *
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     *
     * @param position index to ArrayList to concrete object
     * @return Snd object with data
     */
    @Override
    public Snd getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        SndAdapter.ViewHolder holder;
        final Snd snd = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_select_snd, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (SndAdapter.ViewHolder) convertView.getTag();
        }

        holder.sndName.setText(snd.getName());
        holder.playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlay();
                if(!snd.getExample().isEmpty()) {
                    if (snd.isAdded()) {
                        startPlay(StoreFile.getFile(context, snd.getExample()));
                    } else {
                        MediaPlayer playSnd = MediaPlayer.create(context, snd.getRawExample());
                        playSound(playSnd);
                        if (!playSnd.isPlaying()) {
                            playSnd.release();
                        }
                    }
                }
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fragmentCreateNewDrillFragment != null) {
                    fragmentCreateNewDrillFragment.setSnd(snd.getCls(), snd.getName());
                }
                else if(activityDrillOverviewNewDrillActivity != null){
                    activityDrillOverviewNewDrillActivity.setSnd(snd.getCls(), snd.getName());
                }
            }
        });
        return convertView;
    }

    /**
     * Play sound in mp
     * @param mp - sound that may be play
     */
    private void playSound(MediaPlayer mp){
        mp.start();
    }

    /**
     * Create new holder
     *
     * @param v - view to get holder
     * @return new holder object
     */
    private SndAdapter.ViewHolder createViewHolder(View v) {
        SndAdapter.ViewHolder holder = new SndAdapter.ViewHolder();
        holder.sndName = (TextView) v.findViewById(R.id.snd_name);
        holder.playBtn = (ImageButton) v.findViewById(R.id.snd_play);
        return holder;
    }

    private static class ViewHolder {
        public TextView sndName;
        public ImageButton playBtn;
    }


    public void startPlay(File f){
        stopPlay();
        playFileTask = new SndAdapter.PlayFileTask(f);
        mStatusChecker.run();
    }

    public void stopPlay(){
        if (playFileTask != null && !playFileTask.isCancelled() && playFileTask.getStatus() == RUNNING) {
            playFileTask.cancel(true);
        }
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                playFileTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);//execute();
            } finally {

            }
        }
    };

    public class PlayFileTask extends AsyncTask<Void, Void, Boolean> {

        private int sampleRate = 16000;
        private AudioTrack track;
        private File wav;
        int count = 512 * 1024;

        private PlayFileTask(File wav) {
            track = null;
            this.wav = wav;
        }

        @Override
        protected void onPreExecute(){
            int minSize = AudioTrack.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO
                    , AudioFormat.ENCODING_PCM_16BIT);
            track = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, minSize, AudioTrack.MODE_STREAM);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (track==null){
                Log.d("TCAudio", "audio track is not initialised ");
                return false;
            }

            FileInputStream in = null;
            DataInputStream dataIn = null;
            File file = wav;
            try {
                in = new FileInputStream(file);
                dataIn = new DataInputStream(in);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            byte allData[] = new byte[1];
            byte[] byteData = new byte[(file.length() < count ? (int)file.length()+1 : count)];
            int bytesread = 0, ret = 0;
            int size = (int) file.length()-44;
            try {
                track.play();
                while (bytesread < size) {
                    ret = dataIn.read(byteData, 0, (file.length() < count ? (int) file.length() + 1 : count));
                    if (bytesread == 0) {
                        allData = byteData;
                    } else {
                        allData = Record.concatenate(allData, byteData);
                    }
                    if (ret != -1) { // Write the byte array to the track
                        track.write(Arrays.copyOfRange(byteData, 44, ret), 0, ret-44);
                        bytesread += ret;
                    } else
                        break;
                }
                track.stop();
                track.release();
                if(in != null)
                    in.close();
                if(dataIn != null)
                    dataIn.close();

            } catch (Exception e){
                e.printStackTrace();
            }
            track.release();
            return true;
        }
    }


}
