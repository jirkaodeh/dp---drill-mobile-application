package com.sport.lists;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sport.R;
import com.sport.SoundCreateEditClassActivity;
import com.sport.sound.FeatureFile;
import com.sport.sound.Record;
import com.sport.sound.StoreFile;
import com.sport.utils.HelpDialogs;
import com.sport.utils.Permission;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.os.AsyncTask.Status.RUNNING;

public class VawRecordAdapter extends ArrayAdapter<VawRecord> {

    private List<VawRecord> data;
    private Context context;
    private SoundCreateEditClassActivity activity;
    private PlayFileTask playFileTask;
    private RemoveFileTask removeFileTask;
    private android.support.v7.app.AlertDialog dialog;

    /**
     * Constructor sound list adapter
     *
     * @param context          app context
     * @param layoutResourceId reference to layout on list item style
     * @param data             array of Snd objects with informations abbout each sound
     */
    public VawRecordAdapter(Context context, int layoutResourceId, List<VawRecord> data, SoundCreateEditClassActivity activity) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.activity = activity;
    }

    /**
     * Return count of all snd in set
     *
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     *
     * @param position index to ArrayList to concrete object
     * @return VawRecord object with data
     */
    @Override
    public VawRecord getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        VawRecordAdapter.ViewHolder holder;
        final VawRecord wav = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_vaw_file, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (VawRecordAdapter.ViewHolder) convertView.getTag();
        }

        holder.removeProgress.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context, R.color.drillText), PorterDuff.Mode.SRC_IN);

        holder.sndName.setText("Vzor "+wav.getPosition());

        holder.playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlay();
                startPlay(wav.getFile());
            }
        });

        final ProgressBar progress = holder.removeProgress;
        holder.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = HelpDialogs.loadingDialog( context, activity, "Mazání vzoru");
                startRemove(wav, position);
            }
        });

        return convertView;
    }

    /**
     * Create new holder
     *
     * @param v - view to get holder
     * @return new holder object
     */
    private VawRecordAdapter.ViewHolder createViewHolder(View v) {
        VawRecordAdapter.ViewHolder holder = new VawRecordAdapter.ViewHolder();
        holder.sndName = (TextView) v.findViewById(R.id.vaw_name);
        holder.playBtn = (ImageButton) v.findViewById(R.id.vaw_play);
        holder.removeBtn = (ImageButton) v.findViewById(R.id.vaw_remove);
        holder.removeProgress = (ProgressBar) v.findViewById(R.id.vaw_remove_progress);
        return holder;
    }

    private static class ViewHolder {
        public TextView sndName;
        public ImageButton playBtn;
        public ImageButton removeBtn;
        public ProgressBar removeProgress;
    }


    public void startPlay(File f){
        stopPlay();
        playFileTask = new PlayFileTask(f);
        mStatusChecker.run();
    }

    public void stopPlay(){
        if (playFileTask != null && !playFileTask.isCancelled() && playFileTask.getStatus() == RUNNING) {
            playFileTask.cancel(true);
        }
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                playFileTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);//execute();
            } finally {

            }
        }
    };

    public class PlayFileTask extends AsyncTask<Void, Void, Boolean> {

        private int sampleRate = 16000;
        private AudioTrack track;
        private File wav;
        int count = 512 * 1024;

        private PlayFileTask(File wav) {
            track = null;
            this.wav = wav;
        }

        @Override
        protected void onPreExecute(){
            int minSize = AudioTrack.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO
                    , AudioFormat.ENCODING_PCM_16BIT);
            track = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, minSize, AudioTrack.MODE_STREAM);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (track==null){
                Log.d("TCAudio", "audio track is not initialised ");
                return false;
            }

            FileInputStream in = null;
            DataInputStream dataIn = null;
            File file = wav;
            try {
                in = new FileInputStream(file);
                dataIn = new DataInputStream(in);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            byte allData[] = new byte[1];
            byte[] byteData = new byte[(file.length() < count ? (int)file.length()+1 : count)];
            int bytesread = 0, ret = 0;
            int size = (int) file.length()-44;
            try {
                track.play();
                while (bytesread < size) {
                    ret = dataIn.read(byteData, 0, (file.length() < count ? (int) file.length() + 1 : count));
                    if (bytesread == 0) {
                        allData = byteData;
                    } else {
                        allData = Record.concatenate(allData, byteData);
                    }
                    if (ret != -1) { // Write the byte array to the track
                        track.write(Arrays.copyOfRange(byteData, 44, ret), 0, ret-44);
                        bytesread += ret;
                    } else
                        break;
                }
                track.stop();
                track.release();
                if(in != null)
                    in.close();
                if(dataIn != null)
                    dataIn.close();

            } catch (Exception e){
                e.printStackTrace();
            }
            track.release();
            return true;
        }
    }

    public void startRemove(VawRecord w, int position){
        removeFileTask = new RemoveFileTask(w, position);
        mStatusChecker2.run();
    }

    Runnable mStatusChecker2 = new Runnable() {
        @Override
        public void run() {
            try {
                removeFileTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } finally {}
        }
    };

    public class RemoveFileTask extends AsyncTask<Void, Void, Boolean> {

        VawRecord w;
        int position;
        StoreFile st;

        public RemoveFileTask(VawRecord w, int position){
            this.w = w;
            this.position = position;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            w.getFile().delete();
            st = new StoreFile(context);
            FeatureFile.removeFeatureFromFile(context, w.getSndClass(), position);
            st.getFilesName(context);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean result) {
            activity.updateList(st);
            if(dialog != null)
                dialog.dismiss();
        }
    }
}
