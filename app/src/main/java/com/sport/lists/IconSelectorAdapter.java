package com.sport.lists;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by Jirka on 21.10.2018.
 * Adapter for print icon selector
 */
public class IconSelectorAdapter extends BaseAdapter {
    private Context context;
    int[] imageIdArr;

    // constructor
    public IconSelectorAdapter(Context context, int[] images){
        this.context = context;
        this.imageIdArr = images;
    }

    @Override
    public int getCount(){
        return imageIdArr.length;
    }

    @Override
    public Object getItem(int position){
        return position;
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if(convertView == null){
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(85,85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(2,2,2,2);
        }
        else {
            imageView = (ImageView)convertView;
        }
        imageView.setImageResource(imageIdArr[position]);
        return imageView;
    }
}
