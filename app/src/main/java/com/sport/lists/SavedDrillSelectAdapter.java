package com.sport.lists;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.sport.DrillOverviewNewDrillActivity;
import com.sport.DrillSetListActivity;
import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.utils.Color;

import java.util.List;

import static android.view.View.GONE;

/**
 * Created by Jirka on 12.2.2019.
 * Adapter for print pre-saved drills to select
 */
public class SavedDrillSelectAdapter extends ArrayAdapter<Drill> {

    private List<Drill> data;
    private Context context;
    private DrillSetListActivity activity;

    /** Constructor drill list adapter
     *
     * @param context app context
     * @param layoutResourceId  reference to layout on list item style
     * @param data array of DrillSet objects with informations abbout each drill
     */
    public SavedDrillSelectAdapter(Context context, int layoutResourceId, List<Drill> data, DrillSetListActivity activity) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.activity = activity;
    }

    /**
     * Return count of all drill in set
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     * @param position index to ArrayList to concrete object
     * @return DrillSet object with data
     */
    @Override
    public Drill getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        SavedDrillSelectAdapter.ViewHolder holder;
        final Drill drill = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_select_saved_drill, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (SavedDrillSelectAdapter.ViewHolder) convertView.getTag();
        }

        holder.drillName.setText(drill.getDrillName());
        holder.drillGroupName.setText("Skupina: "+drill.getDrillGroupName());
        holder.drilGroupColor.setBackgroundColor(getContext().getResources().getColor(Color.getColorById(drill.getColor())));
        // wait text
        if(drill.getTime() != 0) {
            holder.drillWait.setText(getContext().getResources().getString(R.string.wait) + " " + String.valueOf(drill.getTime()) + " s");
        }
        else if(drill.isSndWait()){
            Snd snd = drill.getSndInfo(context);
            if(snd != null) {
                holder.drillWait.setText(getContext().getResources().getString(R.string.wait_snd) + " " + snd.getName());
            }
        }
        else {
            holder.drillWait.setText(getContext().getResources().getString(R.string.dont_wait));
        }

        if(drill.isShuffle()){
            holder.drillShuffle.setVisibility(View.VISIBLE);
        }
        else {
            holder.drillShuffle.setVisibility(GONE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.dialogSelectSavedDrillDismiss(data.get(position));
            }
        });

        return convertView;
    }

    /**
     * Create new holder
     * @param v - vychozi view pro ziskani prvku ukladanych do holderu
     * @return novy objekt holderu ViewHolder
     */
    private SavedDrillSelectAdapter.ViewHolder createViewHolder(View v) {
        SavedDrillSelectAdapter.ViewHolder holder = new SavedDrillSelectAdapter.ViewHolder();
        holder.drillName = (TextView) v.findViewById(R.id.drill_name);
        holder.drillWait = (TextView) v.findViewById(R.id.drill_wait);
        holder.drilGroupColor = (View)v.findViewById(R.id.group_color);
        holder.drillGroupName = (TextView) v.findViewById(R.id.drill_group);
        holder.drillShuffle = (ImageView) v.findViewById(R.id.drill_shuffle_icon);
        return holder;
    }

    private static class ViewHolder {
        public TextView drillName;
        public TextView drillWait;
        public View drilGroupColor;
        public TextView drillGroupName;
        public ImageView drillShuffle;
    }
}