package com.sport.lists;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Chronometer;

import com.sport.database.InflectionDB;
import com.sport.database.PreferenceStore;
import com.sport.database.SQLiteDB;
import com.sport.utils.ProgressStatus;

import java.util.StringJoiner;

/**
 * Created by Jirka on 23.10.2018.
 */
public class DrillInSet {
    private int idDrill; // id od drill in drill set
    private int idDrill_drill_table; // id od drill in drill set
    private int position; // position of drill in set
    private boolean isPause; // true - drill is pause ¦ false - drill is normal drill
    private int pauseTime; // pause time in seconds (set only if is_pause == true)
    private boolean alternative; // true - drill is variable  ¦ false - drill is not variable
    private int variantXTimes; // number of how many times must be make this drill (only if alternative is true)
    private String drillName; // original name of drill
    private String editName; // edited drill name (only if alternative is true)
    private boolean jointToPrevDrill; // true if drill is join to previous drill (drills can not be split)
    private boolean timeWait; // wait before go to next drill
    private int time; // wait time before go to next drill in seconds
    private int variantPosition; // position variant int text (pr.: go 5 steps to left - variantPosition = 2)
    private boolean sndWait; // wait to sound (true - wait to sound detection, false - not wait to detection)
    private int snd; // sound (is sndWait is set to true)
    private int sndDetectXTtimes; // wait to x times sound detection
    private int iteration; // iteration in which drill is
    private boolean inflection; // is drill alternative inflection
    private boolean shuffle; // is drill allow to shuffle
    private boolean shuffleModify;  // is drill allow to modify

    public boolean chronometer;  // indicate that chronometer is active
    public boolean chronometerPaused;   // indicate that chronometer is paused in list
    public boolean chronometerContinue; // indicate that chronometer cant continue in counting
    public ProgressStatus progressStaus; // indicate progressbar status
    public boolean stopAndResetDrill;    // indicate reset all info (chronometer, progressbar,...) about drill
    public boolean stopAndPrevDrill;    // indicate move to prev drill

    // special types of atribute
    private boolean isRepeat; // special type, that not represent drill, but how many time be all set repeat
    private int repeat; // how many times be set repeat

    // constructor of drill
    public DrillInSet(int idDrill, int idDrill_drill_table, int position, boolean isPause, int pauseTime, int variantXTimes, boolean alternative,
                      String drillName, String editName, boolean jointToPrevDrill, boolean timeWait, int time,
                      int variantPosition, boolean sndWait, int snd, int sndDetectXTtimes, boolean inflection, boolean shuffle,
                      boolean shuffleModify){
        this.idDrill = idDrill;
        this.idDrill_drill_table = idDrill_drill_table;
        this.position = position;
        this.isPause = isPause;
        this.pauseTime = pauseTime;
        this.variantXTimes = variantXTimes;
        this.alternative = alternative;
        this.drillName = drillName;
        this.editName = editName;
        this.jointToPrevDrill = jointToPrevDrill;
        this.timeWait = timeWait;
        this.time = time;
        this.variantPosition = variantPosition;
        this.sndWait = sndWait;
        this.snd = snd;
        this.sndDetectXTtimes = sndDetectXTtimes;
        this.inflection = inflection;
        this.chronometerPaused = false;
        this.chronometerContinue = false;
        this.progressStaus = ProgressStatus.PREPARE;
        this.stopAndResetDrill = false;
        this.stopAndPrevDrill = false;
        this.shuffle = shuffle;
        this.shuffleModify = shuffleModify;
        this.isRepeat = false;
        //reEditName();
    }

    // constructor of repeater
    public DrillInSet(boolean isRepeat, int repeat){
        this.isRepeat = isRepeat;
        this.repeat = repeat;
        this.variantPosition = Integer.MAX_VALUE;
    }

    // create edited name for variant
    private void reEditName(){
        if(!this.isPause && this.alternative){
           String[] nameSplit = this.drillName.split(" ");
           //int tmpXTimes = Integer.parseInt(nameSplit[this.variantPosition]);
           //TODO: suffix edit

           nameSplit[this.variantPosition-1] = String.valueOf(this.variantXTimes);
           StringBuilder builder = new StringBuilder();
           for(String s : nameSplit){
               builder.append(s+" ");
           }
           this.editName = builder.toString();
        }
        else {
            this.editName = null;
        }
    }

    /**
     * Convert drill time to string
     * @param time time (pause or wait)
     * @return string time
     */
    public String timeToStringFormat(int time){
        int min = time/60;
        int sec = (time%60);
        return ""+(min < 10 ? "0"+min : min)+":"+(sec < 10 ? "0"+sec : sec);
    }

    /**
     * Get pause time with prefix and postfix
     * @param addTextPause - add postfix (true), not add postfix (false)
     * @return pause time format in string
     */
    public String getPauseTimeInString( boolean addTextPause){
        String result = (addTextPause ? "Pauza " : "")+pauseTime+" ";
        switch (pauseTime){
            case 1: result += "sekunda"; break;
            case 2:
            case 3:
            case 4: result += "sekundy"; break;
            default: result += "sekund"; break;
        }
        return result;
    }

    /**
     * Set new variantXTimes with DB store
     * @param ctx - context
     * @param variant - 1 -> +1 / -1 -> -1 / otherwise -> xTimes
     * @param xTimes - new nalue of variantXTimes
     */
    public void setVariantXTimesWithDB(Context ctx, int variant, int xTimes) {
        int oldVariantXTimes = variantXTimes;
        if (variant == 1){
            variantXTimes++;
        }
        else if(variant == -1){
            if(variantXTimes <= 1)
                return;
            variantXTimes--;
        }
        else{
            variantXTimes = xTimes;
        }
        alternativeDrillName(ctx, variantXTimes, oldVariantXTimes);
        if(isTimeWait()){
            SQLiteDB db = new SQLiteDB(ctx);
            double newTimeOnePart = time / oldVariantXTimes;
            time = (int)Math.round(newTimeOnePart * variantXTimes);
            db.updateDrillInSetWaitTime(idDrill_drill_table, time);
            db.close();
        }
        SQLiteDB db = new SQLiteDB(ctx);
        db.updateDrillInSetVariantNumAndName(idDrill, idDrill_drill_table, variantXTimes, drillName);
        db.close();
    }

    /**
     * Update drill text by variant xTimes
     * @param ctx - context
     * @param xTimes - new value of variantXTimes
     * @param oldXTimes - old value of variantXTimes
     */
    private void alternativeDrillName(Context ctx, int xTimes, int oldXTimes){
        String[] parts = drillName.split(" ");
        if(parts.length > (variantPosition-1)){
            parts[variantPosition-1] = String.valueOf(xTimes);
            if(/*inflection*/ parts.length > variantPosition){
                parts[variantPosition] = inflectionDrillName(ctx, parts[variantPosition], xTimes, oldXTimes);
            }
            drillName = parts[0]+" ";
            for(int i = 1; i < parts.length; i++){
                drillName += parts[i];
                if(parts.length != (parts.length-1)){
                    drillName += " ";
                }
            }
        }
    }

    /**
     * Change inflection word after variant value
     * @param ctx - context
     * @param word - word that cant be change
     * @param xTimes - new value of variantXTimes
     * @param oldXTimes - old value of variantXTimes
     * @return
     */
    private String inflectionDrillName(Context ctx, String word, int xTimes, int oldXTimes){
        String name1 = null;
        String name24 = null;
        String name5 = null;
        if(oldXTimes <= 1)
            name1 = word;
        else if(oldXTimes < 5 && oldXTimes > 1)
            name24 = word;
        else if(oldXTimes > 4)
            name5 = word;
        InflectionDB db = new InflectionDB(ctx);
        Cursor itemFromDB = db.getInflection(name1, name24, name5);
        boolean iter = false;
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            iter = true;
            name1 = itemFromDB.getString(itemFromDB.getColumnIndex("name1"));
            name24 = itemFromDB.getString(itemFromDB.getColumnIndex("name24"));
            name5 = itemFromDB.getString(itemFromDB.getColumnIndex("name5"));
        }
        itemFromDB.close();
        db.close();
        String result = word;
        if(iter && inflection){
            switch (xTimes){
                case 1: result = name1; break;
                case 2:
                case 3:
                case 4: result = name24; break;
                default: result = name5; break;
            }
        }
        return result;
    }

    /**
     * Get information about sound (if drill is wait for sound)
     * @param ctx - actual context
     * @return Snd object with sound informations
     */
    public Snd getSndInfo(Context ctx){
        Snd sndResult = null;
        if(sndWait) {
            SQLiteDB db = new SQLiteDB(ctx);
            Cursor itemFromDB = db.getSndById(snd);
            for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
                Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id"));
                String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
                String example = itemFromDB.getString(itemFromDB.getColumnIndex("example"));
                Integer cls = itemFromDB.getInt(itemFromDB.getColumnIndex("class"));
                Integer active = itemFromDB.getInt(itemFromDB.getColumnIndex("active"));
                Integer added = itemFromDB.getInt(itemFromDB.getColumnIndex("added"));
                sndResult = new Snd(id, name, example, cls, active, added);
            }
            db.close();
        }
        return sndResult;
    }

    /**
     * Modify drill in shuffle mode
     * @param ctx - aplication context for shared preference
     */
    public void makeShuffleDrillModify(Context ctx){
        int dispersionValue = PreferenceStore.getDispersion(ctx);
        int min = variantXTimes - dispersionValue;
        if(min <= 0)
            min = 1;
        int max = variantXTimes + dispersionValue;
        int rand = min + (int)(Math.random() * ((max - min) + 1));
        double newTimeOnePart = time / variantXTimes;
        time = (int)Math.round(newTimeOnePart * rand);
        alternativeDrillName(ctx, rand, variantXTimes);
    }

    public int getIteration() {
        return iteration;
    }

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    public boolean isShuffle(){
        return shuffle;
    }

    public boolean isShuffleModify() {
        return shuffleModify;
    }

    public boolean isInflection() {
        return inflection;
    }

    public boolean isAlternative() {
        return alternative;
    }

    public int getPosition() {
        return position;
    }

    public boolean isJointToPrevDrill() {
        return jointToPrevDrill;
    }

    public int getTime() {
        return time;
    }

    public boolean isTimeWait() {
        return timeWait;
    }

    public boolean isSndWait() {
        return sndWait;
    }

    public int getSnd(){
        return snd;
    }

    public boolean isRepeat() {
        return isRepeat;
    }

    public String getName(){
        return this.drillName;
    }

    public void setChronometer(boolean c){
        this.chronometer = c;
    }

    public int getRepeat() {
        return repeat;
    }

    public boolean isPause() {
        return isPause;
    }

    public int getPauseTime() {
        return pauseTime;
    }

    public int getVariantXTimes() {
        return variantXTimes;
    }

    public int getIdDrill() {
        return idDrill;
    }

    public int getIdDrill_drill_table() {
        return idDrill_drill_table;
    }

    public void setRepeat(int repeat) {
        this.repeat = repeat;
    }

    public int getVariantPosition() {
        return variantPosition;
    }

    public int getSndDetectXTtimes() {
        return sndDetectXTtimes;
    }
}
