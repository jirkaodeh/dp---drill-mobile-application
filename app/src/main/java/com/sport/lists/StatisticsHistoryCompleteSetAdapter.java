package com.sport.lists;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.sport.DetailStatisticsActivity;
import com.sport.FinishActivity;
import com.sport.GlobalStatisticsActivity;
import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.fragments.main.GlobalStatisticsHistoryListFragment;
import com.sport.utils.Color;

import java.util.List;

/**
 * Created by Jirka on 20.12.2018.
 * Adapter for print list of completed drill set
 */
public class StatisticsHistoryCompleteSetAdapter extends ArrayAdapter<CompleteSet> {

    private List<CompleteSet> data;
    private Context context;
    private StatisticsHistoryCompleteSetAdapter adapter;
    private GlobalStatisticsHistoryListFragment activity;

    /**
     * Constructor drill list adapter
     *
     * @param context          app context
     * @param layoutResourceId reference to layout on list item style
     * @param data             array of DrillSet objects with informations abbout each drill
     */
    public StatisticsHistoryCompleteSetAdapter(Context context, int layoutResourceId, List<CompleteSet> data, GlobalStatisticsHistoryListFragment activity) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        this.adapter = this;
        this.activity = activity;
    }

    /**
     * Return count of all drill in set
     *
     * @return number that represent count of all drills in list
     */
    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    /**
     * Method return object by position in list
     *
     * @param position index to ArrayList to concrete object
     * @return DrillSet object with data
     */
    @Override
    public CompleteSet getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        StatisticsHistoryCompleteSetAdapter.ViewHolder holder;
        final CompleteSet drill = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_history, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (StatisticsHistoryCompleteSetAdapter.ViewHolder) convertView.getTag();
        }

        holder.setName.setText(drill.getDrillSetName());
        holder.groupName.setText("Skupina: "+drill.getGroup().getName());
        holder.percentage.setText(drill.getPercentage()+"%");
        holder.elapsedTime.setText("Čas: "+FinishActivity.getTime(drill.getElapsedTime()));
        holder.date.setText(drill.getDateTimeString());
        holder.groupColor.setBackgroundColor(getContext().getResources().getColor(Color.getColorById(drill.getGroup().getColor())));

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, convertView.findViewById(R.id.bottom_wrapper));
        holder.swipeLayout.setLeftSwipeEnabled(false);
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {
            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        // remove item
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDB db = new SQLiteDB(context);
                db.deleteSetComplete(drill.getSetCompleteId());
                activity.removeCompleteSet(drill.getSetCompleteId());
                data.remove(position);
                notifyDataSetChanged();
            }
        });

        // open detail about complete set
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context ctx = activity.getContext();
                Intent intent = new Intent(ctx, DetailStatisticsActivity.class);
                intent.putExtra("DRILL_SET_COMPLETE_ID", drill.getSetCompleteId());
                intent.putExtra("DRILL_SET_COMPLETE_NAME", drill.getDrillSetName());
                intent.putExtra("DRILL_SET_COMPLETE_GROUP_NAME", drill.getGroup().getName());
                intent.putExtra("DRILL_SET_COMPLETE_GROUP_ID", drill.getGroup().getgId());
                intent.putExtra("DRILL_SET_COMPLETE_COLOR", drill.getGroup().getColor());
                intent.putExtra("DRILL_SET_COMPLETE_PERCENTAGE", drill.getPercentage());
                intent.putExtra("DRILL_SET_COMPLETE_ELAPSED_TIME", drill.getElapsedTime());
                intent.putExtra("DRILL_SET_COMPLETE_DATE", drill.getDateTime());
                intent.putExtra("DRILL_SET_COMPLETE_NOTE", drill.getNote());
                intent.putExtra("DRILL_SET_COMPLETE_MODE", drill.getModeString());
                activity.startActivity(intent);
            }
        });

        return convertView;
    }

    /**
     * Create new holder
     *
     * @param v - reference to view for get all graphic items for holder
     * @return new holder ViewHolder object
     */
    private StatisticsHistoryCompleteSetAdapter.ViewHolder createViewHolder(View v) {
        StatisticsHistoryCompleteSetAdapter.ViewHolder holder = new StatisticsHistoryCompleteSetAdapter.ViewHolder();
        holder.setName = (TextView) v.findViewById(R.id.set_name);
        holder.groupName = (TextView) v.findViewById(R.id.set_group);
        holder.elapsedTime = (TextView) v.findViewById(R.id.set_time);
        holder.date = (TextView) v.findViewById(R.id.set_date);
        holder.groupColor = (View) v.findViewById(R.id.group_color);
        holder.swipeLayout = (SwipeLayout) v.findViewById(R.id.drill_set_item);
        holder.percentage = (TextView) v.findViewById(R.id.set_percentage);
        holder.remove = (ImageView)  v.findViewById(R.id.remove);
        holder.item = (LinearLayout)  v.findViewById(R.id.set_item);
        return holder;
    }

    private static class ViewHolder {
        public TextView setName;
        public TextView groupName;
        public TextView elapsedTime;
        public TextView date;
        public View groupColor;
        public TextView percentage;
        public SwipeLayout swipeLayout;
        public ImageView remove;
        public LinearLayout item;

    }
}
