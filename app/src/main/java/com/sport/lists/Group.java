package com.sport.lists;

import android.content.Context;

import com.sport.database.SQLiteDB;

/**
 * Created by Jirka on 10.10.2018.
 * Class represent group
 */
public class Group {
    private int gId;    // group identifier
    private int icon;   // group icon
    private String name;    // group name
    private String description;     // group description
    private int countDrillsInGroup; // drills in group
    private int color;    // color of drill group

    // constructor
    public Group(int gId, String name, String description, int icon, int color) {
        this.gId = gId;
        this.description = description;
        this.name = name;
        this.icon = icon;
        this.color = color;
    }

    // constructor
    public Group(int gId, String name, String description, int icon, int color, int countDrillsInGroup) {
        this.gId = gId;
        this.description = description;
        this.name = name;
        this.icon = icon;
        this.color = color;
        this.countDrillsInGroup = countDrillsInGroup;
    }

    /**
     * Get number of drill set in group
     * @param ctx - aplication context
     * @return number of drill set in group
     */
    public int getNumberOfSetsInGroup(Context ctx){
        SQLiteDB db = new SQLiteDB(ctx);
        return db.getActiveSetsInGroup(this.gId);
    }

    public int getIcon() {
        return icon;
    }

    public int getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public int getCountDrillsInGroup() {
        return countDrillsInGroup;
    }

    public int getgId() {
        return gId;
    }

    public String getDescription() {
        return description;
    }
}

