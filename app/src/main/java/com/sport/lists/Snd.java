package com.sport.lists;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;

import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.utils.Permission;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Jirka on 6.2.2019.
 * Class represend sound class in sound recognition
 */
public class Snd {
    private String name, example;
    private int id, cls;
    private int added, active;
    private int patterns;

    public Snd(int id, String name, String example, int cls, int active, int added){
        this.id = id;
        this.name = name;
        this.example = example;
        this.cls = cls;
        this.active = active;
        this.added = added;
        this.patterns = 0;
    }

    /**
     * Get resources with sound example
     * @return  resource id
     */
    public int getRawExample(){
        if(example.compareTo("whistle_example") == 0)
            return R.raw.whistle_example;
        else if(example.compareTo("clap_example") == 0)
            return  R.raw.clap_example;
        else if(example.compareTo("ball_example") == 0)
            return R.raw.ball_example;
        else if(example.compareTo("gun_example") == 0)
            return R.raw.gun_example;
        else if(id >= SQLiteDB.SND_FIRST_ADD_ID){
            return R.raw.whistle;
        }
        return R.raw.whistle;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getExample() {
        return example;
    }

    public int getCls() {
        return cls;
    }

    public boolean isActive() {
        return active == 1;
    }

    public boolean isAdded() {
        return added == 1;
    }

    public void setActive(int active) {
        this.active = active;
    }

    /**
     * Load all information about sounds class
     * @param ctx - aplication context
     * @return array of snd
     */
    public static ArrayList<Snd> getAllSndData(Context ctx, boolean onlyActive){
        ArrayList <Snd> sndArr = new ArrayList<>();
        SQLiteDB db = new SQLiteDB(ctx);
        Cursor itemFromDB = db.getAllSnd();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            String example = itemFromDB.getString(itemFromDB.getColumnIndex("example"));
            Integer cls = itemFromDB.getInt(itemFromDB.getColumnIndex("class"));
            Integer active = itemFromDB.getInt(itemFromDB.getColumnIndex("active"));
            Integer added = itemFromDB.getInt(itemFromDB.getColumnIndex("added"));
            if((onlyActive && active == 1) || !onlyActive)
                sndArr.add(new Snd(id, name, example, cls, active, added));
        }
        itemFromDB.close();
        db.close();
        return sndArr;
    }
}
