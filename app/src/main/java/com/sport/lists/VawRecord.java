package com.sport.lists;

import android.content.Context;
import android.media.MediaPlayer;

import java.io.File;

public class VawRecord {
    private String name;
    private String absPath;
    private File file;
    private int sndClass;
    private int position;

    public VawRecord(String name, String absPath, File file, int sndClass, int position){
        this.name = name;
        this.absPath = absPath;
        this.file = file;
        this.sndClass = sndClass;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public File getFile() {
        return file;
    }

    public int getSndClass() {
        return sndClass;
    }

    public String getAbsPath() {
        return absPath;
    }

    public int getPosition() {
        return position;
    }
}
