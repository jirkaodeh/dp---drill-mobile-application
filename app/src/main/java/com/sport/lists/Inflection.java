package com.sport.lists;

public class Inflection {
    private int id;
    private String name1, name24, name5;
    private boolean addedByUser;

    public Inflection(int id, String name1, String name24, String name5, boolean addedByUser){
        this.id = id;
        this.name1 = name1;
        this.name24 = name24;
        this.name5 = name5;
        this.addedByUser = addedByUser;
    }

    public int getId() {
        return id;
    }

    public String getName1() {
        return name1;
    }

    public String getName5() {
        return name5;
    }

    public String getName24() {
        return name24;
    }

    public boolean isAddedByUser() {
        return addedByUser;
    }

}
