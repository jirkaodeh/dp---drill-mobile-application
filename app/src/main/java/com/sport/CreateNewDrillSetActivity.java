package com.sport;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.sport.database.SQLiteDB;
import com.sport.lists.Group;
import com.sport.utils.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jirka on 05.11.2018.
 */
public class CreateNewDrillSetActivity extends BasicMain {

    Button button;
    EditText drillSetTitle;
    Spinner spinner;
    private int DRILL_SET_ID = 0;
    private int DRILL_SET_COLOR = 0;
    private String DRILL_SET_NAME, DRILL_SET_GROUP;
    List<Group> groupesList;
    private int selectedGroup = 1;
    private List<String> groupesStringList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_drill_set);

        button =  findViewById(R.id.create_drill_set_button);
        drillSetTitle = findViewById(R.id.input_drill_set_title);
        spinner = findViewById(R.id.select_group);

        Intent inIntent = getIntent();
        Bundle bundle = inIntent.getExtras();

        groupesList = getListOfGroupesObject();
        for (Group g : groupesList) {
            groupesStringList.add(g.getName());
        }
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, groupesStringList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedGroup = (int)id+1;
                Log.v("item "+selectedGroup, (String)parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // get data from intent
        if(bundle != null) {
            DRILL_SET_ID = (int) bundle.get("DRILL_SET_ID");
            DRILL_SET_NAME = (String) bundle.get("DRILL_SET_NAME");
            DRILL_SET_GROUP = (String) bundle.get("DRILL_SET_GROUP");
            DRILL_SET_COLOR = (int) bundle.get("DRILL_SET_COLOR");
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.edit_drill_set_title));
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(DRILL_SET_COLOR))));
            button.setText(getApplicationContext().getResources().getString(R.string.edit_group));

            drillSetTitle.setText(DRILL_SET_NAME);
            ArrayAdapter ad = (ArrayAdapter) spinner.getAdapter();
            int defaultPosition = ad.getPosition(DRILL_SET_GROUP);
            spinner.setSelection(defaultPosition);
        }
        else{
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.create_drill_set_title));
            button.setText(getApplicationContext().getResources().getString(R.string.create_group));
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDB db = new SQLiteDB(getApplicationContext());
                // edit
                if(DRILL_SET_ID != 0) {
                    String name = drillSetTitle.getText().toString();
                    int cnt = 1, groupId = 1;
                    for (Group g : groupesList) {
                        if (selectedGroup == cnt){
                            groupId = g.getgId();
                            break;
                        }
                        cnt++;
                    }
                    db.updateDrillSet(DRILL_SET_ID, name, groupId);
                    /*
                    Snackbar.make(fullView, "Edit", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();*/
                }
                // insert
                else{
                    String name = drillSetTitle.getText().toString();
                    int cnt = 1, groupId = 1;
                    for (Group g : groupesList) {
                        if (selectedGroup == cnt){
                            groupId = g.getgId();
                            break;
                        }
                        cnt++;
                    }
                    Long l = db.insertDrillSet(name, groupId, 1);
                    /*Snackbar.make(fullView, "Insert "+l, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();*/

                }
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_back_white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private List<Group> getListOfGroupesObject(){
        SQLiteDB db = new SQLiteDB(this);
        Cursor itemFromDB = db.getAllGroupes(true);
        List<Group> groupsDBArray = new ArrayList<Group>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("gid"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer cnt = itemFromDB.getInt(itemFromDB.getColumnIndex("cnt"));
            String description = itemFromDB.getString(itemFromDB.getColumnIndex("description"));
            Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
            Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
            groupsDBArray.add(new Group(id, name, description, icon, color, cnt));
        }
        itemFromDB.close();
        db.close();
        return groupsDBArray;
    }
}
