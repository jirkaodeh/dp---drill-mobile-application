package com.sport;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sport.database.PreferenceStore;
import com.sport.database.SQLiteDB;
import com.sport.sound.Record;
import com.sport.utils.Color;

import java.util.ArrayList;
import java.util.Locale;

public class FinishActivity extends BasicMain {

    private int DRILL_SET_ID = 0;
    private int DRILL_SET_COLOR = 0;
    private int FINISH_PERCENTAGE = 0;
    private long ELAPSED_TIME = 0;
    private int SET_ITERATION = 0;
    private int DRILL_SET_MODE = 0;
    private String DRILL_SET_GROUP = "";
    private String DRILL_SET_NAME = "";
    private long SET_COMPLETE_ID;
    private TextView setName, percentage, time, setRepeat, repeat;
    private ImageButton record;
    private EditText note;
    private Button save, notSave;
    private MediaPlayer whistleMP;      // whistle sound played during end drill set
    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    private boolean listening = false;
    private Record rec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        final Intent inIntent = getIntent();
        final Bundle bundle = inIntent.getExtras();
        // get data from intent
        if(bundle != null) {
            DRILL_SET_ID = (int) bundle.get("DRILL_SET_ID");
            DRILL_SET_NAME = (String) bundle.get("DRILL_SET_NAME");
            DRILL_SET_COLOR = (int) bundle.get("DRILL_SET_COLOR");
            FINISH_PERCENTAGE = (int) bundle.get("FINISH_PERCENTAGE");
            DRILL_SET_MODE = (int) bundle.get("DRILL_SET_MODE");
            DRILL_SET_GROUP = (String) bundle.get("DRILL_SET_GROUP");
            ELAPSED_TIME = (long) bundle.get("ELAPSED_TIME");
            SET_ITERATION = (int) bundle.get("ITERATION");
            SET_COMPLETE_ID = (long) bundle.get("SET_COMPLETE_ID");
            super.changeToolbarTitle(getResources().getString(R.string.set_finish));
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(DRILL_SET_COLOR))));
            Log.i("ITERATION", SET_ITERATION+"");
        }
        else {
            // Error
        }
        setName = (TextView)findViewById(R.id.drill_set_name);
        percentage = (TextView)findViewById(R.id.percentage);
        time = (TextView)findViewById(R.id.time);
        setRepeat = (TextView)findViewById(R.id.finish_set_repeat);
        note = (EditText)findViewById(R.id.input_drill_note);
        save = (Button)findViewById(R.id.finish_save);
        notSave = (Button)findViewById(R.id.finish_not_save);
        repeat = (Button)findViewById(R.id.repeat);
        record = (ImageButton)findViewById(R.id.record);

        setName.setText(DRILL_SET_NAME);
        percentage.setText(FINISH_PERCENTAGE+"%");
        time.setText(getTime(ELAPSED_TIME));
        setRepeat.setText(SET_ITERATION+"");

        // start whistle sound
        whistleMP = MediaPlayer.create(this, R.raw.whistle_end);

        checkPermission();
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int error) {

            }

            @Override
            public void onResults(Bundle results) {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if(matches != null) {
                    String prevText = note.getText().toString();
                    if(prevText != null && prevText.length() > 0 && prevText.charAt(prevText.length() - 1) != ' ')
                        prevText += " ";
                    note.setText(prevText+ matches.get(0));
                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });

        //record note
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rec != null){
                    rec.stopRecord();
                }
                if(!listening) {
                    Log.i("Note", "listening");
                    listening = true;
                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                }
                else {
                    Log.i("Note", "not listening");
                    listening = false;
                    mSpeechRecognizer.stopListening();
                }
                //SpeechToText stt = new SpeechToText(getApplicationContext());
                //stt.initDetector();
            }
        });

        //save click
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rec != null){
                    rec.stopRecord();
                }
                SQLiteDB db = new SQLiteDB(getApplicationContext());
                String textNote = note.getText().toString();
                db.updateSetComplete((int)SET_COMPLETE_ID, FINISH_PERCENTAGE, textNote, (int)ELAPSED_TIME);
                finish();
            }
        });

        // not save click
        notSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rec != null){
                    rec.stopRecord();
                }
                SQLiteDB db = new SQLiteDB(getApplicationContext());
                db.deleteSetComplete((int)SET_COMPLETE_ID);
                finish();
            }
        });

        // repeat set click
        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeatSetButtonHandler();
            }
        });

        if(PreferenceStore.getSoundStatus(this)) {
            playSound(whistleMP);
        }
        Log.i("DRILL_SET_MODE", ""+DRILL_SET_MODE);
        if(DRILL_SET_MODE == 0) {
            rec = new Record(this, this);
            rec.addClass(13); // ADD REPEAT
            rec.startRecord();
            repeat.setVisibility(View.VISIBLE);
        }

    }

    public static String getTime(long elapsedTime){
        int h = (int)(elapsedTime / 3600000);
        int m = (int)(elapsedTime - h * 3600000)/60000;
        int s = (int)(elapsedTime - h * 3600000 - m * 60000)/1000;
        return (h < 10 ? "0"+h : h)+":"+(m < 10 ? "0"+m : m)+":"+(s < 10 ? "0"+s : s);
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
                //finish();
            }
        }
    }

    /**
     * Play sound in mp
     * @param mp - sound that may be play
     */
    private void playSound(MediaPlayer mp){
        mp.start();
    }

    @Override
    public void onStop(){
        super.onStop();
        if(rec != null){
            rec.stopRecord();
        }
        if(whistleMP != null) {
            whistleMP.release();
        }
        if(mSpeechRecognizer != null){
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
        }
    }

    @Override
    protected void onResume() {
        if(rec != null){
            rec.startRecord();
        }
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(rec != null){
            rec.stopRecord();
        }
        if(whistleMP != null) {
            whistleMP.release();
        }
        if(mSpeechRecognizer != null){
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
        }
    }

    private void repeatSetButtonHandler(){
        Intent intent = new Intent(this, RunDrillActivity.class);
        intent.putExtra("DRILL_SET_ID", DRILL_SET_ID);
        intent.putExtra("DRILL_SET_NAME", DRILL_SET_NAME);
        intent.putExtra("DRILL_SET_COLOR", DRILL_SET_COLOR);
        intent.putExtra("DRILL_SET_GROUP", DRILL_SET_GROUP);
        intent.putExtra("DRILL_SET_MODE", 0); // normal mode
        startActivity(intent);
        this.finish();
    }

    /**
     * Handler get detected sound class and on detected class make action
     * @param detectedCls - identifier of detected class
     * @return - true - repeat start detection | false - not repeat detection
     */
    public boolean detectedHandler(int detectedCls) {
        rec.stopRecord();
        if(detectedCls == 13){
            repeatSetButtonHandler();
        }
        return false;
    }
}
