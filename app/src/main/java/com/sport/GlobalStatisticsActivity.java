package com.sport;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.sport.fragments.main.GlobalStatisticsGraphFragment;
import com.sport.fragments.main.GlobalStatisticsHistoryListFragment;
import com.sport.fragments.main.MainDrillsFragment;
import com.sport.fragments.main.MainGroupFragment;
import com.sport.lists.TabViewAdapter;

public class GlobalStatisticsActivity extends BasicMain {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_statistics);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.help_statistics_title));
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new TabViewAdapter(getSupportFragmentManager());
        adapter.addFrag(new GlobalStatisticsGraphFragment(), "Globální statistiky");
        adapter.addFrag(new GlobalStatisticsHistoryListFragment(), "Historie provedení cvičebních sad");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount() - 1);
    }

}
