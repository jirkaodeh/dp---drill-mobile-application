package com.sport;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.sport.importExport.Import;
import com.sport.lists.Group;
import com.sport.utils.CreateDrillUtils;

import java.util.ArrayList;
import java.util.List;

public class ImportDrillSetActivity extends BasicMain {

    private List<Group> groupesList;
    private List<String> groupesStringList = new ArrayList<String>();
    private int selectedGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_drill_set);
        final EditText importDataEdt = (EditText)findViewById(R.id.import_text);
        final Spinner groupSpinner = (Spinner)findViewById(R.id.select_group);
        final TextView warningTv = (TextView)findViewById(R.id.warning);
        final Button importButton = (Button)findViewById(R.id.import_button);

        // select group
        groupesList = CreateDrillUtils.getListOfGroupesObject(this);
        for (Group g : groupesList) {
            groupesStringList.add(g.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, groupesStringList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(adapter);
        selectedGroup = 1;
        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedGroup = (int)id+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedGroup = 1;
            }
        });

        // import button listener
        importButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(importDataEdt.getText().toString().isEmpty()){
                    warningTv.setText(getResources().getString(R.string.warning_no_data_to_import));
                    warningTv.setVisibility(View.VISIBLE);
                    return;
                }
                String importData = importDataEdt.getText().toString();
                int cnt = 1, groupId = 1;
                for (Group g : groupesList) {
                    if (selectedGroup == cnt){
                        groupId = g.getgId();
                        break;
                    }
                    cnt++;
                }
                boolean result = Import.importDrillSet(getApplicationContext(), importData, groupId);
                if(!result){
                    warningTv.setText(getResources().getString(R.string.warning_broken_data_to_import));
                    warningTv.setVisibility(View.VISIBLE);
                }
                else {
                    finish();
                }
            }
        });
    }
}
