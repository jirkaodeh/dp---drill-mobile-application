package com.sport;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.sport.fragments.main.CreateNewDrillFragment;
import com.sport.fragments.main.CreateNewPauseFragment;
import com.sport.lists.TabViewAdapter;
import com.sport.utils.Color;

public class CreateNewDrillActivity extends BasicMain {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabViewAdapter adapter;
    public String GROUP;
    public int COLOR, SET_ID, DRILL_ID, DRILL_ID_IN_SET;
    public int MODE = 0; // 0  - create, 1 - edit drill, 2 - edit pause

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_drill);

        Intent inIntent = getIntent();
        Bundle bundle = inIntent.getExtras();
        // get data from intent
        if(bundle != null) {
            SET_ID = (int) bundle.get("SET_ID");
            COLOR = (int) bundle.get("COLOR");
            GROUP = (String) bundle.get("GROUP");
            try{
                MODE = (int) bundle.get("MODE");
                DRILL_ID = (int) bundle.get("DRILL_ID");
                DRILL_ID_IN_SET = (int) bundle.get("DRILL_ID_IN_SET");
            }catch (Exception e){
                MODE = 0;
                DRILL_ID = -1;
                DRILL_ID_IN_SET = -1;
            }
            Log.i("MODE", ""+MODE);
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.edit_drill_set_title));
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(COLOR))));
        }
        else{
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.create_drill_set_title));
        }

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(COLOR))));
        if(MODE != 0){
            tabLayout.setVisibility(View.GONE);
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.edit_drill_title));
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.edit_drill_title));
        }
        else {
            super.changeToolbarTitle(getApplicationContext().getResources().getString(R.string.create_drill_title));
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_back_white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // Setting viewPager and setting fragment for each bookmark
    private void setupViewPager(ViewPager viewPager) {
        adapter = new TabViewAdapter(getSupportFragmentManager());
        if(MODE == 0) {
            adapter.addFrag(new CreateNewDrillFragment(), "Vytvořit cvik");
        }
        else if(MODE == 1) {
            adapter.addFrag(new CreateNewDrillFragment(), "Editovat cvik");
        }

        if(MODE == 0) {
            adapter.addFrag(new CreateNewPauseFragment(), "Vytvořit pauzu");
        }
        else if(MODE == 2) {
            adapter.addFrag(new CreateNewPauseFragment(), "Editovat pauzu");
        }
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount() - 1);
    }
}
