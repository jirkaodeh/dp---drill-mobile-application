package com.sport.utils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;


public final class ListUtils {

    /**
     * Static method for compute new height of listview by number of item in list
     * @param listView listView for compute new height
     */
    public static void setDynamicHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        View view = null;
        int sumHeight = 0;

        if (listAdapter == null) {
            return;
        }

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);

        int itemHeihgt = 0;
        // get all item height
        if(listAdapter.getCount() > 0) {
            view = listAdapter.getView(0, view, listView);
            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.MATCH_PARENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            itemHeihgt = view.getMeasuredHeight();
        }

        // height multiply by number of items
        sumHeight = itemHeihgt * (listAdapter.getCount());

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = sumHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));

        // set static height for listview
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
