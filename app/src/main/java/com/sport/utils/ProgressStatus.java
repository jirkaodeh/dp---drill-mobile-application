package com.sport.utils;

/**
 * Status of executed drills
 */
public enum ProgressStatus {
    PREPARE,
    ACTIIVE,
    DONE
}
