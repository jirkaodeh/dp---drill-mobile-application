package com.sport.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gauravbhola.ripplepulsebackground.RipplePulseLayout;
import com.sport.R;

import org.w3c.dom.Text;

/**
 * Class with global help and warnings dialogs
 */
public final class HelpDialogs {

    public static void testAplicationDialog(final Context ctx, Activity act) {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(ctx);
        final LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);

        final View Viewlayout = inflater.inflate(R.layout.dialog_sound_wait,
                (ViewGroup) act.findViewById(R.id.layout_dialog));
        final RipplePulseLayout rpl = Viewlayout.findViewById(R.id.ripple_pulse);
        rpl.startRippleAnimation();

        popDialog.setView(Viewlayout);

        /*popDialog.setPositiveButton(act.getResources().getString(R.string.continue_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });*/

        final AlertDialog dialog = popDialog.create();
        // listener for close dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                //dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ctx.getResources().getColor(R.color.colorAccent));
            }
        });
        dialog.show();
    }

    /**
     *  Show warning, that drill set is empty (no drill in set)
     */
    public static void alertNoDrillDialog(final Context ctx, final Activity act) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(act.getString(R.string.warning_play_drill_set_title));
        builder.setMessage(act.getString(R.string.warning_play_drill_set_text));
        builder
                .setCancelable(false)
                .setPositiveButton(act.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        act.finish();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(act.getResources().getColor(R.color.drillSubText));
            }
        });
        dialog.show();
    }

    /**
     * Dialog with info about application
     */
    public static void aboutAplicationDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_about, R.id.layout_dialog, R.string.continue_text);
    }

    /**
     * Dialog with drill list in set help info
     */
    public static void drillListHelpDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_help_drill_list, R.id.layout_dialog, R.string.continue_text);
    }

    /**
     * Dialog with run drill set help info
     */
    public static void runDrillSetHelpDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_help_run, R.id.layout_dialog, R.string.continue_text);
    }

    /**
     * Dialog with statistics help info
     */
    public static void statisticsHelpDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_help_statistics, R.id.layout_dialog, R.string.continue_text);
    }

    /**
     * Dialog with create new drill help info
     */
    public static void createNewDrillHelpDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_help_create_new_drill, R.id.layout_dialog, R.string.continue_text);
    }

    /**
     * Dialog with saved drill overview help info
     */
    public static void drillOverviewHelpDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_help_drill_overview, R.id.layout_dialog, R.string.continue_text);
    }

    /**
     * Dialog with drill sets and groupes help info
     */
    public static void drillSetAndGroupHelpDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_help_drillset_and_group, R.id.layout_dialog, R.string.continue_text);
    }

    /**
     * Dialog with sound class overview help info
     */
    public static void soundClassOverviewHelpDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_help_sound_class_overview, R.id.layout_dialog, R.string.continue_text);
    }

    /**
     * Dialog with sound class record help info
     */
    public static void soundClassRecordHelpDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_help_sound_class_record, R.id.layout_dialog, R.string.continue_text);
    }

    /**
     * Warning dialog about limit of active sound class
     */
    public static void limitSoundClassWarningDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_limit_active_sound_class, R.id.layout_dialog, R.string.ok);
    }

    /**
     * Warning dialog about no execute drill set because no active snd class in one of drill in set
     */
    public static void noActiveSndClassWarningDialog(final Context ctx, Activity act) {
        dialogPatern(ctx,act, R.layout.dialog_no_active_snd_class, R.id.layout_dialog, R.string.back_text);
    }

    private static void dialogPatern(final Context ctx, Activity act, int res, int layoutRes, int dismissButtonText){
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(ctx);
        final LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);

        final View Viewlayout = inflater.inflate(res,
                (ViewGroup) act.findViewById(layoutRes));

        popDialog.setView(Viewlayout);

        popDialog.setPositiveButton(act.getResources().getString(dismissButtonText), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = popDialog.create();
        // listener for close dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ctx.getResources().getColor(R.color.colorAccent));
            }
        });
        dialog.show();
    }

    /**
     * Loading dialog with progress bar
     * @param act - activity
     * @param textStrTitle - title of dialog
     * @param textStrInfo - info text
     * @return - alert dialog
     */
    public static AlertDialog loadingDialog(Context ctx, Activity act, String textStrTitle, String textStrInfo) {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(ctx);
        final LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);

        final View Viewlayout = inflater.inflate(R.layout.dialog_loading,
                (ViewGroup) act.findViewById(R.id.layout_dialog));
        TextView text = (TextView) Viewlayout.findViewById(R.id.dialog_text);
        TextView text2 = (TextView) Viewlayout.findViewById(R.id.dialog_text2);
        text.setText(textStrTitle);
        if(!textStrInfo.isEmpty()){
            text2.setVisibility(View.VISIBLE);
            text2.setText(textStrInfo);
        }
        popDialog.setView(Viewlayout);
        AlertDialog dialog = popDialog.create();
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }

    /**
     * Loading dialog with progress bar
     * @param act - activity
     * @param str - title of dialog
     * @return - alert dialog
     */
    public static AlertDialog loadingDialog(Context ctx, Activity act, String str) {
        return loadingDialog(ctx, act, str,"");
    }
}
