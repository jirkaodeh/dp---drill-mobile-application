package com.sport.utils;

import com.sport.R;

public final class SportIcon {
    public static int[] iconsWhite = {
            R.drawable.sport_icon_white_1,
            R.drawable.sport_icon_white_2,
            R.drawable.sport_icon_white_3,
            R.drawable.sport_icon_white_4,
            R.drawable.sport_icon_white_5,
            R.drawable.sport_icon_white_6,
            R.drawable.sport_icon_white_7,
            R.drawable.sport_icon_white_8,
            R.drawable.sport_icon_white_9,
            R.drawable.sport_icon_white_10,
            R.drawable.sport_icon_white_11,
            R.drawable.sport_icon_white_12,
            R.drawable.sport_icon_white_13,
            R.drawable.sport_icon_white_14,
            R.drawable.sport_icon_white_15
    };

    public static int[] iconsBlack = {
            R.drawable.sport_icon_black_1,
            R.drawable.sport_icon_black_2,
            R.drawable.sport_icon_black_3,
            R.drawable.sport_icon_black_4,
            R.drawable.sport_icon_black_5,
            R.drawable.sport_icon_black_6,
            R.drawable.sport_icon_black_7,
            R.drawable.sport_icon_black_8,
            R.drawable.sport_icon_black_9,
            R.drawable.sport_icon_black_10,
            R.drawable.sport_icon_black_11,
            R.drawable.sport_icon_black_12,
            R.drawable.sport_icon_black_13,
            R.drawable.sport_icon_black_14,
            R.drawable.sport_icon_black_15
    };

    public static int getGroupIconWhite(int icon){
        switch (icon){
            case 1: return R.drawable.sport_icon_white_1;
            case 2: return R.drawable.sport_icon_white_2;
            case 3: return R.drawable.sport_icon_white_3;
            case 4: return R.drawable.sport_icon_white_4;
            case 5: return R.drawable.sport_icon_white_5;
            case 6: return R.drawable.sport_icon_white_6;
            case 7: return R.drawable.sport_icon_white_7;
            case 8: return R.drawable.sport_icon_white_8;
            case 9: return R.drawable.sport_icon_white_9;
            case 10: return R.drawable.sport_icon_white_10;
            case 11: return R.drawable.sport_icon_white_11;
            case 12: return R.drawable.sport_icon_white_12;
            case 13: return R.drawable.sport_icon_white_13;
            case 14: return R.drawable.sport_icon_white_14;
            case 15: return R.drawable.sport_icon_white_15;
        }
        return R.drawable.sport_icon_white_1;
    }

    public static int getGroupIconBlack(int icon){
        switch (icon){
            case 1: return R.drawable.sport_icon_black_1;
            case 2: return R.drawable.sport_icon_black_2;
            case 3: return R.drawable.sport_icon_black_3;
            case 4: return R.drawable.sport_icon_black_4;
            case 5: return R.drawable.sport_icon_black_5;
            case 6: return R.drawable.sport_icon_black_6;
            case 7: return R.drawable.sport_icon_black_7;
            case 8: return R.drawable.sport_icon_black_8;
            case 9: return R.drawable.sport_icon_black_9;
            case 10: return R.drawable.sport_icon_black_10;
            case 11: return R.drawable.sport_icon_black_11;
            case 12: return R.drawable.sport_icon_black_12;
            case 13: return R.drawable.sport_icon_black_13;
            case 14: return R.drawable.sport_icon_black_14;
            case 15: return R.drawable.sport_icon_black_15;
        }
        return R.drawable.sport_icon_black_1;
    }
}
