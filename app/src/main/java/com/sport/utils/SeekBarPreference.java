package com.sport.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.sport.R;

import java.util.Locale;

public class SeekBarPreference extends Preference implements SeekBar.OnSeekBarChangeListener {
    private TextView valueTv;


    public SeekBarPreference(Context ctx, AttributeSet attrs, int defaultStyle){
        super(ctx, attrs, defaultStyle);
    }

    public SeekBarPreference(Context ctx, AttributeSet attrs){
        super(ctx, attrs);
    }

    public SeekBarPreference(Context ctx){
        super(ctx);
    }

    @Override
    protected View onCreateView(ViewGroup view){
        super.onCreateView(view);
        View v = LayoutInflater.from(getContext()).inflate(R.layout.preference_seekbar, view, false);
        valueTv = v.findViewById(R.id.value);
        TextView titleTv = v.findViewById(R.id.title);
        titleTv.setText(getTitle());
        SeekBar seek = v.findViewById(R.id.seek);
        seek.setOnSeekBarChangeListener(this);

        SharedPreferences pref = getSharedPreferences();
        int value = pref.getInt(getKey(), 100);
        valueTv.setText(String.format(Locale.getDefault(), "%d%%", value));
        seek.setProgress(value-50);
        return v;
    }

    @Override
    public void onProgressChanged(SeekBar seek, int i, boolean b){
        valueTv.setText(String.format(Locale.getDefault(), "%d%%", i+50));
        SharedPreferences.Editor edit = getEditor();
        edit.putInt(getKey(), i+50);
        edit.apply();


    }

    @Override
    public void onStartTrackingTouch(SeekBar seek){

    }

    @Override
    public void onStopTrackingTouch(SeekBar seek){

    }
}
