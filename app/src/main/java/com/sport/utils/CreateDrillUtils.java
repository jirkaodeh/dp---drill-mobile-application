package com.sport.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.sport.R;
import com.sport.database.SQLiteDB;
import com.sport.lists.Group;

import java.util.ArrayList;
import java.util.List;

public final class CreateDrillUtils {

    // detect positions of integer word in string array od words
    public static List<Integer> splitString(String input){
        String[] wordArr = input.split(" ");
        List<Integer> intPositionList = new ArrayList<Integer>();
        int counter = 0;
        for(String s : wordArr){
            counter++;
            if(isInteger(s))
                intPositionList.add(counter);
        }
        return intPositionList;
    }

    // check if string word is integer
    private static boolean isInteger(String s){
        try{
            Integer.parseInt(s);
        } catch (NumberFormatException e){
            return false;
        } catch (NullPointerException e){
            return false;
        }
        return true;
    }

    public static List<Group> getListOfGroupesObject(Activity activity){
        SQLiteDB db = new SQLiteDB(activity);
        Cursor itemFromDB = db.getAllGroupes(true);
        List<Group> groupsDBArray = new ArrayList<Group>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("gid"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer cnt = itemFromDB.getInt(itemFromDB.getColumnIndex("cnt"));
            String description = itemFromDB.getString(itemFromDB.getColumnIndex("description"));
            Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
            Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
            groupsDBArray.add(new Group(id, name, description, icon, color, cnt));
        }
        itemFromDB.close();
        db.close();
        return groupsDBArray;
    }

    public static void hideKeyboard(View v, Activity activity){
        InputMethodManager input = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        input.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void positionInfoDialog(final Activity activity) {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);

        final View Viewlayout = inflater.inflate(R.layout.dialog_info,
                (ViewGroup) activity.findViewById(R.id.layout_dialog));

        popDialog.setView(Viewlayout);

        popDialog.setPositiveButton(activity.getResources().getString(R.string.continue_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = popDialog.create();
        // posluchac pro uzavreni dialogu
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(activity.getResources().getColor(R.color.alertDialogButton));
            }
        });
        dialog.show();
    }
}
