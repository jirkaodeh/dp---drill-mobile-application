package com.sport.utils;

import android.content.Context;
import android.content.res.TypedArray;

import com.sport.R;

public final class Color {
    public static int[] getColorArray(Context context){
        TypedArray ta = context.getResources().obtainTypedArray(R.array.color_array);
        int[] colors = new int[ta.length()];
        for(int i = 0; i < ta.length(); i++){
            colors[i] = ta.getColor(i, 0);
        }
        ta.recycle();
        return colors;
    }

    public static int getColorById(int id){
        switch(id){
            case 1: return R.color.color1;
            case 2: return R.color.color2;
            case 3: return R.color.color3;
            case 4: return R.color.color4;
            case 5: return R.color.color5;
            case 6: return R.color.color6;
            case 7: return R.color.color7;
            case 8: return R.color.color8;
            case 9: return R.color.color9;
            case 10: return R.color.color10;
            default: return R.color.color1;
        }
    }
}
