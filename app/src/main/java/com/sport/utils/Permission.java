package com.sport.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for check and get perissions
 */
public class Permission {
    private static final Integer READ_STORAGE_PERMISSION_REQUEST_CODE = 0x3;
    private static final Integer WRITE_STORAGE_PERMISSION_REQUEST_CODE = 0x4;
    private static final int PERMISSION_RECORD_AUDIO = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static void checkPermisionForRecordSound(Context ctx, Activity act){
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            // Request permission
            ActivityCompat.requestPermissions(act,
                    new String[] { Manifest.permission.RECORD_AUDIO },
                    PERMISSION_RECORD_AUDIO);
        }
    }

    public static void permisionForReadExternalStorage(Context ctx) {
        if(!checkPermissionForReadExternalStorage(ctx)){
            requestPermissionForReadExternalStorage(ctx);
        }
    }

    public static void permisionForWriteExternalStorage(Context ctx) {
        if(!checkPermissionForWriteExternalStorage(ctx)){
            requestPermissionForWriteExternalStorage(ctx);
        }
    }

    private static boolean checkPermissionForReadExternalStorage(Context ctx){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            int result = ctx.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    private static boolean checkPermissionForWriteExternalStorage(Context ctx){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            int result = ctx.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static boolean checkAndRequestPermissions(Activity activity) {
        int perm1 = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.RECORD_AUDIO);
        int perm2 = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int perm3 = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (perm3 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (perm2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (perm1 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private static void requestPermissionForReadExternalStorage(Context ctx){
        try{
            ActivityCompat.requestPermissions((Activity) ctx, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    READ_STORAGE_PERMISSION_REQUEST_CODE);
        } catch (Exception e){
            e.printStackTrace();
            throw  e;
        }
    }

    private static void requestPermissionForWriteExternalStorage(Context ctx){
        try{
            ActivityCompat.requestPermissions((Activity) ctx, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_STORAGE_PERMISSION_REQUEST_CODE);
        } catch (Exception e){
            e.printStackTrace();
            throw  e;
        }
    }


}
