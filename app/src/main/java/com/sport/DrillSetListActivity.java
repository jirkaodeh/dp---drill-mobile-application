package com.sport;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sport.lists.Drill;
import com.sport.lists.DrillOverviewAdapter;
import com.sport.lists.Group;
import com.sport.lists.SavedDrillSelectAdapter;
import com.sport.lists.Snd;
import com.sport.lists.SndAdapter;
import com.sport.recycleList.OnStartDragListener;
import com.sport.recycleList.SimpleItemTouchHelperCallback;
import com.sport.database.SQLiteDB;
import com.sport.lists.DrillInSet;
import com.sport.lists.DrillInSetRecycleAdapter;
import com.sport.lists.DrillSet;
import com.sport.sound.FeatureFile;
import com.sport.utils.Color;
import com.sport.utils.HelpDialogs;
import com.sport.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class DrillSetListActivity extends BasicMain implements OnStartDragListener {

    private int DRILL_SET_ID = 0;
    private int DRILL_SET_COLOR = 0;
    private String DRILL_SET_NAME, DRILL_SET_GROUP;
    private ListView lv;//DragNDropListView
    DrillInSetRecycleAdapter adapter;
    //DrillInSetAdapter adapter;
    private ItemTouchHelper mItemTouchHelper;
    View includedLayout;
    List<DrillInSet> drillArray;
    List<DrillInSet> drillsDBArray;
    com.github.clans.fab.FloatingActionButton fab1, fab2;
    private AlertDialog dialog;
    private ArrayList<Drill> savedDrillArray;
    SavedDrillSelectAdapter savedDrillAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drill_set_list);
        //includedLayout = findViewById(R.id.id_drill_list);
        //lv = (ListView) includedLayout.findViewById(R.id.list);
        //lv.setVisibility(View.GONE);

        final Intent inIntent = getIntent();
        Bundle bundle = inIntent.getExtras();
        // get data from intent
        if(bundle != null) {
            DRILL_SET_ID = (int) bundle.get("DRILL_SET_ID");
            DRILL_SET_NAME = (String) bundle.get("DRILL_SET_NAME");
            DRILL_SET_COLOR = (int) bundle.get("DRILL_SET_COLOR");
            DRILL_SET_GROUP = (String) bundle.get("DRILL_SET_GROUP");
            super.changeToolbarTitle(DRILL_SET_NAME);

            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(Color.getColorById(DRILL_SET_COLOR))));

        }

        getData();


        fab1 = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateNewDrillActivity.class);
                intent.putExtra("SET_ID", DRILL_SET_ID);
                intent.putExtra("COLOR", DRILL_SET_COLOR);
                intent.putExtra("GROUP", DRILL_SET_GROUP);
                startActivity(intent);
                //Snackbar.make(view, "Add drill from dictionary", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });
        fab2 = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(getApplicationContext(), CreateNewDrillSetActivity.class);
                //startActivity(intent);
                dialogSelectSavedDrill();
                /*Snackbar.make(view, "Add new drill", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
    }

    private void getData(){
        // get all drill sets from DB
        drillArray = new ArrayList<DrillInSet>();
        SQLiteDB db = new SQLiteDB(this);
        Cursor itemFromDB = db.getAllDrillsFromDrillSet(DRILL_SET_ID);
        drillsDBArray = new ArrayList<DrillInSet>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id_drill_in_set"));
            Integer drillId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_id"));
            Integer position = itemFromDB.getInt(itemFromDB.getColumnIndex("position"));
            Boolean isPause = itemFromDB.getInt(itemFromDB.getColumnIndex("is_pause")) == 1;
            Integer pauseTime = itemFromDB.getInt(itemFromDB.getColumnIndex("pause_time"));
            Integer variantXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_x_times"));
            String editName = itemFromDB.getString(itemFromDB.getColumnIndex("edit_name"));
            String drillName = itemFromDB.getString(itemFromDB.getColumnIndex("drill_name"));
            Boolean prev = itemFromDB.getInt(itemFromDB.getColumnIndex("joint_to_prev_drill")) == 1;
            Boolean timeWait = itemFromDB.getInt(itemFromDB.getColumnIndex("time_wait")) == 1;
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("time"));
            Boolean alternative = itemFromDB.getInt(itemFromDB.getColumnIndex("alternative")) == 1;
            Integer varPosition = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_position"));
            Boolean sndWait = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_wait")) == 1;
            Integer snd = itemFromDB.getInt(itemFromDB.getColumnIndex("snd"));
            Boolean inflection = itemFromDB.getInt(itemFromDB.getColumnIndex("inflection")) == 1;
            Integer sndDetectXTimes = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_detect_x_times"));
            Boolean shuffle = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle")) == 1;
            Boolean shuffleModify = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle_modify")) == 1;
            drillsDBArray.add(new DrillInSet(id, drillId, position, isPause, pauseTime, variantXTimes, alternative, drillName, editName,
                    prev, timeWait, time, varPosition, sndWait, snd, sndDetectXTimes, inflection, shuffle, shuffleModify));
        }
        db = new SQLiteDB(this);
        itemFromDB = db.getDrillSetById(DRILL_SET_ID);
        List<DrillSet> drillSetsDBArray = new ArrayList<DrillSet>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer repeat = itemFromDB.getInt(itemFromDB.getColumnIndex("repeat"));
            drillsDBArray.add(new DrillInSet(true,repeat));
        }
        //drillsDBArray.add(new DrillInSet(true,0));
        itemFromDB.close();
        db.close();

        //TextView emptyList = (TextView) findViewById(R.id.empty_list);
        if (drillsDBArray.size() > 0) {
            adapter = new DrillInSetRecycleAdapter(this, this, drillsDBArray,
                    DRILL_SET_ID, DRILL_SET_COLOR, DRILL_SET_GROUP);

            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
            mItemTouchHelper = new ItemTouchHelper(callback);
            mItemTouchHelper.attachToRecyclerView(recyclerView);



            /*adapter = new DrillInSetAdapter(this, R.layout.list_item_drill_set, drillsDBArray,
                    DRILL_SET_ID, DRILL_SET_COLOR, DRILL_SET_GROUP);*/
            /*adapter = new DrillInSetAdapter(this, R.layout.list_item_drill_set, drillsDBArray,
                    new DrillInSetAdapter.Listener() {
                @Override
                public void onGrab(int position, SwipeLayout row) {
                    lv.onGrab(position, row);
                }
            });*/
            //saAdapter = new SwipeActionAdapter(adapter);
            /*saAdapter.setSwipeActionListener(this)
                    .setDimBackgrounds(true)
                    .setListView(lv);*/
            //lv.setDragNDropAdapter(adapter);
            //lv.setDraggingEnabled(true);
            //lv.setCheeseList(mCheeseList);
            //lv.saveAdapter(adapter);
            //lv.setAdapter(adapter);
            /*lv.setDropListener(onDrop);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                        long arg3) {
                }
            });*/
            //lv.setDropListener(onDrop);
            /*lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                }
            });*/
            //lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            /*saAdapter.addBackground(SwipeDirection.DIRECTION_NORMAL_RIGHT, R.layout.list_item_drill_in_set_bg_right)
                    .addBackground(SwipeDirection.DIRECTION_NORMAL_LEFT, R.layout.list_item_drill_in_set_bg_left);*/
            /*ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);*/
        } else {
            lv.setVisibility(View.GONE);
            //emptyList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.drill_in_set_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Context ctx;
        Intent intent;
        switch (id){
            case R.id.action_settings:
                Intent i = new Intent(getApplicationContext(), SettingActivity.class);
                startActivityForResult(i, 1);
                return true;
            case R.id.action_play:
                if(FeatureFile.isNoActiveSndClassInDrillSet(this, drillsDBArray)){
                    HelpDialogs.noActiveSndClassWarningDialog(this, this);
                    return true;
                }
                ctx = getApplicationContext();
                intent = new Intent(ctx, RunDrillActivity.class);
                intent.putExtra("DRILL_SET_ID", DRILL_SET_ID);
                intent.putExtra("DRILL_SET_NAME", DRILL_SET_NAME);
                intent.putExtra("DRILL_SET_COLOR", DRILL_SET_COLOR);
                intent.putExtra("DRILL_SET_GROUP", DRILL_SET_GROUP);
                intent.putExtra("DRILL_SET_MODE", 0); // normal mode
                startActivity(intent);
                return true;
            case R.id.action_play_shuffle:
                shuffleInfoDialog(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void shuffleInfoDialog(final Activity act) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.wrning_shuffle_title));
        builder.setMessage(getResources().getString(R.string.wrning_shuffle_text));
        builder
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if(FeatureFile.isNoActiveSndClassInDrillSet(act, drillsDBArray)){
                            HelpDialogs.noActiveSndClassWarningDialog(act, act);
                        }
                        else {
                            Context ctx = getApplicationContext();
                            Intent intent = new Intent(ctx, RunDrillActivity.class);
                            intent.putExtra("DRILL_SET_ID", DRILL_SET_ID);
                            intent.putExtra("DRILL_SET_NAME", DRILL_SET_NAME);
                            intent.putExtra("DRILL_SET_COLOR", DRILL_SET_COLOR);
                            intent.putExtra("DRILL_SET_GROUP", DRILL_SET_GROUP);
                            intent.putExtra("DRILL_SET_MODE", 1); // shuffle mode
                            startActivity(intent);
                        }
                    }
                })
                .setNegativeButton(getResources().getString(R.string.back_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.drillSubText));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.drillSubText));
            }
        });
        dialog.show();
    }

    private void dialogSelectSavedDrill(){
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
        final LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);

        final View Viewlayout = inflater.inflate(R.layout.dialog_select_saved_drill,
                (ViewGroup) findViewById(R.id.layout_dialog));
        View includedLayout = Viewlayout.findViewById(R.id.id_drill_list);
        ListView lv = (ListView) includedLayout.findViewById(R.id.list);

        savedDrillArray = getSelectSavedDrillData(false);

        final TextView allTV = Viewlayout.findViewById(R.id.all);
        final TextView onlyTV = Viewlayout.findViewById(R.id.only_set);

        TextView emptyList = (TextView) Viewlayout.findViewById(R.id.empty_list);
        if (savedDrillArray.size() > 0) {
            savedDrillAdapter = new SavedDrillSelectAdapter(getApplicationContext(), R.layout.list_item_select_saved_drill, savedDrillArray, this);
            lv.setAdapter(savedDrillAdapter);
            ListUtils.setDynamicHeight(lv);
            lv.setVisibility(View.VISIBLE);
            emptyList.setVisibility(View.GONE);
        } else {
            lv.setVisibility(View.GONE);
            emptyList.setVisibility(View.VISIBLE);
        }

        allTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allTV.setTextColor(getResources().getColor(R.color.colorAccent));
                onlyTV.setTextColor(getResources().getColor(R.color.drillText));
                getDrillsSaved(false);
            }
        });
        onlyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allTV.setTextColor(getResources().getColor(R.color.drillText));
                onlyTV.setTextColor(getResources().getColor(R.color.colorAccent));
                getDrillsSaved(true);
            }
        });


        popDialog.setView(Viewlayout);

        popDialog.setPositiveButton(getResources().getString(R.string.back_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        dialog = popDialog.create();
        // listener for close dialog
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface in) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
            }
        });
        dialog.show();
    }

    public void dialogSelectSavedDrillDismiss(Drill drill){
        dialog.dismiss();
        drill.saveSelectedDrillToDB(getApplicationContext(), DRILL_SET_ID);
        getData();
    }

    private void getDrillsSaved(boolean only){
        savedDrillArray.clear();
        savedDrillArray.addAll(getSelectSavedDrillData(only));
        if(savedDrillAdapter != null)
            savedDrillAdapter.notifyDataSetChanged();
    }

    private ArrayList<Drill> getSelectSavedDrillData(boolean only){
        ArrayList<Group> groupArray = getGroupes();
        ArrayList<Drill> drillArray = new ArrayList<Drill>();
        SQLiteDB db = new SQLiteDB(this);
        Cursor itemFromDB = db.getSavedDrillsByGroupId(getSelectedGroupIDs(groupArray, only));
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("id_drill"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("drill_name"));
            Integer groupeId = itemFromDB.getInt(itemFromDB.getColumnIndex("drill_group"));
            Boolean timeWait = itemFromDB.getInt(itemFromDB.getColumnIndex("time_wait")) == 1;
            Integer time = itemFromDB.getInt(itemFromDB.getColumnIndex("time"));
            Boolean alternative = itemFromDB.getInt(itemFromDB.getColumnIndex("alternative")) == 1;
            Integer varPosition = itemFromDB.getInt(itemFromDB.getColumnIndex("variant_position"));
            Boolean sndWait = itemFromDB.getInt(itemFromDB.getColumnIndex("snd_wait")) == 1;
            Integer snd = itemFromDB.getInt(itemFromDB.getColumnIndex("snd"));
            Boolean inflection = itemFromDB.getInt(itemFromDB.getColumnIndex("inflection")) == 1;
            Boolean shuffle = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle")) == 1;
            Boolean shuffleModify = itemFromDB.getInt(itemFromDB.getColumnIndex("shuffle_modify")) == 1;
            String groupeName = "";
            int color = 2;
            for(int i = 0; i < groupArray.size(); i++){
                if(groupArray.get(i).getgId() == groupeId) {
                    groupeName = groupArray.get(i).getName();
                    color = groupArray.get(i).getColor();
                    break;
                }
            }
            Drill d = new Drill(id, name, groupeId, groupeName, alternative, varPosition, timeWait, time, sndWait, snd, inflection, shuffle, shuffleModify);
            d.setColor(color);
            drillArray.add(d);
        }
        itemFromDB.close();
        db.close();
        return drillArray;
    }

    private ArrayList<Integer> getSelectedGroupIDs(ArrayList<Group> groupArray, boolean only){
        ArrayList<Integer> result = new ArrayList<>();
        for(int i = 0; i < groupArray.size(); i++){
            if(only){
                if(groupArray.get(i).getName().compareTo(DRILL_SET_GROUP) == 0){
                    result.add(groupArray.get(i).getgId());
                }
            }
            else{
                result.add(groupArray.get(i).getgId());
            }
        }
        return result;
    }

    private ArrayList<Group> getGroupes(){
        // get all groups from DB
        ArrayList<Group> groupArray = new ArrayList<Group>();
        SQLiteDB db = new SQLiteDB(this);
        Cursor itemFromDB = db.getAllGroupes(true);
        List<Group> groupsDBArray = new ArrayList<Group>();
        for (itemFromDB.moveToFirst(); !itemFromDB.isAfterLast(); itemFromDB.moveToNext()) {
            Integer id = itemFromDB.getInt(itemFromDB.getColumnIndex("gid"));
            String name = itemFromDB.getString(itemFromDB.getColumnIndex("name"));
            Integer cnt = itemFromDB.getInt(itemFromDB.getColumnIndex("cnt"));
            String description = itemFromDB.getString(itemFromDB.getColumnIndex("description"));
            Integer icon = itemFromDB.getInt(itemFromDB.getColumnIndex("icon"));
            Integer color = itemFromDB.getInt(itemFromDB.getColumnIndex("color"));
            groupArray.add(new Group(id, name, description, icon, color, cnt));
        }
        itemFromDB.close();
        db.close();
        return groupArray;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }
}
